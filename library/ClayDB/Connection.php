<?php
/**
 * ClayDB Connection Library
 *
 * @copyright (C) 2012-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package ClayDB
 */

namespace ClayDB;

/**
 * Include ClayDB library
 */
\Library('ClayDB');

/**
 * Trait for ClayDB connection resource
 */
trait Connection {	
	/**
	 * Database Connection
	 * 
	 * Allows a class to import the db() method to be used as [class]::db()
	 * @param string Database configuration name
	 * @return object ClayDB Resource
	 */
	protected static function DB($db = NULL){
		
		return \ClayDB::Connect($db);
	}
}