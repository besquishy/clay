<?php
/**
* ClayDB Data Dictionary Library
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
* @since 2012-07-01
*/

namespace ClayDB;

/**
 * ClayDB Data Dictionary Interface.
 * An interface for creating Data Dictionary classes in ClayDB
 * @author David L Dyess II
 * @TODO See abstract below
 */
interface DataDictionaryInterface {
	/**
	 * Define database resource as $this->link
	 * @param resource $arg
	 */
	public function __construct($arg);
	/**
	 * CREATE TABLE
	 * @param $table string (optional array)
	 * @param $args array (optional string)
	 */
	public function CreateTable($table,$args);
	/**
	 * Alter Table handler method. Used to route requests to appropriate methods for altering tables
	 * @param $table string (optional array)
	 * @param $args array (optional string)
	 */
	public function alterTable($table,$args);
	/**
	 * Create an Index
	 * @param string $table
	 * @param array $args
	 */
	public function createIndex($table,$args);
	/**
	 * Delete Index
	 * @param string $table
	 * @param array $args
	 */
	public function dropIndex($table,$args);
	/**
	 * Drop Table(s)
	 * @param string $table
	 */
	public function dropTable($table);
	/**
	 * Data Type Translation.
	 * Generates Database-specifc data type information.
	 * @param $args array
	 */
	public function dataType($args);
	/**
	 * Register Tables.
	 * Add database tables to the database.tables configuration file
	 * @param array $tables
	 */
	public function registerTables($tables);
	/**
	 * Unregister Tables
	 * Remove database tables from the database.tables configuration file
	 * @param array $tables
	 */
	public function unregisterTables($tables);
	
}

/**
 * ClayDB Data Dictionary Abstract Class
 * @see \claydb\DataDictionaryInterface
 * @author David L Dyess
 * @TODO Determine methods that can be moved out of individual datadicts and implemented here
 * @TODO Add dropTables() method
 * @TODO Add parameter to optionally unregister tables to dropTable() and dropTables()
 */
abstract class Datadict implements DataDictionaryInterface {
	/**
	 * Define database resource as $this->link
	 * @param resource $arg
	 */
	abstract public function __construct($arg);
	/**
	 * CREATE TABLE
	 * @param $table string (optional array)
	 * @param $args array (optional string)
	 */
	abstract public function createTable($table,$args);
	/**
	 * Alter Table handler method. Used to route requests to appropriate methods for altering tables
	 * @param $table string
	 * @param $args array
	 */
	abstract public function alterTable($table,$args);
	/**
	 * Create an Index
	 * @param string $table
	 * @param array $args
	 */
	abstract public function createIndex($table,$args);
	/**
	 * Delete an Index
	 * @param string $table
	 * @param array $args
	 */
	abstract public function dropIndex($table,$args);
	/**
	 * Drop Table(s)
	 * @param string $table
	 */
	abstract public function dropTable($table);
	/**
	 * Data Type Translation.
	 * Generates Database-specifc data type information.
	 * @param $args array
	 */
	abstract public function dataType($args);
	
	/**
	 * Register Tables.
	 * Add database tables to the database.tables configuration file
	 * @param array $tables
	 */
	public function registerTables($tables){
		
		$path = !empty(\claydb::$cfg) ? 'sites/'.\claydb::$cfg.'/database.tables' : 'sites/'.\clay\CFG_NAME.'/database.tables';
		$storedTables = \clay::config($path);
		switch(true) {
			case !is_array($tables):
				return false;
			case !is_array($storedTables) AND is_array($tables):
				$tableData = $tables;
				break;
			case is_array($storedTables) AND is_array($tables):
				$tableData = array_merge($storedTables,$tables);
				break;
		}
		\clay::setConfig($path,$tableData);
		# Refresh the \ClayDB::$tables array with the new table info (also added table prefix)
		# This is the same as \ClayDB::tables() @TODO: in the future register, unregister, and tables() should be merged		
		foreach($tableData as $table => $info){
			\claydb::$tables[$table] = !empty($info['prefix']) ? $info['prefix'] : \claydb::$prefix .'_'. $info['table'];
		}		
		return true;
	}
	/**
	 * Unregister Tables
	 * Remove database tables from the database.tables configuration file
	 * @param array $tables
	 */
	public function unregisterTables($tables){
		$path = !empty(\claydb::$cfg) ? 'sites/'.\claydb::$cfg.'/database.tables' : 'sites/'.\clay\CFG_NAME.'/database.tables';
		$storedTables = \clay::config($path);
		foreach($tables as $table){
			if(!empty($storedTables[$table])){
				unset($storedTables[$table]);
			}
		}
		\clay::setConfig($path,$storedTables);
		# Refresh the \ClayDB::$tables array with the new table info (also added table prefix)
		# This is the same as \ClayDB::tables() @TODO: in the future register, unregister, and tables() should be merged
		foreach($tableData as $table => $info){
			\claydb::$tables[$table] = !empty($info['prefix']) ? $info['prefix'] : \claydb::$prefix .'_'. $info['table'];
		}	
		return true;
	}
}