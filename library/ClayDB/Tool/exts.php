<?php
/**
 * ClayDB Database Tools
 *
 * @copyright (C) 2007-2010 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package ClayDB Tools
 */

namespace ClayDB\Tool;

/**
 * List of Database Drivers
 * @param array $args Not implemented
 */
function exts($args){
	$dbs = array('MSSQL','MySQL','MySQLi','ODBC','Oracle','PDO MySQL','PDO SQLite','PostgreSQL','SQLite');
	return $dbs;
}
?>
