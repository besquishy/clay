<?php
/**
 * ClayDB PDO MySQL Adapter
 *
 * @copyright (C) 2007-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package ClayDB Adapter
 */

namespace ClayDB\Adapter;

# Import the Adapter Abstract and Interface
\Library('ClayDB/Adapter');

/**
 * PDO MySQL ClayDB Adapter
 */
class PDO_MySQL extends \ClayDB\Adapter {
	/**
	 * Database
	 * @var string
	 */
	public $database;
	/**
	 * Database Object
	 * @var object
	 */
	public $link;
	/**
	 * Connect to a Database
	 * 
	 * Internal method for creating a database connection to a MySQL server
	 * @param string $driver Database Type
	 * @param string $host
	 * @param string $database
	 * @param string $user
	 * @param string $pw Password
	 */
	public function Connect($driver, $host, $database, $user, $pw){
		$dsn = 'mysql:host='.$host.';dbname='.$database;
		$link = new \PDO($dsn,$user,$pw);
		$link->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		$link->exec('USE '.$database);
		$this->link = $link;
		$this->database = $database;
	}	
	/**
	 * Fetch a result set as an associative array.
	 * 
	 * Select Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[offset], [limit]"
	 */
	public function Get($sql,$bind=array(),$limit=''){
		$total = array();
		if(!empty($limit)) {
			$total = \explode(',',$limit);
			$limit = "LIMIT $limit";
		}
		$sth = $this->link->prepare("SELECT $sql $limit");
		$sth->execute($bind);
		# If a limit of 1 has been set, only 1 row as an array
		if(!empty($total[1]) AND $total[1] == '1'){
			# Return the row as an array
			return $sth->fetch(\PDO::FETCH_ASSOC);
		}
		# Return an array of rows
		return $sth->fetchAll(\PDO::FETCH_ASSOC);
	}
	/**
	 * Fetch a result set as an object
	 * @param string $sql
	 * @param array $bind
	 * @param string $limit
	 * @return object OR array of objects
	 */
	public function getObject($sql,$bind=array(),$limit=''){

		$total = array();
		if(!empty($limit)) {
			$total = \explode(',',$limit);
			$limit = "LIMIT $limit";
		}
		$sth = $this->link->prepare("SELECT $sql $limit");
		$sth->execute($bind);
		# If a limit of 1 has been set, only fetch 1 row as an object
		if(!empty($total[1]) AND $total[1] == '1'){
			# Return the row as an object
			return $sth->fetchObject();
		}
		# Return an array of rows as objects
		return $sth->fetchAll(\PDO::FETCH_OBJ);
	}
	/**
	 * Insert Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 */
	public function Add($sql,$bind=array()){
		$sth = $this->link->prepare("INSERT into $sql");
		$sth->execute($bind);
		return $this->link->lastInsertID();
	}
	/**
	 * Update Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"
	 */
	public function Update($sql,$bind=array(),$limit=''){
		return $this->change('UPDATE',$sql,$bind,$limit);
	}
	/**
	 * Delete Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"
	 */
	public function Delete($sql,$bind=array(),$limit=''){
		return $this->change('DELETE FROM',$sql,$bind,$limit);
	}
	/**
	 * Process Statement
	 * @param string $action Statement type
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"|"[offset],[limit] Dependent on Statement type
	 */
	public function Change($action,$sql,$bind=array(),$limit=''){
		if(!empty($limit)) {
			$limit = "LIMIT $limit";
		}
		$sth = $this->link->prepare($action.' '.$sql.' '.$limit);
		try {
			$sth->execute($bind);
		} catch(PDOException $e) {
			throw new \Exception($e);
		}
		return $sth->rowCount();
	}
	/**
	 * Select a Database
	 * @param string $database Database name
	 */
	public function selectDB($database){
		$this->link->exec('USE '.$database);
		$this->database = $database;
	}
	/**
	 * Data Dictionary
	 * 
	 * Used to Manipulate Databases
	 */
	public function Datadict(){
		\Library("ClayDB/Datadict/PDO_MySQL");
		$datadict = new \ClayDB\Datadict\PDO_MySQL($this->link);
		return $datadict;
	}
}
?>