<?php
/**
 * ClayDB Library
 *
 * @copyright (C) 2007-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @since 2012-07-01
 * @package ClayDB
 */

namespace ClayDB;

/**
 * ClayDB Adapter Interface.
 * An interface for creating database adapter classes in ClayDB
 * @author David
 *
 */
interface AdapterInterface {
	/**
	 * Connect to a Database
	 * @param string $driver Database Type
	 * @param string $host
	 * @param string $database
	 * @param string $user
	 * @param string $pw Password
	 */
	public function Connect($driver, $host, $database, $user, $pw);
	/**
	 * Select Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[offset], [limit]"
	 */
	public function Get($sql,$bind=array(),$limit='');
	/**
	 * Insert Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 */
	public function Add($sql,$bind=array());
	/**
	 * Update Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"
	 */
	public function Update($sql,$bind=array(),$limit='');
	/**
	 * Delete Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"
	 */
	public function Delete($sql,$bind=array(),$limit='');
	/**
	 * Process Statement
	 * @param string $action Statement type
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"|"[offset],[limit] Dependent on Statement type
	 */
	public function Change($action,$sql,$bind=array(),$limit='');
	/**
	 * Select a Database
	 * @param string $database Database name
	 */
	public function selectDB($database);
	/**
	 * Data Dictionary
	 * 
	 * Used to Manipulate Databases
	 */
	public function Datadict();
}

/**
 * ClayDB Adapter Abstract Class.
 * @see \claydb\AdapterInterface
 * @author David
 */
abstract class Adapter implements AdapterInterface {
	/**
	 * Connect to a Database
	 * @param string $driver Database Type
	 * @param string $host
	 * @param string $database
	 * @param string $user
	 * @param string $pw Password
	 */
	abstract public function Connect($driver, $host, $database, $user, $pw);
	/**
	 * Select Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[offset], [limit]"
	 */
	abstract public function Get($sql,$bind=array(),$limit='');
	/**
	 * Insert Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 */
	abstract public function Add($sql,$bind=array());
	/**
	 * Update Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"
	 */
	abstract public function Update($sql,$bind=array(),$limit='');
	/**
	 * Delete Statement
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"
	 */
	abstract public function Delete($sql,$bind=array(),$limit='');
	/**
	 * Process Statement
	 * @param string $action Statement type
	 * @param string $sql SQL Select Query
	 * @param array $bind Bind data (each key replaces ? in SQL, in order of appearance)
	 * @param string $limit "[limit]"|"[offset],[limit] Dependent on Statement type
	 */
	abstract public function Change($action,$sql,$bind=array(),$limit='');
	/**
	 * Select a Database
	 * @param string $database Database name
	 */
	abstract public function SelectDB($database);
	/**
	 * Data Dictionary
	 * 
	 * Used to Manipulate Databases
	 */
	abstract public function Datadict();	
}