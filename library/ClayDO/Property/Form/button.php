<?php
/**
* Clay Data Objects Form
*
* @copyright (C) 2007-2011 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace ClayDO\Property\Form;

\Library('ClayDO/DataObject/Form');

/**
 * Clay Data Objects Form Button
 */
class Button extends \ClayDO\DataObject\Form {
	/**
	 * Label
	 * @var string
	 */
	public $label;
	/**
	 * Icon
	 * @var string
	 */
	public $icon;
	/**
	 * Template
	 * @var string
	 */
	public $template = 'claydo/properties/form/button';

}

?>