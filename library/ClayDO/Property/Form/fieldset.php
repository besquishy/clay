<?php
/**
* Clay Data Objects Form
*
* @copyright (C) 2007-2011 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace ClayDO\Property\Form;

\Library('ClayDO/DataObject/Form');

/**
 * Clay Data Objects Form Fieldset
 */
class Fieldset extends \ClayDO\DataObject\Form {
	/**
	 * Legend
	 * @var string
	 */
	public $legend;
	/**
	 * Introduction
	 * @var string
	 */
	public $intro;
	/**
	 * Template
	 * @var string
	 */
	public $template = 'claydo/properties/form/fieldset';

}

?>