<?php
/**
* Clay Data Objects Form
*
* @copyright (C) 2007-2011 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace ClayDO\Property\Form;

\Library('ClayDO/DataObject/Form');

/**
 * Clay Data Objects Form Textarea
 */
class Textarea extends \ClayDO\DataObject\Form {
	/**
	 * Template
	 * @var string
	 */
	public $template = 'claydo/properties/form/textarea';
	/**
	 * Label
	 * @var string
	 */
	public $label;
	/**
	 * Content
	 * @var string
	 */
	public $content;

}

?>