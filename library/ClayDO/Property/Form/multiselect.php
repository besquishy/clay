<?php
/**
* Clay Data Objects Form
*
* @copyright (C) 2007-2011 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace ClayDO\Property\Form;

\Library('ClayDO/property/form/select');

/**
 * Clay Data Objects Form Multiselect
 */
class Multiselect extends \ClayDO\Property\Form\Select {
	/**
	 * Label
	 * @var string
	 */
	public $label;
	/**
	 * Options
	 * @var array
	 * @sample <option> tags - options[] array('value' => x, 'content' => y, 'selected' => z)
	 */
	public $options = array();
	/**
	 * Template
	 * @var string
	 */
	public $template = 'claydo/properties/form/multiselect';

}