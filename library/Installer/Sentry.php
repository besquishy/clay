<?php
/**
 * Clay Installer
 *
 * @copyright (C) 2007-2010 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Installer - Sentry Library
 */

namespace Installer;

/**
 * Clay Installer Sentry Library
 * 
 * Responsible for ensuring the current user is authenticated and allowed access to the Installer
 * @author david
 *
 */
class Sentry {
	/**
	 * Installer Configuration Folder
	 * @var string
	 */
	public static $dir = 'sites/installer/';
	/**
	 * Security
	 * 
	 * @param array $args
	 */
	public static function Security($args){
		if(file_exists(\clay\CFG_PATH.static::$dir.'sentry.php')){
			$conf = \clay::config(static::$dir.'sentry');
			if(!empty($conf['token'])) return true;
	    }
	    return false;
	}
	/**
	 * Authenticate
	 */
	public static function Authenticate(){
		if(file_exists(\clay\CFG_PATH.static::$dir.'sentry.php')){
			$conf = \clay::config(static::$dir.'sentry');
			If(!empty($_SESSION['csi']) && $_SESSION['csi'] == $conf['token']) return true;
		}
		return false;
	}
	/**
	 * Initiated
	 * 
	 * Determines if the Installer has been set up
	 * @return boolean
	 */
	public static function Initiated(){
		if(file_exists(\clay\CFG_PATH.static::$dir.'sentry.php')){
			return true;
	    }
	    return false;
	}
}