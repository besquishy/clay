<?php
/**
 * Clay Installer
 *
 * @copyright (C) 2007-2010 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Installer Controller
 */

namespace Installer;

\Library('Clay/Application');

/**
 * Clay Installer Application Library
 * 
 * An integrated Controller and View library - this library is the backbone for Applications and Themes
 */
abstract class Application extends \Clay\Application {

	/**
	 * Builds the query string for an application url
	 * @param string $com Package Component
	 * @param string $act Package Action
	 * @param array $extra Key/Value pairs for GET input
	 * @param boolean $null
	 * @return string
	 */
	public static function URL($com = '',$act = '',$extra = array(),$null = NULL){
		$args = !empty($_GET['s']) ? '?s='.\clay\data\get('s','string','base') : '?';
		if(is_array($com)){
			foreach($com as $piece){
				$args = $args.empty($args) ? '?'.key($com).'='.$piece : '&'.key($com).'='.$piece;
				next($com);
			}
			return $args;
		} else {
			if(!empty($com)){
				$args = $args."&com=$com";
			}
			if(!empty($act)){
				$args = $args."&act=$act";
			}
			$params = '';
			if(is_array($extra)){
				foreach($extra as $piece){
					$params = $params.'&'.key($extra).'='.$piece;
					next($extra);
				}
			}
			return $args.$params;
		}
	}
	/**
	 * Redirect the browser to another Package component url
	 * @param string $com Package Component
	 * @param string $act Package Action
	 * @param array $extra Key/Value pairs for GET input
	 * @param boolean $null
	 */
	public static function Redirect($com = '',$act = '',$extra = array(), $null = NULL){
		session_write_close();
		header( 'Location: '.self::url($com,$act,$extra) );
	}
	/**
	 * Package Information File
	 * @param string $name Package name
	 * @return array
	 */
	public static function Package($name) {
		$file = \clay\APPS_PATH.$name.'/package.php';
		if(file_exists($file)){
			include($file);
			return $data;
		} else {
			return false;
		}
	}
	/**
	 * Menu Data File
	 * 
	 * Includes the menu.php file from a Package
	 */
	public static function Menu(){
		$file = \clay\APPS_PATH.\installer\PACKAGE.'/menu.php';
		if(file_exists($file)){
			include($file);
			return $data;
		} else {
			return false;
		}
	}
}