# Clay

## Libraries

`/library/Installer.php`

> Namespace: `\` (global)

* The Base Library that creates the foundation for Clay Installer
* Extends `\Clay` Class
* Includes a Class

### Classes

#### class Installer extends Clay

> Namespace + Class : `\Installer`

* Static Class : No Objects

> Extends `\Clay` Class in `/library/Clay.php`

##### Class Constants

> Inherits Constants from `\Clay` Class

##### Class Properties

> Inherits Properties from `\Clay` Class

##### Class Methods (Functions)

> Inherits Functions from `\Clay`

**public static function site($site)**

> Namespace + Class + Function : `\Installer::site()`

> string `$site` : Name of a Clay configuration

* Used to retrieve Clay Installer information about a site installation

* Data is collected when an Installation is created in the Clay Installer

> return `array` : Clay Installer configuration data Array contained in a Clay Installer configurations file

* **Known Issue** : This is supposed to return the Array Index for $site in the Clay Installer Configurations file, instead it returns the entire contents of the file.

**public static function setSite($site,$data)**

> Namespace + Class + Function : `\Installer::setSite()`

> string `$site` : Name of the Clay configuration

> array `$data` : Data collected by the Clay Installer during an Installation

* Used to set Clay configuration data to the Clay Installer configurations file

> return `boolean` : `TRUE` on success; `FALSE` on failure

**public static function upgrade($site,$version)**

> Namespace + Class + Function : `\Installer::upgrade()`

> string `$site` : Name of the Clay configuration

> string `$version` : New Clay Package version

* Used to update the Clay Package version of an Installation in the Clay Installer configurations file

> return `boolean` : `TRUE` on success; `FALSE` on failure

**public static function siteConfig($site,$config)**

> Namespace + Class + Function : `\Installer::siteConfig()`

> string `$site` : Name of a Clay configuration

> string `$config` : Name of a Configuration file

* Used to retrieve a site-specific configuration file

> return `array` : `Data` on success; `FALSE` on failure

**public static function setSiteConfig($site,$config,$data)**

> Namespace + Class + Function : `\Installer::setSiteConfig()`

> string `$site` : Name of a Clay configuration

> string `$config` : Name of a Configuration file

> array `$data` : Data to be in the configuration file

* Used to create a site-specific configuration file

> return `boolean` : `TRUE` on success; `FALSE` on failure

**public static function bootstrap($config = 'installer')**

> Namespace + Class + Function : `\Installer::Bootstrap()`

> string `$config` : Name of a Configuration file

* Used as a callback for `\Clay::Bootstrap()`
* Calls `\Clay::Init()`
* Calls `\Installer::Output()`
* Used to bypass `\Clay::Output()`

**public static function output()**

> Namespace + Class + Function : `\Installer::Output()`

> string `$config` : Name of a Configuration file

* Overrides inherited `\Clay::Output()`
* Utilizes configuration data in `\Installer::$config` to generate page output
* Utilizes `\Installer\User` Class to determine if the user is an admin
* Utilizes `\Installer\Sentry` Class to determine if the Clay Installer has been secured and the user has been authenticated
* Utilizes `\Installer\Application` Class to load Clay Installer Package files
* Determines Package for Output
    * Dependent on user authentication state and Clay Installer state
* Determines Theme for Output
* Determines Page for Output
* Generates Output utilizing `\Clay\Application\Component` Class
    * Extended by Clay Installer Packages, same as Clay Applications

## Usage

Clay Installer is used by running install.php in the browser.

The Installer serves as an example of using the Clay Framework to build a custom CMS. Its 'applications' are called Packages and include a package for Clay itself, which depends on the Installer for installation.