# Clay

## Libraries

`/library/Autoboot.php`

> Namespace: `\` (global)

* The Base Library that creates the foundation for Autoboot - Multisite Loader; Clay Package
* Extends `\Clay` Class
* Includes a Class
* When set as the `default` site configuration, Autoboot allows you to run multiple sites from the same copy of Clay code

### Classes

#### class Autoboot extends Clay

> Namespace + Class : `\Autoboot`

* Static Class : No Objects

> Extends `\Clay` Class in `/library/Clay.php`

##### Class Constants

> Inherits Constants from `\Clay` Class

##### Class Properties

> Inherits Properties from `\Clay` Class

##### Class Methods (Functions)

> Inherits Functions from `\Clay`

**public static function selector()**

> Namespace + Class + Function : `\Autoboot::selector()`

* Used as callback for `\Clay::Bootstrap()`
* Compares requested URL to a list of domains in the Autoboot configuration file
* Calls `\Clay::Bootstrap()` with the correct configuration name
* Performs the same basic function as `\Clay()` in an entry file

> return `\Clay::Bootstrap()` : Re-initializes Clay as the requested site