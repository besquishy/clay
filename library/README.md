Sunday, 31. December 2017 09:25PM 

# Clay

## Libraries

`/library` is the default folder used for Clay Libraries.

Libraries are low-level APIs used in all aspects of Clay and form the foundation for Modules, Applications, and Themes

Libraries use a namespace that matches their respective file structure within the `/library` folder

* `/library/Clay.php` is in the `\` global namespace
	* `function Clay($config='default')` resolves to `\Clay()`
	* `class Clay` resolves to `\Clay`
		* `public static function Library` resolves to `\Clay::Library()`

* `/library/Clay` is in the `\Clay` namespace
	* Every file within `/library/Clay` will declare a namespace of `Clay`
		* Each folder within `/library/Clay` will add a level to the namespace
			* `/library/Clay/Application`resolves to `\Clay\Application`
			
The `Clay` Library (Clay.php) provides APIs for importing Libraries at different namespace levels

* function Import($file) - \Import()
	* Global import function, prevents files from being imported more than once
		* Similiar to PHP's include_once and require_once
	* Requires complete path as `$file`
	
* function Library($path) - \Library()
	* Library import function, utilizes \Import()
	* Imports files under the `/library` path, which is defined in `constant` `\clay\LIB_PATH`
	* `\Library( 'ClayDB' )` imports `/library/ClayDB.php`
	
* `Clay` Class `public static function Library($path)` - `\Clay::Library()`
	* Clay Library import function, utilizes \Import()
	* Imports files under the `/library/Clay` path
	* Added to increase code readability