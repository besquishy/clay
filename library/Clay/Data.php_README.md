Sunday, 31. December 2017 09:24PM 

# Clay

## Libraries

### Clay Data Library - \Clay\Data

The Clay Data Library is the data handler within Clay. It was introduced as a standalone library named Granule in the earliest build of Clay, but today depends on the \Library() function as its only dependency. \Clay\Data provides an interface to retrieve, validate, and filter input data. It also provides an interface for a data cache. The cache allows you to share data across Clay, without using Globals or Sessions to store the data.

### \Clay\Data

Found in `/library/Clay/Data.php`, this namespace serves as the home to several functions and the \clay\data\cache class. We'll begin with the functions.

#### \Clay\Data\Fetch(string $index, string $val = '', string $filter = '', mixed $default = '');

POST and GET Handler

Fetch is a multipurpose function that retrieves input from either $_POST or $_GET, with a preference for $_POST. If $index is in both $_POST and $_GET, Fetch will return the input found in $_POST. It offers the ability to Validate and Filter the input. It also provides an autofill parameter that will be returned if no input is found.

    Parameter string $index - the key in either $_POST or $_GET to retrieve.
    Parameter string $val - a ":" delimited string matching Validations (optional).
        See \clay\data\validate
    Parameter string $filter - a " | " delimited string matching Filters (optional).
    Parameter mixed $default - a value to return if no input is found (optional).
    Uses \clay\data\process() to process input data.
    Returns:
        Processed Input (if input exists).
        Value of $default.
        FALSE on failure.
            Including Validation rejection.

#### \Clay\Data\Post(string $index, string $val = '', string $filter = '', mixed $default = '');

Post Handler

Post works exactly the same as Fetch, but only uses $_POST input.

#### \Clay\Data\Get(string $index, string $val = '', string $filter = '', mixed $default = '');

Get Handler

Get works exactly the same as Fetch, but only uses $_GET input.

#### \Clay\Data\Process(mixed $var, string $val = '', string $filter = '', mixed $default = '');

Data Processor

Process is used by the Data handlers, it acts as a utility function to determine Validation and Filters and apply them to $var. Process with work with anything sent to it in $var, it has no regard for the data's origin.

    Parameters are the same as the Handlers, with the exception of mixed $var is any type of data.
    Uses Functions:
        \clay\data\validate();
        \clay\data\filter();
    Returns:
        Filtered and Validated $var.
        Value of $default.
        FALSE on Validation rejection.

#### \Clay\Data\Validate(string $val = '', mixed $var);

Data Validation

Used to validate data to any type or range within a type.

    Parameter string $val - a ":" delimited string matching Validations.
    Parameter mixed $var - any type of data to validate.
    Returns:
        Validated value of $var.
        FALSE on Validation rejection.

#### \ClayData\Filter(string $filter = '', mixed $var);

Data Filtering

Used to filter data to meet requirements.

    Parameter string $filter - a " | " delimited string matching Filters.
    Parameter mixed $var - any type of data to filter.
    Returns:
        Filtered value of $var.
