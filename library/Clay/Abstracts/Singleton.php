<?php
/**
 * Clay Singleton Base Class Library
 *
 * @copyright (C) 2007-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
namespace Clay\Abstracts;
/**
 * Provides a base abstraction class for implementing the Singleton Design Pattern
 */
abstract class singleton {
	/**
	 * Construct (empty)
	 */
	private function construct(){}
	/**
	 * Used to instantiate and pass objects from child classes
	 * 
	 * Inherited by the child, allows you to call [class]::getInstance() and create/retrieve a singleton object of that class
	 * @return object
	 */
	public static function getInstance(){
		static $instance; # Class object storage
		$class = get_called_class(); # Child class being instantiated
		# If the child class hasn't been instantiated
		if(!isset($instance[$class])) {
			# Instantiate the class and add it to the storage array
			$instance[$class] = new $class;
		}
		# Return the stored object
		return $instance[$class];
	}
}