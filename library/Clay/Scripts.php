<?php
/**
 * Clay Script Handler Library
 *
 * @copyright (C) 2007-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay;

/**
 * Provides a means of dynamically linking or including javascripts from Application and Themes, as well as external URLs.
 */
class Scripts {
	/**
	 * Include Mode; 'internal' or 'external';
	 * @var string
	 */
	public static $mode = 'external';
	/**
	 * Data Storage Array
	 * @var array
	 */
	protected static $map = array();
	/**
	 * Pattern for use within static::$map
	 * @param array $args type : theme|app|url; name : of type; file : Filename, relative to type's path
	 * @return string
	 */
	private static function pattern($args){
		return $args['type'].':'.$args['name'].':'.$args['file'];
	}
	/**
	 * Add Script
	 * 
	 * Internal utility function; use addTheme(), addApplication(), addURL instead
	 * @param array $args array('type' => 'theme', 'name' => [name], 'file' => [file], 'position' => [position])
	 */
	public static function add($args){
		$pattern = self::pattern($args);
		if(empty($args['position'])) $args['position'] = 'head';
		if(!isset(self::$map['scripts.'.$args['position']][$pattern]['req'])){
			self::$map['scripts.'.$args['position']][$pattern]['req'] = $args;
		}
	}
	/**
	 * Add Theme Script
	 * @param string $name Theme name
	 * @param string $file File name
	 * @param string $position head|body (optional); Default is 'body'
	 */
	public static function addTheme($name,$file,$position='body'){
		self::add(array('type' => 'theme', 'name' => $name, 'file' => $file, 'position' => $position));
	}
	/**
	 * Add Application Script
	 * @param string $name Theme name
	 * @param string $file File name
	 * @param string $position head|body (optional); Default is 'body'
	 */
	public static function addApplication($name,$file,$position='body'){
		self::add(array('type' => 'app', 'name' => $name, 'file' => $file, 'position' => $position));
	}
	/**
	 * Add URL Script
	 * @param string $name Theme name
	 * @param string $file File name
	 * @param string $position head|body (optional); Default is 'body'
	 */
	public static function addURL($name,$file,$position='body'){
		self::add(array('type' => 'url', 'name' => $name, 'file' => $file, 'position' => $position));
	}
	/**
	 * Check if a Theme Script Exists
	 * @param string $name Theme name
	 * @param string $file File name
	 */
	private static function inTheme($name,$file){
		return file_exists(THEMES_PATH.$name.'/scripts/'.$file);
	}
	/**
	 * Check if a Theme overrides an Application Script
	 * @param string $application Application name
	 * @param string $file File name
	 */
	private static function themeOverride($application,$file){
		return file_exists(THEMES_PATH.THEME.'/applications/'.$application.'/scripts/'.$file);
	}
	/**
	 * Check if an Application Script Exists
	 * @param string $name Application name
	 * @param string $file File name
	 */
	private static function inApplication($name,$file){
		return file_exists(APPS_PATH.$name.'/scripts/'.$file);
	}
	/**
	 * Add External Scripts to Output
	 * @param array $args
	 */
	private static function addExternal($args){
		$pattern = self::pattern($args);

			switch($args['type']){
				case('app'):
					if(self::themeOverride($args['name'],$args['file'])){
						self::$map['scripts.'.$args['position']][$pattern]['src'] = REL_THEMES_PATH.THEME.'/applications/'.$args['name'].'/scripts/'.$args['file'];
					} elseif(self::inApplication($args['name'],$args['file'])) {
						self::$map['scripts.'.$args['position']][$pattern]['src'] = REL_APPS_PATH.$args['name'].'/scripts/'.$args['file'];
					}
					break;
				case('theme'):
					if(self::inTheme($args['name'],$args['file'])){
						self::$map['scripts.'.$args['position']][$pattern]['src'] = REL_THEMES_PATH.$args['name'].'/scripts/'.$args['file'];
					}
					break;
				case('url'):
					self::$map['scripts.'.$args['position']][$pattern]['src'] = $args['file'];
			}
			if(!empty(self::$map['scripts.'.$args['position']][$pattern]['src'])){
				unset(self::$map['scripts.'.$args['position']][$pattern]['req']);
			} else {
				self::$map['missing.scripts.'.$args['position']][] = $pattern;
				unset(self::$map['scripts.'.$args['position']][$pattern]);
			}


	}
	/**
	 * Add Internal Scripts to Output
	 * @param array $args
	 */
	private static function addInternal($args){
		$pattern = self::pattern($args);
		switch($args['type']){
			case('app'):
				if(self::themeOverride($args['name'],$args['file'])){
					self::$map['scripts.'.$args['position']][$pattern] = THEMES_PATH.THEME.'/applications/'.$args['name'].'/scripts/'.$args['file'];
				} elseif(self::inApplication($args['name'],$args['file'])) {
					self::$map['scripts.'.$args['position']][$pattern] = APPS_PATH.$args['name'].'/scripts/'.$args['file'];
				}
				break;
			case('theme'):
				if(self::inTheme($args['name'],$args['file'])){
					self::$map['scripts.'.$args['position']][$pattern] = THEMES_PATH.$args['name'].'/scripts/'.$args['file'];
				}
				break;
		}
		if(!is_string(self::$map['scripts.'.$args['position']][$pattern])){
			self::$map['missing.scripts.'.$args['position']][] = $pattern;
			unset(self::$map['scripts.'.$args['position']][$pattern]);
		}
	}
	/**
	 * Display Scripts
	 * 
	 * Used in Theme Page template
	 * @param string $position head|body
	 */
	public static function js($position){
		if(!empty(self::$map['scripts.'.$position])){
			switch(self::$mode){
				case('internal'):
					foreach(self::$map['scripts.'.$position] as $args){
						if(!empty($args['req'])){
							switch($args['req']['type']){
								case('url'):
									self::addExternal($args['req']);
									break;
								default:
									self::addInternal($args['req']);
									break;
							}
						}
					}
					break;
				case('external'):
					foreach(self::$map['scripts.'.$position] as $args){
						if(!empty($args['req'])){
							self::addExternal($args['req']);
						}
					}
					break;
			}
			#TODO Change the templates folder to something better reflecting this purpose
			include APPS_PATH.'common/templates/scripts/'.self::$mode.'-js.tpl';
		}
		#TODO Change the templates folder to something better reflecting this purpose
		if(!empty(self::$map['missing.scripts.'.$position])) include APPS_PATH.'common/templates/scripts/missing-js.tpl';
	}
}
