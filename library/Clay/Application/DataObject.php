<?php
/**
 * Clay Data Objects Library
 *
 * @copyright (C) 2007-2012 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Application
 */

namespace Clay\Application;

/**
 * Data Objects
 */
class DataObject {
	/**
	 * Object Storage
	 * @var object $object 
	 */
	public $object;
	/**
	 * Properties assigned
	 * @var array $properties
	 */
	public $properties = array();
	/**
	 * Attributes assigned
	 * @var array $attributes
	 */
	public $attributes = array();

	/**
	 * Create/Access a Property Object
	 * @param string $name Name of Property
	 * @param string $application Application name
	 * @param string $type Type of Property (on creation only)
	 */
	abstract function property($name, $application, $type);
	/**
	 * Add an attribute to a Property.
	 * For use in HTML attributes or other more property specific purposes.
	 * @param string $name Name of attribute
	 * @param string $value Value of attribute
	 */
	public function attribute($name, $value){
		$this->attributes[$name] = $value;
	}
	/**
	 * Add attributes to a Property.
	 * For use in HTML attributes or other more property specific purposes.
	 * @param array $array array('attribute' => 'value')
	 */
	public function attributes($array){
		foreach($array as $attr => $value){
			$this->attributes[$attr] = $value;
		}
	}
	/**
	 * Output for Child object's template
	 */
	abstract function template();
	/**
	 * Alias for property() - Distinguish Container Properties
	 * @param string $name Name of the Property
	 * @param string $application Application name
	 * @param string $type Type of Property (on creation only)
	 */
	public function container($name, $application, $type = NULL){
		return $this->property($name,$type);
	}

}