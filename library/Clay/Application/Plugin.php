<?php
/**
 * Clay Application Plugin Library
 *
 * @copyright (C) 2007-2015 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Application
 */

namespace Clay\Application;

/**
 * Clay Application Plugin library
 *
 */
abstract class Plugin {
	/**
	 * @var string $App Application name
	 */
	public $App;
	/**
	 * @var integer $ItemType Item Type ID
	 */
	public $ItemType;
	/**
	 * @var integer $ItemID Item ID
	 */
	public $ItemID;
	/**
	 * @var string $PluginApp Application name owning the plugin
	 */
	public $PluginApp;
	/**
	 * @var string $PluginType Type of Plugin
	 */
	public $PluginType;
	/**
	 * @var string $Plugin Plugin name
	 */
	public $Plugin;
	/**
	 * @var string $Template The template to be displayed
	 */
	public $Template;

	/**
	 * Process Hook - used for editors, transforms, etc to return or store processed data
	 * @param string $action Plugin function, corresponding to action in the calling application
	 * @param array $param Application data to send to the processor
	 */
	public function Process( $action, $param = array() ){

		if( method_exists( $this, $action )){
			return $this->$action( $param );
		}		
	}
	
	/**
	* Object wrapper Method for Static \clay\application::template()
	* Object template method for Plugins (based on Application Component::Template()). This allows templates to assume $this is in context.
	* @param string $action Generally 'view' (summary) or 'display'
	* @param array $param Application data to send to the plugin
	*/
	public function Plugin( $action, $param = array() ){
		# Plugin object access allows setting a custom template name
		if( !empty( $this->Template )){
			
			$tpl = 'plugins/'.$this->PluginType.'/'.$this->Template;
			
		} else {
		# Default Template - eg. plugins/content/example_view.tpl
			$tpl = 'plugins/'.$this->PluginType.'/'.$this->Plugin.'_'.$action;
		}
		# Run the Plugin function to assign template variables
		$data = $this->$action( $param );
		# Check for output
		if( $data == FALSE ){
			# Privileges or other conditions weren't met and the Plugin stopped
			return FALSE;
		}
		# Build template information array to build the file path
		$args = array( 'application' => $this->PluginApp,
						'template' => $tpl,
						'data' => $this->$action( $param ),
					  );
		# Get template path (and override detection)
		$this->template( $args );
	}
	
	/**
	 * Output for Plugin Templates
	 * @param array $args ('application' => [app], 'template' => [template], 'data' => [array..])
	 */
	public function template( $args ){
		
		if( empty( $args['application'] )){
			# Application defaults to current 
			$args['application'] = $this->PluginApp;
		}
		
		if( empty( $args['data'] )){
			# Provide empty array
			$args['data'] = array();
		}
		# Process template (checks for existence and overrides)
		$template = \clay\application::template( $args );
		# If nothing was found, stop here.
		if(empty($template['tpl'])) return;
		# If the supplied template data is an array, we extra each array key into it's own variable.
		if(is_array($template['data'])) extract($template['data']);
		# Template Debugging - Make this a system setting
		echo "<!-- Clay Template: ".$template['tpl']." --> \n";
		# Here's Johnny
		include $template['tpl'];
	}
}