<?php
/**
 * Clay Application Core Library
 *
 * @copyright (C) 2007-2014 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Application
 */

namespace Clay\Application;

/**
 * Clay Application Core
 * 
 * A central library to retrieve information provided by an application
 */
class Core {
	
	/**
	 * Application Item Types
	 * @param string $app Application Name
	 * @return mixed
	 */
	public static function ItemTypes( $app ){
		
		return \Clay\Application::API( $app, 'core', 'ItemTypes');
	}
	/**
	 * Application Item Type
	 * @param string $app Application Name
	 * @param integer $id Item Type ID
	 */
	public static function ItemType( $app, $id ){
	
		return \Clay\Application::API( $app, 'core', 'ItemType', $id );
	}
	/**
	 * Application Items
	 * @param string $app Application Name
	 * @param integer $itemType Item Type ID (optional)
	 */
	public static function Items( $app, $itemType = NULL ){
	
		return \Clay\Application::API( $app, 'core', 'Items', $itemType );
	}
	/**
	 * Application Item
	 * @param string $app Application Name
	 * @param integer $itemType Item Type ID (optional)
	 * @param integer $id Item ID
	 */
	public static function Item( $app, $itemType = NULL, $id ){
	
		return \Clay\Application::API( $app, 'core', 'Item', array( $itemType, $id ));
	}
	/**
	 * Application Fields
	 * @param string $app Application Name
	 * @param integer $itemType Item Type ID (optional)
	 * @param integer $id Item ID (optional)
	 */
	public static function Fields( $app, $itemType = NULL, $id = NULL ){
		
		return \Clay\Application::API( $app, 'core', 'Fields', array( $itemType, $id ));
	}
	/**
	 * Application Labels
	 * @param string $app Application Name
	 * @param integer $type Label Item Type
	 */
	public static function Label( $app, $type ){

		return \Clay\Application::API( $app, 'core', 'Label', $type );
	}
}