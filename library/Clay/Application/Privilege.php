<?php
/**
 * Clay Application Privilege Library
 * @author David L. Dyess II
 * @copyright 2012-2017
 * @license GPL
 * @package Clay Application
 */

namespace Clay\Application;

/**
 * Clay Application Privilege Library
 */
abstract class Privilege {
	/**
	 * @var mixed $Request Used by the Application Privilege to determine Requests match assigned privileges
	 */
	public $Request;
	/**
	 * @var string $Privilege Privilege name
	 */
	public $Privilege;

	/**
	 * Used to Validate Privilege Requests
	 * @param array scope
	 */
	abstract function Scope($scope);
	/**
	 * Used by the Users Application for Assigning Privileges to Roles
	 * @param string $privilege Privilege name
	 * @param string $scope Privilege scope
	 */
	abstract function Scopes($privilege, $scope = NULL);
	
	/**
	 * Validate an Assigned Privilege Scope Against Privilege Requirements
	 * @param string $method
	 */
	public function Validate($method){
	
		$privileges = \Clay\Module::Object('Privileges')->Roles($this->Privilege);
	
		if(!empty($privileges)){
				
			foreach($privileges as $privilege){
	
				if($this->$method(explode("::",$privilege['scope']))){
	
					return TRUE;
				}
			}
		}
	
		return FALSE;
	}
	/**
	 * Get Privilege
	 * @param string $application Application name
	 * @param string $component Privilege name
	 * @param string $name Privilege mask
	 */
	function getPrivilege($application,$component,$name){
		
		$privilege = \Clay\Module::Object('Privileges')->Get($application,$component,$name);
		
		if(!empty($privilege)){
			
			$this->Privilege = $privilege['pid'];

			return TRUE;
		
		} else {
			
			return FALSE;
		}
	}
	/**
	 * Compare
	 * 
	 * Not implemented
	 */
	function compare(){
		
		if(count($this->base[0]) > 1){
			
		}
		
	}

	/**
	 * Default Create Privilege Mask
	 * @param array $scope
	 */
	public function Create($scope){
		
		return $this->Scope($scope);
	}

	/**
	 * Default Edit/Update Privilege Mask
	 * @param array $scope
	 */
	public function Update($scope){
		
		return $this->Scope($scope);		
	}
	
	/**
	 * Default Delete Privilege Mask
	 * @param array $scope
	 */
	public function Delete($scope){

		return $this->Scope($scope);
	}
	
	/**
	 * Default View Privilege Mask
	 * @param array $scope
	 */
	public function View($scope){
		
		return $this->Scope($scope);
	}	
	
}