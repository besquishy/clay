<?php
/**
 * Clay Stylesheet Handler Library
 * 
 * Used to either link or include stylesheets into the Page template of a theme.
 *
 * @copyright (C) 2007-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay;

/**
 * Provides a means of dynamically linking or including stylesheets from Application and Themes, as well as external URLs.
 */
class Styles {
	/**
	 * @var string $mode Determines if stylesheets will be included inline or linked externally into the template
	 */
	public static $mode = 'external';
	/**
	 * @var array $map Array of requested stylesheets
	 */
	public static $map = array();
	/**
	 * Builds a string to distinguish each item in the $map array
	 * @param array $args ['type', 'name', 'file']
	 */
	private static function pattern($args){
		return $args['type'].':'.$args['name'].':'.$args['file'];
	}
	/**
	 * Add a stylesheet
	 * @param array $args ['type', 'name', 'file']
	 */
	public static function add($args){
		$pattern = self::pattern($args);
		if(!isset(self::$map['styles'][$pattern]['req'])){
			self::$map['styles'][$pattern]['req'] = $args;
		}
	}
	/**
	 * Add a theme stylesheet
	 * @param string $name Theme name
	 * @param string $file File name
	 */
	public static function addTheme($name,$file){
		self::add(array('type' => 'theme', 'name' => $name, 'file' => $file));
	}
	/**
	 * Add an application stylesheet
	 * @param string $name Application name
	 * @param string $file File name
	 */
	public static function addApplication($name,$file){
		self::add(array('type' => 'app', 'name' => $name, 'file' => $file));
	}
	/**
	 * Add an external stylesheet
	 * @param string $name URL
	 * @param string $file File name
	 */
	public static function addURL($name,$file){
		self::add(array('type' => 'url', 'name' => $name, 'file' => $file));
	}
	/**
	 * Check for a stylesheet's existence in a theme
	 * @param string $name Theme name
	 * @param string $file File name
	 */
	private static function inTheme($name,$file){
		return file_exists(THEMES_PATH.$name.'/styles/'.$file);
	}
	/**
	 * Check for an Application stylesheet being overridden by a Theme
	 * @param string $application Application name
	 * @param string $file File name
	 */
	private static function themeOverride($application,$file){
		return file_exists(THEMES_PATH.THEME.'/applications/'.$application.'/styles/'.$file);
	}
	/**
	 * Check for a stylesheet's existence in an application
	 * @param string $name Application name
	 * @param string $file File name
	 */
	private static function inApplication($name,$file){
		return file_exists(APPS_PATH.$name.'/styles/'.$file);
	}
	/**
	 * Process External stylesheets
	 * @param array $args ['type', 'name', 'file']
	 */
	private static function processExternal($args){
		$pattern = self::pattern($args);
		switch($args['type']){
			case('app'):
				if(self::themeOverride($args['name'],$args['file'])){
					self::$map['styles'][$pattern]['src'] = REL_THEMES_PATH.THEME.'/applications/'.$args['name'].'/styles/'.$args['file'];
				} elseif(self::inApplication($args['name'],$args['file'])) {
					self::$map['styles'][$pattern]['src'] = REL_APPS_PATH.$args['name'].'/styles/'.$args['file'];
				}
				break;
			case('theme'):
				if(self::inTheme($args['name'],$args['file'])){
					self::$map['styles'][$pattern]['src'] = REL_THEMES_PATH.$args['name'].'/styles/'.$args['file'];
				}
				break;
			case('url'):
				self::$map['styles'][$pattern]['src'] = $args['file'];
				break;
		}
		if(!empty(self::$map['styles'][$pattern]['src'])){
			unset(self::$map['styles'][$pattern]['req']);
		} else {
			self::$map['missing.styles'][] = $pattern;
			unset(self::$map['styles'][$pattern]);
		}
	}
	/**
	 * Process Internal stylesheets
	 * @param array $args ['type', 'name', 'file']
	 */
	private static function processInternal($args){
		$pattern = self::pattern($args);
		switch($args['type']){
			case('app'):
				if(self::themeOverride($args['name'],$args['file'])){
					self::$map['styles'][$pattern] = THEMES_PATH.THEME.'/applications/'.$args['name'].'/styles/'.$args['file'];
				} elseif(self::inApplication($args['name'],$args['file'])) {
					self::$map['styles'][$pattern] = APPS_PATH.$args['name'].'/styles/'.$args['file'];
				}
				break;
			case('theme'):
				if(self::inTheme($args['name'],$args['file'])){
					self::$map['styles'][$pattern] = THEMES_PATH.$args['name'].'/styles/'.$args['file'];
				}
				break;
		}
		if(!is_string(self::$map['styles'][$pattern])){
			self::$map['missing.styles'][] = $pattern;
			unset(self::$map['styles'][$pattern]);
		}
	}
	/**
	 * Include / Link stylesheets
	 * 
	 * Used in Theme Page template
	 */
	public static function css(){
		if(!empty(self::$map['styles'])){
			switch(self::$mode){
				case('internal'):
					foreach(self::$map['styles'] as $args){
						switch($args['req']['type']){
							case('url'):
								self::processExternal($args['req']);
								break;
							default:
								self::processInternal($args['req']);
								break;
						}
					}
					break;
				case('external'):
					foreach(self::$map['styles'] as $args){
						self::processExternal($args['req']);
					}
					break;
			}
			include APPS_PATH.'common/templates/styles/'.self::$mode.'-css.tpl';
		}
		if(!empty(self::$map['missing.styles'])) include APPS_PATH.'common/templates/styles/missing-css.tpl';
	}
}
