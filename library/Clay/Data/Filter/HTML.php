<?php
/**
 * Clay Data HTML Filter Library
 * Strip attribute Class
 * Remove attributes from XML elements
 * @author (original) David (semlabs.co.uk)
 * @author (as-is) David L. Dyess II
 * @version 0.2.1-clay
 * @package Clay Data Filter
 */

namespace Clay\Data\Filter;

/**
 * Escape Characters in a String
 * @param string $str String to Escape
 */
function Reg_Escape( $str )
{
	$conversions = array( "^" => "\^", "[" => "\[", "." => "\.", "$" => "\$", "{" => "\{", "*" => "\*", "(" => "\(", "\\" => "\\\\", "/" => "\/", "+" => "\+", ")" => "\)", "|" => "\|", "?" => "\?", "<" => "\<", ">" => "\>" );
	return strtr( $str, $conversions );
}
/**
 * Filter HTML tags/attributes with a whitelist, removing all other tags/attributes
 */
class HTML
{
	/**
	 * String to Filter
	 * @var string
	 */
	public $str			= '';
	/**
	 * Allow
	 * @var array
	 */
	public $allow		= array();
	/**
	 * Whitelisted Tags & Attributes
	 * @var array
	 */
	public $exceptions	= array();
	/**
	 * Ignore
	 * @var array
	 */
	public $ignore		= array();
	/**
	 * Whitelisted Tags only
	 */
	public $tags		= '';
	/**
	 * Strip Tags
	 * @param string $str String to filter
	 */
	public function strip( $str )
	{
		$this->str = $str;
		
		if( is_string( $str ) && strlen( $str ) > 0 )
		{
			$res = $this->findElements();
			if( is_string( $res ) )
				return $res;
			$nodes = $this->findAttributes( $res );
			$this->removeAttributes( $nodes );
			# Added by David Dyess (clay)
			$this->str = \strip_tags($this->str, $this->tags);
		}
		
		return $this->str;
	}
	/**
	 * Find Elements
	 */
	private function findElements()
	{
		
		# Create an array of elements with attributes
		$nodes = array();
		preg_match_all( "/<([^ !\/\>\n]+)([^>]*)>/i", $this->str, $elements );
		foreach( $elements[1] as $el_key => $element )
		{
			if( $elements[2][$el_key] )
			{
				$literal = $elements[0][$el_key];
				$element_name = $elements[1][$el_key];
				$attributes = $elements[2][$el_key];
				if( is_array( $this->ignore ) && !in_array( $element_name, $this->ignore ) )
					$nodes[] = array( 'literal' => $literal, 'name' => $element_name, 'attributes' => $attributes );
			}
		}
		
		# Return the XML if there were no attributes to remove
		if( empty($nodes[0]) )
			return $this->str;
		else
			return $nodes;
	}
	/**
	 * Find Attributes
	 * @param array $nodes
	 */
	private function findAttributes( $nodes )
	{
		
		# Extract attributes
		foreach( $nodes as &$node )
		{
			preg_match_all( "/([^ =]+)\s*=\s*[\"|']{0,1}([^\"']*)[\"|']{0,1}/i", $node['attributes'], $attributes );
			if( $attributes[1] )
			{
				foreach( $attributes[1] as $att_key => $att )
				{
					$literal = $attributes[0][$att_key];
					$attribute_name = $attributes[1][$att_key];
					$value = $attributes[2][$att_key];
					$atts[] = array( 'literal' => $literal, 'name' => $attribute_name, 'value' => $value );
				}
				$node['attributes'] = $atts;
				unset( $atts );
			} else {
				$node['attributes'] = null;
			}
						
		}
		
		return $nodes;
	}
	/**
	 * Strip Attributes not whitelisted
	 * @param array $nodes
	 */
	private function removeAttributes( $nodes )
	{
		
		# Remove unwanted attributes
		foreach( $nodes as $node )
		{
			
			# Check if node has any attributes to be kept
			$node_name = $node['name'];
			$new_attributes = '';
			if( is_array( $node['attributes'] ) )
			{
				foreach( $node['attributes'] as $attribute )
				{
					if( ( is_array( $this->allow ) && in_array( $attribute['name'], $this->allow ) ) || $this->isException( $node_name, $attribute['name'], $this->exceptions ) )
						$new_attributes = $this->createAttributes( $new_attributes, $attribute['name'], $attribute['value'] );
				}
			}
			$replacement = ( $new_attributes ) ? "<$node_name $new_attributes>" : "<$node_name>";
			$this->str = preg_replace( '/'. reg_escape( $node['literal'] ) .'/', $replacement, $this->str );
		}
		
	}
	/**
	 * Identify Exception
	 * @param string $element_name
	 * @param string $attribute_name
	 * @param array $exceptions
	 */
	private function isException( $element_name, $attribute_name, $exceptions )
	{
		if( array_key_exists($element_name, $this->exceptions) )
		{
			if( in_array( $attribute_name, $this->exceptions[$element_name] ) )
				return true;
		}
		
		return false;
	}
	/**
	 * Create Attribute
	 * @param string $new_attributes
	 * @param string $name
	 * @param string $value
	 */
	private function createAttributes( $new_attributes, $name, $value )
	{
		if( $new_attributes )
			$new_attributes .= " ";
		$new_attributes .= "$name=\"$value\"";
		
		return $new_attributes;
	}

}

?>
