<?php
/**
 * ClayDB
 * 
 * Database Extraction Layer and Manipulation Library
 * @copyright (C) 2007-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package ClayDB
 */
	/**
	 * ClayDB
	 * 
	 * A database abstraction layer, extendable by standalone adapters
	 */
	class ClayDB {
		/**
		 * @var array $connections Resource (connections) cache
		 */
		private static $connections = array();
		/**
		 * @var array $adapters Map drivers (adapters) to PHP extensions or libraries
		 */
		private static $adapters = array('pdo_mysql' => 'PDO_MySQL', 'pdo_sqlite' => 'PDO_SQLite');
		/**
		 * @var string $cfg Override the configuration name
		 */
		public static $cfg = '';
		/**
		 * @var array $tables Tables listed in site configuration data
		 */
		public static $tables = array();
		/**
		 * @var string $prefix Default Table Prefix
		 */
		public static $prefix = 'clay';
		/**
		 * @var string $version ClayDB version
		 */
		public static $version = '1.96';
		/**
		 * Database Connection
		 * 
		 * Uses the appropriate driver to create a connection
		 * @param array $dsn Database connection info
		 * @return resource
		 */
		private static function Connection($dsn){
			# Provide a better error message if database configuration isn't complete.
			if( empty( $dsn['driver'] )){
				# Database configuration isn't properly configured
				throw new \Exception( 'Please check database configuration in Clay Installer.' );
			}
			# Import our adapter
			\Library('ClayDB/Adapter/'.self::$adapters[$dsn['driver']]);
			# Adapter class as a string (notice difference between path and namespace)
			$driver = '\ClayDB\Adapter\\'.self::$adapters[$dsn['driver']];
			$conn = new $driver;
			# Don't be confused here, we are not using $this->connect(). We are using the adapter's connect method.
			$conn->connect($dsn['driver'], $dsn['host'], $dsn['database'], $dsn['user'], $dsn['pw']);
			# Return our resource - Callable using $conn->link-> ...
			return $conn;
		}
		/**
		 * Database Status
		 * 
		 * Checks for an active connection to a database
		 * @param string $host Hostname
		 * @param string $type Database Driver
		 * @param string $usern Database User
		 * @return boolean
		 */
		public static function Active($host,$type,$usern){
			return !empty(self::$conections[$host.$type.$usern]);
		}
		# Our "singleton factory" - takes db config settings and deals connections
		# FIXME: A lot of the caching procedure isn't necessary any longer, this needs to be refactored for cache[src] and cache[rsrc].
		/**
		 * ClayDB Connect
		 * 
		 * @param string $src Site Database Configuration Name
		 */
		public static function Connect($src = 'default'){
			/** This cache stores all database configurations for a site */
			static $cache = array();
			# get connection settings
			# TODO: Load configuration specific db settings? FIXME: Configuration specific enough?
			# XXX: Trying to decide how to use the database configurations to sort out connections. Considering app.[app] / app.[app].table
			# If self::$cfg has been set, we use that as the site configuration, otherwise we use the one Clay is using
			$config = !empty(self::$cfg) ? self::$cfg : \clay\CFG_NAME;
			//self::$cfg = '';
			if(empty($cache[$config.'.databases'])){ 
				$cache[$config.'.databases'] = \Clay::Config('sites/'.$config.'/databases');
			}
			if(!empty($cache[$config.'.databases'][$src])){
				$dbcfg = $cache[$config.'.databases'][$src]['connection'];
				$dbname = $cache[$config.'.databases'][$src]['database'];
				self::$prefix = !empty($cache[$config.'.databases'][$src]['prefix']) ? $cache[$config.'.databases'][$src]['prefix'] : 'clay';
			} else {
				$dbcfg = !empty($cache[$config.'.databases']['default']['connection']) ? $cache[$config.'.databases']['default']['connection'] : '';
				$dbname = !empty($cache[$config.'.databases']['default']['database']) ? $cache[$config.'.databases']['default']['database'] : '';
				self::$prefix = !empty($cache[$config.'.databases']['default']['prefix']) ? $cache[$config.'.databases']['default']['prefix'] : '';
			}
			# Cache system databases
			$cache['system.databases'] = !empty($cache['system.databases']) ? $cache['system.databases'] : \Clay::Config("databases");
			$dbinfo = !empty($dbcfg) ? $cache['system.databases'][$dbcfg] : $cache['system.databases'][1]; # if the clay configuration doesn't have a specified DB set, use the first one from the system db list
			# Unnecessary, but easier to read.
			$host = $dbinfo['host'];
			$type = $dbinfo['type'];
			$usern = $dbinfo['usern'];
			# Check if a connection has already been made.
			if(!empty(self::$connections["$host.$type.$usern"])) {
				# Success, reuse the connection
				$conn =  self::$connections["$host.$type.$usern"];
				$conn->selectDB($dbname);
				return $conn;
			}
			# DB connection info
			$dsn = array('driver' => $type, 'host' => $dbinfo['host'], 'user' => $dbinfo['usern'], 'pw' => $dbinfo['passw'], 'database' => $dbname);
			$conn = self::Connection($dsn);
			# Save our connection resource, so it can be reused later.
			self::$connections["$host.$type.$usern"] = $conn;
			$conn->selectDB($dbname);
			self::Tables();
			# Returns the connection resource
			return $conn;
		}
		/** 
		 * ClayDB Tables
		 * 
		 * Build the \ClayDB::$tables array with table info
		 * @TODO: in the future datadict::registerTables(), datadict::unregistertables(), and claydb::tables() should be merged
		 * @return array()
		 */
		public static function Tables() {			

			$tables = !empty(self::$cfg) ? \Clay::Config('sites/'. self::$cfg .'/database.tables') : \Clay::Config('sites/'. \clay\CFG_NAME .'/database.tables');
			if(!empty($tables)){
				foreach($tables as $table => $info){
					self::$tables[$table] = !empty($info['prefix']) ? $info['prefix'] : self::$prefix .'_'. $info['table'];
				}
			}
			return self::$tables;
		}
	}
?>