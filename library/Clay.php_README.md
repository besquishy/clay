# Clay

## Libraries

`/library/Clay.php`

> Namespace: \ (global)

* The Base Library that creates the foundation for Clay
* Includes Functions and a Class

### Functions

#### function Import($file)

> Namespace + Function : `\Import()`

> string `$file` : Complete file path as a string, excluding file extension

* Include a PHP file only once.
* Only works with PHP files
* Most often used by other utility functions

#### function Library($path)

> Namespace + Function : `\Library()`

> string `$path` : File name with path starting at `/library`, excluding file extension

* Utilizes `\Import()`
* Example: `\Library( 'ClayDB' );`

#### function Clay($config='default')

> Namespace + Function : `\Clay()`

> string || array `$config` : Name of a Clay configuration or an Array containing the configuration data

* Initializes the Clay Core and results in Output
* Calls `\Clay::Bootstrap()`
* The Runtime used from entry points to initialize Clay
    * index.php
    * install.php

### Classes

#### class Clay

> Namespace + Class : `\Clay`

* Static Class : No Objects

##### Class Constants

constant `name : 'Clay Framework'`

> Namespace + Class + Constant : `\Clay::name`

* Name of Clay

constant `version : '1.1.0'`

> Namespace + Class + Constant : `\Clay::version`

* Version of Clay

constant `build : '2600'`

> Namespace + Class + Constant : `\Clay::build`

* Build Number of Clay
    * Allows tracking of changes without incrementing Version

constant `cname : 'Rai'`

> Namespace + Class + Constant : `\Clay::cname`

* Code Name of Clay

##### Class Properties

public static `$config : array()`

> Namespace + Class + Property : `\Clay::$config`

* Defined in `\Clay::Bootstrap()`

public static `$mode : 'LIVE'`

> Namespace + Class + Property : `\Clay::$mode`

* Server Mode
* Allows manipulation of output depending on Server operational status

##### Class Methods (Functions)

**public static function `Config($config)`**

> Namespace + Class + Function : `\Clay::Config()`

> string `$config` : Name of the Clay configuration

* Used to retrieve Clay configuration data from a configuration file

> return `array` : Clay configuration data Array contained in a Clay configuration file

**public static function `setConfig($config,$data)`**

> Namespace + Class + Function : `\Clay::setConfig()`

> string `$config` : Name of a Clay configuration

> array `$data` : Configuration data

* Used to set or update an array of configuration data into a configuration file 

> return `boolean` : `TRUE` on success; `FALSE` on failure

**public static function `Bootstrap($config = 'default')`**

> Namespace + Class + Function : `\Clay::Bootstrap()`

> string || array `$config` : Name of a Clay configuration or an Array containing the configuration data

* Sets non-configurable `\Clay` constants
* Sets `\Clay::$config` 
* Calls `\Clay::Init()` and `\Clay::Output()`
* Optionally configuration data may provide 
    `array(...'init' => [class], [function]...)` 
    to call `\[Class]::[Function]()` as a callback instead of `\Clay::Init()` and `\Clay::Output()`

**public static function `Init()`**

> Namespace + Class + Function : `\Clay::Init()`

* Calls `\Clay\Core::Init()`

**public static function `Output()`**

> Namespace + Class + Function : `\Clay::Output()`

* Calls `\Clay\Core::Output()`

**public static function `Library($path)`**

> Namespace + Class + Function : `\Clay::Library()`

> string `$path` : File name with path starting at `/library/Clay`, excluding file extension

* Utilizes `\Import()`
* Example: `\Clay::Library( 'Core' );`

**public static function `Redirect($url)`**

> Namespace + Class + Function : `\Clay::Redirect()`

> string `$url` : URL

* Redirects the page to the specified URL

**public static function `Vendor()`**

> Namespace + Class + Function : `\Clay::Vendor()`

* Imports the Composer Autoloader
* Utilizes `\Import()`
* Allows Composer Classes in `/vendor` to be autoloaded with including the file directly

> return `boolean` : `TRUE` on success; throws `\Exception` on failure