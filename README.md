# Clay Project #

Clay is a PHP 7.0+ development framework and content management system.

> **Note**: I attempt to maintain compatibility with PHP 5.4+, but I only test Clay on the latest release version of PHP. If you encounter an issue using PHP 5.4+, please submit an Issue and I will assist.

## Requirements ##

Clay has minimal requirements often available on shared hosting services:

- Apache 2
- MySQL 5
- PHP 5.4+ (PHP 7 Recommended)

## Developer Tools ##

Clay supports Composer Packages (`https://packagist.org`). Packages are generally considered to supplement Clay's libraries and Composer's autoloader.php can be loaded by calling \Clay::Vendor(). None of Clay's Core libraries or modules currently depend on any Composer Packages.

Clay does not require Composer for installation of Clay, it's only a developer option. Clay is not currently available as a Composer package.

Update packages/lock file:

CLI: `composer.phar update`

Install a new package/update json file

CLI: `composer.phar require [package]`

## Getting Started ##

Three ways to get Clay:

1. Use Git to clone Clay from the github repo: `git://github.com/daviddyess/clay.git`
2. Download the Master branch from `http://github.com/daviddyess/clay`

Upload the files to your server, as necessary.

Clay is designed to only have one folder below the web root, so your domain name should be pointed to the `/html` folder. If you are testing Clay without a domain, you will have to access the Clay-powered website by pointing to the `/html` folder in the URL (ie. `http://localhost/clay/html`).

Create a `clay/data` folder and make it writable by the web server.

- Generally chmod 700 is good.

> **Note**: `clay/data` is above the web root (`clay/html`) and will not be accessible by a browser if the domain is pointing to the intended web root.

## Installation ##

Point your browser to `http://[clay site]/install.php`

If you did not create the `data` folder or did not make it writable, you will be told to check it.

Setup the Clay Installer.

- The Installer has it's own security built-in, so it never has to be removed after installation.
- Enter a Passcode and a Pass Key
- Enter the Security Question and Answer. This allows you to enter the Installer if you ever forget the Passcode.
- All of the data entered here is saved a configuration file and information is encrypted.

Once the Installer is setup, you will be taken to the Installer Overview, which provides several options. 

> The Clay Installer allows multiple installations of Clay and other packages built to use the installer.

Click the Databases or Manage System Databases link.

Add information about your database server(s). 

> ClayDB uses a selector type database approach, which allows connections to multiple database servers and databases. System databases are universally available to all web site installations, but have to be designated as a Site database to be used by a specific site.

If you are on the `Databases` page, click the `Installations` link. If you are on the Overview page, click the `Manage Packages / Web Sites` link.

> This part of the Installer displays packages that are available to install. Simply enter an Installation name and click one of the Packages to select it for installation. The primary site installation will need to be named `default`. Choose `Clay x.x` to install Clay.

This step prepares the file system for a package installation. Enter the site configuration name and select which package to install, then click `Create`.

Once created, the installation will display beneath the `Installations` header. Click `Setup` next to your new installation to complete the installation process.

> As previously mentioned, an installation has to specify a database connection to use. This is done using named configurations and more specific information than is used for System Databases.

> The primary database for an installation has to be named `default`. 

Once you are on the Setup page, add a Site Database, remember the name has to be `default` for the site's primary database. Choose your desired System Database from the dropdown.

Many of the paths used by Clay are configurable. If you change folder names or paths, ensure you change the Path options to match.

Enter information to create the Site Administrator. Click `Install`.

Point your browser to `http://[clay site]/index.php` and view your new Clay-powered web site.