<?php
/**
 * Roles Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'User Roles',
			'name' => 'Roles',
			'version' => '1.0',
			'date' => 'September 10, 2017',
			'description' => 'User Roles Class',
			'class' => 'Security',
			'category' => 'Users',
			'depends' => array('modules' => array('Users')),
			'core' => TRUE,
			'setup' => TRUE,
			);
?>