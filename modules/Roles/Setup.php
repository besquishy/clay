<?php
/**
 * Roles Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module\Roles;

/**
 * Roles Module Setup
 */
class Setup {
	/**
	 * Construct
	 */
	public function __constructor(){
		
		# @todo Add way to determine installation/upgrade requirements and any dependencies
		
	}
	/**
	 * Install
	 */
	public static function Install(){
		
		\Library('ClayDB');
		$dbconn = \claydb::connect();
		$datadict = $dbconn->datadict();
		$tables = array('roles' => array('table' => 'roles'),				
						'user_roles' => array('table' => 'user_roles'));
		$datadict->registerTables($tables);
		\claydb::tables('flush');
		
		# Roles Table
		$roles = array();
		$roles['column'] = array('rid' => array('type' => 'ID'),
								'prid' => array('type' => 'INT', 'size' => '10'),
								'state' => array('type' => 'TINYINT', 'size' => '1'),
								'name' => array('type' => 'VARCHAR', 'size' => '25', 'null' => FALSE),
								'descr' => array('type' => 'VARCHAR', 'size' => '140'));
		 
		$roles['index'] = array('parent' => array('type' => 'KEY', 'index' => 'prid'),
								'role' => array('type'=> 'UNIQUE', 'index' => 'name'));
		 
		$datadict->createTable(\claydb::$tables['roles'],$roles);
		 
		# User Roles Table
		$userRoles = array();
		$userRoles['column'] = array('rmid' => array('type' => 'ID'),
									'rid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
									'uid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE));
			  
		$userRoles['index'] = array('user_role' => array('type' => 'UNIQUE', 'index' => 'rid, uid'),
									'user' => array('type' => 'KEY', 'index' => 'uid'));
			  
		$datadict->createTable(\claydb::$tables['user_roles'],$userRoles);
		
		return TRUE;
		
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){
		
	}
	/**
	 * Delete
	 */
	public static function Delete(){
		
	}
}