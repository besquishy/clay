<?php
/**
 * Session Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'User Sessions',
			'name' => 'Session',
			'version' => '1.1',
			'date' => 'September 10, 2017',
			'description' => 'Provides a database backend and session management class',
			'class' => 'Security',
			'category' => 'Users',
			'depends' => array(),
			'core' => TRUE,
			'setup' => TRUE,
			);
?>