<?php
/**
 * Session Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module\Session;

/**
 * Session Module Setup
 */
class Setup {
	/**
	 * Construct
	 */
	public function __construct(){
		
		# Use Manager Module here to control setup
		
	}
	/**
	 * Install
	 */
	public static function Install(){
		
		\Library('ClayDB');
		
		$dbconn = \claydb::connect();
		$datadict = $dbconn->datadict();
		$tables = array('sessions' => array('table' => 'sessions'));
		
		$datadict->registerTables($tables);
		\claydb::tables('flush');
		
		# User Sessions Table
	    $sessions = array();
	    $sessions['column'] = array('usid' => array('type' => 'VARCHAR', 'size' => '255', 'null' => FALSE),
	    							'exp' => array('type' => 'INT', 'size' => '10', 'null' => FALSE),
	    							'sdata' => array('type' => 'TEXT'));
	    
	    $sessions['index'] = array('sessionid' => array('type' => 'UNIQUE', 'index' => 'usid'),
	    							'expires' => array('type' => 'KEY', 'index' => 'exp'));	    
	    
	    $datadict->createTable(\claydb::$tables['sessions'],$sessions);
		
	    return TRUE;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){
		
		$dbconn = \claydb::connect();
		$datadict = $dbconn->datadict();
		
		switch($version){
			# 1.1 - Change sdata column from VARCHAR to TEXT
			case $version <= '1.0':
				$sessions = array();
				$sessions[] = array('modifyColumn', array('name' => 'sdata', 'type' => 'TEXT'));
				$datadict->alterTable(\ClayDB::$tables['sessions'], $sessions);
				break;
			# No Upgrade Path Specified
			default:
				
				break;
		}
		
		return TRUE;
	}
	/**
	 * Delete
	 */
	public static function Delete(){
		
		# Core Modules cannot be removed
		
	}
	
}