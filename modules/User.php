<?php
/**
 * User Module
 *
 * @copyright (C) 2010-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module;

\Clay\Module('Pattern/Singleton');

\Clay\Module('Users');

\Clay\Module('Security');

/**
 * User Module
 */
class User {
	
	/*
	 * Import Singleton Method - self::Instance()
	 */
	use \Clay\Module\Pattern\Singleton;
	
	/**
	 * Logged In
	 * @var boolean
	 */
	public $isMember = FALSE;
	
	/**
	 * User ID
	 * @var int
	 */
	private $id = NULL;

	/**
	 * New \Module\User Object
	 * 
	 * Sets session settings, initiates session, sets current User ID
	 */
	private function __construct(){
		
		$session['expire'] = \Clay\Application::Setting('system','session.expire', 100000);
		
		\ini_set("session.gc_maxlifetime", $session['expire']);
		
		$session['name'] = \Clay\Application::Setting('system','session.name');
		
		if(!empty($session['name'])){
			
			\ini_set("session.name", $session['name']);
		}
		
		unset($session);
		
		\ini_set("session.gc_probability", 20);
		
		\Clay\Module::API('Session','Start');
		
		if(empty($_SESSION['userid'])){
			
			$_SESSION['userid'] = 1;
		}
		
		$this->id = $_SESSION['userid'];
		$this->isMember = ($_SESSION['userid'] > 1) ? TRUE : FALSE;
	}
	
	/**
	 * Get or Set the User ID
	 * @param int $id
	 * @return int
	 */
	public function id($id=NULL){
		
		if(is_null($id)){

			# Maintain current Userid (should be same as $_SESSION['userid']
			return $this->id;
		}
		
		if(is_int($id)) {
			
			# Set new Userid
			$_SESSION['userid'] = $id;
			$this->id = $id;
			# Destroy the old session and create a new one
			\session_regenerate_id(true);
			return $id;
		}
		
		# Optional, Return the ID stored in the session - notice this means $id is neither NULL nor INT (presumably BOOLEAN)
		return $_SESSION['userid'];
	}
	
	/**
	 * Get User Information about the Current User
	 * @alias acct()
	 */
	public function Info(){
		
		# 1 = Not logged in
		if($this->id == 1){
			
			return array('uname' => 'Guest');
		}
		# @TODO: Eventually this will support a user's profile info, perhaps?
		# Return basic account info
		return $this->acct();
	}
	
	/**
	 * Get User Information about the Current User
	 * @return array
	 */
	public function Acct(){
		
		return \Clay\Module\Users::Get($this->id);
	}
	
	/**
	 * User Privilege Check
	 * Verify Authorization to Perform Privileged Actions
	 * @param string $application
	 * @param string $component
	 * @param string $name
	 * @param string $request
	 * @throws \Exception
	 */
	public function privilege($application,$component,$name,$request=TRUE){
		
		static $admin;
		
		if(!isset($admin)){
			
			$privilege = \Clay\Application_Privilege('system','admin');
		
			$privilege->Request = 'ALL';
		
			if(!$privilege->getPrivilege('system','admin','user')){
			
				#@todo return FALSE;
				# Throw an exception for testing only
				throw new \Exception("Application: $application Component: $component Privilege: $name is not registered!");
			}
		
			$admin = $privilege->Validate('user');
		}
		
		if(!empty($admin)){
			
			return TRUE;
		}
		
		$privilege = \Clay\Application_Privilege($application,$component);
		
		$privilege->Request = $request;
		
		if(!$privilege->getPrivilege($application,$component,$name)){
			
			#@todo return FALSE;
			# Throw an exception for testing only
			throw new \Exception("Application: $application Component: $component Privilege: $name is not registered!");
		}
		
		return $privilege->Validate($name);
	}	
	
	/**
	 * Get a User's Setting by Name
	 * 
	 * @param string $setting
	 * @param mixed $default (optional)
	 * @example \Clay\Module\User::Setting('timezone','America/Chicago');
	 * @return mixed $value or $default
	 */
	public function Setting($setting,$default=false){
		
		return \Clay\Module\Users::Setting($this->id, $setting, $default);
	}
	
	/**
	 * Set a User's Setting by Name
	 * @param string $setting
	 * @param mixed $value
	 * @example \Clay\Module\User::Set('timezone','America/Toronto');
	 * @return NULL
	 */
	public function Set($setting, $value){
		
		return \Clay\Module\Users::Set($this->id, $setting, $value);
	}
	/**
	 * AuthID
	 * 
	 * Generate Authentication ID
	 * @param string $app Application name
	 */
	public function authID($app){

		return \Clay\Module::Object('Security')->authID(array('origin' => $app));
	}
	/**
	 * Auth
	 * 
	 * Authenticate an Authentication ID
	 * @param string $app Application name
	 */
	public function auth($app){

		return \Clay\Module::Object('Security')->authCheck(array('origin' => $app));
	}
}