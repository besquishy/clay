# Clay

## Modules

`/modules` is the default folder used for Clay Modules.

Clay Modules are intermediate level APIs, which are intended to enhance Applications and Libraries.

Clay Modules reside in the `\Clay\Module` namespace

Modules can be either Static or Object Classes and reside in a file with the same name as the Class

* `Plugins Module` resides in `/modules/Plugins.php`
	* If a Module has utility classes (such as using SubModule or API Traits referenced later on), those will be placed in a subfolder with the Module's name
		* `/modules/Plugins/`

Modules can be imported using the `\Clay\Module()` function

* `function Module($path)`
	* Resides in `/library/Clay/Module.php`
	* Utilizes \Import()
	
Modules can also be imported using the `\Clay\Module::Import()` function

* `protected static function Import($module)`
	* Resides in `/library/Clay/Module.php`
	* Utilizes \Import()
	* Only available for use within the `\Clay\Module` Class
	
There is no difference between the two import functions, `\Clay\Module()` is a legacy function and retained after the Module system was converted to Class-based. Only Classes that Extend the `\Clay\Module` Class can call `\Clay\Module::Import()` - it's `protected`

> Clay Modules rarely need to be imported, but older versions of Clay required them to be imported using `\Clay\Module()` before they could be used. The functions discussed next will automatically import the called Module.

As noted previously, Clay Modules can be Static or Object Classes. 

* `public static function Object( $module,$args=NULL,$name=NULL )` - `\Clay\Module::Object()`
	* Returns an `object` of the Module requested (by Class name)
	* Caches objects when the `$name` parameter is specified
	* Passes `$args` parameter to the Module object
	* Recognizes a Class' `public function instance()` as a factory and returns it instead of creating a new object
	
* `public static function API( $module,$method,$args=NULL )` - `\Clay\Module::API()`
	* Calls a Static function within the requested Module
	* Passes `$args` parameter to the Module function

### Setup

Clay Modules are installable, similarly to Applications, and has the same installation requirements as applications. 

* For a Module to be installable it must provide
	* `/modules/[module]/info.php`
	* `/modules/[module]/setup.php`
	
### Module Design Patterns

Clay Modules do not have a specific requirement for design, other than to follow namespace and naming conventions. With that said, Clay does provide some design patterns that make Modules easier to build

> The `use` keyword imports a Trait into the Class, as if it were a written part of the Class

* `Singleton Trait` - Singleton Object
	* Implemented using `\Clay\Module( 'Pattern/Singleton' );`
	* Within the Module Class, declare `use \Clay\Module\Pattern\Singleton;`
	* Allows the Module to be called with `\Clay\Module::Object()` instead of using `\Clay\Module::API()`
		
* `Module API Trait` - Static SubClass
	* Implemented using `\Library(  'Clay/Module/API' );`
		* or `\Clay::Library( 'Module/API' )`
	* Within the Module Class, declare `use \Clay\Module\API;`
	* Imports `public static function API($class,$method,$args)`
	* Allows you to have Static SubClasses in a subfolder of the module and call it with `self::API()`
	* This Trait isn't currently used by any Modules, but was retained for future use if needed
	* Not a requirement to use `\Clay\Module::API()`
	
* `SubModules Trait` - Object SubClass
	* Implemented using `\Library( 'Clay/Module/SubModule' );`
		* or `\Clay::Library( 'Module/SubModule' );`
	* Within the Module Class, declare `use \Clay\Module\SubModule;`
	* Imports `public function Object($class, $args = NULL)`
	* Allows you to have Object SubClasses in a subfolder of the module and call it with `$this->Object();`
	* See the `Plugins Module` for an example using SubModules