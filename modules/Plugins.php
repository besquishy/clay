<?php
/**
 * Plugins Module
 *
 * @copyright (C) 2013-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module;

\Library( 'ClayDB/Connection' );
\Library( 'Clay/Module/API' );
\Library( 'Clay/Module/SubModule' );
\Library( 'Clay/Application/Plugin' );

/**
 * Plugins Module
 */
class Plugins {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	
	/*
	 * Use Trait SubModule API - imports $this->Object()
	*/
	use \Clay\Module\SubModule;
	
	/**
	 * Register Plugin Type or Plugin
	 *
	 * @param $args array('type' or 'plugin' or 'group', or 'instance', array([type: string 'app', string 'name', string 'descr'] or [plugin]))
	 * @return method
	 */
	public function Setup( $args ){

		if(($args[0] == 'type') OR ($args[0] == 'plugin')){
			# Return Plugins Setup API :: register()
			return $this->Object( 'PluginSetup' )->Register( $args );
		}

		if(($args[0] == 'group')){
			# Return Groups Setup API :: register()
			return $this->Object( 'GroupSetup' )->Register( $args );
		}

		if(($args[0] == 'instance')){
			# Return Instance Setup API :: register()
			return $this->Object( 'InstanceSetup' )->Register( $args );
		}
	}
	
	/**
	 * Delete Plugin Type or Plugin
	 *
	 * @param $args array('type' or 'plugin' or 'group' or 'instance', array([type: string 'app', string 'name', string 'descr'] or [plugin]))
	 * @return method
	 */
	public function Delete( $args ){
		
		if(($args[0] == 'type') OR ($args[0] == 'plugin')){
			# Return Plugins Setup API :: register()
			return $this->Object( 'PluginSetup' )->Unregister( $args );
		}

		if(($args[0] == 'group')){
			# Return Groups Setup API :: register()
			return $this->Object( 'GroupSetup' )->Unregister( $args );
		}

		if(($args[0] == 'instance')){
			# Return Instance Setup API :: register()
			return $this->Object( 'InstanceSetup' )->Unregister( $args );
		}
	}

	/**
	 * Remove All Plugins and associations belonging to an Application
	 * @param integer|string $app Application ID | Application name
	 */
	public function Remove ( $app ){

		if( !is_int( $app )){
			# Get the App ID
			$app = \Clay\Application::getID( $app );
		}

		$plugins = $this->Get()->Plugins( $app );
		# Remove all Plugins 
		foreach( $plugins as $plugin ){
			$pid = array( 'pid' => $plugin['pid'] );
			
			$this->Object( 'PluginSetup' )-> Unregister( array( 'plugin', $pid));
		}
		# Remove all Hooks
		$this->Object( 'HookSetup' )->Remove( array( 'appid' => $app ));
	}
	/**
	 * Plugin Hook Loader
	 * 
	 * @param string $app Application name
	 * @param integer $itemtype
	 * @param integer $itemid
	 */
	public function Hooks( $app, $itemtype = NULL, $itemid = NULL ){
		# Reuse data if possible
		static $hooksCache;
		
		if( !empty( $hooksCache["$app.$itemtype.$itemid"] )){
			
			return $hooksCache["$app.$itemtype.$itemid"];
		}
		# User Module
		$user = \Clay\Module::API( 'User', 'Instance' );
		# Get hooked plugins
		$hooks = $this->Get()->AppHooks( $app, $itemtype, $itemid );
		
		$types = array();
		# Process our hooks
		foreach( $hooks as $plugin => $value ){
			# Privilege check, if not allowed the plugin isn't displayed
			if( !$user->privilege( 'plugins', 'Hook', 'View', $value['hid'] )) continue;
			# Privilege check passed
			$hookedApp = \Clay\Application::getName( $value['plugin_appid'] );
			$hook = $this->Import( $hookedApp, $value['plugin_type'], $value['plugin_name'] );
			$hook = new $hook;
			$hook->App = $app;
			$hook->Itemtype = $itemtype;
			$hook->ItemID = $itemid;
			$value['object'] = $hook;    # Index Key = Plugin Type/Plugin Name
			$types[$value['plugin_type']][$hookedApp.'/'.$value['plugin_name']] = $value;
			unset($hook);
		}
		# Cache the data for later
		$hooksCache["$app.$itemtype.$itemid"] = $types;
		# Return Hooks
		return $types;
	}

	/**
	 * Plugin Instance Loader
	 * Used for Groups
	 * @param int $group
	 */
	public function Instances( $group ){
		
		$hooks = $this->Get()->GroupHooks( $group );
		# User Module
		$user = \Clay\Module::API( 'User', 'Instance' );
		$instances = array();
		# Process our hooks
		foreach( $hooks as $plugin => $value ){
			# Privilege check, if not allowed the plugin isn't displayed
			if( !$user->privilege( 'plugins', 'Instance', 'View', $value['iid'] )) continue;
			# Privilege check passed
			# Get Name of the Application the Plugin/Instance belongs to
			$hookedApp = \Clay\Application::getName( $value['plugin_appid'] );
			# Import the Plugin
			$hook = $this->Import( $hookedApp, $value['plugin_type'], $value['plugin_name'] );
			$hook = new $hook;
			$hook->App = 'plugins';
			$hook->ItemType = $group;
			# Get Instance options (NULL or Serialized)
			if( !is_null( $value['instance_options'] )){

				$instance['options'] = unserialize( $value['instance_options'] );

			} else { 
				# Provide empty array
				$instance['options'] = array();
			}
			# Set the Instance template
			$hook->Template = $instance['options']['template'];
			# Get Instance content (NULL or Serialized)
			if( !is_null( $value['instance_content'] )){

				$instance['content'] = unserialize( $value['instance_content'] );

			} else {
				# Provide empty array
				$instance['content'] = array();
			}
			# Set instance to the Plugin object
			$instance['instance'] = $hook;
			# Build array of hooked instances
			$instances[] = $instance;
			unset($hook);
		}
		# Return Hooks
		return $instances;
	}

	/**
	 * Check if a Plugin is Hooked
	 * 
	 * @param mixed $plugin int || array( 'type' => [plugin type], 'app' => [plugin app], 'name' => [plugin])
	 * @param mixed $app int || string
	 * @param int $itemtype
	 * @param int $itemid
	 */
	public function Hooked( $plugin, $app, $itemtype = NULL, $itemid = NULL ){
		
		$plugin = !is_int( $plugin ) ? $this->Get()->PluginID( $plugin ) : $plugin;
		$app = !is_int( $app ) ? \Clay\Application::getID( $app ) : $app;
		
		$hooked = self::db()->get('hid FROM '.\claydb::$tables['plugin_hooks'].' WHERE pid = ?, appid = ?, itemtype = ?, itemid = ?', array( $plugin, $app, $itemtype, $itemid ), '0,1');
		return !empty( $hooked ) ? TRUE : FALSE;
	}
	
	/**
	 * Enable (Add) a Hook in an Application
	 * 
	 * @param array $args
	 */
	public function Hook( $args ){
		# Return Hook Setup API :: register()
		return $this->Object( 'HookSetup' )->Register( $args );
	}
	
	/**
	 * Disable (Remove) a Hook from an Application
	 * 
	 * @param array $args
	 */
	public function unHook( $args ){
		# Return Hook Setup API :: unregister()
		return $this->Object( 'HookSetup' )->Unregister( $args );
	}
	/**
	 * Plugin Loader
	 * 
	 * @param string $type Plugin Type
	 * @param string $app Application name
	 * @param string $plugin Plugin name
	 */
	 public function Plugin( $type, $app, $plugin ){

		$pluginNS = $this->Import( $app, $type, $plugin );
		# Return Plugin Object
		return new $pluginNS;
	}
	
	/**
	 * Plugins Utility API
	 * 
	 * @param string $method
	 * @param mixed $args
	 */
	public function Get( $method = NULL, $args = NULL ){
		# Placeholder for plugin info utility
		if(!empty( $method )){
			# Call the requested method
			return $this->Object( 'Get' )->$method( $args );
		}
		# Return the Get submodule object
		return $this->Object( 'Get' );
	}
	
	/**
	 * Import a Plugin 
	 * @param string|int $app
	 * @param string|int $pluginType
	 * @param string $plugin
	 * @throws \Exception
	 */
	public function Import( $app, $pluginType, $plugin ) {
		# $app should always be a string 'name' here, but we'll check in case
		if( is_int( $app )){
			# Get the $app name from ID
			$app = \Clay\Application::getName( $app );
		}
		# $pluginType should always be a string 'name' here, but again we'll check in case
		if( is_int( $pluginType )){
			
			$pluginType = $this->Get()->TypeName( $pluginType );
		}
		if( is_int( $plugin )){

			$plugin = $this->Get()->PluginByID( $plugin );
			$plugin = $plugin['name'];
		}
		# Build the file path to the Plugin's class
		$path = \clay\APPS_PATH.$app.'/plugins/'.$pluginType.'/'.$plugin;
		# Import the Plugin class
		if( !import( $path )){

			throw new \Exception( "Application Plugin $app\\plugin\\$pluginType\\$plugin doesn't exist! in ".\clay\APPS_PATH );
		}
		
		return '\application\\'.$app.'\plugin\\'.$pluginType.'\\'.$plugin;
		
	}
	/**
	 * Load a Plugin
	 * @param string $plugin Plugin namespace
	 * @param array $args
	 * @throws \Exception
	 */
	public function Load( $plugin, $args = array() ) {
				
		return new $plugin($args);
	}
}