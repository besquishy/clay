<?php 
/**
 * Security Module
 * 
 * Provides functions to facilitate safer user environment
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright (C) 2012-2017 David L. Dyess II 
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 */

namespace Clay\Module;

/**
 * Security Module
 */
class Security {
	
	/**
	 * Generate Authorization ID 
	 * @param array $args
	 * @return string
	 */
	public function authId($args=array()){

		\extract($args);
		$id = md5(time() * time() / 1.1);
		$_SESSION[$origin]['authid'] = $id;
		$field = md5(time() / 50);
		$_SESSION[$origin]['authfield'] = $field;
		return array('field' => $field, 'id' => $id);
	}
	
	/**
	 * Validate Authorization ID
	 * @param array $args
	 */
	public function authCheck($args){

		\extract($args);
		$field = $_SESSION[$origin]['authfield'];
		
		if($_SESSION[$origin]['authid'] != \Clay\Data\Post($field, 'string')){

			if(empty($bypass)){

				throw new \exception('You are not authorized to submit POST requests without using a form.');
			}

			return false;

		} else {

			return true;
		}
	}
	
	/**
	 * Generate Hash String
	 * @param string $string
	 * @param string $key
	 * @return string
	 */
	public function Hash($string,$key){
		
		if(empty($key)){
			
			$key =  \md5(\time().\mt_rand());
			
		}
		
		Return \hash_hmac('sha512', $string , $key);
		
	}
	
	/**
	 * Authenticate a User Against the User Database
	 * 
	 * @param string $username
	 * @param string $password
	 * @todo Add other authentication options. 
	 */
	public function Authenticate($username,$password){
	
		\Library('ClayDB');
		$dbconn = \claydb::connect();
		\claydb::tables();

		if(empty($username) || empty($password)){
			\Clay\Application::API('system','message','send','Invalid Username or Password');
			goto end;
		}
		# This *should* be the only place the user password is retrieved
		$req = $dbconn->get('uid, passw FROM '.\claydb::$tables['users'].' WHERE uname = ?',array($username),'0,1');
		$key = $dbconn->get('value FROM '.\claydb::$tables['user_settings'].' WHERE uid = ? AND name = ?',array($req['uid'],'key'),'0,1');
		$hash = \hash_hmac('sha512', $password , $key['value']);
		
		if($req['passw'] == $hash){

			$_SESSION['userid'] = $req['uid'];
			\Clay\Application::API('system','message','send','You are now logged in!');
			\session_regenerate_id(true);
		} else {
			\Clay\Application::API('system','message','send','Invalid Username and/or Password');
		}
	
		end:
		return \clay::redirect($_SERVER['HTTP_REFERER']);
	
	}
}