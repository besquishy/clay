<?php
/**
 * Privileges Module
 * @copyright 2012-2017
 * @author David L. Dyess II
 * @license GPL
 */

namespace Clay\Module\Privileges;

/**
 * Privileges Module Setup
 */
class Setup {
	/**
	 * Construct
	 */
	public function __construct(){
		
		# Use Manager Module here to control setup
		
	}
	/**
	 * Install
	 */
	public static function Install(){
		
		\Library('ClayDB');
		
		$dbconn = \claydb::connect();
		$datadict = $dbconn->datadict();
		$tables = array('privileges' => array('table' => 'privileges'),
						'role_privileges' => array('table' => 'role_privileges'));
		
		$datadict->registerTables($tables);
		\claydb::tables('flush');
		
		$privileges = array();
		$privileges['column'] = array('pid' => array('type' => 'ID'),
									'appid' => array('type' => 'INT', 'size' => '10'),
									'component' => array('type' => 'VARCHAR', 'size' => '25', 'null' => FALSE),
									'name' => array('type' => 'VARCHAR', 'size' => '25', 'null' => FALSE));
		 
		$privileges['index'] = array('app_priv' => array('type' => 'UNIQUE', 'index' => 'appid, component, name'));
		 
		$datadict->createTable(\claydb::$tables['privileges'],$privileges);
		 
		# Role Privileges Table
		$rolePrivileges = array();
		$rolePrivileges['column'] = array('rpid' => array('type' => 'ID'),
										'rid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
										'pid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
										'scope' => array('type' => 'VARCHAR', 'size' => '255', 'null' => FALSE));
						 
		$rolePrivileges['index'] = array('role_priv' => array('type' => 'UNIQUE', 'index' => 'rid, pid, scope'),
										'priv' => array('type' => 'KEY', 'index' => 'pid'));
								 
		$datadict->createTable(\claydb::$tables['role_privileges'],$rolePrivileges);
		
		return TRUE;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){		
		
	}
	/**
	 * Delete
	 */
	public static function Delete(){		
		
	}	
}