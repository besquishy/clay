<?php
/**
 * Privileges Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Privileges',
			'name' => 'Privileges',
			'version' => '1.0',
			'date' => 'September 10, 2017',
			'description' => 'User Privileges Class',
			'class' => 'Security',
			'category' => 'Users',
			'depends' => array('modules' => array('Roles')),
			'core' => TRUE,
			'setup' => TRUE,
			);
?>