<?php
/**
 * Setup Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module;

/**
 * Setup Module
 */
class Setup {
	
	/**
	 * Static Class
	 */
	private function __constructor(){
		# Static Class / No Object
	}
	
	/**
	 * Setup Type Selector
	 * @param string $setup
	 * @return object
	 */
	public static function Type($setup){
		
		\Clay\Module('Setup/'.$setup);
		
		$setup = '\Clay\Module\Setup\\'.$setup;
		
		return new $setup();
		
	}

	/**
	 * Get All Dependents
	 * @param string $type
	 * @param string $name
	 */
	public static function Dependents($type,$name){
		
		
		
	}
	
	/**
	 * Get All Dependencies
	 * @param string $type
	 * @param string $name
	 */
	public static function Depends($type,$name){
		
		switch($type){
			
			case 'application':
				
				$config = \Clay\Application::Info('Application',$name);
				$depends = $config['depends'];
				unset($config);
				break;
				
			case 'module':
				
				$config = \Clay\Module::Info($name);
				$depends = $config['depends'];
				unset($config);
				break;
		}
		
		return $depends;		
	}
	
	/**
	 * Resolve Dependencies Before Installation
	 * @param string $type
	 * @param string $name
	 */
	public static function preinstall($type,$name){
		
	}
}