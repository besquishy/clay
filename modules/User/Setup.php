<?php
/**
 * User Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module\User;

/**
 * User Module Setup
 */
class Setup {
	/**
	 * Construct
	 */
	public function __constructor(){
		
		# @todo Add way to determine installation/upgrade requirements and any dependencies
		
	}
	/**
	 * Install
	 */
	public static function Install(){
		
		return TRUE;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){
		
	}
	/**
	 * Delete
	 */
	public static function Delete(){
		
	}
}