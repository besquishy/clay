<?php
/**
 * Module Singleton Trait
 *
 * @copyright (C) 2007-2012 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Singleton Trait Class
 */

namespace Clay\Module\Pattern;

/**
 * Provides a base Trait for implementing the Singleton Design Pattern
 */
trait Singleton {
	/**
	 * Class has to implement __construct
	 */
	abstract function __construct();
	/**
	 * Used to instantiate, store, and return the Class object
	 */
	public static function Instance(){
	
		static $instance; # Class object storage
		
		$class = __CLASS__; # Child class being instantiated
		
		# If the child class hasn't been instantiated
		if(empty($instance)) {
			
			# Instantiate the class and add it to the storage array
			$instance = new $class;
		}
		
		# Return the stored object
		return $instance;
	}
}