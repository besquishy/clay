<?php
/**
 * Pattern Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Pattern',
			'name' => 'Pattern',
			'version' => '1.0',
			'date' => 'September 10, 2017',
			'description' => 'Extendable Classes for Implementing Design Patterns',
			'class' => 'Developer',
			'category' => 'Utility',
			'depends' => array(),
			'core' => FALSE,
			'setup' => FALSE,
			);
?>