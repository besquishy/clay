<?php
/**
 * Privileges Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module;

\Library('ClayDB/Connection');
\Clay\Module('Pattern/Singleton');

/**
 * Privileges Module
 */
class Privileges {
	/**
	 * Privileges
	 * @var array
	 */
	private $Privileges = array();
	
	/**
	 * Import $this->db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Import Singleton Method - self::Instance()
	 */
	use \Clay\Module\Pattern\Singleton;
	/**
	 * Construct
	 */
	private function __construct(){
		
	}
	
	/**
	 * Builds an array mapping Privilege assignments to Roles and their Scope.
	 * 
	 * Used as a utility functionality, not intended to be used for verifying Privilege assignment
	 * @param integer $pid
	 * @return array
	 * @todo Fix this! This is not efficient and should allow Mapping in different levels, ie. All Privileges => Roles, User Privileges => Roles
	 */
	public function Roles($pid=NULL){
		
		# This method populates $Privileges and only has to be ran once
		if(!empty($this->Privileges)){
		
			if(!empty($pid)){
					
				return !empty($this->Privileges[$pid]) ? $this->Privileges[$pid] : FALSE;
			}
			
			return !empty($this->Privileges) ? $this->Privileges : FALSE;
			
		} else {
		
			# Loop through Roles mapped in the Roles::Graph()
			foreach(\Clay\Module::Object('Roles')->Graph() as $roleID){
			
				# Get Privileges assigned to this Role ID
				foreach ($this->getRole($roleID) as $privilege){
							
		
						# Map the Role Privileges with Scope
						$this->Privileges[$privilege['pid']][] = array('rid' => $roleID, 'scope' => $privilege['scope']);
									
				}
			}

			if(!empty($pid)){
					
				return !empty($this->Privileges[$pid]) ? $this->Privileges[$pid] : FALSE;
			}
				
			return !empty($this->Privileges) ? $this->Privileges : FALSE;
		}
	}
	
	/**
	 * Get a registered Privilege
	 * @param string $app
	 * @param string $component
	 * @param string $privilege
	 * @return array
	 */
	public function Get($app, $component, $privilege){
		
		$where = " WHERE appid = ? AND component = ? AND name = ?";
		$bind = array(\Clay\Application::getID($app), $component, $privilege);
		
		return $this->db()->get("pid, appid, component, name FROM ".\claydb::$tables['privileges']. $where, $bind, '0,1');
	}
	/**
	 * Get Privilege Info
	 * @param integer $pid Privilege ID
	 */
	public function getInfo($pid){
		
		return $this->db()->get("pid, appid, name, component FROM ".\claydb::$tables['privileges']." WHERE pid = ?", array($pid), '0,1');
	}
	/**
	 * Get All Registered Privileges or All for a Specific Application
	 * @param string $app
	 * @return array
	 */
	public function getAll($app=NULL){
		
		$where = '';
		$bind = array();
		
		if(!empty($app)) {
			
			$where = $where . " WHERE appid = ?";
			$bind[] = \clay\application::getID($app);
		}
		
		return $this->db()->get("pid, appid, name, component FROM ".\claydb::$tables['privileges']. $where, $bind);
	}
	/**
	 * Add a Privilege to the system
	 * @param string $app
	 * @param string $component
	 * @param string $privilege
	 */
	public function Register($app, $component, $privilege){
		
		$priv = $this->Get($app,$privilege,$component);
		
		if(empty($priv)){
			
			return $this->db()->add(\claydb::$tables['privileges']." (appid, name, component) VALUES (?,?,?)", array(\clay\application::getID($app),$privilege,$component));
		
		} else {
			
			return $this->db()->update(\claydb::$tables['privileges']." SET component = ? WHERE pid = ?", array($component,$priv['pid']), 1);
		}
	}
	/**
	 * Alias for register()
	 * @param string $app
	 * @param string $component
	 * @param string $privilege
	 */
	public function Add($app, $component, $privilege){
		
		return $this->register($app, $component, $privilege);
	}
	/**
	 * Update a Privilege, alias for register()
	 * @param string $app
	 * @param string $privilege
	 * @param string $component
	 */
	public function Update($app, $privilege, $component){
		
		return $this->register($app, $component, $privilege);
	}
	
	/**
	 * Remove a Privilege from the system
	 * @param string $app
	 * @param string $component
	 * @param string $privilege
	 */
	public function Unregister($app, $component, $privilege){
		
		$privilege = $this->get($app,$component,$privilege);
		
		if(!empty($privilege)) {
			
			$this->db()->delete(\claydb::$tables['privileges']." WHERE pid = ?",array($privilege['pid']),1);
			$this->db()->delete(\claydb::$tables['role_privileges']." WHERE pid = ?",array($privilege['pid']));
		}
	}
	
	/**
	 * Alias for unregister()
	 * @param string $app
	 * @param string $component
	 * @param string $privilege
	 */
	public function Delete($app, $component, $privilege){
		
		return $this->unregister($app, $component, $privilege);
	}
	
	/**
	 * Remove all privileges of an application (uninstall)
	 * @param string $app
	 * @return bool
	 */
	public function Remove($app){
		
		$privileges = $this->getAll($app);
		
		foreach($privileges as $privilege){
			
			$this->db()->delete(\claydb::$tables['privileges']." WHERE pid = ?",array($privilege['pid']),1);
			$this->db()->delete(\claydb::$tables['role_privileges']." WHERE pid = ?",array($privilege['pid']));
		}
	}
	
	/**
	 * Get the Privileges assigned to a specific Role.
	 * @param integer $roleID
	 */
	public function getRole($roleID){
		
		return $this->db()->get('rpid, pid, scope FROM '.\claydb::$tables['role_privileges'].' WHERE rid = ?',array($roleID));
	}
	
	/**
	 * Get a Privilege Assigned to a Role
	 * @param int $rolePrivilegeID
	 */
	public function getRolePrivilege($rolePrivilegeID){
		
		return $this->db()->get('rpid, rid, pid, scope FROM '.\claydb::$tables['role_privileges'].' WHERE rpid = ?',array($rolePrivilegeID),'0,1');
	}
	
	/**
	 * Get the Roles assigned a specific Privilege.
	 * @param integer $privilegeID
	 */
	public function getRoles($privilegeID){
		
		return $this->db()->get('rpid, rid FROM '.\claydb::$tables['role_privileges'].' WHERE pid = ?',array($privilegeID));
	}
	
	/**
	 * Assign a Privilege to a Role
	 * @param integer $roleID
	 * @param integer $privilegeID
	 * @param string $scope
	 */
	public function addRole($roleID, $privilegeID, $scope){
		
		return $this->db()->add(\claydb::$tables['role_privileges']." (rid, pid, scope) VALUES (?,?,?)", array($roleID,$privilegeID,$scope));
	}
	
	/**
	 * Update a Privilege assigned to a Role
	 * @param integer $rolePrivilegeID
	 * @param string scope
	 */
	public function updateRole($rolePrivilegeID,$scope){
		
		return $this->db()->update(\claydb::$tables['role_privileges']." SET scope = ? WHERE rpid = ?", array($scope, $rolePrivilegeID), 1);
	}
	
	/**
	 * Remove a Privilege assigned to a Role
	 * @param integer $rolePrivilegeID
	 */
	public function removeRole($rolePrivilegeID){
		
		return $this->db()->delete(\claydb::$tables['role_privileges']." WHERE rpid = ?",array($rolePrivilegeID),1);
	}	
}