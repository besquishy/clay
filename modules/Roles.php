<?php
/**
 * Roles Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module;

\Library('ClayDB/Connection');

/**
 * Roles Module
 */
class Roles {
	
	/**
	 * Assigned Roles (recursive)
	 * @var array
	 */
	private $Roles = array();
	/**
	 * User ID
	 * @var integer
	 */
	public $User;
	
	/**
	 * Import $this->db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All Role IDs of a User, including Parent Roles
	 */
	public function Graph(){
		
		# This method creates $this->Roles and is only ran once
		if(!empty($this->Roles)) {
			
			return $this->Roles;
		}
		
		# Get user roles:		
		$assigned = $this->User(\Clay\Module::Object('User')->id());
		
		# Recursive Loop through Role -> Parent relationships
		Loop:
		
			foreach($assigned as $key => $role) {			
				
				# If we have already processed this Role, move to next
				if(!empty($this->Roles[$role['rid']])){
					unset($assigned[$key]);
					continue;
				}
				
				$this->Roles[$role['rid']] = $role['rid'];
				
				# Add user's base role's parent
				if(!empty($role['prid'])){
					
					$assigned[] = array('rid' => $role['prid']);
					
				} else {
	
					$parent = $this->getParent($role['rid']);
					
					# Ensure there IS a parent
					if(!empty($parent)){
					
						$assigned[] = array('rid' => $parent);
					}
				}
				
				# Untrack this role
				unset($assigned[$key]);
			}

		# If new Roles were identified through inheritance, Loop again
		if(!empty($assigned)){
			
			goto Loop;
		}

		return $this->Roles;		
	}
	
	/**
	 * Build a Role Tree, starting at a specific Role ID
	 * @param integer $roleID
	 */
	public function Tree($roleID){
		
		# Get the Requested Role's Info
		$assigned = array($roleID => $this->Get($roleID));
		
		$roles = array();
		
		# Recursive Loop through Role -> Parent relationships
		Loop:
		
			foreach($assigned as $key => $role) {			
				
				# If we have already processed this Role, move to next
				if(!empty($roles[$role['rid']])){
					
					continue;
				}
				
				$roles[$role['rid']] = $role['rid'];
				
				# Add user's base role's parent
				if(!empty($role['prid'])){
					
					$assigned[] = array('rid' => $role['prid']);
					
				} else {
	
					$parent = $this->getParent($role['rid']);
					
					# Ensure there IS a parent
					if(!empty($parent)){
					
						$assigned[] = array('rid' => $parent);
					}
				}
				
				# Untrack this role
				unset($assigned[$key]);
			}

		# If new Roles were identified through inheritance, Loop again
		if(!empty($assigned)){
			
			goto Loop;
		}

		return $roles;		
	}
	
	/**
	 *
	 * Get Role Info
	 * @param integer $roleID
	 * @return array [rid,prid,state,name,descr]
	 */
	public function Get($roleID){
		
		return $this->db()->get("rid, prid, state, name, descr FROM ".\claydb::$tables['roles']." WHERE rid = ?", array($roleID), '0,1');
	}
	
	/**
	 *
	 * Add a new Role to the system
	 * @param string $name
	 * @param integer $state
	 * @param string $description
	 * @param integer $parent
	 * @return BOOLEAN
	 */
	public function Register($name,$state,$description=NULL,$parent=NULL){
		
		$roleID = $this->getID($name);
		
		if(empty($roleID)){
			
			return $this->db()->add(\claydb::$tables['roles']." (prid, state, name, descr) VALUES (?,?,?,?)", array($parent,$state,$name,$description));
		
		} else {
			
			throw new \Exception("Role: $name (id: $roleID) already exists!");
		}
		
	}
	
	/**
	 * Add a New Role to the System
	 * 
	 * alias to register()
	 * @param string $name
	 * @param integer $state
	 * @param string $description
	 * @param integer $parent
	 * @return BOOLEAN
	 * @see \claycms\user\roles::register
	 */
	public function Add($name,$state,$description=NULL,$parent=NULL){
		
		return $this->register($name,$state,$description,$parent);
	}
	
	/**
	 *
	 * Update a Role
	 * @param int $roleID
	 * @param string $name
	 * @param integer $state
	 * @param string $description
	 * @param integer $parent
	 * @return BOOLEAN
	 */
	public function Update($roleID,$name,$state,$description=NULL,$parent=NULL){
		
		$roleCheckID = $this->getID($name);
		
		if(($roleID == $roleCheckID) OR empty($roleCheckID)){
			
			return $this->db()->update(\claydb::$tables['roles']." SET prid = ?, state = ?, name = ?, descr = ? WHERE rid = ?", array($parent,$state,$name,$description,$roleID), 1);
		
		} else {
			
			throw new \Exception("Role: $name (id: $roleCheckID) already exists!");
		}
	}
	
	/**
	 *
	 * Remove a Role and All Associations
	 * @param integer $roleID
	 * @return array [roles,role_privileges,user_roles] OR FALSE
	 */
	public function Unregister($roleID){
		
		$role = $this->Get($roleID);
		
		if(!empty($role)) {
			
			$trace = array();
			$trace['roles'] = $this->db()->delete(\claydb::$tables['roles']." WHERE rid = ?",array($role['rid']),1);
			$trace['role_privileges'] = $this->db()->delete(\claydb::$tables['role_privileges']." WHERE rid = ?",array($role['rid']));
			$trace['user_roles'] = $this->db()->delete(\claydb::$tables['user_roles']." WHERE rid = ?",array($role['rid']),1);
			$trace['child_roles'] = $this->db()->update(\claydb::$tables['roles']." SET prid = ? WHERE prid = ?", array(NULL,$roleID));
			
			return $trace;
		}
		
		return false;
	}
	
	/**
	 *
	 * Add a User to a Role
	 * @param integer $userID
	 * @param integer $roleID
	 * @return boolean
	 */
	public function addUser($userID,$roleID){
		
		return $this->db()->add(\claydb::$tables['user_roles']." (rid, uid) VALUES (?,?)", array($roleID,$userID));
	}
	
	/**
	 * Remove a User from a Role
	 * @param integer $userID
	 * @param integer $roleID
	 * @return boolean
	 */
	public function removeUser($userID,$roleID){
		
		return $this->db()->delete(\claydb::$tables['user_roles']." WHERE rid = ? and uid = ?",array($roleID,$userID),1);
	}
	
	/**
	 * Remove all Roles from a User
	 * @param integer $userID
	 * @return boolean
	 */
	public function dropUser($userID){
		
		return $this->db()->delete(\claydb::$tables['user_roles']." WHERE uid = ?",array($userID));
	}
	
	/**
	 *
	 * Get a Role's ID from its Name
	 * @param string $role
	 * @return int or FALSE
	 */
	public function getID($role){
		
		$role = $this->db()->get("rid FROM ".\claydb::$tables['roles']." WHERE name = ?", array($role), '0,1');
		
		return $role['rid'];
	}
	
	/**
	 * Get a Role ID's Name
	 * @param integer $roleID
	 * @return string OR FALSE
	 */
	public function getName($roleID){
		
		$role = $this->db()->get("name FROM ".\claydb::$tables['roles']." WHERE rid = ?", array($roleID), '0,1');
		
		return $role['name'];
	}
	
	/**
	 * Get a Role ID's Description
	 * @param integer $roleID
	 * @return string OR FALSE
	 */
	public function getDescription($roleID){
		
		$role = $this->db()->get("descr FROM ".\claydb::$tables['roles']." WHERE rid = ?", array($roleID), '0,1');
		
		return $role['descr'];
	}
	
	/**
	 * Get a Role ID's Parent Role
	 * @param int
	 */
	public function getParent($roleID){
		
		$parent = $this->db()->get('prid FROM '.\claydb::$tables['roles'].' WHERE rid = ?',array($roleID),'0,1');
		
		return $parent['prid'];		
	}
	
	/**
	 * Get All Roles
	 * @return array [rid,prid,state,name,descr]
	 */
	public function getAll(){
		
		return $this->db()->get("rid, prid, state, name, descr FROM ".\claydb::$tables['roles']);
	}
	
	/**
	 * Get a User's Assigned Role IDs
	 * @param integer $userID
	 * @return array [rid]
	 */
	public function User($userID){
		
		return $this->db()->get("rid FROM ".\claydb::$tables['user_roles']." WHERE uid = ?", array($userID));
	}		
}