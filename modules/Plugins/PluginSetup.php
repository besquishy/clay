<?php
/**
 * Plugins Setup
 * 
 * @copyright (C) 2014-2015 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

/**
 * Plugins Module Plugin Setup
 */
class PluginSetup {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	
	/**
	 * Register Plugin Type or Plugin
	 *
	 * @param array $args 
	 * @return method
	 * @sample array( 'type' or 'plugin', array([type: string 'app', string 'name', string 'descr'] or [plugin]))
	 */
	public function Register( $args ){
		
		switch( $args[0] ){
			
			case 'type':
				
				return $this->addType( $args[1] );
				
			case 'plugin':
				
				return $this->Add( $args[1] );
		}
	}
	
	/**
	 * Unregister Plugin Type or Plugin
	 * @param array $args 
	 * @sample array( 'type', array( 'name' => [Plugin Type] )
	 * @sample array( 'plugin', array( 'type' => [Plugin Type], 'name' => [Plugin], 'app' => [Application] )
	 */
	public function Unregister( $args ){
		
		switch( $args[0] ){
			
			case 'type':
				
				return $this->removeType( $args[1] );
				
			case 'plugin':
				
				return $this->Remove( $args[1] );
		}
	}
	
	/**
	 * Private Method for Creating Plugin Types
	 * @param array $args
	 * @throws \Exception
	 */
	private function addType( $args ){
		
	    if( empty( $args['name'] )){
	    	
	    	throw new \Exception( 'Plugin Types must have a unique name (string "name")' );
	    }
	    
	    # @FIXME: Need to inform the admin of existing Plugin Type - gracefully
	    #if(self::getTypeID($args['name'])) throw new \Exception('Plugin Type '.$args['name'].' is already registered to Application ID '.$args['appid']);
	    
	    if( empty( $args['descr'] )){
	    	
	    	$args['descr'] = '';
	    }
	    # Check for existing Plugin Type using the same name
	    if( !\Clay\Module::Object( 'Plugins' )->Get()->Type( $args['name'] )){
	    	# Create the new plugin
	    	return self::db()->add( \claydb::$tables['plugin_types']." (name, descr) VALUES (?,?)", array( $args['name'],$args['descr'] ));
	    
	    } else {
	    	# Use the old plugin (even though this can cause conflicts)
	    	# @TODO Log this potential issue in the future.
	    	return TRUE;
	    }
	}
	
	/**
	 * Private Method for Creating Plugins
	 * @param array $args
	 * @throws \Exception
	 */
	private function Add($args){
		
		if(!empty($args['app'])){
			# Get appid from app
			$args['appid'] = \Clay\Application::getID($args['app']);
		}
		
		if(empty($args['appid'])){
			# No appid found
			throw new \Exception('Plugins must be registered under an Application (string "app" / int "appid")');
		}
		
	    if(empty($args['name'])){
	    	# No Plugin name provided
	    	throw new \Exception('Plugins must have a unique name (string "name") per Plugin Type - Application denomination');
	    }
	    # Get the Plugin Type ID
	    $ptype = \Clay\Module::Object('Plugins')->Get()->TypeID($args['type']);
	    
	    if(empty($ptype)){
	    	# Plugin Type doesn't exist
	    	throw new \Exception('Plugins must be assigned to an existing Plugin Type (string "type")');
	    }
	    
	    if(\Clay\Module::Object('Plugins')->Get()->Plugin($ptype,$args['appid'],$args['name'])){
	    	# Plugin already registered
	    	throw new \Exception('Plugin '.$args['name'].' is already registered to Application ID '.$args['appid'].' for Plugin Type '.$args['type']);
	    }
	    
	    if(empty($args['descr'])){
	    	# No Plugin description provided
	    	$args['descr'] = '';
		}
		
		if(empty($args['inst'])){
	    	# No Plugin description provided
	    	$args['inst'] = NULL;
	    }
	    
	    return self::db()->add(\claydb::$tables['plugins']." (ptype, appid, name, descr, inst) VALUES (?,?,?,?,?)",array($ptype,$args['appid'],$args['name'],$args['descr'],$args['inst']));
	}
	
	/**
	 * Private Method for Removing Plugins
	 * @param array $args
	 */
	public function Remove( $args ){

		if( empty( $args['appid'] ) AND !empty( $args['app'] ) AND !is_int( $args['app'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}

		# Remove a specific Plugin
		if( !empty( $args['appid'] ) AND !empty( $args['name'] ) AND !empty( $args['type'] AND empty( $args['pid'] ))){
			# Plugin Type ID
			$ptype = \Clay\Module::Object( 'Plugins' )->Get()->TypeID( $args['type'] );
			# Plugin ID
			$plugin = \Clay\Module::Object( 'Plugins' )->Get()->App( $args['appid'], $ptype, $args['name'] );
			$args['pid'] = $plugin['pid'];
		}

		if( !empty( $args['pid'] )){
			$pid = array( 'pid' => $args['pid'] );
			\Clay\Module::Object( 'Plugins' )->Object( 'HookSetup' )->Remove( $pid );
			\Clay\Module::Object( 'Plugins' )->Object( 'InstanceSetup' )-> Remove( $pid );
			return self::db()->delete(\claydb::$tables['plugins']." WHERE pid = ?", array( $args['pid'] ));
		}
		
		return FALSE;
	}
	
	/**
	 * Private Method for Removing Plugin Types
	 * @param array $args
	 */
	private function removeType($args){

		# Make sure no plugins are currently registered that use this plugin type
		$plugins = \Clay\Module::Object('Plugins')->Get()->All($args['name']);
		
		if(empty($plugins)){
			
			return self::db()->delete(\claydb::$tables['plugin_types']." WHERE name = ?",array($args['name']));
		}
		
		/* This isn't operational
		 * # Remove all registered Plugin Types
		if(empty($args['name']) AND empty($args['type'])){
			return $dbconn->delete(\claydb::$tables['plugins']." WHERE appid = ?",array(\claycms\application::getID($args['app'])));
		}
		# Remove all registered Plugins of a specific Plugin Type
		if(empty($args['name']) AND !empty($args['type'])){
			return $dbconn->delete(\claydb::$tables['plugins']." WHERE stype = ? AND appid = ?",array(self::getTypeID($args['type']),\claycms\application::getID($args['app'])));
		}
		# Remove a specific Plugin
		if(!empty($args['name']) AND !empty($args['type'])){
			return $dbconn->delete(\claydb::$tables['plugins']." WHERE stype = ? AND appid = ? AND name = ?",array(self::getTypeID($type),\claycms\application::getID($args['app']),$name),1);
		}*/
		
		return FALSE;
	}
}