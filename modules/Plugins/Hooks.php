<?php
/**
 * Plugin Info
 * 
 * Submodule API mapped to \Clay\Module\Plugins::Get()
 * 
 * @copyright (C) 2014 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

/**
 * Plugins Module Hooks Submodule
 */
class Hooks {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	
	# Plugins Hooks API - placeholder - experimental
}