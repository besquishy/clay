<?php
/**
 * Plugin Group Setup
 * 
 * @copyright (C) 2014-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

/**
 * Plugins Module Group Setup
 */
class GroupSetup {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	
	/**
	 * Register Plugin Group
	 *
	 * @param array $args
	 * @return method
	 * @sample array('group', array(bool 'update', int 'state', string 'name', array 'options']))
	 */
	 public function Register( $args ){
		
		if( !empty( $args[1]['update'] )){
			
			return $this->Update( $args[1] );
		}
		
		return $this->Add( $args[1] );
	}
	/**
	 * Unregister Plugin Group
	 * @param array $args
	 */
	public function Unregister( $args ){
		# An individual Hook is being removed
		return $this->Delete( $args[1] );
	}
	/**
	 * Private Method for Creating a Group
	 * @param array $args
	 * @throws \Exception
	 */
	private function Add( $args ){
		# Set Defaults
		$args = $this->setDefaultArgs( $args );
		
		return self::db()->add(\claydb::$tables['plugin_groups']." (name, state, options) VALUES (?,?,?)", array($args['name'], $args['state'], $args['options']));
	}
	/**
	 * Private Method for Updating a Group
	 * @param array $args
	 */
	private function Update( $args ){
		# Set Defaults
		$args = $this->setDefaultArgs( $args );
		
		return self::db()->update(\claydb::$tables['plugin_groups']." SET name = ?, state = ?, options = ? WHERE gid = ?", array($args['name'],$args['state'],$args['options'], $args['gid']), 1);
	}
	/**
	 * Private Method for Removing A Group and All Instances from a Group
	 * @param int $group
	 */
	private function Delete( $group ){
		# Group Instances are Hooked to Plugins app
		$appid = \Clay\Application::getID( 'plugins' );
		# Delete Hooks to this Group
		$hooks = self::db()->delete( \claydb::$tables['plugin_hooks']." WHERE appid = ? AND itemtype = ?",array( $appid, $group['gid'] ), 1);
		# Delete Group
		$group = self::db()->delete( \claydb::$tables['plugin_groups']." WHERE gid = ?",array( $group['gid'] ), 1);

		return true;
	}
	/**
	 * Private Method for Setting Default Values to Args
	 * @param array $args
	 */
	private function setDefaultArgs( $args ){

		if( empty( $args['state'] )) $args['state'] = NULL;
		if(!empty( $args['options'] ) AND is_array( $args['options'] ))	$args['options'] = serialize( $args['options'] );
		if( empty( $args['options'] )) $args['options'] = NULL;

		return $args;
	}
}