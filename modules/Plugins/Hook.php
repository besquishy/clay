<?php
/**
 * Plugin Hook
 * 
 * Submodule API mapped to \Clay\Module\Plugins::Get()
 * 
 * @copyright (C) 2014 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

/**
 * Plugin Module Hook Submodule
 */
class Hook {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	
	# Plugins Hook API - placeholder - experimental
}