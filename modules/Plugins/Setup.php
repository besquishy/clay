<?php
/**
 * Plugins Module
 *
 * @copyright (C) 2013-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

\Library('ClayDB','Connection');

/**
 * Plugins Module Setup
 */
class Setup {
	/**
	 * Import ClayDB Trait
	 */
	use \ClayDB\Connection;
	/**
	 * Construct
	 */
	public function __construct(){
		
		# Use Manager Module here to control setup
	}
	/**
	 * Install
	 */
	public static function Install(){
		
		$dbconn = self::db();
		$datadict = $dbconn->datadict();
		# Register Tables for ClayDB
		$tables = array('plugin_types' => array('table' => 'plugin_types'),
						'plugins' => array('table' => 'plugins'),
						'plugin_groups' => array('table' => 'plugin_groups'),
	    				'plugin_instances' => array('table' => 'plugin_instances'),
						'plugin_hooks' => array('table' => 'plugin_hooks')
	    				);
	    $datadict->registerTables($tables);
	    \claydb::tables('flush');
	    
		# Plugin Types table
		$plugintypes = array();
		# -- Columns
	    $plugintypes['column'] = array('ptype' => array('type' => 'ID'),
	    								'name' => array('type' => 'VARCHAR','size' => '20', 'null' => FALSE),
	    								'descr' => array('type' => 'STRING'));
	    # -- Indices
	    $plugintypes['index'] = array('name' => array('type' => 'UNIQUE', 'index' => 'name'));
	    # -- Create the Table
	    $datadict->createTable(\claydb::$tables['plugin_types'],$plugintypes);
	    
		# Application Plugins table
		$plugins = array();
	    # -- Columns
		$plugins['column'] = array('pid' => array('type' => 'ID'),
							'inst' => array('type' => 'TINYINT', 'size' => '1', 'unsigned' => TRUE),
	    					'ptype' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
	    					'appid' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
	    					'name' => array('type' => 'VARCHAR','size' => '20', 'null' => FALSE),
	    					'descr' => array('type' => 'STRING'));
	    # -- Indices
	    $plugins['index'] = array('plugin_type' => array('type' => 'Key', 'index' => 'ptype'),
									'plugin' => array('type' => 'UNIQUE', 'index' => 'ptype, appid, name'),
									'instance' => array('type' => 'Key', 'index' => 'inst'));
	    # -- Create the Table
		$datadict->createTable(\claydb::$tables['plugins'],$plugins);
		
		# Plugin Groups Table
		$groups = array();
		# -- Columns
		$groups['column'] = array('gid' => array('type' => 'ID'),
									'state' => array('type' => 'TINYINT', 'size' => '1', 'unsigned' => TRUE),
									'name' => array('type' => 'VARCHAR', 'size' => '30', 'null' => FALSE),
									'options' => array('type' => 'VARCHAR', 'size' => '255'));
		# -- Indices
		$groups['index'] = array('group_name' => array('type' => 'UNIQUE', 'index' => 'name'));
		# -- Create the Table
		$datadict->createTable(\claydb::$tables['plugin_groups'],$groups);
		
		# Plugin Instances table
		$inst = array();
		# -- Columns
		$inst['column'] = array('iid' => array('type' => 'ID'),
								'state' => array('type' => 'TINYINT', 'size' => '1'),
								'pid' => array('type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
								'name' => array('type' => 'VARCHAR', 'size' => '30', 'null' => FALSE),
								'options' => array('type' => 'VARCHAR', 'size' => '255'),
								'content' => array('type' => 'TEXT'));
		# -- Indices
		$inst['index'] = array('instance_state' => array('type' => 'KEY', 'index' => 'state'),
								'plugin' => array('type' => 'KEY', 'index' => 'pid'),
								'instance' => array('type' => 'UNIQUE', 'index' => 'name'));
		# -- Create the Table
		$datadict->createTable(\claydb::$tables['plugin_instances'],$inst);
		
	    # Plugin Hooks table
		$hooks = array();
	    # -- Columns
	    $hooks['column'] = array('hid' => array('type' => 'ID'),
								'pid' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
								'iid' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE, 'null' => TRUE),
								'appid' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
								'itemtype' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE),
								'itemid' => array('type' => 'INT', 'size' => '11', 'unsigned' => TRUE),
								'pos' => array('type' => 'INT', 'size' => '2', 'unsigned' => TRUE),
								'field' => array('type' => 'VARCHAR', 'size' => '25'),
								'options' => array('type' => 'STRING'));
	    # -- Indices
		$hooks['index'] = array('plugin' => array('type' => 'KEY', 'index' => 'pid'),
								'instance' => array('type' => 'KEY', 'index' => 'iid'),
	    						'app' => array('type' => 'KEY', 'index' => 'appid, itemtype, itemid, field'));
	    # -- Create the Table
	    $datadict->createTable(\claydb::$tables['plugin_hooks'],$hooks);
	    return TRUE;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){
		
		$dbconn = self::db();
		$datadict = $dbconn->datadict();

		switch($version){
			# Upgrade from version 1.0
			case ($version <= '1.0'):
			# New plugin index
			$oldPluginsIndex[] = array('dropIndex', array('name' => 'plugin'));
			$datadict->alterTable(\claydb::$tables['plugins'], $oldPluginsIndex);
			$newPluginsIndex[] = array('addIndex', array('name' => 'plugin', 'type' => 'UNIQUE', 'index' => 'ptype, appid, name'));
			$datadict->alterTable(\claydb::$tables['plugins'], $newPluginsIndex);
			# Upgrade from version 1.0.1
			case($version <= '1.0.1'):
			# Register new Tables
			$tables = array('plugin_groups' => array('table' => 'plugin_groups'),
							'plugin_instances' => array('table' => 'plugin_instances'),
							);
			$datadict->registerTables($tables);
			# Plugins_Hooks Table Updates
			# -- New Instance ID column
			$addInstanceCol[] = array('addColumn', array('name' => 'iid','type' => 'INT','size' => '11', 'unsigned' => TRUE, 'null' => TRUE));
			$datadict->alterTable(\claydb::$tables['plugin_hooks'], $addInstanceCol);
			# -- New Instance ID Index
			$newInstanceIndex[] = array('addIndex', array('name' => 'instance', 'type' => 'key', 'index' => 'iid'));
			$datadict->alterTable(\claydb::$tables['plugin_hooks'], $newInstanceIndex);
			# New Plugin Groups Table
			$groups = array();
			$groups['column'] = array('gid' => array('type' => 'ID'),
										'state' => array('type' => 'TINYINT', 'size' => '1', 'unsigned' => TRUE),
										'name' => array('type' => 'VARCHAR', 'size' => '30', 'null' => FALSE),
										'options' => array('type' => 'VARCHAR', 'size' => '255'));
			# -- Add Index
			$groups['index'] = array('group_name' => array('type' => 'UNIQUE', 'index' => 'name'));
			# -- Create the Table
			$datadict->createTable(\claydb::$tables['plugin_groups'],$groups);
			# New Plugin Instances table
			$inst = array();
			$inst['column'] = array('iid' => array('type' => 'ID'),
									'state' => array('type' => 'TINYINT', 'size' => '1'),
									'pid' => array('type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
									'name' => array('type' => 'VARCHAR', 'size' => '30', 'null' => FALSE),
									'options' => array('type' => 'VARCHAR', 'size' => '255'),
									'content' => array('type' => 'TEXT'));
			# -- Add Indices
			$inst['index'] = array('instance_state' => array('type' => 'KEY', 'index' => 'state'),
									'plugin' => array('type' => 'KEY', 'index' => 'pid'),
									'instance' => array('type' => 'UNIQUE', 'index' => 'name'));
			# -- Create the Table
			$datadict->createTable(\claydb::$tables['plugin_instances'],$inst);
			# Plugins Table Updates
			# -- New Instance Bool column
			$addInstCol[] = array('addColumn', array('name' => 'inst','type' => 'TINYINT','size' => '1', 'unsigned' => TRUE));
			$datadict->alterTable(\claydb::$tables['plugins'], $addInstCol);
			# -- New Instance Index
			$newInstIndex[] = array('addIndex', array('name' => 'instance', 'type' => 'key', 'index' => 'inst'));
			$datadict->alterTable(\claydb::$tables['plugins'], $newInstIndex);
			break;
		}
		
		
	}
	/**
	 * Delete
	 */
	public static function Delete(){
		
	}
}