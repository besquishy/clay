<?php
/**
 * Plugin Info
 * 
 * Submodule API mapped to \Clay\Module\Plugins::Get()
 * 
 * @copyright (C) 2014-2015 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

/**
 * Plugins Module Get Submodule
 */
class Get {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	/**
	 * Plugin Types
	 * @var array
	 */
	public $pluginTypes = array();
	
	/**
	 * Get a Plugin Type by Name or ID
	 * @param int|string $arg
	 * @example \Clay\Module\Plugins\Get::Type([name])
	 * @example \Clay\Module\Plugins\Get::Type([type id])
	 */
	public function Type($arg){
	
		if(is_int($arg)) {
				
			return self::db()->get('ptype, name, descr FROM '.\claydb::$tables['plugin_types']." WHERE ptype = ?", array($arg), '0,1');
		}
		
		if(is_string($arg)){
			
			return self::db()->get('ptype, name, descr FROM '.\claydb::$tables['plugin_types']." WHERE name = ?", array($arg), '0,1');
		} 
	}
	
	/**
	 * Get All Plugin Types
	 */
	public function Types(){
		
		return self::db()->get('ptype, name, descr FROM '.\claydb::$tables['plugin_types']);
	}
	
	/**
	 * Get a Plugin Type's ID by Name
	 * @param string $name
	 */
	public function TypeID($name){
		
		if(!empty($this->pluginTypes[$name])){
			
			return $this->pluginTypes[$name];
		}
		
		$ptypeId = self::db()->get('ptype FROM '.\claydb::$tables['plugin_types']." WHERE name = ?", array($name), '0,1');
		# Cache the Plugin Type
		$this->pluginTypes[$ptypeId['ptype']] = $name;
		$this->pluginTypes[$name] = $ptypeId['ptype'];
		return $ptypeId['ptype'];
	}
	
	/**
	 * Get a Plugin Type's Name by ID
	 * @param int $id
	 * @example \Clay\Module\Plugins\Get::TypeName([type id])
	 */
	public function TypeName($id){
		
		if( !empty( $this->pluginTypes[$id] )){
			
			return $this->pluginTypes[$id];
		}
		$ptype = self::db()->get('name FROM '.\claydb::$tables['plugin_types']." WHERE ptype = ?", array($id), '0,1');
		# Cache the Plugin Type
		$this->pluginTypes[$id] = $ptype['name'];
		$this->pluginTypes[$ptype['name']] = $id;
		return $ptype['name'];
	}
	
	/**
	 * Get an Application's Plugin
	 * @param int $app
	 * @param int $type
	 * @param string $plugin
	 */
	public function App($app,$type,$plugin){

		return self::db()->get('pid, ptype, appid, descr, name FROM '.\claydb::$tables['plugins']." WHERE ptype = ? AND appid = ? AND name = ?", array( $type, $app, $plugin), '0,1');
	}

	/**
	 * Get All Plugins registered by an Application
	 * @param integer|string $app Application ID | Application name
	 */
	public function Plugins( $app ){
		
		if( !is_int( $app )){
			# Get the App ID
			$app = \Clay\Application::getID( $app );
		}

		return self::db()->get('pid, ptype, appid, descr, name FROM '.\claydb::$tables['plugins']." WHERE appid = ?", array( $app));
	}
	
	/**
	 * Get a Plugin by Type and Name/Plugin ID
	 * @param int $ptype
	 * @param int|string $app
	 * @param string $plugin
	 */
	public function Plugin($ptype,$app,$plugin){
		
		if(!is_int($ptype)){
		# Get the Type ID	
			$ptype = $this->TypeID($ptype);
		}

		if(!is_int( $app )){
		# Get the App ID
			$app = \Clay\Application::getID($app);
		}
		
		if(is_int($plugin)){
		# Use the Plugin ID	
			$where = 'pid = ?';
			$bind = array($plugin);
		
		} else {
		# Use the Plugin name	
			$where = 'ptype = ? AND appid = ? AND name = ?';
			$bind = array($ptype, $app, $plugin);
		}
		
		return self::db()->get( 'pid, ptype, appid, descr, name FROM '.\claydb::$tables['plugins']." WHERE ".$where, $bind, '0,1' );
	}

	/**
	 * Get a Plugin ID
	 * @param array $plugin
	 */
	public function PluginID( $plugin ){
		
		if( !is_int($plugin['type'] )){
		# Get the Type ID	
			$plugin['type'] = $this->TypeID( $plugin['type'] );
		}

		if( !is_int( $plugin['app'] )){
		# Get the App ID
			$plugin['app'] = \Clay\Application::getID( $plugin['app'] );
		}
		
		if( !empty( $plugin['pid'] AND is_int( $plugin['pid']))){
		# Use the Plugin ID	
			$where = 'pid = ?';
			$bind = array( $plugin['pid'] );
		
		} else {
		# Use the Plugin name	
			$where = 'ptype = ? AND appid = ? AND name = ?';
			$bind = array( $plugin['type'], $plugin['app'], $plugin['name'] );
		}
		
		$pid = self::db()->get( 'pid FROM '.\claydb::$tables['plugins']." WHERE ".$where, $bind, '0,1' );
		return $pid['pid'];
	}

	/**
	 * Get a Plugin by Plugin ID
	 * @param int $plugin
	 */
	 public function PluginByID($plugin){

		# Use the Plugin ID	
		$where = 'pid = ?';
		$bind = array( $plugin );
		
		return self::db()->get( 'pid, ptype, appid, descr, name FROM '.\claydb::$tables['plugins']." WHERE ".$where, $bind, '0,1' );
	}
	
	/**
	 * Get Plugins that Require Instances
	 */
	 public function InstancePlugins(){
		
				# Use the Plugin ID	
				$where = 'inst = ?';
				$bind = array('1');
				
				return self::db()->get( 'pid, ptype, appid, descr, name FROM '.\claydb::$tables['plugins']." WHERE ".$where." ORDER BY ptype, name ASC", $bind );
			}
	
	/**
	 * Get All Application Plugins
	 * Optionally, by Plugin Type ($ptype) and Sorted ($order)
	 * @param int $ptype
	 * @param string $order
	 */
	public function All($ptype=NULL,$order=NULL){
		
		$order = !empty($order) ? " ORDER BY $order ASC" : '';
		
		if(!is_null($ptype)){
			
			if(!is_int($ptype)) $ptype = $this->TypeID($ptype);
			return self::db()->get( 'pid, ptype, appid, descr, name FROM '.\claydb::$tables['plugins']." WHERE ptype = ? $order", array( $ptype ));
		}
		
		return self::db()->get( 'pid, ptype, appid, descr, name FROM '.\claydb::$tables['plugins'].$order );
	}
	/**
	 * Get a Hook
	 * @param int $id
	 */
	public function Hook( $id ){
	
		$h = \claydb::$tables['plugin_hooks'];
		$p = \claydb::$tables['plugins'];
		$t = \claydb::$tables['plugin_types'];
		$columns = "$h.hid, $h.pid, $h.appid, $h.itemtype, $h.itemid, $h.pos, $h.field, $h.options, $p.name as plugin_name, $p.ptype, $p.descr, $t.name as plugin_type";
		//return self::db()->get( 'hid, pid, appid, itemtype, itemid, pos, field, options FROM '.\claydb::$tables['plugin_hooks']." $where", $args );
		return self::db()->get( "$columns FROM $h LEFT JOIN $p ON $h.pid=$p.pid LEFT JOIN $t on $p.ptype=$t.ptype WHERE $h.hid= ?", array( $id ), '0,1' );
	}
	/**
	 * Get all Hooks - optionally by assigned application
	 * @param int $app optional
	 * @param int $itemType optional
	 * @param int $itemID optional
	 */
	public function Hooks( $app = NULL, $itemType = NULL, $itemID = NULL ){
		# If an Application ID is specified, return AppHooks()
		if( !is_null( $app )){
			
			return $this->AppHooks( $app, $itemType, $itemID );
		}
		
		return self::db()->get( 'hid, pid, appid, itemtype, itemid, pos, field, options FROM '.\claydb::$tables['plugin_hooks']." ORDER BY hid ASC" );
	}
	/**
	 * Get all Hooks assigned to an Application
	 * @param string|int $app
	 * @param string $itemType
	 * @param string $itemID
	 */
	public function AppHooks( $app, $itemType = NULL, $itemID = NULL ){
		# Build the WHERE clause and parameters
		$where = "";
		# $app needs to be the App ID
		if( !is_int( $app )){
			
			$app = \Clay\Application::getID($app);
		}
		
		# @TODO: Work in some Sort options
		$h = \claydb::$tables['plugin_hooks'];
		$p = \claydb::$tables['plugins'];
		$t = \claydb::$tables['plugin_types'];
		
		$args = array( $app );
		# Check for specified Application Item Type and Item ID
		if( !is_null( $itemType ) || !is_null( $itemID )){
			
			if( !is_null( $itemType )){
				# Append Item Type
				$where = $where."AND $h.itemtype = ? ";
				$args[] = $itemType;
			}
			
			if( !is_null( $itemID )){
				# Append Item ID
				$where = $where."AND $h.itemid = ? ";
				$args[] = $itemID;
			}
		}
		
		$columns = "$h.hid, $h.pid, $h.appid, $h.itemtype, $h.itemid, $h.pos, $h.field, $h.options, $p.name as plugin_name, $p.ptype, $p.appid as plugin_appid, $p.descr, $t.name as plugin_type";
		//return self::db()->get( 'hid, pid, appid, itemtype, itemid, pos, field, options FROM '.\claydb::$tables['plugin_hooks']." $where", $args );
		return self::db()->get( "$columns FROM $h LEFT JOIN $p ON $h.pid=$p.pid LEFT JOIN $t on $p.ptype=$t.ptype WHERE $h.appid = ? $where", $args );
	}
	/**
	 * Get all Hooks Assigned from a Plugin
	 * @param in $pid (Plugin ID)
	 */
	public function PluginHooks( $pid ){
		
		return self::db()->get( 'hid, pid, appid, itemtype, itemid, pos, field, options FROM '.\claydb::$tables['plugin_hooks']." WHERE pid = ? ORDER BY appid ASC", array($pid) );
	}
	/**
	 * Fetch a Plugin Group
	 * @param int|string $group (ID) or string (NAME) $group
	 * @throws \Exception
	 * @return array
	 */
	 public static function Group( $group ){
		
		if(is_int($group)){
			# get by group id
			$where = 'gid';
					
		} elseif(is_string($group)) {
			# get by group name
			$where = 'name';
		}
		
		if(is_null($group)){
			
			throw new \Exception('Application API '.__NAMESPACE__.'::get() only accepts a Group ID (int) or Name (string).');
		}
		
		return self::db()->get('gid, name, state, options FROM '.\claydb::$tables['plugin_groups'].' WHERE '.$where.' = ?', array($group), '0,1');
	}
	/**
	 * Get all Hooks (Instances) assigned to a Group
	 * @param int $group
	 */
	public function GroupHooks( $group ){
		
		$h = \claydb::$tables['plugin_hooks'];
		$p = \claydb::$tables['plugins'];
		$i = \Claydb::$tables['plugin_instances'];
		$t = \claydb::$tables['plugin_types'];

		$app = \Clay\Application::getID( 'Plugins' );
		
		$where = " $h.appid = ? AND $h.itemtype = ? ";
		$args = array( $app, $group );		
		
		$columns = "$h.hid, $h.pid, $h.appid, $h.itemtype, $h.pos, $h.options as hook_options, 
		$p.name as plugin_name, $p.ptype, $p.appid as plugin_appid, $p.descr,
		$i.iid, $i.state as instance_state, $i.options as instance_options, $i.content as instance_content, $i.name as instance_name, 
		$t.name as plugin_type";
		//return self::db()->get( 'hid, pid, appid, itemtype, itemid, pos, field, options FROM '.\claydb::$tables['plugin_hooks']." $where", $args );
		return self::db()->get( "$columns FROM $h LEFT JOIN $p ON $h.pid=$p.pid LEFT JOIN $i on $i.iid=$h.iid LEFT JOIN $t on $p.ptype=$t.ptype WHERE $where ORDER BY $h.pos ASC", $args );
	}
	/**
	 * Get Group Instance Assignments
	 * @param int $group
	 */
	public static function GroupInstances($group){
		
		$inst = \claydb::$tables['plugin_instances'];
		$memb = \claydb::$tables['plugin_hooks'];
		$pluginsAppid = \Clay\Application::getID( 'plugins' );
		
		return self::db()->get($inst.'.iid, '.$inst.'.state, '.$memb.'.itemtype, '.$inst.'.pid, '.$inst.'.name, '.$inst.'.options, '.$inst.'.content FROM '.$inst.'
				LEFT JOIN '.$memb.' ON ( '.$inst.'.iid = '.$memb.'.iid ) 
				WHERE '.$memb.'appid = ? AND '.$memb.'.itemtype = ? ORDER BY '.$memb.'.pos ASC',array($plugisAppid, $group));
	}
	/**
	 * Get List of Group Instance Assignments
	 * @param int $group
	 */
	public static function GroupInstancesList($group){
		
		$inst = \claydb::$tables['plugin_instances'];
		$memb = \claydb::$tables['plugin_hooks'];
		$pluginsAppid = \Clay\Application::getID( 'plugins' );
		
		return self::db()->get($inst.'.iid, '.$inst.'.name, '.$inst.'.pid FROM '.$inst.'
				LEFT JOIN '.$memb.' ON ( '.$inst.'.iid = '.$memb.'.iid ) 
				WHERE '.$memb.'.appid = ? AND '.$memb.'.itemtype = ? ORDER BY '.$memb.'.pos ASC',array($pluginsAppid, $group));
	}
	/**
	 * Get List of Group Instance Assignments
	 */
	 public static function GroupsInstancesList(){
		
		$inst = \claydb::$tables['plugin_instances'];
		$memb = \claydb::$tables['plugin_hooks'];
		$groups = \ClayDB::$tables['plugin_groups'];
		$plugins = \ClayDB::$tables['plugins'];
		$pluginsAppid = \Clay\Application::getID( 'plugins' );
		
		return self::db()->get($inst.'.iid, '.$inst.'.name, '.$inst.'.pid, '.$plugins.'.appid, '.$plugins.'.ptype, '.$plugins.'.name, '.$groups.'.gid, '.$groups.'.name, '.$groups.'.state FROM '.$groups.'
				LEFT JOIN '.$memb.' ON ( '.$memb.'.itemtype = '.$groups.'.gid )		
				LEFT JOIN '.$inst.' ON ( '.$inst.'.iid = '.$memb.'.iid )
				LEFT JOIN '.$plugins.' ON ( '.$inst.'.pid = '.$plugins.'.pid )
				ORDER BY '.$groups.'.name ASC');
	}
	/**
	 * Fetch all Plugin Groups
	 * @throws \Exception
	 * @return array
	 */
	 public static function Groups(){
		
		return self::db()->get('gid, name, state, options FROM '.\claydb::$tables['plugin_groups'], array());
	}
	/**
	 * Fetch an instance
	 * @param int|string $instance (ID) or string (NAME) $group
	 * @throws \Exception
	 * @return array
	 */
	 public static function Instance($instance){
		
		if(is_int($instance)){
			# get by instance id
			$where = 'iid';
					
		} elseif(is_string($instance)) {
			# get by instance name
			$where = 'name';
		}
		
		if(is_null($instance)){
			
			throw new \Exception('Application API '.__NAMESPACE__.'::Instance() only accepts a Instance ID (int) or Name (string).');
		}
		
		return self::db()->get('iid, state, pid, name, options, content FROM '.\claydb::$tables['plugin_instances'].' WHERE '.$where.' = ?', array($instance), '0,1');
	}
	/**
	 * Get Instance Group Assignments
	 * @param int $instance
	 */
	public static function InstanceGroups($instance){
		
		$groups = \claydb::$tables['plugin_groups'];
		$memb = \claydb::$tables['plugin_hooks'];
		
		return self::db()->get($memb.'.hid, '.$memb.'.pid, '.$memb.'.iid, '.$memb.'.appid, '.$memb.'.itemtype, '.$memb.'.itemid, '.$memb.'.pos, '.$memb.'.field, '.$memb.'.options, '.$groups.'.gid, '.$groups.'.name FROM '.$memb.' LEFT JOIN '.$groups.' ON ( '.$groups.'.gid = '.$memb.'.itemtype ) WHERE iid = ?', array($instance));
	}
	/**
	 * Fetch all Plugin Instances
	 * @throws \Exception
	 * @return array
	 */
	 public static function Instances(){
		
		return self::db()->get('iid, state, pid, name, options, content FROM '.\claydb::$tables['plugin_instances'], array());
	}
}