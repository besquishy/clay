<?php
/**
 * Plugin Instance Setup
 * 
 * @copyright (C) 2014-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

/**
 * Plugins Module Instance Setup
 */
class InstanceSetup {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	
	/**
	 * Register Plugin Instance
	 *
	 * @param array $args
	 * @return method
	 * @sample array('instance', array(bool 'update', int 'pid', int 'state', string 'name', array 'options', array 'content']))
	 */
	 public function Register( $args ){
		
		if( !empty( $args[1]['update'] )){
			
			return $this->Update( $args[1] );
		}
		
		return $this->Add( $args[1] );
	}
	/**
		* Unregister Plugin Instance
		* @param array $args
		*/
	public function Unregister( $args ){
		# An individual Hook is being removed
		return $this->Delete( $args[1] );
	}
	/**
		* Private Method for Creating a Instance
		* @param array $args
		* @throws \Exception
		*/
	private function Add( $args ){
		# Set Defaults
		$args = $this->setDefaultArgs( $args );
		
		return self::db()->add(\claydb::$tables['plugin_instances']." (state, pid, name, options, content) VALUES (?,?,?,?,?)", array($args['state'], $args['pid'], $args['name'], $args['options'], $args['content']));
	}
	/**
	* Private Method for Updating a Instance
	* @param array $args
	*/
	private function Update( $args ){
		# Set Defaults
		$args = $this->setDefaultArgs( $args );
		
		return self::db()->update(\claydb::$tables['plugin_instances']." SET state = ?, pid = ?, name = ?, options = ?, content = ? WHERE iid = ?", array( $args['state'], $args['pid'], $args['name'], $args['options'], $args['content'], $args['iid']), 1);
	}
	/**
	* Private Method for Removing A Instance and All Instances from a Group
	* @param array $args
	*/
	private function Delete( $args ){
		
		# Delete Hooks to this Instance
		$hooks = self::db()->delete( \claydb::$tables['plugin_hooks']." WHERE iid = ?",array( $args['iid'] ));
		# Delete Instance
		$group = self::db()->delete( \claydb::$tables['plugin_instances']." WHERE iid = ?",array( $args['iid'] ), 1);

		return true;
	}
	/**
	 * Remove all Instances of a Plugin (Plugin or Application Removal)
	 * @param array $args
	 */
	public function Remove( $args ){

		# Delete Instances
		$group = self::db()->delete( \claydb::$tables['plugin_instances']." WHERE pid = ?",array( $args['pid'] ));

		return true;
	}
	/**
	 * Private Method for Setting Default Values to Args
	 * @param array $args
	 */
	private function setDefaultArgs( $args ){

		if( empty( $args['state'] )) $args['state'] = NULL;
		if(!empty( $args['options'] ) AND is_array( $args['options'] ))	$args['options'] = serialize( $args['options'] );
		if( empty( $args['options'] )) $args['options'] = NULL;

		return $args;
	}
}