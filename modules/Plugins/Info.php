<?php
/**
 * Plugins Module
 *
 * @copyright (C) 2013-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Application Plugins',
			'name' => 'Plugins',
			'version' => '1.1.0',
			'date' => 'October 14, 2017',
			'description' => 'Provides functionality to create Application Hooks and plugins',
			'class' => 'Applications',
			'category' => 'Tools',
			'depends' => array(),
			'core' => TRUE,
			'setup' => TRUE,
			);
?>