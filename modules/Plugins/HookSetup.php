<?php
/**
 * Plugin Hooks Setup
 * 
 * @copyright (C) 2014-2015 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module\Plugins;

/**
 * Plugins Module Hook Setup
 */
class HookSetup {
	
	/*
	 * Use Trait ClayDB Connection - imports self::db()
	 */
	use \ClayDB\Connection;
	
	/**
	 * Register Hook or Update Hook
	 *
	 * @param array $args array([type: string 'app', string 'name', string 'descr'])
	 * @return method
	 */
	public function Register( $args ){

		if( !empty( $args['app'] )){
			# Optional 'app' index is present, set 'appid'
			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}

		if( !empty( $args['plugin'] )){
			# Optional 'plugin' index is present, set 'pid'
			$plugin = \Clay\Module::Object( 'Plugins' )->Get()->Plugin( $args['plugin']['type'], $args['plugin']['app'], $args['plugin']['name'] );
			$args['pid'] = $plugin['pid'];
		}
		
		if( !empty( $args['update'] )){
			
			return $this->Update( $args );
		}
		
		return $this->Add( $args );
	}
	/**
	 * Unregister Hook(s)
	 * @param array $hook
	 */
	public function Unregister( $hook ){

		if( is_array( $hook )){
			# An Application is being uninstalled
			return $this->Remove( $hook );
		
		} else {
			# An individual Hook is being removed
			return $this->Delete( $hook );
		}
	}
	/**
	 * Private Method for Creating a Hook
	 * @param array $args
	 * @throws \Exception
	 */
	private function Add( $args ){
		
		$args = $this->setDefaultArgs( $args );

		return self::db()->add( \claydb::$tables['plugin_hooks']." (pid, iid, appid, itemtype, itemid, pos, field, options) VALUES (?,?,?,?,?,?,?,?)",
				array( $args['pid'],$args['iid'],$args['appid'],$args['itemtype'],$args['itemid'],$args['pos'],$args['field'], $args['options'] )
		);
	}
	/**
	 * Private Method for Updating a Hook
	 * @param array $args
	 */
	private function Update( $args ){

		$args = $this->setDefaultArgs( $args );
		
		return self::db()->update( \claydb::$tables['plugin_hooks']." SET itemtype = ?, itemid = ?, pos = ?, field = ?, options = ? WHERE hid = ?",
				array( $args['itemtype'],$args['itemid'],$args['pos'],$args['field'], $args['options'], $args['hid'] ),1
		);
	}
	/**
	 * Private Method for Deleting a Hook
	 * @param int $hook
	 */
	private function Delete( $hook ){
		# Remove a specific Hook
		return self::db()->delete( \claydb::$tables['plugin_hooks']." WHERE hid = ?",array( $hook ), 1);
	}
	/**
	 * Public Method for Removing an Application
	 * @param int $plugin
	 */
	public function Remove( $plugin ){
		
		if( !empty( $plugin['app']) OR !empty( $plugin['appid'] )){
			
			if( empty( $plugin['appid']) ){
				# App ID is required
				$appid = !is_int( $plugin['app'] ) ? \Clay\Application::getID( $plugin['app'] ) : $plugin['app'];
			} else {

				$appid = $plugin['appid'];
			}
			# Remove all application Hooks
			return self::db()->delete( \claydb::$tables['plugin_hooks']." WHERE appid = ?",array( $appid ));
		
		} else {
			
			# Remove all plugin Hooks
			return self::db()->delete( \claydb::$tables['plugin_hooks']." WHERE pid = ?",array( $plugin['pid'] ));
		}
	}
	/**
	 * Private Method for Setting Default Values to Args
	 * @param array $args
	 */
	private function setDefaultArgs( $args ){

		if( empty( $args['iid'])) $args['iid'] = NULL;
		if( empty( $args['itemtype'] )) $args['itemtype'] = NULL;
		if( empty( $args['itemid'] )) $args['itemid'] = NULL;
		if( empty( $args['pos'] )) $args['pos'] = NULL;
		if( empty( $args['field'] )) $args['field'] = NULL;
		if( empty( $args['options'] )) $args['options'] = NULL;

		return $args;
	}
}