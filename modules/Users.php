<?php
/**
 * Users Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace Clay\Module;

\Library('ClayDB/Connection');

/**
 * Users Module
 */
class Users {
	
	/**
	 * Import ClayDB Connection Method - self::db()
	 */
	use \ClayDB\Connection;
	/**
	 * Construct
	 */
	private function __construct(){
		
		# Static API
	}
	
	/**
	 * Add a New User
	 * @param array $args
	 * @return array
	 */
	public static function Add($args){
		
		return self::db()->add(\claydb::$tables['users']." (state, uname, email,passw) VALUES (?,?,?,?)", array($args['state'],$args['username'],$args['email'],$args['password']));
	}
	
	/**
	 * Update User
	 * Update All or Parts of User Information
	 * @param array $args
	 * @sample $args['username']
	 * @sample $args['password']
	 * @sample $args['email']
	 * @sample $args['state']
	 */
	public static function Update($args){
			
		if(!empty($args['username'])){
			
			self::db()->update(\claydb::$tables['users']." SET uname = ? WHERE uid = ?", array($args['username'], $args['userid']), 1);
		}
		if(!empty($args['password'])){			
			
			self::db()->update(\claydb::$tables['users']." SET passw = ? WHERE uid = ?", array($args['password'], $args['userid']), 1);
		}
		if(!empty($args['email'])){
			
			self::db()->update(\claydb::$tables['users']." SET email = ? WHERE uid = ?", array($args['email'], $args['userid']), 1);
		}
		if(!empty($args['state'])){
			
			self::db()->update(\claydb::$tables['users']." SET state = ? WHERE uid = ?", array($args['state'], $args['userid']), 1);
		}
		
		return TRUE;
	}
	/**
	 * Remove a User
	 * Optionally Remove all User's Content
	 * @param int $uid
	 * @param boolean $content
	 */
	public static function Remove($uid,$content=FALSE){
		
		
	}
	
	/**
	 * Activate User Account
	 * Account Status Change
	 * @param int $uid
	 */
	public static function Activate($uid){
		
		
	}
	
	/**
	 * Deactivate User Account
	 * Account Statuc Change
	 * @param int $uid
	 */
	public static function Deactivate($uid){
		
		
	}
	
	/**
	 * Get a User's Setting
	 * @param int $uid
	 * @param string $setting
	 * @param string $default
	 * @return string
	 */
	public static function Setting($uid, $setting, $default=false){
		
		$setting = self::db()->get("value FROM ".\claydb::$tables['user_settings']." WHERE uid = ? AND name = ?", array($uid,$setting), '0,1');
		
		return !empty($setting['value']) ? $setting['value'] : $default;
	}
	
	/**
	 * Set a User's Setting
	 * @param int $uid
	 * @param string $setting
	 * @param string $value
	 */
	public static function Set($uid, $setting, $value){
		
		if(!self::db()->update(\claydb::$tables['user_settings']." SET value = ? WHERE uid = ? AND name = ?", array($value,$uid,$setting), 1)){
			
			self::db()->add(\claydb::$tables['user_settings']." (uid, name, value) VALUES (?,?,?)", array($uid,$setting,$value));
		}
	}
	
	/**
	 * Remove a User's Setting
	 */
	public static function deleteSetting(){
		
		
	}
	
	/**
	 * Get a User ID from Username
	 * @param string $username
	 * @return int
	 */
	public static function getId($username){
		
		$user = self::db()->get("uid FROM ".\claydb::$tables['users']." WHERE uname = ?", array($username), '0,1');
		
		return $user['uid'];
	}
	
	/**
	 * Get a Username from User ID
	 * @param int $uid
	 * @return string
	 */
	public static function getUser($uid){
	
		$user = self::db()->get("uname FROM ".\claydb::$tables['users']." WHERE uid = ?", array($uid), '0,1');
		
		return $user['uname'];
	}

	/**
	 * Get a User's Email Address
	 * @param int $uid
	 * @return string
	 */
	public static function Email($uid){
		
		if(!\Clay\Data\Cache::isCached( 'u.'.$uid, 'email' )) {
		
			$user = self::db()->get("email FROM ".\claydb::$tables['users']." WHERE uid = ?", array($uid), '0,1');
			$email = trim( $user['email'] );
			$email = strtolower( $email );
			\Clay\Data\Cache::Set( 'u.'.$uid, 'email', $email );
			return $email;
		} else {

			return \Clay\Data\Cache::Get( 'u.'.$uid, 'email' );
		}
	}

	/**
	 * Get a User's Gravatar
	 * @param int $uid
	 * @return string
	 */
	public static function Gravatar($uid){
			
		return 'https://www.gravatar.com/avatar/'.md5( self::Email( $uid )); 
	}
	
	/**
	 * Audit Users for Email and Username
	 * @param array $args
	 * @return array
	 */
	public static function Audit($args){
		
		$data = array();

		if(!empty($args['email'])){
			
			$email = self::db()->get("uid FROM ".\claydb::$tables['users']." WHERE email = ?", array($args['email']), '0,1');
			
			if(!empty($email)){
				
				$data['email'] = TRUE;
			}
		}
		
		if(!empty($args['uname'])){
			
			$uname = self::db()->get("uid FROM ".\claydb::$tables['users']." WHERE uname = ?", array($args['uname']), '0,1');
			
			if(!empty($uname)){
				
				$data['uname'] = TRUE;
			}
		}
		
		return $data;		
	}
	
	/**
	 * Get User Information
	 * @param int $uid
	 * @return array
	 */
	public static function Get($uid){
		
		return self::db()->get("uid, state, uname, email FROM ".\claydb::$tables['users']." WHERE uid = ?", array($uid), '0,1');
	}
	
	/**
	 * Get All Users
	 * @return array
	 */
	public static function getALL(){
		
		return self::db()->get("uid, state, uname, email FROM ".\claydb::$tables['users']);
	}	
}