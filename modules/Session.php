<?php
/**
* Session Module
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace Clay\Module;

\Library('ClayDB/Connection');

/**
 * Session Module
 */
class Session {

	/**
	 * Static Class
	 */
	private function __construct(){

	}

	/**
	 * Session Start
	 * @TODO - add param string $session - Use a custom session handler
	 */
	public static function Start() {
		# Start the handler
		$handler = new SessionHandler;
		session_set_save_handler($handler, true);

		#@TODO Determine need for the shutdown (recommended for PHP 5.4)
		#\register_shutdown_function('session_write_close');

		\session_start();
		\session_regenerate_id(true);
	}
}

/**
 * Session handler
 */
class SessionHandler implements \SessionHandlerInterface {

	/*
	 * Import ClayDB Connection Method - self::db()
	 */
	use \ClayDB\Connection;

	/**
	 * Session Data
	 * @var array
	 */
	public $sdata = NULL;

	/**
	 * Session Open
	 * @param string $save_path
	 * @param string $session_name
	 * @return boolean
	 */
	public function open($save_path, $session_name) {
		return true;
	}

	/**
	 * Session Close
	 */
	public function close() {
		return true;
	}

	/**
	 * Session Database Reader
	 * @param string $id
	 */
	public function read($id) {

		$data = self::db()->get("sdata, exp FROM ".\claydb::$tables['sessions']." WHERE usid = ?", array($id), '0,1');

		# Tell write() there is an existing Session to update.
		if(!empty($data)) $this->sdata = 1;

		if($data['exp'] < time()){

			$data['sdata'] = (string) ''; # Basically logs the expired Session's user out.
		}

		return !empty($data['sdata']) ? (string) $data['sdata'] : (string) '';
	}

	/**
	 * Session Database Writer
	 * @param string $id
	 * @param serialized array $data
	 */
	public function write($id, $data) {

		# Set Session Expiration
		$timestamp = time() + \ini_get( "session.gc_maxlifetime" );

		# See read()
		if(!is_null($this->sdata)){

			self::db()->update(\claydb::$tables['sessions']." SET sdata = ?, exp = ? WHERE usid = ?", array($data,$timestamp,$id), 1);

		} else {

			self::db()->add(\claydb::$tables['sessions']." (usid, sdata, exp) VALUES (?,?,?)", array($id,$data,$timestamp));
		}

		return true;
	}

	/**
	 * Session Destroy
	 * @param string $id
	 */
	public function destroy($id) {

		$stmt = self::db()->delete(\claydb::$tables['sessions']." WHERE usid = ?",array($id));
		$this->sdata = NULL; # Login/Logout, new usid so new write()

		return true;
	}

	/**
	 * Session Garbage Collection
	 * @param integer $maxlifetime
	 */
	public function gc($maxlifetime) {

		$stmt = self::db()->delete(\claydb::$tables['sessions']." WHERE exp <= ?",array(time()));

		return true;
	}
}
