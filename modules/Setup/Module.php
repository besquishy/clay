<?php
/**
* Setup Module
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace Clay\Module\Setup;

\Library('Clay/Module/BaseSetup');

\Library('ClayDB/Connection');

/**
 * Module Setup Submodule
 * @author David
 *
 */
class Module extends \Clay\Module\BaseSetup {

	/**
	 * Import $this->db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;

	/**
	 * Register the Installed Module
	 */
	public function Register(){
		
		$info = $this->Info();
		$this->version = $info['version'];
		
		if($this->Update()){
			
			return true;
			
		} else {
			
			return $this->db()->add(\claydb::$tables['modules']." (state, version, name) VALUES (?, ?, ?)",array($this->state,$this->version,$this->module));
		}
	}

	/**
	 * Get Module Version
	 */
	public function Version(){
		
		$mod = $this->db()->get('version FROM '.\claydb::$tables['modules'].' WHERE name = ?', array($this->module), '0,1');
		return $mod['version'];
	}

	/**
	 * Update Registered Module
	 */
	public function Update(){
		
		if(is_numeric($this->module)) { goto modid; }

		modname:
			$query = \claydb::$tables['modules'].' SET version = ?, state = ? WHERE name = ?';
			goto end;

		modid:
			$query = \claydb::$tables['modules'].' SET version = ?, state = ? WHERE modid = ?';

		end:
			return $this->db()->update($query, array($this->version, $this->state, $this->module), 1);
	}

	/**
	 * Removed Registered Module
	 * @param string $mod Module name
	 */
	public function Remove($mod=NULL){
		
		# Holding off on going this route, we'll just update the state to "removed" instead.
		/*if(is_numeric($this->module)) { goto modid; }

		modname:
			$query = \claydb::$tables['modules'].' WHERE name = ?';
			goto end;

		modid:
			$query = \claydb::$tables['modules'].' WHERE appid = ?';

		end:
			return self::db()->delete($query, array($this->module), 1);*/

		# Force Removal of the Module from the Database
		if( !empty( $mod )){

			return self::db()->delete(\claydb::$tables['modules'].' WHERE name = ?', array( $mod ), 1);

		} else {
		# Keep the Module in the Database
			# Set Version to NULL and State to Not Installed/Removed
			$this->version = NULL;
			$this->state = 4;
			return $this->Update();
		}
	}
}