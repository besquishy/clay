<?php
/**
* Setup Module
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace Clay\Module\Setup;

\Library('Clay/Application/Setup');

\Library('ClayDB/Connection');

/**
 * Application Setup Submodule
 * @author David
 *
 */
class Application extends \Clay\Application\Setup {

	/**
	 * Import $this->db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;

	/**
	 * Register the Installed Application
	 */
	public function Register(){
		
		$info = $this->Info();
		$this->version = $info['version'];
		
		if($this->Update()){
			
			return true;
			
		} else {
			
			return $this->db()->add(\claydb::$tables['apps']." (state, version, name) VALUES (?, ?, ?)",array($this->state,$this->version,$this->application));
		}
	}

	/**
	 * Get Application Version
	 */
	public function Version(){
		
		$app = $this->db()->get('version FROM '.\claydb::$tables['apps'].' WHERE name = ?', array($this->application), '0,1');
		return $app['version'];
	}

	/**
	 * Update Registered Application
	 */
	public function Update(){
		
		if(is_numeric($this->application)) { goto appid; }

		appname:
		$query = \claydb::$tables['apps'].' SET version = ?, state = ? WHERE name = ?';
		goto end;

		appid:
		$query = \claydb::$tables['apps'].' SET version = ?, state = ? WHERE appid = ?';

		end:
		return $this->db()->update($query, array($this->version, $this->state, $this->application), 1);
	}

	/**
	 * Removed Registered Application
	 * @param string $app Application name
	 */
	public function Remove($app=NULL){
		
		# Holding off on going this route, we'll just update the state to "removed" instead.
		/*if(is_numeric($this->application)) { goto appid; }

		appname:
		$query = \claydb::$tables['apps'].' WHERE name = ?';
		goto end;

		appid:
		$query = \claydb::$tables['apps'].' WHERE appid = ?';

		end:
		return self::db()->delete($query, array($this->application), 1);*/
		if( !empty( $app )){

			return self::db()->delete(\claydb::$tables['apps'].' WHERE name = ?', array($app), 1);
		
		} else {

			$this->state = 4;
			return $this->Update();
		}
	}
}