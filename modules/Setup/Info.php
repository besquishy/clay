<?php
/**
 * Setup Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Component Setup Manager',
			'name' => 'Setup',
			'version' => '1.0',
			'date' => 'September 10, 2017',
			'description' => 'Provides functionality for installing, upgrading, and removing components, such as Applications and Modules.',
			'class' => 'System',
			'category' => 'Setup',
			'depends' => array(),
			'core' => TRUE,
			'setup' => TRUE,
			);
?>