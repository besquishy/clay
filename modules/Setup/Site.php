<?php
/**
 * Setup Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module\Setup;

/**
 * Setup Module Site Setup
 * 
 * @todo Implement this in Clay Installer
 */
class Site {
	/**
	 * Installer Configuration Folder
	 * @var string
	 */
	public $installer = 'sites/installer/';
	
	/**
	 * Fetch data about a site installation
	 * @param string $site
	 * @FIXME: BUG - this should return 'site', not the entire configurations array.
	 */
	public function site($site) {
		# @FIXME: Should be change to (requires review):
		# $sdata = self::siteConfig('installer','configurations');
		# return $sdata[$site];
		
		return \Clay::Config('sites/installer/configurations');
	}
	
	/**
	 * Set Site Configuration Info (Package & version) in Installer Configurations data file.
	 * @param string $site - configuration name, ie. 'default'
	 * @param array $data - package and version data
	 */
	public function setSite($site,$data) {
		$sdata = $this->siteConfig('installer','configurations');
		$data = array_merge($sdata,$data);
		return $this->setSiteConfig('installer','configurations',$data);
	}
	
	/**
	 * Upgrade a Site's package version (info only).
	 * @param string $site
	 * @param string $version
	 * @return boolean
	 */
	public function upgrade($site,$version) {
		$sdata = $this->siteConfig('installer','configurations');
		# Make sure the new version is newer.
		if($sdata[$site]['version'] < $version){
			# set the new version in the data array
			$sdata[$site]['version'] = $version;
			# save the updated data
			return $this->setSiteConfig('installer','configurations',$sdata);
		}
		# returns NULL otherwise
	}
	
	/**
	 * Fetch data from a site specific configuration file
	 * @param string $site
	 * @param string $config
	 * @return array or false
	 */
	public function siteConfig($site,$config) {
		return \Clay::Config('sites/'.$site.'/'.$config);
	}
	
	/**
	 * Set data in a site specific configuration file
	 * @param string $site
	 * @param string $config
	 * @param array $data
	 * @return boolean
	 */
	public function setSiteConfig($site,$config,$data) {
		return \Clay::setConfig('sites/'.$site.'/'.$config,$data);
	}
	/**
	 * Security
	 * @param array $args
	 */
	public function security($args){
		if(file_exists(\clay\CFG_PATH.static::$dir.'sentry.php')){
			$conf = \clay::config(static::$dir.'sentry');
			if(!empty($conf['token'])) return true;
	    }
	    return false;
	}
	/**
	 * Authenticate
	 */
	public function authenticate(){
		if(file_exists(\clay\CFG_PATH.static::$dir.'sentry.php')){
			$conf = \clay::config(static::$dir.'sentry');
			If(!empty($_SESSION['csi']) && $_SESSION['csi'] == $conf['token']) return true;
		}
		return false;
	}
	/**
	 * Initiated
	 * 
	 * Checks if Clay Installer has been set up
	 */
	public function initiated(){
		if(file_exists(\clay\CFG_PATH.static::$dir.'sentry.php')){
			return true;
	    }
	    return false;
	}
}