<?php
/**
 * Setup Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module\Setup;

\Library('ClayDB','Connection');
/**
 * Setup Module Setup Class
 */
class Setup {
	/**
	 * Import ClayDB Trait
	 */
	use \ClayDB\Connection;
	/**
	 * Construct
	 */
	public function __construct(){
		
		# Use Manager Module here to control setup
		
	}
	/**
	 * Install
	 */
	public static function Install(){
		
		$dbconn = self::db();
		$datadict = $dbconn->datadict();
		
		$tables = array('apps' => array('table' => 'apps'),
	    				'app_settings' => array('table' => 'app_settings'),
						'modules' => array('table' => 'modules'),
	    				);
	    $datadict->registerTables($tables);
	    \claydb::tables('flush');
	    
	    # Applications table
	    $apps = array();
	    	    
	    $apps['column'] = array('appid' => array('type' => 'ID'),
	    						'state' => array('type' => 'TINYINT', 'size' => '1', 'null' => FALSE),
	    						'version' => array('type' => 'VARCHAR', 'size' => '10','null' => FALSE),
	    						'name' => array('type' => 'VARCHAR', 'size' => '20', 'null' => FALSE));
	    
	    $apps['index'] = array('name' => array('type' => 'UNIQUE', 'index' => 'name'));
	    
	    $datadict->createTable(\claydb::$tables['apps'],$apps);
	    
		# Application Settings table
		$settings = array();
	    
	    $settings['column'] = array('appsid' => array('type' => 'ID'),
	    							'appid' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE, 'null' =>  FALSE),
	    							'name' => array('type' => 'STRING', 'null' => FALSE),
	    							'value' => array('type' => 'TEXT'));
	    
	    $settings['index'] = array('name' => array('type' => 'UNIQUE', 'index' => 'appid,name'));
	    
	    $datadict->createTable(\claydb::$tables['app_settings'],$settings);
	    
	     # Modules table
	    $mods = array();
	    	    
	    $mods['column'] = array('modid' => array('type' => 'ID'),
	    						'state' => array('type' => 'TINYINT', 'size' => '1', 'null' => FALSE),
	    						'version' => array('type' => 'VARCHAR', 'size' => '10','null' => FALSE),
	    						'name' => array('type' => 'VARCHAR', 'size' => '20', 'null' => FALSE));
	    
	    $mods['index'] = array('name' => array('type' => 'UNIQUE', 'index' => 'name'));
	    
	    $datadict->createTable(\claydb::$tables['modules'],$mods);
	    
	    return TRUE;		
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){
		
		switch($version){
			default:
				break;
		}
		
		return TRUE;
	}
	/**
	 * Delete
	 */
	public static function Delete(){
		
	}
}