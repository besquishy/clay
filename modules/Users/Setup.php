<?php
/**
 * Users Module
 *
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */

namespace Clay\Module\Users;

/**
 * Users Module Setup 
 */
class Setup {
	/**
	 * Construct
	 */
	public function __constructor(){
		
		# @todo Add way to determine installation/upgrade requirements and any dependencies
	}
	/**
	 * Install
	 */
	public static function Install(){
		
		\Library('ClayDB');
		$dbconn = \claydb::connect();
		$datadict = $dbconn->datadict();
		$tables = array('users' => array('table' => 'users'),
	    				'user_settings' => array('table' => 'user_settings'));
		$datadict->registerTables($tables);
		\claydb::tables('flush');
		
		 # Users Table
	    $users = array();
	    $users['column'] = array('uid' => array('type' => 'ID'),
	    						'state' => array('type' => 'TINYINT', 'size' => '1'),
	   							'uname' => array('type' => 'VARCHAR', 'size' => '254', 'null' => FALSE),
	    						'email' => array('type' => 'VARCHAR', 'size' => '254', 'null' => FALSE),
	    						'passw' => array('type' => 'VARCHAR', 'size' => '254', 'null' => FALSE));
	    
	    $users['index'] = array('user' => array('type' => 'UNIQUE', 'index' => 'uname'),
	    						'email' => array('type' => 'UNIQUE', 'index' => 'email'));
	    
	    $datadict->createTable(\claydb::$tables['users'],$users);
	        
	    # Users' Settings Table
	    $settings['column'] = array('usid' => array('type' => 'ID'),
	    							'uid' => array('type' => 'INT','size' => '11', 'unsigned' => TRUE),
	    							'name' => array('type' => 'STRING', 'null' => FALSE),
	    							'value' => array('type' => 'TEXT'));
	    
	    $settings['index'] = array('user_setting' => array('type' => 'UNIQUE', 'index' => 'uid,name'));
	    
	    $datadict->createTable(\claydb::$tables['user_settings'],$settings);

	    return TRUE;
		
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){
		
		$dbconn = \claydb::connect();
		$datadict = $dbconn->datadict();
		
		switch($version){
			# 1.1 - Change value column from VARCHAR to TEXT
			case $version <= '1.0':
				$settings = array();
				$settings[] = array('modifyColumn', array('name' => 'value', 'type' => 'TEXT'));
				$datadict->alterTable(\ClayDB::$tables['user_settings'], $settings);
				break;
			# No Upgrade Path Specified
			default:
				
				break;
		}
		
		return TRUE;
	}
	/**
	 * Delete
	 */
	public static function Delete(){
		
		# Core Modules cannot be removed
	}
}