[0] #toc "Table of Contents"
[1] #intro "Clay Framework Introduction"
[2] #theory "Clay Framework Theory of Operation"
[3] #boot "Boot Loader"
[4] #config "Configuration"
[5] #libraries "Libraries"
[6] #library-anatomy "Anatomy of a Library"
[7] #core "Clay Core"
[8] #claydb "ClayDB"
[9] #claydb-adapters "ClayDB Adapters"
[10] #claydb-datadictionaries "ClayDB Datadictionaries"
[11] #developer-guide "Clay Developer Guide"
[12] #application-guide "Building an Application using Clay Framework"
[13] #claydb-guide "ClayDB Guide"


# Clay Framework Documentation

This is an overview of the Clay Framework (CF). We are in the process of creating the CF documentation, so this will not
be all inclusive. This is a general guide to CF. More in-depth guides are in work and will fill in any lack of depth
provided here.

<a name="toc"> </a>

## Table of Contents

* [Introduction](#intro)
* [Theory](#theory)
* [The Boot Process](#boot)
* [Configuration](#config)
* [Libraries](#libraries)
    * [Anatomy of a Library](#library-anatomy)
    * [Clay Core](#core)
* [ClayDB](#claydb)
    * [Adapters](#claydb-adapters)
    * [DataDictionaries](#claydb-datadictionaries)
    * Usage Examples
*    [Clay Framework Guides](#guides)
     *    Developer Guide
     *    Building an Application Using Clay Framework
     *    ClayDB Guide
     *    Clay Framework Reference Guide
    
<a name="intro" /> </a>

### Introduction    
    
The Clay Framework (CF) is a PHP 5.3+ application development framework, born from the need to have a framework that is light-weight and scaleable. Many of the initial concepts were based on [Xaraya](http://xaraya.com), but over the years it has evolved into something much different. Xaraya is an all-inclusive framework and very powerful. Originally called simply Clay, CF was split off from the larger code-base that was called Clay, to pursue the original goals I set out to do. By doing so, CF broke away from the bulky nature of Xaraya and moved toward a pure framework that must be built upon. It should be noted that Clay nor CF used code from Xaraya's core, but was built using the same philosophies as Xaraya.

The Clay Framework is a simple set of [Libraries](Libraries) that work together to form the basis for a powerful, dynamic platform. Much of the platform functionality belongs to the platform, CF simply provides the tools to deploy the functionality. CF provides Database Abstraction, through ClayDB, using a wrapper for the PDO extensions. CF also provides configuration file handling, a boot loader, and data handling, amongst other things.

Read about CF's [Table of Contents](#toc) of Operations for more in-depth details or how CF works.

<a name="theory"> </a>

### Theory of Operations

The Clay Framework (CF) is built on the theory that a framework should simply be a starting point and a means to build upon a standard set of libraries. It is intended to provide the minimum amount of code required to implement an application or application platform. It provides a non-traditional MVC pattern for development, implementing: an application controller object; a templating system that allows application templates, page templates, and themes; and a database abstraction layer. 

The Clay Framework's template system is designed to put the Theme's Page Template in charge of what gets displayed, but delegates authority to the Application as well. When an Application is loaded, it is able to provide the HTML page title, specify it's own style sheets and javascripts, declare a Template for Application data output, and declare which Theme and/or Page Template is used. This combination of delegation and flexibility provides the basis for a dynamic template system.

CF is rather small and, as previously noted, simply a starting point. The Application class is Abstract and will likely always be extended by a platform implemented with CF. This Application Library provides basic Application support, beginning with a couple of procedural functions to call an Application Component and Application Libraries. The library also provides an Abstract \clay\application\object class that serves as the Application Controller. This Object class acts as the parent object to Applications, which may also inherit methods from its Application Platform.

Go back to [Table of Contents](#toc).

<a name="boot"> </a>

### Boot Loader

CF uses PHP file-based configuration files or an Array passed to the Boot Loader to initialize the framework. Configuration data handling and creation is provided in the \clay class. If the Boot Loader doesn't receive an Array, it uses a named system to load a specified configuration file. The default configuration name in CF is 'default'. 

The Boot Loader, called \clay::bootstrap in the code, uses the configuration information it is given and determines whether it should use the \clay\core class to Initialize and Output or if the current Configuration has specified its own Initialization and Output methods. Initialization defines constants to be used system-wide, while Output determines what Application should be loaded, which then helps determine the Theme and Page Template to display. From there the current Theme and Application are in control.

Example /html/index.php:

`Example goes here`

Example /data/sites/default/config.php:
`Example goes here`

Process Explained:
`Explanation here`

Go back to [Table of Contents](#toc).

<a name="config"> </a>

### Configuration

Clay Framework (CF) uses a name-based configuration system to store and retrieve configuration data. Configuration files are stored in the /data folder, structured by the Configuration Name. This system allows any number of entry points into the system, originating with a (string) 'name' or (array) ['conf'] => 'name' passed to \clay::bootstrap(). Configuration names are stored in the /data/sites folder, the default is 'default', so the configuration file for 'default' would be /data/sites/default/config.php. 

The Clay Installer can be used to generate configuration files or they can be created manually. The required data contained in the file depends on the platform using CF, but there are a few the CF can use to enable file structure flexibility.

`Example config.php goes here`

Go back to [Table of Contents](#toc).

<a name="libraries"> </a>

### Libraries

The Clay Framework (CF) is a set of Libraries. Libraries are sets of files containing functions and classes that are intended to perform a specific purpose. Libraries are grouped and segregated to easily identify which library is used to handle certain tasks. In CF, Libraries have very few requirements: a leading parent file located in /libraries; any child libraries are contained in /libraries/[library]; the library's children should utilize a namespace scheme starting with the name of the library. 

Go Back to the [Table of Contents](Home) or move on to [Anatomy of a Library](Library-Anatomy).

<a name="library-anatomy"> </a>

#### Anatomy of a Library

There are not many strict requirements for Libraries in Clay Framework (CF). A Library simply has to reside in the folder assigned to \clay\LIB_PATH. If it is in the root of the \clay\LIB_PATH folder, it resides in the Global namespace. Any libraries beneath it should reside in a namespace similar to the file structure.

Example: mylibrary

\clay\LIB_PATH . 'mylibrary';

Namespace: GLOBAL

Example: \mylibrary\tools

\clay\LIB_PATH . 'mylibrary/tools';

Namespace: mylibrary\tools

Note: The current code base allows the folder names to be different from the namespace. This can be confusing and prevents the use of __autoload(). There is a Bug Report to change all folder names to match namespaces.

Libraries can be any type of class, including interfaces. They can be skeleton classes to be extended. They can extend other Libraries or act as wrappers.

**Libraries are not intended to have output, unless written specifically for that purpose.**

Go Back to the [Table of Contents](Home) or move on to [Clay Core](Clay-Core).

<a name="core"> </a>

#### Clay Core

The Clay Core is a static class in the Clay library that serves as an example initialization for the Clay Framework (CF). Clay Core is located in /libraries/clay/core.php. The Core class is responsible for Initialization, which is defining some Constants, and then the final Output. That sounds like an overly simplistic explanation, but that is exactly what the Core class does. CF uses the output() method to determine what is output, so that single method is where all of the real magic happens.

Clay Core or \clay\core is loaded by the \clay class in \clay::init() and/or \clay::output(). \clay uses Static methods to allow an extending library to override it's methods, which means \clay\core could be excluded altogether by the extending library. \clay\core is only used if the extending library doesn't implement its own init() and/or output() methods. 

Go Back to the [Table of Contents](Home) or move on to [ClayDB](ClayDB).

<a name="claydb"> </a>

### ClayDB

ClayDB is a library separate from \clay, but included in the Clay Framework. It is a Database Abstraction Layer, acting as a wrapper class to the PDO extension, and also acts as a DataDictionary for table management. The library allows Adapters and DataDictionaries to be used with different database systems. ClayDB attempts allow 1-line queries and data handling from within a library or application.

Adapters provide DB specific requirements for connection, querying, etc, to keep ClayDB's foot print as small as possible. DataDictionaries provide DB specific commands and data types and provide methods for creating, deleting, altering, and cataloging DB tables.

Go Back to the [Table of Contents](Home) or move on to [ClayDB Adapters](ClayDB-Adapters).

<a name="claydb-adapters"> </a>

#### ClayDB Adapters

Go Back to the [Table of Contents](Home) or move on to [ClayDB DataDictionaries](ClayDB-DataDictionaries).

<a name="claydb-datadictionaries"> </a>

#### ClayDB DataDictionaries

Go Back to the [Table of Contents](Home) or move on to the [Developer Guide](Dev).