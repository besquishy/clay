[0] #toc "Table of Contents"
[1] #intro "Clay Framework Introduction"
[2] #install "Clay Installer"
[3] #config "Custom Configuration"
[4] #dash "Clay Dashboard"
[5] #libraries "Libraries"
[6] #library-anatomy "Anatomy of a Library"
[7] #core "Clay Core"
[8] #claydb "ClayDB"
[9] #claydb-adapters "ClayDB Adapters"
[10] #claydb-datadictionaries "ClayDB Datadictionaries"
[11] #developer-guide "Clay Developer Guide"
[12] #application-guide "Building an Application using Clay Framework"
[13] #claydb-guide "ClayDB Guide"


# Clay Framework Documentation

This is an overview of the Clay Framework (CF). We are in the process of creating the CF documentation, so this will not
be all inclusive. This is a general guide to CF. More in-depth guides are in work and will fill in any lack of depth
provided here.

<a name="toc"> </a>

## Table of Contents

* [Introduction](#intro)
* [Theory](#theory)
* [The Boot Process](#boot)
* [Configuration](#config)
* [Libraries](#libraries)
    * [Anatomy of a Library](#library-anatomy)
    * [Clay Core](#core)
* [ClayDB](#claydb)
    * [Adapters](#claydb-adapters)
    * [DataDictionaries](#claydb-datadictionaries)
    * Usage Examples
*    [Clay Framework Guides](#guides)
     *    Developer Guide
     *    Building an Application Using Clay Framework
     *    ClayDB Guide
     *    Clay Framework Reference Guide
    
<a name="intro" /> </a>

## Introduction    
    
Clay is a PHP 5.4+ application development platform built on top of the Clay Framework. It started out as an example content management system to display the abilities of the Clay Framework and became the focal point of the project.

Read more about the Clay Framework.

<a name="install"> </a>

## Getting Started ##

Three ways to get Clay:

1. Download Clay from [https://drive.google.com/folderview?id=0B-s8BlKebPhqTmdMdjNXS3lXaVk&usp=sharing](https://drive.google.com/folderview?id=0B-s8BlKebPhqTmdMdjNXS3lXaVk&usp=sharing "Google Drive")
2. Use Git to clone Clay from the github repo: `git://github.com/clay/clay.git`
3. Download the Master branch from `http://github.com/clay/clay`

Upload the files to your server, as necessary.

Clay is designed to only have one folder below the web root, so your domain name should be pointed to the `/html` folder. If you are testing Clay without a domain, you will have to access the Clay-powered website by pointing to the `/html` folder in the URL (ie. `http://localhost/clay/html`).

Create a `clay/data` folder and make it writable by the web server.

- Generally chmod 700 is good.

> **Note**: `clay/data` is above the web root (`clay/html`) and will not be accessible by a browser if the domain is pointing to the intended web root.

## Installation ##

Point your browser to `http://[clay site]/install.php`

If you did not create the `data` folder or did not make it writable, you will be told to check it.

Setup the Clay Installer.

- The Installer has it's own security built-in, so it never has to be removed after installation.
- Enter a Passcode and a Pass Key
- Enter the Security Question and Answer. This allows you to enter the Installer if you ever forget the Passcode.
- All of the data entered here is saved a configuration file and information is encrypted.

Once the Installer is setup, you will be taken to the Installer Overview, which provides several options. 

> The Clay Installer allows multiple installations of Clay and other packages built to use the installer.

Click the Databases or Manage System Databases link.

Add information about your database server(s). 

> ClayDB uses a selector type database approach, which allows connections to multiple database servers and databases. System databases are universally available to all web site installations, but have to be designated as a Site database to be used by a specific site.

If you are on the `Databases` page, click the `Installations` link. If you are on the Overview page, click the `Manage Packages / Web Sites` link.

> This part of the Installer displays packages that are available to install. Simply enter an Installation name and click one of the Packages to select it for installation. The primary site installation will need to be named `default`. Choose `Clay x.x` to install Clay.

This step prepares the file system for a package installation. Enter the site configuration name and select which package to install, then click `Create`.

Once created, the installation will display beneath the `Installations` header. Click `Setup` next to your new installation to complete the installation process.

> As previously mentioned, an installation has to specify a database connection to use. This is done using named configurations and more specific information than is used for System Databases.

> The primary database for an installation has to be named `default`. 

Once you are on the Setup page, add a Site Database, remember the name has to be `default` for the site's primary database. Choose your desired System Database from the dropdown.

Many of the paths used by Clay are configurable. If you change folder names or paths, ensure you change the Path options to match.

Enter information to create the Site Administrator. Click `Install`.

Point your browser to `http://[clay site]/index.php` and view your new Clay-powered web site.

<a name="config"> </a>

<a name="dash"> </a>

## Clay Dashboard

> **Note:** Clay is theme-based, so every template and style sheet can be overridden by a theme. The location and description of links or icons can change depending on the theme being used.

The Dashboard is the central administrative area for Clay. It is designed to use AJAX as much as possible and will generally open as an overlay above the current page. To access the Dashboard, click the Login link, generally located on the top toolbar. Log in using the administrator account you created during Clay setup.

Once logged in, the Tool Bar will change and the Login link should be replaced by an icon or link to the Dashboard. Click the Dashboard icon or link to access the Dashboard. The Dashboard will display menus offered by Applications that are currently installed.

### System Application

The System application is the primary core application of Clay and provides a way to adjust settings that are used system-wide. 

*	Site Settings
	*	Used to adjust settings to determine the default Theme, default Application, Site Name, etc.
*	Server Info
	*	Built-in phpinfo(), for displaying specifics about your PHP and hosting environment

### Applications

	