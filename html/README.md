# Clay

## Document Root (default)

/html is the default folder used as the Document Root for Clay. 

The web server should be configured to point to this folder as the top level of a domain or subdomain. 

Clay is designed so writeable folders and files are above this folder in the file hierarchy, which is a security precaution.

### Files & Folders

The Document Root will typically have an index.php and an install.php. .htaccess files and other scripts may reside here as well. These files are accessed directly in the browser.

The Document Root will contain folders to files that require browser access, such as stylesheets (.css) and javascript (.js), which are accessed by an HTTP request in a browser.

* Applications Folder
	* /html/applications
	* Contains Clay applications, segregated by individual folders

* Installer Folder
	* /html/installer
	* Contains Clay Installer Packages, segregated by individual folders

* Themes Folder
	* /html/themes
	* Contains Clay themes, segregated by individual folders
	* Contains ClaySS CSS Framework - .sass folder
	