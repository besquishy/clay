<?php
\Library('Clay/Styles');
//\clay\styles::addApplication('common','system/styles.css');
\clay\styles::addTheme('ctx-2','main.css');
\Library('Clay/Scripts');

?>
<!DOCTYPE html>
<html lang="en" class="u-bgcolor-dark">
<!-- default.tpl -->
  <head>
    <title><?php $this->pageTitle() ?></title>
    <?php
	\clay\styles::css();
	\clay\scripts::js('head');
	?>
  </head>
  <body class="u-bgcolor-container">
    <header class="c-header">
      <h1><?= $this->siteName; ?></h1>
    </header>

    <div class="o-grid o-grid--equal-height">
	    <?php if(!empty($menu)){ ?>
			<div class="o-grid__col u-1/6@sm  u-mb-x3">
				<aside class="c-sidebar">
					<?= $this->template(array('application' => 'common','template' => 'menu', 'data' => $menu))?>
				</aside>
			</div>
			<div class="o-grid__col u-5/6@sm  u-mb-x3">
				<main class="u-2/2@sm u-pr@sm">
					<?= $this->template('main'); ?>
				</main>
			</div>
		  <?php } else { ?>
		  <div class="o-grid__col u-2/2@sm u-mb-x3">
				<main class="u-2/2@sm u-pr@sm">
		      <?= $this->template('main'); ?>
				</main>
		  </div>
		  <?php } ?>
    </div>
    <footer class="o-grid__col u-6/6@sm u-pl@sm">
      <h6><?= clay::name.' '.clay::version.' Build '.clay::build . ' "'.clay::cname.'"'; ?></h6>
    </footer>
    <?php \clay\scripts::js('body'); ?>
  </body>
</html>