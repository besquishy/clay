<!-- Begin dashapp.tpl -->
<?php \Library('Clay/Styles'); \Library('Clay/Scripts');?>

	<?php 
	\clay\styles::css();
	\clay\scripts::js('head');
	?>
	
	<?php 
	$this->template('main');
	\clay\scripts::js('body'); 
	?>
<!-- End dashapp.tpl -->	