<script type="text/javascript">
	$('document').ready(function(){
		$('.dashclose').hide();
		$("body").delegate('.dashopen','click' ,function(){
			$('.dashclose').hide();
			$.ajax({
				//url: this.href + "&pageName=app",
				url: "<?= \clay\application::url('dashboard');?>" + "&pageName=app",
				cache: false,
				success: function(html){
				$("#dashmenu").empty();
				$("#dashmenu").append(html).hide();
				$("#dashmenu").show();
				}
			});
			$('.dashopen').hide();
			$('.dashclose').show();
			return false;
		});
		$("body").delegate('.dashlink','click' ,function(){
			$('.dashopen').hide();
			$.ajax({
				url: this.href + "&pageName=dashapp",
				cache: false,
				success: function(html){
					$("#dashbox").empty();
					$("#dashbox").append(html).hide();
					$("#dashbox").show();
					$('input.dashEdit').hide();
				}
			});
			$('#dashmenu').hide();
			$('.dashback').show();
			$('.dashclose').show();
			$('html,body').animate({scrollTop:$('#dashbar').offset().top}, 500);
			return false;
		});
		$("body").delegate('.dashbutton','click' ,function(){
			$('.dashopen').hide();
			$.ajax({
				url: this.value + "&pageName=app",
				cache: false,
				success: function(html){
					$("#dashbox").empty();
					$("#dashbox").append(html).hide();
					$("#dashbox").show();
					$('input.dashEdit').hide();
				}
			});
			$('#dashmenu').hide();
			$('.dashback').show();
			$('.dashclose').show();
			return false;
		});
		$('.dashclose').click(function(){
			$('.dashclose').hide();
			$('.dashback').hide();
			$('.dashopen').show();
			$("#dashmenu").empty().hide();
			$("#dashbox").empty().hide();
			//$('body').removeClass('dashbox_bg')
			return false;
		});
		$('.dashback').click(function(){
			$('.dashback').hide();
			$("#dashbox").empty().hide();
			$("#dashmenu").show();
			return false;
		});
		
		$("body").delegate("input.dashSubmit", "click", function(event) {
			$('input.dashSubmit').toggle();
        	$('span.loading').show();
        	$('input.dashEdit').toggle();
        	var formid;
        	if(typeof $(this).attr('form') !== 'undefined'){
            	formid = 'form#' + $(this).attr('form');
        	} else {
            	formid = 'form#dashForm';
        	}
        	$(formid).prepend('<input type="hidden" name="dashForm" value="true" />');
        	$.ajax({
        		type:'POST',
				url: $(formid).attr('action') + "&pageName=app",
				data: $(formid).serialize(),
				cache: false,
				success: function(html){
					//var alertdata = $('form#dashForm').attr('action');
					//alert();
				//$('#show-time').show().showtime();
				}
				//return false;
			}).done(function(){
				$.ajax({
					//url: this.href + "&pageName=app",
					url: "<?= \clay\application::url('dashboard','main','messages');?>" + "&pageName=app",
					cache: false,
					success: function(html){
					$('#dashMessageBox').show().append(html);
					//$('#show-time').show().showtime();
					}
				})});
        	$('span.loading').hide();
        	return false;
		});
		
		
		$('body').delegate("input.dashEdit", "click", function(event) {
        	$('input.dashEdit').toggle();
        	$('input.dashSubmit').toggle();
        });

		
	    $("[rel=tooltip]").tooltip();
	
	});
	
</script>


	<div id="dashbar" class="navbar navbar-inverse navbar-static-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="index.php"><?= $this->siteName; ?></a>
          <div class="nav-collapse collapse">
            <ul class="nav pull-right">
            <?php if($user->isMember){ ?>
                        
           		<li class="dropdown">
           			<a class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> <strong><?= strtoupper($user->info['uname']); ?></strong> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><img style="text-align:center;" src="<?= $user->gravatar.'?s=175'; ?>" /></li>
							<li><a class="dashlink" href="<?= \clay\application::url('users','settings') ?>">Account Settings</a></li>
							<li class="divider"></li>
							<li><a href="<?= \clay\application::url('users','main','logout') ?>">Log Out</a></li>
						</ul>
                </li>
						
				<li><button class="dashopen btn btn-info btn-mini" style="margin-top:9px;" title="Dashboard"><i class="icon-chevron-down"></i></button></li>
				
			<?php } else { ?>
			
				<li><a href="<?= \clay\application::url('dashboard') ?>" class="dashopen" id="login">Login or Register</a></li>
				
			<?php } ?>
				<li><button class="dashclose btn btn-danger btn-mini" style="margin-top:9px;" title="Close Dashboard"><i class="icon-remove icon-white"></i></button></li>
			
            </ul>
            <ul class="nav">
              <li><a href="index.php">Home</a></li>
              <li><a href="<?= \clay\application::url('blog') ?>">Blog</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
	<div id="dashmenu" class="container-fluid"> </div>
	<div id="dashbox" class="container-fluid"> </div>
	<?php $this->template($messagebar);?>

