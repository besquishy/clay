<div class="block">
	<?php if(!empty($title)){ ?>
	<div class="block-title">
		<?= $title; ?>
	</div>
	<?php } ?>
	<div class="block-content">
		<?= $this->template(array('application' => $application, 'template' => $blockTemplate, 'data' => $data)); ?>
	</div>
</div>