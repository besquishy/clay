<?php
\clay\styles::addApplication('bootstrap','bootstrap.min.css');
\clay\styles::addApplication('bootstrap','bootstrap-theme.min.css');
\clay\styles::addApplication('fontawesome','font-awesome.min.css');
\clay\styles::addTheme('vision','styles.css');
\clay\scripts::addApplication('common','jquery/jquery.js', 'head');
\clay\scripts::addApplication('bootstrap','bootstrap.min.js');

$leftBlocks = $blocks->action('view',array('group' => 'left'));
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?= $site['title'] . ' :: Powered by ' . $this->siteName; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <!--
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    -->
    <?php
	\clay\styles::css();
	\clay\scripts::js('head');
  	?>
  </head>
  <body>
  <?php $this->template($toolbar); ?>
    <div class="container-fluid page-wrapper">
	      <?php if(!empty($leftBlocks)){ ?>
	      <div class="page-main">
	      		<div class="page-sidebar pull-left">
	      		<?= $this->template($leftBlocks); ?>
	      		</div>
	        	<div class="page-panel-left page-content">
	        	Site App will go here
	        	</div>
		  </div>
		  <?php } else { ?>
		  <div class="page-main">
			  	<div class="page-content">
		        	Site App will go here
				</div>
		  </div>
		  <?php } ?>
    </div><!--/.fluid-container-->
    <div class="container-fluid">
	    <footer>
	      <?= $this->siteFooter; ?>
	      <h6><?= $this->siteCopyright; ?></h6>
	    </footer>
	</div>
  	<?php \clay\scripts::js('body'); ?>
  </body>
</html>
