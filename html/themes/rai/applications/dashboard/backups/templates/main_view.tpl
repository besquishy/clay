<section class="c-app">
	<div class="c-app__head">
		<h1>Dashboard</h1>
	</div>
	<div id="dashboard" class="app-content">	
		<?php if(empty($member)){ ?>
		<div class="grid-center">
		<?php foreach($items as $dash){ ?>
			<?php \Clay\Application\Component::Template($dash); ?>
		<?php } ?>
		</div>
		<?php } else { ?>
		<div class="grid-center">			
			<?php foreach($items as $dash){ ?>	
				<div class="grid-cell grid-cell-sm dash-grid">
					<?php \Clay\Application\Component::Template($dash); ?>
				</div>
			<?php } } ?>
		</div>
	</div>
</section>