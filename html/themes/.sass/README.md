Node command when used within a Clay theme:

`node-sass main.scss main.css --output-style compressed --include-path ../../.sass`

Live reload (on file change):

`node-sass main.scss main.css --output-style compressed --include-path ../../.sass -w`
