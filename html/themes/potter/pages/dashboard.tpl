<?php
\Library('Clay/Styles'); \Library('Clay/Scripts');
\clay\styles::addApplication('fontawesome','font-awesome.min.css');
\clay\styles::addTheme('potter','main.css');
\clay\scripts::addApplication('dashboard','vue-dashboard.js','body');
$dashboard = \Clay\Application('dashboard');
$toolbar = $dashboard->action('toolbar', array('dashboard' => true));
$groups = \Clay\Application( 'plugins' );
$headerBlocks = $groups->action( 'view', array( 'group' => 'header' ));
$leftBlocks = $groups->action( 'view', array( 'group' => 'left' ));
?>
<!DOCTYPE html>
<html lang="en" class="u-bgcolor-dark">
  <head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
		<meta name="theme-color" content="#a4d3ec">

		<title><?= $this->pageTitle(); ?></title>
    <!-- Le styles -->
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <!--
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
    -->
    <?php
	\Clay\Styles::css();
	\Clay\Scripts::js('head');
  	?>
  </head>
  <body class="u-bgcolor-container">
		<header>
    <?= $this->template($headerBlocks); ?>
		<?= $this->template($toolbar); ?>
		</header>
    <div class="o-grid o-grid--equal-height">
		  <div class="o-grid__col u-mb-x3">
				<main id="dashboard" class="u-2/2@sm">
		      <?= $this->template('main'); ?>
				</main>
		  </div>
    </div>
		<div class="o-grid u-bgcolor-accent-alt u-pl@sm">
			<footer class="o-grid__col u-2/2@sm">
				<?= $this->siteFooter; ?>
				<h6><?= $this->siteCopyright; ?></h6>
			</footer>
		</div>
  	<?= \Clay\Scripts::js('body'); ?>
  </body>
</html>