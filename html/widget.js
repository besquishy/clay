$('document').ready(function(){
    $.each($('.zillow-reviews'), function(){
        $(this).html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>');
        var dataelement = this;
        if(dataelement.dataset.count !== undefined){ 
            var count = '&count='+dataelement.dataset.count;
        } else {
            var count = '';
        }
        if(dataelement.dataset.team !== undefined){ 
            var team = '&returnTeamMemberReviews='+dataelement.dataset.team;
        } else {
            var team = '';
        }
        if(dataelement.dataset.branding !== undefined){ 
            var branding = '&branding='+dataelement.dataset.branding;
        } else {
            var branding = '';
        }
        $.ajax({
            type: 'GET',
            url: 'https://daviddyess.com/?app=widgets&com=zillow&act=reviews&screenname=' + dataelement.dataset.user + count + team + branding,
            cache: false,
            success: function(data) {
                    if(data.error){
                        dataelement.innerHTML = '<p>An error has occurred</p>';
                    } else {
                    dataelement.innerHTML = data;
                }
            },
            error: function() {
                    dataelement.innerHTML = '<p>An error has occurred</p>';
            }
        });
    });
});