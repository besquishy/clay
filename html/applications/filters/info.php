<?php
/**
 * ClayCMS
 *
 * @copyright (C) 2007-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Dashboard
 */
$data = array('title' => 'Filters',
			'name' => 'filters',
			'version' => '1.0.2',
			'date' => 'September 16, 2017',
			'description' => 'Filters and transforms for data output.',
			'class' => 'System',
			'category' => 'Tools',
			'depends' => array('applications' => array('dashboard')),
			'core' => TRUE,
			);
?>