<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		<a href="<?= \clay\application::url('filters','admin');?>" class="dashlink" title="Dashboard">Filters</a> / HTML Filter Options</h1>
	</div>
	<div class="c-app__content">
		<form v-on:submit.prevent="onSubmit('htmlSettings', $event)" id="dashForm" action="<?= \clay\application::url('filters','html','update');?>" method="post">
			<table id="htmlOptions" class="c-table c-table--border">
				<tr class="c-table__row">
					<th class="c-table__head">Allowed HTML Tags</th><th class="c-table__head">Allowed Tag Attributes (comma separated list)</th><th class="c-table__head">Delete</th>	
				</tr>
			<?php foreach($vars as $element => $attributes){?>
				<tr id="option-<?= $element;?>" class="c-table__row">
					<td  class="c-table__cell" align="center">
						<input type="text" name="vars[<?= $element;?>]" value="<?= $element;?>" class="c-field" placeholder="Enter HTML Tag" /> 
					</td>
					<td class="c-table__cell">
					<?php $string = ''; 
					foreach($attributes as $attribute){ $string = !empty($string) ? $string.','.$attribute : $attribute;}?>
						<input type="text" name="vars[<?= $element;?>][]" value="<?= $string;?>" class="c-field" placeholder="No Allowed HTML Attributes" />
					</td>
					<td class="c-table__cell"><input type="checkbox" name="delete[]" class="deleteHTMLOption" value="<?= $element;?>" /></td>
				</tr>
			<?php } ?>
				<tr v:if="tags" v-for="(tag, index) in tags" class="c-table__row">
					<td class="c-table__cell">
						<input type="text" :name="addArray('new', index, 'element')" value="" class="c-field" placeholder="Enter HTML Tag" />
					</td>
					<td class="c-table__cell">
						<input type="text" :name="addArray('new', index, 'attributes')" value="" class="c-field" placeholder="No Allowed HTML Attributes" />
					</td>
				</tr>
			</table>
			<fieldset class="c-fieldset__controls">
				<div class="o-grid">
					<div class="o-grid__col u-1/3@sm">
						<button v-if="saved !== 'htmlSettings'" type="submit" class="c-button" name="submit" id="submit"><i class="fa fa-check-circle fa-lg "></i>&emsp;Save Options</button> 
						<button v-on:click="toEdit('htmlSettings', $event)" v-if="saved == 'htmlSettings'" type="button" class="c-button" name="edit" id="edit">Edit Options</button>
						<button v-on:click="tags++" type="button" class="c-button" name="newTag" id="newTag">New Tag</button>
					</div>
					<div class="o-grid__col u-1/3@sm">
						<div v-if="response == 'htmlSettings'" id="htmlSettings_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
						<div v-if="error == 'htmlSettings'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
					</div>
				</div>
			</fieldset>
		</form>	
	</div>
</section>