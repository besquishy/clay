<section class="c-app">
  <div class="c-app__head">
    <h1>Filters</h1>
  </div>
  <div class="c-app__content">
    Filters provide output data filtering, text transformations, and other content and data processing.
  </div>
</section>