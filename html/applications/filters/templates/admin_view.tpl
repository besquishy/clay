<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		Filters</h1>
	</div>
	<div class="c-app__content">
		<?php foreach($items as $item){?>
		<div class="u-bgcolor-container u-p">
			<h2><?= $item['name']?></h2>
			<p><?= $item['description'];?></p>
			<a class="c-button" href="<?= $item['URL']; ?>">Edit Options</a>
		</div>
		<?php } ?>
	</div>
</section>