<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<i class="fa fa-plug dash-icon-lg"></i> Filters
		</div>
	</div>
	<div class="c-card__content">
		<a href="<?= $admin; ?>" class="dashlink">Manage Filters</a>
	</div>
</div>