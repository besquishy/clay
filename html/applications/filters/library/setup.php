<?php
/**
* Filters Application
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\filters\library;

\Clay\Module('Privileges');

/**
 * Filters Application Setup
 */
class setup {
	/**
	 * Install
	 */
	public static function install(){
		
		\Clay\Module::Object('Privileges')->register('filters','Item','Modify');
		# Filters Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'filters', 'name' => 'filters', 'descr' => 'Manage Content and Data Filters.'));
		\Clay\Module::Object('Plugins')->Setup($dashboard);
		# Hook Filters Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'filters', 'name' => 'filters'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		# HTML Filter Configuration (data file)
		$html = array ('br' => array (0 => '',),
						'p' => array (0 => 'style',1 => 'class',2 => 'id',),
						'div' => array (0 => 'style',1 => 'class',2 => 'id',3=> 'data-url'),
		  				'strong' => array (0 => '',),
		  				'em' => array (0 => '',),
		  				'span' => array (0 => 'style',1 => 'class',2 => 'id',),
		  				'ul' => array (0 => '',),
		  				'li' => array (0 => '',),
		  				'ol' => array (0 => '',),
		  				'sub' => array (0 => '',),
		  				'sup' => array (0 => '',),
		  				'img' => array (0 => 'src',1 => 'title',2 => 'alt',3 => 'style',),
		  				'a' => array (0 => 'href',1 => 'title',2 => 'style',3 => 'name',4 => 'target',),
		  				'b' => array (0 => 'style',),);
		\installer::setSiteConfig(\installer\SITE,'html',$html);
		return TRUE;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function upgrade($version){
		
		switch($version){
			
			case ($version <= '1.0'):
				# Filters Dashboard Plugin
				$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'filters', 'name' => 'filters', 'descr' => 'Manage Content and Data Filters.'));
				\Clay\Module::Object('Plugins')->Setup($dashboard);

			case ($version <= '1.0.1'):
				# Hook Filters Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'filters', 'name' => 'filters'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );
				break;
		}
		return TRUE;
	}
	/**
	 * Delete
	 */
	public static function delete(){

		\Clay\Module::Object('Privileges')->remove('filters');
	    return TRUE;
	}
}