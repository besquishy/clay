<?php
/**
* Filters Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\filters\component;

/**
 * Filters Application Admin Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
	/**
	 * View
	 */
	public function view(){
		
		$this->pageTitle = "Filters";
		$data = array();
		
		$user =\Clay\Module::API('User','Instance');	
		if(!$user->privilege('filters','services','modify')) throw new \Exception('Access Denied.');
		
		$data['items'][] = array( 'name' => 'HTML', 'URL' => \Clay\Application::URL( 'filters', 'html', 'edit' ), 'description' => 'Filter and tidy HTML before output to the user.');

		return $data;

	}
}