<?php
/**
* Filters Application
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\filters\component;

/**
 * Filter Application HTML Component
 * @author David
 *
 */
class html extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
	/**
	 * View
	 */
	public function view(){
		
		$this->pageTitle = "Filters::HTML";
		$data = array();
		
		$user =\Clay\Module::API('User','Instance');	
		if(!$user->privilege('system','Admin','User')) throw new \Exception('Access Denied.');

		

		return $data;

	}
	/**
	 * Edit
	 * @param array $args
	 */
	public function edit($args){
	
		$this->pageTitle = "HTML Filter Options";
		$data = array();
	
		$user = \Clay\Module::API('User','Instance');
		if(!$user->privilege('system','Admin','User')) throw new \Exception('Access Denied.');
		
		$data['vars'] = \clay::config('sites/'. \clay\CFG_NAME .'/html');
		
		return $data;
	
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
	
		$this->pageTitle = "Update Filter Options";
		$data = array();
	
		$user = \Clay\Module::API('User','Instance');
		if(!$user->privilege('system','Admin','User')) throw new \Exception('Access Denied.');
		
		$vars = \clay\data\post('vars');

		foreach($vars as $tag => $attributes){
			if(!empty($attributes)){
				# The Editor converts the arrays of attributes into a string - convert it back to arrays of attributes.
				if(!is_array($attributes[0])){
					$attributes = explode(',',$attributes[0]);
				}
				$vars[$tag] = $attributes;
			}
		}
		unset($tag);
		$new = \clay\data\post('new');
		if(!empty($new)){
			foreach($new as $tag){
				if(!is_array($tag['attributes'])){
					$tag['attributes'] = explode(',',$tag['attributes']);
				}
				if(!empty($tag['element'])){
					$vars[$tag['element']] = $tag['attributes'];
				}
			}
		}
		unset($tag);
		$delete = \clay\data\post('delete');
		if(!empty($delete)){
			foreach($delete as $tag){
				unset($vars[$tag]);
			}
		}
		if(\clay::setConfig('sites/'. \clay\CFG_NAME .'/html', $vars)){
			\clay\application::api('system','message','send',"HTML Filter Options Successfully Updated!");
			return true;
		} else {
			\clay\application::api('system','message','send',"An Error Occurred!");
			return false;
		}
	
	}

}