<?php
/**
* Blog Application
*
* @copyright (C) 2007-2018 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\blog\component;

/**
 *  Blog Application Main Component
 * @author David
 */
class main extends \Clay\Application\Component {

	/**
	 * Blog Posts View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('blog','Post','View')){
			
			#throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		
		$startnum = \clay\data\get('startnum','int','int',1) - 1;
		$postsCount = \clay\application::api('blog','posts','count');
		# Items per page @todo: Make items per page a Blog setting in the Dashboard
		$perPage = 12;
		$posts = \clay\application::api('blog','posts','getAll',array('items' => $perPage, 'startnum' => $startnum));
		
		if($user->privilege('blog','Post','Create')) {
			$data['add'] = \clay\application::url('blog','posts','create');
		}
		$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'blog' );
		
		$bgclasses = array( 'primary', 'secondary', 'accent', 'accent-alt', 'info', 'danger', 'success', 'warning', 'light', 'dark');
		foreach($posts as $post){
			//if($user->privilege('blog','Post','View',$post['pid'])) {
			
				if($user->privilege('blog','Post','Update',$post['pid'])) {
					
					$post['edit'] = \clay\application::url('blog','post','edit',array('pid' => $post['pid']));
				}
				
				if($user->privilege('blog','Post','Delete',$post['pid'])) {
					
					$post['delete'] = \clay\application::url('blog','post','delete',array('pid' => $post['pid']));
				}
				$post['wrapper'] = $bgclasses[\mt_rand( 0, 9)];
				$post['title'] = \clay\data\filter('text',$post['title']);
				$post['body'] = \clay\data\filter('html',$post['body']);
				$post['url'] = \Clay\Application::URL( 'blog', 'post', 'view', array( 'pid' => $post['pid'] ));
				$post['gravatar'] =  \Clay\Module::API( 'Users', 'Gravatar', $post['uid'] );
				$post['hooks'] = !empty( $hooks['content'] ) ? $hooks['content'] : array();
				$data['posts'][] = $post;
			//}
		}
		
		if(($postsCount > 10) && !empty($perPage)){
			
			$data['pager'] = \clay\application::api('apps','pager','view',array('items' => $perPage,
																				'url' => array('blog','main','view'),
																				'count' => $postsCount,
																				'first' => 'Newest',
																				'last' => 'Oldest'));
		}
		
		$data['postsCount'] = $postsCount;
		
		return $data;
	}
}