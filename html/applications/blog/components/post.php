<?php
/**
* Blog Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\blog\component;

/**
 * Blog Application Post Component
 * @author David
 *
 */
class post extends \Clay\Application\Component {

	/**
	 * Blog Post View
	 * @param array $args
	 * @throws \Exception
	 */
	public function view($args){

		$pid = !empty($args['pid']) ? $args['pid'] : \Clay\Data\Get('pid','int','int');

		if(empty($pid)){

			throw new \Exception('Blog Post ID (pid) must be NUMERIC');
		}

		$user = \Clay\Module::API('User','Instance');

		if(!$user->privilege('blog','Post','View',"POSTID::$pid")) {

			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}

		$post = \Clay\Application::API('blog','post','get',array('pid' => $pid));
		$post['title'] = \clay\data\filter('text',$post['title']);
		$post['body'] = \clay\data\filter('html',$post['body']);
		$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'blog' );
		$post['hooks'] = !empty( $hooks ) ? $hooks['content'] : array();

		return array('post' => $post,
					 'date' => date('d M y',$post['pubdate']),
					 'user' => \Clay\Module::API('Users','Get',$post['uid']));
	}

	/**
	 * Blog Post Create
	 */
	public function create(){

		$user = \Clay\Module::API('User','Instance');

		if(!$user->auth('blog.post')){

			throw new \exception('You are not authorized to submit POST requests without using a form.');
		}

		if($user->privilege('blog','Post','Create')) {

			$args['status'] = \clay\data\post('status','int','int',1);
			$args['date'] = \clay\data\post('date','num','num',time());
			$args['userid'] = $user->id();
			$args['title'] = substr(\clay\data\post('title','string'),0,155);
			$args['body'] = \clay\data\post('body','string');

			if(!empty($args['body'])) {
				# Preserve original content for hooks
				$params = $args;
				# Load up assigned hooks
				$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'blog' );
				# Call the Transform hook in case plugins have processing
				foreach( $hooks as $tkey => $type ){

					foreach ( $type as $pkey => $plugin ){

						$proc[$tkey] = $plugin['object']->Process( 'transform', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => NULL, 'content' => $args['body'] ));
					}
				}
				# This can be expanded later to look for different types of processing
				# If an editor is hooked and supplied processed text, we'll update our text
				if( !empty( $proc['editor'] ) AND ( $proc['editor']['content'] !== $args['body'] )){
					# Update body to match processed content
					$args['body'] = $proc['editor']['content'];
				}

				$pid = \clay\application::api('blog','post','create',$args);
				# Creates the Markdown copy of the unstyled text
				# Run this after the blog post is created so we don't have to worry about duplicates
				foreach( $hooks as $type ){

					foreach ( $type as $plugin ){

						$plugin['object']->Process( 'create', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => $pid, 'content' => $params['body'] ));
					}
				}

			} else {

				\clay\application::api('system','message','send','Although a Title is optional, all blog posts must have Text.');

				return \clay\application::redirect('blog','posts','create');
			}

			\clay\application::api('system','message','send',"Blog Post (ID: ".$pid.') '.substr($args['title'],0,50).' - Created Successfully! <p>Go to <a href="'.\clay\application::url('blog','posts','manage').'" class="dashlink">Blog Manager</a>');
		}

		return \clay\application::redirect('blog');
	}

	/**
	 * Blog Post Edit
	 * @param array $args
	 * @throws \Exception
	 */
	public function edit($args){

		$this->page = 'dashboard';

		$user = \Clay\Module::API('User','Instance');

		$pid = \clay\data\get('pid','int','int',!empty($args) ? $args : '');

		if(!$user->privilege('blog','Post','Update',$pid)) throw new \Exception('Additional Privileges Are Required to View This Page.');

		$post = \clay\application::api('blog','post','get',array('pid' => $pid));

		$post['title'] = \clay\data\filter('text',$post['title']);

		$post['hooks'] = \Clay\Module::Object( 'Plugins' )->Hooks( 'blog' );

		if( !empty( $post['hooks']['editor']['markdown/markdown'] )){
			# We'll use the preformatted Markdown text instead of the processed HTML
			$body = $post['hooks']['editor']['markdown/markdown']['object']->Process( 'editor',
				array( 'app' => 'blog', 'itemtype' => NULL, 'itemid' =>$pid ));
			# Preformatted Markdown text is stored
			if( !is_null( $body['content'] )){
				# Markdown has a preformatted version of this body
				$post['body'] = $body['content'];
			# No Preformatted Markdown text is stored
			} else {
				# If Markdown is enabled, strip all tags from the body so it can be formatted and stored as Markdown -> HTML
				$post['body'] = \clay\data\filter('noTags', $post['body']);
			}
		}

		return array('post' => $post, 'form' => \clay\application::api('blog','post','input',array('data' => $post, 'authid' => $user->authId('blog.update'))));
	}

	/**
	 * Blog Post Update
	 * @param array $args
	 * @throws \Exception
	 */
	public function update($args){

		$user = \Clay\Module::API('User','Instance');

		if(!$user->auth('blog.update')){

			throw new \exception('You are not authorized to submit POST requests without using a form.');
		}

		$args['pid'] = \clay\data\post('pid','int','int');

		if(!$user->privilege('blog','Post','Update',$args['pid'])) throw new \Exception('Additional Privileges Are Required to Update This Item.');

		$args['title'] = \clay\data\post('title','string');
		$args['body'] = \clay\data\post('body','string');

		if(!empty($args['body'])) {
			# Preserve original content for hooks
			$params = $args;
			# Load up assigned hooks
			$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'blog' );
			# Call the Transform hook in case plugins have processing
			foreach( $hooks as $tkey => $type ){

				foreach ( $type as $pkey => $plugin ){

					$proc[$tkey] = $plugin['object']->Process( 'transform', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => NULL, 'content' => $args['body'] ));
				}
			}
			# This can be expanded later to look for different types of processing
			# If an editor is hooked and supplied processed text, we'll update our text
			if( !empty( $proc['editor'] ) AND ( $proc['editor']['content'] !== $args['body'] )){
				# Update body to match processed content
				$args['body'] = $proc['editor']['content'];
			}

			\clay\application::api('blog','post','update',$args);

			foreach( $hooks as $type ){

				foreach ( $type as $plugin ){

					$plugin['object']->Process( 'update', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => $params['pid'], 'content' => $params['body'] ));
				}
			}

		} else {

			\clay\application::api('system','message','send','Although a Title is optional, all blog posts must have Text.');

			return \clay\application::redirect('blog','post','edit',array('pid' => $args['pid']));
		}

		\clay\application::api('system','message','send',"Blog Post (ID: ".$args['pid'].') '.$args['title'].' - Updated Successfully! <p>Go to <a href="'.\clay\application::url('blog','posts','manage').'" class="dashlink">Blog Manager</a>');

		return \clay\application::redirect('blog');
	}

	/**
	 * Blog Post Delete
	 * @param array $args
	 * @throws \Exception
	 */
	public function delete($args){

		$this->page = 'dashboard';

		$pid = \clay\data\fetch('pid','int','int',!empty($args) ? $args : '');

		if(empty($pid)) throw new \Exception('A Post ID (int) is Required to Perform This Action.');

		$user = \Clay\Module::API('User','Instance');

		if(!$user->privilege('blog','Post','Delete',$pid)){

			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}


		$post = \clay\application::api('blog','post','get',array('pid' => $pid));

		$post['title'] = \clay\data\filter('text',$post['title']);

		if(!empty($_GET['pid']) || !empty($args)){

			if(empty($post['title'])) $post['title'] = 'untitled';

			return array('pid' => $post['pid'], 'title' => $post['title'], 'authid' => $user->authId('blog.delete'));
		}

		$this->template = NULL;

		$title = !empty($post['title']) ? $post['title'] : 'untitled';

		if(!$user->auth('blog.delete')){

			throw new \exception('You are not authorized to submit POST requests without using a form.');
		}
		# Delete Items that may have been created by Plugins (Hooks)
		# Load up assigned hooks
		/** @TODO Use the itemid here Hooks('blog', null, $pid); */
		$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'blog' );
		foreach( $hooks as $tkey => $type ){

			foreach ( $type as $pkey => $plugin ){

				$proc[$tkey] = $plugin['object']->Process( 'delete', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => $pid ));
			}
		}
		# Delete the Blog Post
		\clay\application::api('blog','post','delete',array('pid' => $pid));
		# Show notification message
		\clay\application::api('system','message','send',"Blog Post (ID: $pid) $title - Deleted Successfully! <p>Go to <a href=".'"'.\clay\application::url('blog','posts','manage').'" class="dashlink">Blog Manager</a>');

		return \clay\application::redirect('blog');
	}
}
?>
