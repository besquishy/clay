<?php
/**
* Blog Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\blog\component;

/**
 * Blog Posts
 * @author David
 */
class posts extends \Clay\Application\Component {

	/**
	 * View
	 */
	public function view(){
		
		return \Clay\Application::Redirect('blog');
	}

	/**
	 * Create
	 */
	public function create(){

		$this->page = 'dashboard';

		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('blog','Post','Create')) {

			return \Clay\Application::Redirect('blog');
		}

		$data = array();
		
		$data['form'] = \Clay\Application::API('blog','post','input', array('authid' => $user->authId('blog.post')));

		$data['hooks'] = \Clay\Module::Object( 'Plugins' )->Hooks( 'blog' );
		//$data['hooks'] = !empty( $hooks ) ? $hooks['content'] : array();
			
		return $data;
	}

	/**
	 * Manager
	 */
	public function manage(){

		$this->page = 'dashboard';
		
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('blog','Post','Manage')) {
			
			$data = array();

			$posts = \Clay\Application::API('blog','posts','getAll');
			
			if(is_array($posts)){
				
				foreach($posts as $key => $value){
					
					$posts[$key]['author'] = \Clay\Module::API('Users','getUser',$posts[$key]['uid']);
					$posts[$key]['title'] = \Clay\Data\Filter('text',$posts[$key]['title']);
				}
				
			} else {
				
				$posts = array();
			}
			
			$data['posts'] = $posts;
			return $data;
		}
		
		return \Clay\Application::Redirect('blog');
	}
}