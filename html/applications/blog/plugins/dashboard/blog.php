<?php
/**
 * Blog Dashboard Plugin
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\blog\plugin\dashboard;

/**
 * Blog Application Dashboard Plugin
 */
class blog extends \Clay\Application\Plugin {
	/**
	 * Plugin App
	 * @var string
	 */
	public $PluginApp = 'blog';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'dashboard';
	/**
	 * Plugin Name
	 * @var string
	 */
	public $Plugin = 'blog';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function view( $args ){
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('blog','Post','View')) {
			
			$data['view'] = \clay\application::url('blog','posts','view');
		}
		
		if($user->privilege('blog','Post','Create')) {
			
			$data['new'] = \clay\application::url('blog','posts','create');
		}
		
		if($user->privilege('blog','Post','Manage')) {
			
			$data['manage'] = \clay\application::url('blog','posts','manage');
		}
		
		if($user->privilege('blog','Post','Admin')) {
			
			$data['admin'] = \clay\application::url('blog','admin');
		}
		
		if(empty($data)){
			
			$data['count'] = \clay\application::api('blog','posts','count');
		}
		
		return $data;
	}
	/**
	 * Stats - Info - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function stats( $args ){
	
		
	}
	/**
	 * Display - to be displayed on an item page
	 * @param array $args
	 * @return string
	 */
	public function display( $args ){

		
	}
	/**
	 * Add Item
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		
		
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		
	}
}