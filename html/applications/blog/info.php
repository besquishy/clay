<?php
/**
 * Blog Application
 *
 * @copyright (C) 2007-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Blog',
			'name' => 'blog',
			'version' => '1.0.2',
			'date' => 'September 16, 2017',
			'description' => 'A simple blog.',
			'class' => 'User',
			'category' => 'Publishing',
			'depends' => array('applications' => array('dashboard')),
			'core' => FALSE,
			);
?>