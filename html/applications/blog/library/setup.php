<?php
/**
* Blog Application
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\blog\library;

\Clay\Module('Privileges');

/**
 * Blog Application Setup
 */
class setup {
	/**
	 * Install
	 */
	public static function install(){
		\Library('ClayDB');
		$dbconn = \claydb::connect();
	    $datadict = $dbconn->datadict();
	    $tables = array('blog_posts' => array('table' => 'blog_posts'));
	    $datadict->registerTables($tables);
	    //\claydb::tables('flush');
	    
	    $cols = array();
	    $cols['column'] = array('pid' => array('type' => 'ID'),
	    						'status' => array('type' => 'TINYINT', 'size' => '1'),
	    						'pubdate' => array('type' => 'INT', 'size' => '10', 'null' => FALSE),
	    						'uid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    						'title' => array('type' => 'VARCHAR', 'size' => '255'),
	    						'body' => array('type' => 'TEXT'));
	    
	    $cols['index'] = array('status' => array('type' => 'KEY', 'index' => 'status'),
	    						'date' => array('type' => 'KEY', 'index' => 'pubdate'),
	    						'userid' => array('type' => 'KEY', 'index' => 'uid'));
	    
	    $datadict->createTable(\claydb::$tables['blog_posts'],$cols);

		# Blog Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog', 'descr' => 'Blog Administration.'));
		\Clay\Module::Object('Plugins')->Setup($dashboard);

		# Hook Blog Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );

		\Clay\Module::Object('Privileges')->register('blog','Post','Create');
		\Clay\Module::Object('Privileges')->register('blog','Post','Update');
		\Clay\Module::Object('Privileges')->register('blog','Post','Delete');
		\Clay\Module::Object('Privileges')->register('blog','Post','View');

		\clay\application::set('blog','title',"Blog");

		return true;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function upgrade($version){

		switch($version){
			
			case ($version <= '0.1.5'):
				\clay\application::set('blog','title',"Blog");
				
			case ($version <= '1.0'):
				# Blog Dashboard Plugin
				$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog', 'descr' => 'Blog Administration.'));
				\Clay\Module::Object('Plugins')->Setup($dashboard);
			
			case ($version <= '1.0.1'):
				# Hook Blog Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );
				break;
		}
		return true;
	}
	/**
	 * Delete
	 */
	public static function delete(){
		\Library('ClayDB');
		$dbconn = \claydb::connect();
	    $datadict = $dbconn->datadict();

	    $datadict->dropTable(\claydb::$tables['blog_posts']);

	    \Clay\Module::Object('Privileges')->remove('blog');

	    return true;
	}
}
