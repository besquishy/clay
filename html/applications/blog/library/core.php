<?php
/**
 * Blog Application
 *
 * @copyright (C) 2014-2015 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\blog\library;

/**
 * Blog Application Core Library
 */
 class Core {
 	
 	/**
 	 * Import self::db() using a Trait \ClayDB\Connection
 	 */
 	use \ClayDB\Connection;
 	
 	/**
 	 * Blog Item Types (currently none)
 	 * @return array
 	 */
 	public static function ItemTypes(){
 		
 		return array();
 	}
 	/**
 	 * Blog Item Type
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function ItemType( $itemType ){
 		
 		return array();
 	}
 	/**
 	 * Blog Posts
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function Items( $itemType = NULL ){
 		# Get all Blog Posts
 		$posts = self::db()->get( 'pid, pubdate, uid, title, body FROM '.\claydb::$tables['blog_posts'].' ORDER BY pid ASC' );
 		
 		foreach( $posts as $post ){
 			# Titles are optional, make sure we have a filler
 			if( empty( $post['title'] )){
 				
 				$post['title'] = 'Untitled';
 			}
 			$items[] = array( 'id' => $post['pid'],
 						      'name' => $post['title'],
 						      'title' => $post['title'],
 						      'uid' => $post['uid'],
 							  'itemtype' => NULL,
 							  'date' => $post['pubdate'],
							   'content' => $post['body']
 							 );
 		}
 		
 		return !empty($items) ? $items : array();
 	}
 	/**
 	 * Blog Post
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Item( $args ){
 		
 		$post = self::db()->get( 'pid, pubdate, uid, title, body FROM '.\claydb::$tables['blog_posts'].' WHERE pid = ? ORDER BY pid ASC', array($args[1]), '0,1' );
 		
 		return array( 'id' => $post['pid'],
 					   'name' => $post['title'],
 					   'title' => $post['title'],
 					   'uid' => $post['uid'],
 				 	   'itemtype' => NULL,
 					   'date' => $post['pubdate'],
					   'content' => $post['body']
 					  );
 	}
 	/**
 	 * Blog Fields
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Fields( $args ){
 		
 		return array( 'uid', 'pubdate', 'title', 'body' );
	 }
	 /**
	 * Blog Labels
	 * @param string $type - itemtype or item
	 * @return array
	 */
	public static function Labels( $type ){
		
		switch( $type ){

			case 'itemtype':
				return NULL;

			case 'item':
				return 'POST';
				
			default:
			break;
		}
		return;
	}
 }