<?php
/**
* Blog Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\blog\library;

/**
 * Blog Posts API
 * @author David
 */
class posts {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All (or some) Blog Posts
	 * @param array $args
	 * @return array
	 */
	public static function getAll($args=array()){
		
		# Select a range starting number using 'startnum'
		$offset = !empty($args['startnum']) ? $args['startnum'] : '0';
		# Fetch 'items' number of Blog Posts, offset by 'startnum' if applicable
		if(!empty($args['items'])){
			
			if(!empty($args['startnum'])){
				
				$limit = $args['startnum'].','.$args['items'];
				
			} else {
				
				$limit = '0,'.$args['items'];
			}
			
		} else {
			
			$limit = '';
		}
		# Return associative array
		return self::db()->get('pid, pubdate, uid, title, body FROM '.\claydb::$tables['blog_posts'].' WHERE status = ? ORDER BY pubdate DESC',array(1),$limit);
	}
	
	/**
	 * Count Blog Posts
	 * @param array $args - inert
	 * @return int
	 */
	public static function count($args){

		$count = self::db()->get('COUNT(*) as posts FROM '.\claydb::$tables['blog_posts']);
		return $count[0]['posts'];
	}
}