<?php
/**
* Blog Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\blog\library;

/**
 * Blog Post API
 * @author David
 *
 */
class post {

	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;

	/**
	 * Create a Blog Post
	 * @param array $args
	 */
	public static function create($args){

		return self::db()->add(\claydb::$tables['blog_posts']." (status, pubdate, uid, title, body) VALUES (?,?,?,?,?)",
							array($args['status'],$args['date'],$args['userid'],$args['title'],$args['body'])
						);
	}

	/**
	 * Update a Blog Post
	 * @param array $args
	 */
	public static function update($args){

		return self::db()->update(\claydb::$tables['blog_posts']." SET title = ?, body = ? WHERE pid = ?",
								array($args['title'],$args['body'],$args['pid']),1
						);
	}

	/**
	 * Delete a Blog Post
	 * @param array $args
	 */
	public static function delete($args){

		return self::db()->delete(\claydb::$tables['blog_posts']." WHERE pid = ?",
								array($args['pid']),1
						);
	}

	/**
	 * Blog Post Input Form
	 * @param array $args
	 */
	public static function input($args){

		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;

		if(!empty($args['data']['pid'])) {

			$action = \clay\application::url('blog','post','update');

		} else {

			$action = \clay\application::url('blog','post','create');
		}

		$form->attributes(array('method' => 'POST', 'action' => $action, 'enctype' => 'multipart/form-data'));
		$form->container('set1','fieldset')->legend = !empty($args['data']['pid']) ? 'Edit Post' : 'New Blog Post';
		$title = array('id' => 'title', 'name' => 'title','title' => 'Enter a blog post title.','class' => 'input-xlarge');
		$title['value'] = !empty($args['data']['title'])? $args['data']['title'] : '';
		$form->container('set1')->property('title','text')->attributes($title);
		$form->container('set1')->property('title')->label = "Title";
		$body = array('id' => 'body', 'name' => 'body','title' => 'Enter blog post text.','class' => 'c-field', 'rows' => 15);
		$form->container('set1')->property('body','textarea')->attributes($body);
		$form->container('set1')->property('body')->content = !empty($args['data']['body'])? $args['data']['body'] : '';
		$form->container('set1')->property('body')->label = "Text";
		$authid = array('type' => 'hidden', 'name' => $args['authid']['field'], 'id' => $args['authid']['field'], 'value' => $args['authid']['id']);
		$form->container('set1')->property('authid','input')->attributes($authid);

		if(!empty($args['data']['pid'])) {

			$pid = array('type' => 'hidden', 'name' => 'pid', 'id' => 'pid', 'value' => $args['data']['pid']);
			$form->container('set1')->property('pid','input')->attributes($pid);
		}

		$form->container('footer','fieldset')->property('submit','submit')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => !empty($args['data']['pid']) ? 'Save Changes' : 'Create Entry'));

		return $form;
	}

	/**
	 * Get a Blog Post
	 * @param array $args
	 * @throws \Exception
	 */
	public static function get($args=array()){

		if(empty($args['pid'])){

			Throw new \Exception('A numeric Post ID (pid) must be supplied');
		}

		$post = self::db()->get('pid, pubdate, uid, title, body FROM '.\claydb::$tables['blog_posts'].' WHERE pid = ?',array($args['pid']),'0,1');

		return $post;
	}

	/**
	 * Get Blog Post Info
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getInfo($args=array()){

		if(empty($args['pid'])){

			Throw new \Exception('A numeric Post ID (pid) must be supplied');
		}

		$posts = self::db()->get('pubdate, uid, title FROM '.\claydb::$tables['blog_posts'].' WHERE pid = ?',array($args['pid']),'0,1');

		return $posts;
	}

	/**
	 * Get Blog Post Author's User ID
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getAuthor($args=array()){

		if(empty($args['pid'])){

			Throw new \Exception('A numeric Post ID (pid) must be supplied');
		}

		$posts = self::db()->get('uid FROM '.\claydb::$tables['blog_posts'].' WHERE pid = ?',array($args['pid']),'0,1');

		return $posts;
	}
}
