<section class="o-container">
	<style type="text/css">/*
	.post-time {
	padding:6px;vertical-align:middle;text-align:center;font-size:1.2em;width:90px;
	}
	.post-options {
	padding:6px;vertical-align:middle;text-align:center;font-size:1em;width:90px;margin-top: 3px;
	}
	.blog-post{
	padding:7px;
	}
	.post-info{
	float:left;width:90px;
	}
	*/</style>
	<div class="c-app__head">
		<h1>Blog</h1>
		<?php if(!empty($add)) { ?>
		<div class="c-button-group">
			<a class="c-button c-button--default" href="<?= $add; ?>" title="Add a New Blog Post"><i class="fa fa-plus"></i>&emsp;Add Post</a>
			<div class="c-dropdown"><a class="c-button c-button--default" href="#news">Options&emsp;<i class="fa fa-caret-down"></i></a>
				<div class="c-dropdown__item">
					<a class="dashlink" href="<?= \Clay\Application::URL('blog','posts','manage');?>"><i class="fa fa-sliders"></i>&emsp;Manage Posts</a>
					<a class="dashlink" href="<?= \Clay\Application::URL('blog','admin');?>"><i class="fa fa-cog"></i>&emsp;Settings</a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<?php //if(!empty($new)) echo '<div style="width:300px;">'.$new->template($post).'</div>'; ?>
	<div class="c-app__content">
	<?php 
	//if(!empty($add)) { echo '<a href="'.$add.'" class="dashlink">Add New Post</a>';}
	if(!empty($posts)) {
		$lastdate = NULL;
		foreach($posts as $post) {
		$date = date('d M y',$post['pubdate']);
		if($date != $lastdate) echo '<h2>'.$date.'</h2>';
		//echo '<div class="post-info"><div class="post-time accent accent-outline">'.date('g:i a',$post['pubdate']).'</div>'; if(!empty($post['edit'])) { echo '<div class="post-options accent accent-outline"><a href="'.$post['edit'].'" class="dashlink">Edit</a><br /><a href="'.$post['delete'],'" class="dashlink">Delete</a></div>'; } echo '</div>';
		?>
		<div class="post-time accent"><?= date('g:i a',$post['pubdate']); ?></div>
		<div class="accent-outline blog-post">
			<?php if(!empty($post['edit'])) { ?>
		<div class="post-options pull-right clearfix">
			<div class="dropdown">
				<div class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
					<i class="fa fa-pencil-square-o"></i>
					<span class="caret"></span>
				</div>
				<ul class="dropdown-menu" role="menu" aria-labelledby="blogPostdropdownMenu1">
					<li role="presentation"><a class="dashlink" role="menuitem" tabindex="-1" href="<?= $post['edit']; ?>"><i class="fa fa-pencil"></i>&emsp;Edit</a></li>
					<li role="presentation"><a class="dashlink" role="menuitem" tabindex="-1" href="<?= $post['delete'];?>"><i class="fa fa-trash"></i>&emsp;Delete</a></li>
				</ul>
			</div>
		</div>
		<?php } ?>
			<?php if(!empty($post['title'])){ ?>
			<h3><a href="<?= $post['url'];?>"><?= $post['title']; ?></a></h3>
			<?php } ?>
			<div class="item-text">
				<?= $post['body']; ?>
			</div>
			<a class="btn btn-primary btn-xs" href="<?= $post['url'];?>">Read</a>
			<?php if(!empty($post['hooks'])) { ?>
			<?php foreach( $post['hooks'] as $hook ){ $hook['object']->Plugin('stats', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => $post['pid'] )); }?>
			<?php } ?>
		</div>
		<br style="clear:both" />
		<?php $lastdate = $date; }
		if(!empty($pager)) $this->template($pager);
	} else { echo "No Blog posts have been created yet!"; }?>
	</div>
</section>
