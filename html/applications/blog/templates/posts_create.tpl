<section class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> /
    <a href="<?= \clay\application::url('blog','posts','manage');?>" class="dashlink" title="Manage Users">Manage Blog</a> /
    New Post</h1>
  </div>
  <div class="c-app__content blogEdit">
  <?php $form->template(); ?>
  <?= !empty($hooks['editor']) ? $hooks['editor']['markdown/markdown']['object']->Plugin( 'editor' ) : ''; ?>
  </div>
</section>