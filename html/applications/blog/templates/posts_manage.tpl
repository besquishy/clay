<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	Manage Blog Posts</h1>
	</div>
	<div class="c-app__content">
	<a class="c-button c-button--primary" href="<?= \clay\application::url('blog','posts','create');?>">Add Post</a>
	<?php if(!empty($posts)) { ?>
		<table class="c-table c-table--border">
			<tr class="c-table__row">
				<th class="c-table__head">ID</th><th class="c-table__head">Author</th><th class="c-table__head">Published</th><th class="c-table__head">Title</th><th class="c-table__head">options</th>
			</tr>
			<?php foreach($posts as $post) { ?>
			<tr class="c-table__row">
				<td class="c-table__cell"><?= $post['pid']; ?></td>
				<td class="c-table__cell"><?= $post['author'] . ' (id:' . $post['uid'] . ')'; ?></td>
				<td class="c-table__cell"><?= date('d M y g:i a',$post['pubdate']); ?></td>
				<td class="c-table__cell"><?= empty($post['title']) ? 'untitled' : $post['title']; ?></td>
				<td class="c-table__cell"><a class="dashlink" href="<?= \Clay\Application::url('blog','post','edit',array('pid' => $post['pid'])); ?>" title="Edit">Edit</a> 
					<a class="dashlink" href="<?= \Clay\Application::url('blog','post','delete',array('pid' => $post['pid'])); ?>" title="Delete">Delete</a>
				</td>
			</tr>
			<?php } ?>
		</table>
	<?php } else { echo "No Blog posts have been created yet!"; }?>
	</div>
</section>