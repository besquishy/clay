<section class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('blog','posts','manage');?>" class="dashlink" title="Manage Users">Manage Blog</a> / 
    Delete Post</h1>
  </div>
  <h2>Editor</h2>
  <div class="c-app__content blogEdit">
  <form id="blogDelete" action="<?= \clay\application::url('blog','post','delete'); ?>" method="POST">
  <p class="u-bgcolor-info u-p">Are you sure you want to delete Blog Post ID: <?= $pid; ?> - <?= $title; ?>?</p>
  <fieldset class="c-fieldset__controls">
    <input type="hidden" name="pid" id="pid" value="<?= $pid;?>" />
    <input type="hidden" name="<?= $authid['field'] ?>" id="<?= $authid['field'] ?>" value="<?= $authid['id'] ?>" />
    <input class="c-button" type="submit" name="submit" id="submit" value="Yes, Delete it." /> <a class="c-button" href="<?= \clay\application::url('blog','posts','manage'); ?>">No, Cancel.</a>
  </fieldset>
  </form>
  </div>
</section>