<div class="c-app__head">
  <h1>Blog</h1>
</div>
<div class="c-app__content">
	<div class="">
		<div class="u-color-primary">
			<?php if(!empty($post['title'])){ ?>
			<h1><?= $post['title']; ?></h1>
			<?php } ?>
			<?php $date = date('d F Y',$post['pubdate']); ?>	
			<div>Posted by <?= $user['uname'];?> on <?= $date.' at '.date('g:i a',$post['pubdate']); ?></div>
		</div>
		<div class="u-p u-color-body">
			<?= $post['body']; ?>
		</div>
	</div>
	<?php if(!empty($post['hooks'])) { ?>
	<div class="">
		<div class="">
			<?php foreach( $post['hooks'] as $hook ){ $hook['object']->Plugin('display', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => $post['pid'] )); }?>
		</div>
	</div>
	<?php } ?>
</div>