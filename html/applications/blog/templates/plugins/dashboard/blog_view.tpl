<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<i class="fa fa-child dash-icon-lg"></i> Blog
		</div>
	</div>
	<div class="c-card__content">
		<?php if(!empty($new))?><a href="<?= $new; ?>">Create a Post</a><br />
		<?php if(!empty($manage))?><a href="<?= $manage; ?>" class="dashlink">Manage Posts</a><br />
	</div>
</div>