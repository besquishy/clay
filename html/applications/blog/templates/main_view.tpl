<section  class="c-app u-2/2@sm">
	<div class="c-app__head">
		<h1>Blog</h1>
		<?php if(!empty($add)) { ?>
		<div class="c-button-group c-button--default">
			<a class="c-button c-button--sm c-button--default" href="<?= $add; ?>" title="Add a New Blog Post"><i class="fa fa-plus"></i>&emsp;Add Post</a>
			<div class="c-dropdown"><a class="c-button c-button--sm c-button--default" href="#news">Options&emsp;<i class="fa fa-caret-down"></i></a>
				<div class="c-dropdown__item">
					<a class="dashlink" href="<?= \Clay\Application::URL('blog','posts','manage');?>"><i class="fa fa-sliders"></i>&emsp;Manage Posts</a>
					<a class="dashlink" href="<?= \Clay\Application::URL('blog','admin');?>"><i class="fa fa-cog"></i>&emsp;Settings</a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="o-grid">
		<?php
		if(!empty($posts)) {
		foreach($posts as $post) { ?>
		<div class="o-grid__col u-1/3@sm">
			<div class="c-card">
				<div class="c-card__wrapper u-bgcolor-<?= $post['wrapper']; ?>">
					<div class="c-card__header">
						<div class="c-card__title c-card__title--bar-light-r">
							<span class="u-align-top u-float-right"><?= date('g:i a',$post['pubdate']); ?></span><img style="border-radius: 10%" src="<?= $post['gravatar']; ?>?s=33"><?php if(!empty($post['title'])){ ?><span class="u-pl"><a href="<?= $post['url'];?>"><?= $post['title']; ?></a></span><?php } ?>
						</div>
					</div>
					<div class="c-card__content c-card--clear">
						<?= $post['body']; ?>
					</div>
					<div class="c-card__footer">
						<span class="u-float-right u-pl u-pr u-pb u-weight-bold"><?= date('D, d M Y',$post['pubdate']); ?></span>
						<a class="c-button c-buttton--primary c-button--sm" href="<?= $post['url'];?>">Read</a>
						<?php if(!empty($post['hooks'])) { ?>
						<?php foreach( $post['hooks'] as $hook ){ $hook['object']->Plugin('stats', array('app' => 'blog', 'itemtype' => NULL, 'itemid' => $post['pid'] )); }?>
						<?php } ?>
						<?php if(!empty($post['edit'])) { ?>					
						<div class="c-dropdown c-button c-button--sm">
							<div class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
								<i class="fa fa-pencil-square-o"></i>
								<span class="fa fa-caret-down"></span>
							</div>
							<div class="c-dropdown__item" role="menu" aria-labelledby="blogPostdropdownMenu1">
								<a class="dashlink" role="menuitem" tabindex="-1" href="<?= $post['edit']; ?>"><i class="fa fa-pencil"></i>&emsp;Edit</a>
								<a class="dashlink" role="menuitem" tabindex="-1" href="<?= $post['delete'];?>"><i class="fa fa-trash"></i>&emsp;Delete</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php } } else { echo "No Blog posts have been created yet!"; } ?>
	</div>
	<?php if(!empty($pager)) $this->template($pager); ?>
</section>