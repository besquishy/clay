<?php
/**
 * Blog Post Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2012
 * @todo Add Status field in blog_posts table for use with 'PUBLIC', 'PRIVATE', 'DRAFT', etc
 */

namespace application\blog\privilege;

\Library('Clay/Application/Privilege');

/**
 * Blog Application Post Privilege
 */
class Post extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var array
	 */
	public $Request;
	
	/**
	 * Used to Validate Privilege Requests
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. POSTID::1 = array('POSTID',1)

		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
			
			case 'AUTHOR':
				
				# Fetch some info about the requested Blog Post
				$post = \Clay\Application::API('blog','post','getInfo',array('pid' => $this->Request));

				switch($scope[1]){
					
					case 'MYSELF':
						
						# Pseudo User - If assigned and User created the Post, returns TRUE
						if($post['uid'] == \Clay\Module::Object('User')->id()){
							
							return TRUE;
						}
						
						return FALSE;
						
					case $post['uid']:
						
						return TRUE;
				}
				
				return FALSE;
				
			case 'POSTID':
				
				if($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
				
			case 'PUBLIC':
				
				return TRUE;
		}
		
		return FALSE;
	}
	
	
	
	/**
	 * Used by the Users Application for Assigning Privileges to Roles
	 * @param string $privilege
	 * @param string $scope
	 */
	public function Scopes($privilege, $scope = NULL){
		# Return Privilege Masks for Building Scope
		# Cases can be: 'Create' , 'Update' , 'View' , 'Delete' , or custom method
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'AUTHOR', 'POSTID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
			
			case 'AUTHOR':
				
				$users = \Clay\Module::API('Users','getALL');
				
				# Pseudo User - allows privileges for the User that created a Post
				$users[] = array('uid' => 'MYSELF', 'name' => 'MYSELF');
				
				# do a foreach loop here to make it match POSTID below
				
				foreach($users as $user){
					
					$item['id'] = $user['uid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					$item['title'] = NULL;
					$item['name'] = !empty($user['uname']) ? $user['uname'] : $user['name'];
					
					$option['items'][] = $item;
				}
				unset($users);
				$option['type'] = 'Users';
				return $option;
				
			case 'POSTID':
				
				$posts = \Clay\Application::API('blog','posts','getAll');
				
				foreach($posts as $post){
					
					$item['id'] = $post['pid'];
					$item['date'] = $post['pubdate'];
					$item['uid'] = $post['uid'];
					$item['title'] = !empty($post['title']) ? $post['title'] : 'Untitled';
					$item['name'] = NULL;
					
					$option['items'][] = $item;
				}
				
				unset($posts);
				$option['type'] = 'Blog Posts';
				return $option;
		}
	}
}