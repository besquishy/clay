<?php
/**
* Plugins Application
*
* @copyright (C) 2013-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Hook Component
 * @author David
 *
 */
class hooks extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
	
	/**
	 * Hook Overview
	 * @return array $data
	 */
	public function view(){
		
		$data = array();		
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' )){
				
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$appid = \Clay\Data\Get( 'appid', 'Int', 'Int' );
		$app = \Clay\Application::Info( 'application', \Clay\Application::getName( $appid ));
		$app['id'] = $appid;
		$data['app'] = $app;
		
		$Plugins = \Clay\Module::Object('Plugins');
		
		$data['hooks'] = $Plugins->Get()->Hooks( $appid );
		//die(var_dump($data['hooks']));
		return $data;
	}
}