<?php
/**
* Clay
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Group Component
 */
class group extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
	
	/*
	 * Import self::db()
	 */
	use \ClayDB\Connection;
	/**
	 * Add Group Action
	 * @throws \Exception
	 * @return array
	 */
	 public function add(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		
		$form = new \ClayDO\DataObject\Form;
		
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('plugins','group','create'), 'id' => 'dashForm'));
		# Begin Fieldset for 'New Role'
		$addInfo = $form->container('groupInfo','fieldset');
		$addInfo->legend = 'New Plugins Group';
		# Role Name input
		$groupname = array('type' => 'text', 'name' => 'name', 'id' => 'name', 'title' => 'Group Name');
		$addInfo->property('group','input')->attributes($groupname);
		$addInfo->property('group')->label = 'Group Name';
		# Role State input
		$state = array('name' => 'state', 'id' => 'state','title' => 'Group State');
		$addInfo->property('state','select')->attributes($state);
		$addInfo->property('state')->label = 'Group State';
		$addInfo->property('state')->options[] = array('value' => 1, 'content' => 'Active');
		$addInfo->property('state')->options[] = array('value' => 2, 'content' => 'Disabled');
		# Fieldset for Submit button
		$form->container('footer','fieldset')->property('submit','submit')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Create Plugins Group', 'class' => 'c-button c-button--primary dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	}
	
	/**
	 * Create a Group Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function create(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$name = \Clay\Data\Post('name','string','string');
		//$descr = \Clay\Data\Post('descr','string','string', NULL);
		$state = \Clay\Data\Post('state','int:1','int');
		
		$group[] = 'group';
		$group[] = array(	'name' => $name,
						  	'state' => $state,
						 	'options' => '' );
		\Clay\Module::Object( 'Plugins' )->Setup( $group );
		\Clay\Application::API('system','message','send',"Plugins Group: $name, Successfully Added!");

		\Clay\Application::Redirect('plugins','groups','view');
		
		return TRUE;
	}
	
	/**
	 * Edit Group Action
	 * @throws \Exception
	 * @return array
	 */
	public function edit(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$group = \Clay\Data\Get('gid','int:1','int');
		
		$data['plugingroup'] = \Clay\Module::Object( 'Plugins' )->Get()->Group( $group );
		
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		
		$form = new \ClayDO\DataObject\Form;
		
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('plugins','group','update'), 'id' => 'dashForm'));
		# Begin Fieldset for 'New Role'
		$addInfo = $form->container('groupInfo','fieldset');
		$addInfo->legend = 'Edit Plugins Group';
		# Group Name input
		$groupname = array('type' => 'text', 'name' => 'name', 'id' => 'name', 'title' => 'Group Name', 'value' => $data['plugingroup']['name']);
		$addInfo->property('group','input')->attributes($groupname);
		$addInfo->property('group')->label = 'Group Name';
		# Group State input
		$state = array('name' => 'state', 'id' => 'state','title' => 'Group State');
		$addInfo->property('state','select')->attributes($state);
		$addInfo->property('state')->label = 'Group State';
		$addInfo->property('state')->selected = $data['plugingroup']['state'];
		$addInfo->property('state')->options[] = array('value' => 1, 'content' => 'Active');
		$addInfo->property('state')->options[] = array('value' => 2, 'content' => 'Disabled');
		$addInfo->property('gid','input')->attributes(array('type' => 'hidden', 'name' => 'gid', 'id' => 'gid', 'value' => $data['plugingroup']['gid']));
		# Fieldset for Submit button
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Update Plugins Group', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;

		$data['instances'] = \Clay\Module::Object( 'Plugins' )->Get()->GroupInstancesList( $group );

		\Library('Clay/Styles');
		\clay\styles::addApplication('common','jquery-ui/jquery-ui.min.css');
		\Library('Clay/Scripts');
		# @FIXME This should use the 2nd parameter 'body', but body doesn't seem to work here
		\Clay\Scripts::addApplication('common','jquery-ui/jquery-ui.min.js');
		return $data;
	}
	
	/**
	 * Update a Group Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function update(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$gid = \Clay\Data\Post('gid','int:1','int');
		$name = \Clay\Data\Post('name','string','string');
		//$descr = \Clay\Data\Post('descr','string','string', NULL);
		$state = \Clay\Data\Post('state','int:1','int');
		
		$group[] = 'group';
		$group[] = array(	'update' => TRUE,
							'gid' => $gid,
							'name' => $name,
							'state' => $state,
							'options' => '' );
		\Clay\Module::Object( 'Plugins' )->Setup( $group );
		\Clay\Application::API('system','message','send',"Plugins Group: $name, Successfully Updated!");

		\Clay\Application::Redirect('plugins','groups','view');
		
		return TRUE;
	}
	/**
	 * Update Plugin Group's Plugin Positions
	 * POST
	 * @throws \Exception
	 * @TODO Move DB functionality to an API
	 */
	public function position(){

		$this->pageTitle = "Plugin Groups :: Plugins Administration";
		
		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('You do not have privileges assigned to view this item.');
		}
		
		$gid = \Clay\Data\Get('gid','int:1','int');
		$plugins = \Clay\Data\Post('iid');
		$appid = \Clay\Application::getID( 'plugins' );
		
		foreach($plugins as $pos => $plugin){
			
			$pos++;
			self::db()->update(\claydb::$tables['plugin_hooks']." SET pos = ? WHERE iid = ? AND appid = ? AND itemtype = ?", array($pos,$plugin,$appid,$gid), 1);
		}
		
		\Clay\Application::API('system','message','send',"Plugins order Successfully Updated!");
	}
	/**
	 * Delete Group Action
	 * @throws \Exception
	 * @return array
	 */
	public function delete(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$group = \Clay\Data\Get('gid','int:1','int');
		
		$data['plugingroup'] = \Clay\Module::Object( 'Plugins' )->Get()->Group( $group );
		
		return $data;
	}
	
	/**
	 * Remove a Group Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function remove(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$gid = \Clay\Data\Post('gid','int:1','int');
		
		$group[] = 'group';
		$group[] = array( 'gid' => $gid );
		\Clay\Module::Object( 'Plugins' )->Delete( $group );
		\Clay\Application::API('system','message','send',"Plugins Group ID: $gid, Successfully Deleted!");

		\Clay\Application::Redirect('plugins','groups','view');
		
		return TRUE;
	}
}
?>