<?php
/**
* Plugins Application
*
* @copyright (C) 2013-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Admin Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
	
	/**
	 * Plugins Dashboard Overview
	 * @return array $data
	 */
	public function view(){
		
		$data = array();		
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' )){
				
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$data['settings'] = \Clay\Application::URL('plugins', 'admin', 'settings');
		$data['plugins'] = \Clay\Application::URL('plugins', 'admin', 'plugins');
		$data['pluginTypes'] = \Clay\Application::URL('plugins', 'admin', 'plugintypes');
		$data['hooks'] = \Clay\Application::URL('plugins', 'admin', 'hooks');
		$data['instances'] = \Clay\Application::URL('plugins', 'instances', 'view');
		$data['groups'] = \Clay\Application::URL('plugins', 'groups', 'view');
		$data['help'] = \Clay\Application::URL('plugins', 'admin', 'help');
		
		return $data;
	}
	/**
	 * Plugins Dashboard Settings
	 * @return array $data
	 */
	public function settings(){
		
		$data = array();
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' )){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		return $data;
	}
	/**
	 * Plugins List
	 * @return array $data
	 */
	public function plugins(){
		
		$data = array();
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' )){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$plugins = \Clay\Module::Object('Plugins');
		$data['plugins'] = $plugins->Get()->All();
		# Add some extra info
		foreach($data['plugins'] as $key => $plugin){
			# Add the Application name and Plugin Type
			$data['plugins'][$key]['app'] = \Clay\Application::getName( $plugin['appid'] );
			$data['plugins'][$key]['type'] = $plugins->Get()->Type( $plugin['ptype'] );
		}
		
		return $data;
	}
	/**
	 * Plugin Types List
	 * @return array $data
	 */
	public function plugintypes(){
		
		$data = array();
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' )){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$data['pluginTypes'] = \Clay\Module::Object('Plugins')->Get()->Types();
		
		return $data;
	}
	/**
	 * Applications' Hooks
	 * @return array $data
	 */
	public function hooks(){
		
		$data = array();
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' ) ){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$data['applications'] = \Clay\Application::getAll( 'name' );
		# We just reuse the component action here, gets what we need
		$data['plugins'] = $this->plugins();
		
		return $data;
	}
	/**
	 * A Plugin's Hooks
	 * @param integer $pid
	 */
	public function pluginhooks($pid){
		
		$data = array();
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' ) ){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		if( empty( $pid )){
			
			$pid = \Clay\Data\Get( 'pid', 'Int', 'Int' );
		}
		
		$plugins = \Clay\Module::Object('Plugins');
		$data['hooks'] = $plugins->Get()->PluginHooks($pid);
		
		
		return $data;
	}
	/**
	 * Plugins Help
	 * @return array $data
	 */
	public function help(){
		
	}
}