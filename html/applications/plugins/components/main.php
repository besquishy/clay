<?php
/**
* Plugins Application
*
* @copyright (C) 2013-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Main Component
 */
class main extends \Clay\Application\Component {
	
    /**
	 * Group Display
	 * @param array $args
	 * @throws Exception
	 * @return array
	 */
	public function view($args){		
        
        $this->pageTitle = "Plugins Group";
        
        $data = array();
        # Check $args['gid'] --> $args['name'] --> GET['gid'] --> GET['name']
        $group = !empty($args['gid']) ? $args['gid'] : !empty($args['group']) ? $args['group'] : \Clay\Data\Get('gid','int:1','int', \Clay\Data\Get('group','string','string'));
        # Get group data
        $group = \Clay\Module::Object( 'Plugins' )->Get()->Group( $group );
        $user = \Clay\Module::API('User','Instance');
        # Privilege Check for Group View
        if(!$user->Privilege('plugins','Group','View', $group['gid'])){			
            # Fail silently if no privileged access
            return FALSE;
        }
        # Privilege Check for Group Update (edit)
        if($user->Privilege('plugins','Group','Update', $group['gid'])){			
            # Fail silently if no privileged access
            $data['edit'] = TRUE;
            
        } else {
            # No Privileges
            $data['edit'] = FALSE;
        }
        # Get group instances
        $instances = \Clay\Module::Object( 'Plugins' )->Instances( $group['gid'] );
        # If no instances are assigned we stop the operation (action call returns FALSE)
        if(empty($instances)){			
            return FALSE;
        }
        # Group options (serialized)
        if(!empty($group['options'])){
            # Group options (serialized)
            $group['options'] = unserialize($group['options']);
        }
        # Template data
        $data['group'] = $group;
        $data['groupTemplate'] = !empty($group['options']['template']) ? $group['options']['template'] : $group['name'];
        $data['instances'] = $instances;
        unset( $instances );
        return $data;
    }
}