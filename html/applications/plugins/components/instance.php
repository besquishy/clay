<?php
/**
* Clay
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Application Instances Component
 * @author David
 *
 */
class instance extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * Plugin Instances View Action
	 * Used in Dashboard for Plugins administration
	 * @throws \Exception
	 */
	public function view(){

		$this->pageTitle = "Plugin Instances :: Plugins Administration";
		
		$data = array();
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if(!$user->privilege( 'system', 'Admin', 'User' )){
			
			throw new \Exception( 'You do not have privileges assigned to view this item.' );
		}

		$pluginsObject = \Clay\Module::Object( 'Plugins' );

		$instances = $pluginsObject->Get()->Instances();
		
		foreach( $instances as $instance ){

			$instance['groups'] = $pluginsObject->Get()->InstanceGroups( $instance['iid'] );
			$instance['plugin'] = $pluginsObject->Get()->PluginByID( $instance['pid'] );
			$instance['plugin']['app'] = \Clay\Application::getName( $instance['plugin']['appid'] );
			$data['instances'][] = $instance;
		}
		unset( $pluginsObject );
		return $data;
	}
	
	/**
	 * Add Plugins Instance Action
	 * Used in Dashboard for Plugins administration
	 * @throws \Exception
	 * @return array
	 */
	public function add(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$pid = \Clay\Data\Get('pid','int:1','int');

		$pluginsObject = \Clay\Module::Object( 'Plugins' );

		$plugins = $pluginsObject->Get()->InstancePlugins();

		foreach($plugins as $plugin){
			
			$plugin['app'] = \Clay\Application::getName($plugin['appid']);
			$plugin['type'] = $pluginsObject->Get()->TypeName( $plugin['ptype'] );
			$data['plugins'][] = $plugin;
			
			if($pid == $plugin['pid']){
				
				$data['plugin'] = $plugin;
			}
		}
		
		if(!empty( $data['plugin'] )){

			$data['groups'] = $pluginsObject->Get()->Groups();
			$data['states'] = array( 1 => 'Active', 2 => 'Hidden', 3 => 'Disabled' );
			$data['instance'] = $pluginsObject->Plugin( $data['plugin']['type'], $data['plugin']['app'], $data['plugin']['name'] );
		}
		unset( $pluginsObject );
		return $data;
	}
	
	/**
	 * Create a Plugin Instance Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function create(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$pid = \Clay\Data\Post('pid','int:1','int');
		$name = \Clay\Data\Post('name','string','string');
		$title = \Clay\Data\Post('title','string','string', NULL);
		$groups = \Clay\Data\Post('group','','', NULL);
		$state = \Clay\Data\Post('state','int:1','int');
		$template = \Clay\Data\Post('template','string','string', NULL);
		$content = \Clay\Data\Post('content');		
		
		$instance = array('state' => $state,
						'gid' => $groups,
						'pid' => $pid,
						'name' => $name,						
						'options' => serialize(array('title' => $title, 'template' => $template)),
						'content' => serialize($content));
		$newInstance = array('instance', $instance);

		$iid = \Clay\Module::Object( 'Plugins' )->Setup( $newInstance );
		
		\Clay\Application::API('system','message','send',"Plugin Instance: $name, Successfully Added!");

		if( !empty( $groups )){

			$appid = \Clay\Application::getID( 'plugins' );
			foreach( $groups as $group ){
				# Hook this Instance into a Group
				$g = array( 'pid' => $pid, 'iid' =>  $iid, 'appid' => $appid, 'itemtype' => $group );
				if( \Clay\Module::Object( 'Plugins' )->Hook( $g )) {
					# Visual Confirmation
					\Clay\Application::API('system','message','send',"Plugin Instance: $name, Assigned to Group!");
				}
			}
		}
		
		\Clay\Application::Redirect( 'plugins', 'instance', 'edit', array( 'iid' => $iid ));
		
		return TRUE;
	}
	
	/**
	 * Edit Plugin Instance Action
	 * Used in Dashboard for Plugins administration
	 * @throws \Exception
	 * @return array
	 */
	public function edit(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$iid = \Clay\Data\Get('iid','int:1','int');
		
		$data['states'] = array(1 => 'Active', 2 => 'Hidden', 3 => 'Disabled');	
		$pluginObject = \Clay\Module::Object( 'Plugins' );
		$data['instance'] = $pluginObject->Get()->Instance( $iid );
		$data['plugin'] = $pluginObject->Get()->PluginByID( $data['instance']['pid'] );
		$data['plugin']['app'] = \Clay\Application::getName( $data['plugin']['appid'] );
		$data['plugin']['type'] = $pluginObject->Get()->TypeName( $data['plugin']['ptype'] );
		$data['instance']['options'] = unserialize( $data['instance']['options'] );
		$data['instance']['content'] = unserialize( $data['instance']['content'] );
		$groups = $pluginObject->Get()->Groups();
		$data['instance']['groups'] = $pluginObject->Get()->InstanceGroups( $iid );
		
		if( !empty( $groups ) && !empty( $data['instance']['groups'] )){

			foreach( $groups as $group ){

				foreach( $data['instance']['groups'] as $igroup ){

					if( $group['gid'] == $igroup['gid'] ){

						$group['selected'] = TRUE;
					}
				}
				$data['groups'][] = $group;
			}

		} else {

			$data['groups'] = $groups;
		}
		$data['instance']['object'] = $pluginObject->Plugin( $data['plugin']['type'], $data['plugin']['app'], $data['plugin']['name'] );

		return $data;
	}
	
	/**
	 * Update a Plugin Instance Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function update(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}

		$iid = \Clay\Data\Post('iid','int:1','int');
		$pid = \Clay\Data\Post('pid','int:1','int');
		$name = \Clay\Data\Post('name','string','string');
		$title = \Clay\Data\Post('title','string','string', NULL);
		$groups = \Clay\Data\Post('group','','', NULL);
		$state = \Clay\Data\Post('state','int:1','int');
		$template = \Clay\Data\Post('template','string','string', NULL);
		$content = \Clay\Data\Post('content');
		# Plugin Processing
		$pluginObject = \Clay\Module::Object( 'Plugins' );
		$obj['instance'] = $pluginObject->Get()->Instance( $iid );
		$obj['plugin'] = $pluginObject->Get()->PluginByID( $obj['instance']['pid'] );
		$obj['plugin']['app'] = \Clay\Application::getName( $obj['plugin']['appid'] );
		$obj['plugin']['type'] = $pluginObject->Get()->TypeName( $obj['plugin']['ptype'] );
		$obj['instance']['object'] = $pluginObject->Plugin( $obj['plugin']['type'], $obj['plugin']['app'], $obj['plugin']['name'] );
		$processContent = $obj['instance']['object']->Update( $content );
		# Update Content if Applicable
		if (!empty($processContent)) $content = $processContent;
		# Build Instance array
		$instance = array('update' => TRUE,
						'iid' => $iid,
						'state' => $state,
						'pid' => $pid,
						'name' => $name,						
						'options' => serialize(array('title' => $title, 'template' => $template)),
						'content' => serialize($content));
		$updateInstance = array('instance', $instance);
		# Update the Instance
		\Clay\Module::Object( 'Plugins' )->Setup( $updateInstance );
		# Get Current Instance Group Assignments (hooks)
		$igroups = \Clay\Module::Object( 'Plugins' )->Get()->InstanceGroups( $iid );
		# New Groups selected
		if( !empty( $groups )){
			# Groups are selected
			if( !empty( $igroups )){
				# Groups are already assigned
				foreach( $groups as $group => $gvalue ){
					# Match assigned groups to selected groups
					foreach( $igroups as $igroup => $ivalue ){
						# If selected and already assigned, remove them from the array
						if( $gvalue == $ivalue['gid'] ){
							# Already assigned
							unset( $groups[$group] );
							unset( $igroups[$igroup] );
						}
						# Anything remaining in $igroups should be unassiged (no longer selected)
					}
					# Anything remaining in $groups should be assigned (additional selection)
				}
			}
		}
		# Hook this Instance into a Group
		if( !empty( $groups )){
			# Get Plugins application's ID for hook's appid field
			$appid = \Clay\Application::getID( 'plugins' );
			# Additional groups were selected
			foreach( $groups as $group ){
				# Assign new group selections
				$g = array( 'pid' => $pid, 'iid' =>  $iid, 'appid' => $appid, 'itemtype' => $group );
				if( \Clay\Module::Object( 'Plugins' )->Hook( $g )) {
					# Visual Confirmation
					\Clay\Application::API( 'system','message','send',"Plugin Instance: $name, Assigned to Group!" );
				}
			}
		}
		# Unhook this Instance from a Group
		if( !empty( $igroups )){
			# Groups are no longer selected
			foreach( $igroups as $igroup ){
				# Removed unselected groups
				if( \Clay\Module::Object( 'Plugins' )->unHook( $igroup['hid'] )) {
					# Visual Confirmation
					\Clay\Application::API( 'system','message','send',"Plugin Instance: $name, Removed from ".$igroup['name'] );
				}
			}
		}
		\Clay\Application::API( 'system','message','send',"Plugin: $name, Successfully Updated!" );

		\Clay\Application::Redirect( 'plugins', 'instance', 'edit', array( 'iid' => $iid ));
		
		return TRUE;
	}
	
	/**
	 * Delete Plugin Instance Action
	 * Used in Dashboard for Plugins administration
	 * @throws \Exception
	 * @return array
	 */
	public function delete(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$iid = \Clay\Data\Get('iid','int:1','int');
		
		$data['states'] = array(1 => 'Active', 2 => 'Hidden', 3 => 'Disabled');	
		$pluginObject = \Clay\Module::Object( 'Plugins' );
		$data['instance'] = $pluginObject->Get()->Instance( $iid );
		$data['plugin'] = $pluginObject->Get()->PluginByID( $data['instance']['pid'] );
		$data['plugin']['app'] = \Clay\Application::getName( $data['plugin']['appid'] );
		$data['plugin']['type'] = $pluginObject->Get()->TypeName( $data['plugin']['ptype'] );
		$data['instance']['options'] = unserialize( $data['instance']['options'] );
		$data['instance']['content'] = unserialize( $data['instance']['content'] );
		$groups = $pluginObject->Get()->Groups();
		$data['instance']['groups'] = $pluginObject->Get()->InstanceGroups( $iid );
		
		if( !empty( $groups ) && !empty( $data['instance']['groups'] )){

			foreach( $groups as $group ){

				foreach( $data['instance']['groups'] as $igroup ){

					if( $group['gid'] == $igroup['gid'] ){

						$group['selected'] = TRUE;
					}
				}
				$data['groups'][] = $group;
			}

		} else {

			$data['groups'] = $groups;
		}
		$data['instance']['object'] = $pluginObject->Plugin( $data['plugin']['type'], $data['plugin']['app'], $data['plugin']['name'] );

		return $data;
	}
	
	/**
	 * Remove a Plugin Instance Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function remove(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$iid = \Clay\Data\Post('iid','int:1','int');

		\Clay\Module::Object( 'Plugins' )->Delete ( array( 'instance', array( 'iid' => $iid )));
		
		\Clay\Application::API('system','message','send',"Plugin Instance ID: $iid, Successfully Deleted!");
		
		return TRUE;
	}
}
?>