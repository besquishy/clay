<?php
/**
* Clay
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Application Instances Component
 * @author David
 *
 */
class instances extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * Plugin Instances View Action
	 * Used in Dashboard for Plugins administration
	 * @throws \Exception
	 */
	public function view(){

		$this->pageTitle = "Plugin Instances :: Plugins Administration";
		
		$data = array();
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if(!$user->privilege( 'system', 'Admin', 'User' )){
			
			throw new \Exception( 'You do not have privileges assigned to view this item.' );
		}
		# Plugins Module Object
		$pluginsObject = \Clay\Module::Object( 'Plugins' );
		# Get Plugin Instances
		$instances = $pluginsObject->Get()->Instances();
		# Supplement Each Instance's Data
		foreach( $instances as $instance ){
			# Groups Assigned
			$instance['groups'] = $pluginsObject->Get()->InstanceGroups( $instance['iid'] );
			# Plugin Info
			$instance['plugin'] = $pluginsObject->Get()->PluginByID( $instance['pid'] );
			$instance['plugin']['app'] = \Clay\Application::getName( $instance['plugin']['appid'] );
			# Build Output Data
			$data['instances'][] = $instance;
		}
		unset( $pluginsObject );
		return $data;
	}
}