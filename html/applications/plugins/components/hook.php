<?php
/**
* Plugins Application
*
* @copyright (C) 2013 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Hook Component
 * @author David
 *
 */
class hook extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * Hook Overview
	 * @param array $args
	 * @return array $data
	 */
	public function view( $args ){
		
		$data = array();
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' ) ){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$data['ptype'] = \Clay\Data\Get( 'ptype', 'String', 'String' );
		$data['pname'] = \Clay\Data\Get( 'plugin', 'String', 'String' );
		$data['appid'] = \Clay\Data\Get( 'appid', 'Int', 'Int' );
		
		$plugins = \Clay\Module::Object( 'Plugins' );
		
		$data['plugin'] = $plugins->Get()->Plugin( $data['ptype'], $data['pname'] );
		
		$data['hooks'] = $plugins->Get()->PluginHooks( $data['plugin']['pid'] );
				
		
		return $data;
	}
	/**
	 * Add Hook
	 */
	public function add(){
		
		$data = array();
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		# Check for privileges
		if( !$user->privilege( 'system', 'Admin', 'User' )){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		# GET appid
		$appid = \Clay\Data\Get( 'appid', 'Int', 'Int' );
		
		$app = \Clay\Application::Info( 'application', \Clay\Application::getName( $appid ));
		$app['id'] = $appid;
		
		$data['app'] = $app;
		# GET pid
		$pid = \Clay\Data\Get( 'pid', 'Int', 'Int' );
		
		$Plugins = \Clay\Module::Object( 'Plugins' );
		
		$data['plugins'] = $Plugins->Get()->All( NULL, 'appid, ptype' );
		
		foreach( $data['plugins'] as $plugin => $value ){
			
			$pluginTypes = array();
			
			$data['plugins'][$plugin]['app'] = \Clay\Application::getName( $value['appid'] );
			# Get the Plugin Type's name (once) and add it to $data
			if( !empty( $pluginTypes[$value['ptype']] )){
				
				$data['plugins'][$plugin]['type'] = $pluginTypes[$value['ptype']];
			
			} else {
				# Maybe change this to Types() and assign the key as the ptype id (above foreach)?
				$pluginType = $Plugins->Get()->Type( (int) $value['ptype'] );
				$data['plugins'][$plugin]['type'] = $pluginType['name'];
				$pluginTypes[$value['ptype']] = $pluginType['name'];
			}
			//die(var_dump($data['plugins']));
			# Set 'hook' if one was selected
			if( !empty( $pid ) AND ( $value['pid'] == $pid )){
				
				$data['hook'] = $value;
			}
		}
		# Pull app core data (wait until a plugin is selected)
		if( !empty( $pid )){
			
			$data['itemtypes'] = \Clay\Application\Core::ItemTypes( $app['name'] );
			# @TODO: Add this once we can standardize
			/*if( empty( $data['itemtypes'] )){
				
				$data['itemtypes'] = array( array( 'id' => NULL, 'name' => 'All' ));
			}*/
			//die(var_dump($data['itemtypes']));
			$itemtype = \Clay\Data\Get( 'itemtype', 'Int', 'Int', 'NULL' );
			
			if( !empty( $itemtype )){
				
				$data['itemtype'] = \Clay\Application\Core::ItemType( $app['name'], $itemtype );
				# @TODO: Same here
				/*if( empty( $data['itemtype'] )){
					
					$data['itemtype'] = array( 'id' => NULL, 'name' => 'All' );
				}*/
				
				$data['items'] = \Clay\Application\Core::Items( $app['name'] );
				//die(var_dump($data['items']));
			}
		}
		
		return $data;
	}
	/**
	 * Create a Hook
	 * 
	 * POST Request
	 */
	public function create(){
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		# Check for privileges
		if( !$user->privilege( 'system', 'Admin', 'User' )){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		# Build array for adding the Hook
		$args = array( 'appid' => \Clay\Data\Post( 'appid', 'Int', 'Int', \Clay\REQUIRED ),
					   'pid' => \Clay\Data\Post( 'pid', 'Int', 'Int', \Clay\REQUIRED ), 
					   'itemtype' => \Clay\Data\Post( 'itemtype', 'Int', 'Int', NULL ), 
					   'itemid' => \Clay\Data\Post( 'item', 'Int', 'Int', NULL ),
					   'pos' => \Clay\Data\Post( 'pos', 'Int', 'Int', NULL ),
					   'field' => \Clay\Data\Post( 'field', 'Int', 'Int', NULL ),
				       'options' => \Clay\Data\Post( 'options', 'Int', 'Int', NULL ),
					  );
		
		$Plugins = \Clay\Module::Object( 'Plugins' );
		# Create the Hook
		$hid = $Plugins->Hook( $args );
		# Send the Notification
		\Clay\Application::API('system','message','send','Hook created successfully! <p>Go to <a href="'.\Clay\Application::URL( 'plugins','hooks','view', array( 'appid' => $args['appid'] )).'" class="dashlink">Hooks Dashboard</a>');
		
		\Clay\Application::Redirect('plugins','hooks','view', array('appid' => $args['appid']));
		
		return TRUE;
	}
	/**
	 * Edit a Hook
	 */
	public function edit(){
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		# Check for privileges
		if( !$user->privilege( 'system', 'Admin', 'User' )){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$hid = \Clay\Data\Get( 'hid', 'Int', 'Int', \Clay\REQUIRED );
		
		$Plugins = \Clay\Module::Object( 'Plugins' );
		$hook = $Plugins->Get()->Hook( $hid );
		
		$app = \Clay\Application::Info( 'application', \Clay\Application::getName( $hook['appid'] ));
		$app['id'] = $hook['appid'];
		
		$data['hook'] = $hook;
		$data['app'] = $app;
		$data['itemtypes'] = \Clay\Application\Core::ItemTypes( $app['name'] );
		$data['itemtype'] = \Clay\Data\Get( 'itemtype', 'Int', 'Int', !empty( $hook['itemtype'] ) ? $hook['itemtype'] : '' );
		$data['items'] = \Clay\Application\Core::Items( $app['name'] );
		
		
		return $data;
	}
	/**
	 * Update a Hook
	 * 
	 * POST Request
	 */
	public function update(){
	
		$user = \Clay\Module::API( 'User', 'Instance' );
		# Check for privileges
		if( !$user->privilege( 'system', 'Admin', 'User' )){
	
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		# Build array for updating the Hook
		$args = array(  'hid' => \Clay\Data\Post( 'hid', 'Int', 'Int', \Clay\REQUIRED ),
						'appid' => \Clay\Data\Post( 'appid', 'Int', 'Int', \Clay\REQUIRED ),
						'itemtype' => \Clay\Data\Post( 'itemtype', 'Int', 'Int', NULL ),
						'itemid' => \Clay\Data\Post( 'item', 'Int', 'Int', NULL ),
						'pos' => \Clay\Data\Post( 'pos', 'Int', 'Int', NULL ),
						'field' => \Clay\Data\Post( 'field', 'Int', 'Int', NULL ),
						'options' => \Clay\Data\Post( 'options', 'Int', 'Int', NULL ),
						'update' => TRUE
						);
		$Plugins = \Clay\Module::Object( 'Plugins' );
		# Update the Hook
		$hid = $Plugins->Hook( $args );
		# Send the Notification
		\Clay\Application::API('system','message','send','Hook updated successfully! <p>Go to <a href="'.\Clay\Application::URL( 'plugins','hooks','view', array( 'appid' => $args['appid'] )).'" class="dashlink">Hooks Dashboard</a>');
		
		\Clay\Application::Redirect('plugins','hooks','view', array('appid' => $args['appid']));
		
		return TRUE;
	}
	/**
	 * Delete a Hook
	 */
	public function delete(){
	
		$user = \Clay\Module::API( 'User', 'Instance' );
		# Check for privileges
		if( !$user->privilege( 'system', 'Admin', 'User' )){
	
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
	
		$hid = \Clay\Data\Get( 'hid', 'Int', 'Int', \Clay\REQUIRED );
	
		$Plugins = \Clay\Module::Object( 'Plugins' );
		$hook = $Plugins->Get()->Hook( $hid );
	
		$app = \Clay\Application::Info( 'application', \Clay\Application::getName( $hook['appid'] ));
		$app['id'] = $hook['appid'];
	
		$data['hook'] = $hook;
		$data['app'] = $app;
		$data['itemtypes'] = \Clay\Application\Core::ItemTypes( $app['name'] );
		$data['itemtype'] = !empty( $hook['itemtype'] ) ? $hook['itemtype'] : '';
		$data['items'] = \Clay\Application\Core::Items( $app['name'] );
	
		return $data;
	}
	/**
	 * Remove a Hook
	 * 
	 * POST Request
	 */
	public function remove(){
		
		$user = \Clay\Module::API( 'User', 'Instance' );
		# Check for privileges
		if( !$user->privilege( 'system', 'Admin', 'User' )){
	
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		# Get the Hook ID
		$hid = \Clay\Data\Post( 'hid', 'Int', 'Int', \Clay\REQUIRED );
		$appid = \Clay\Data\Post( 'appid', 'Int', 'Int', \Clay\REQUIRED );
		
		$Plugins = \Clay\Module::Object( 'Plugins' );
		# Update the Hook
		$hid = $Plugins->unHook( $hid );
		# Send the Notification
		\Clay\Application::API('system','message','send','Hook deleted successfully! <p>Go to <a href="'.\Clay\Application::URL( 'plugins','hooks','view', array( 'appid' => $appid )).'" class="dashlink">Hooks Dashboard</a>');
		
		\Clay\Application::Redirect('plugins','hooks','view', array('appid' => $appid));
		
		return TRUE;
	}
}