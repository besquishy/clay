<?php
/**
* Clay
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\component;

/**
 * Plugins Groups Component
 */
class groups extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
	
	/**
	 * Groups View Action
	 * Dashboard
	 * @throws \Exception
	 */
	public function view(){

		$this->pageTitle = "Plugin Groups :: Plugins Administration";
		
		$data = array();
		
		$user = \Clay\Module::API( 'User','Instance' );
		
		if(!$user->privilege( 'system','Admin','User' )){
			
			throw new \Exception('You do not have privileges assigned to view this item.');
		}		

		$groups = \Clay\Module::Object( 'Plugins' )->Get()->Groups();
		
		foreach($groups as $group){
			
			$group['plugins'] = \Clay\Module::Object( 'Plugins' )->Get()->GroupInstancesList( $group['gid'] );
			$data['plugingroups'][] = $group;
		}
		
		return $data;
	}
}