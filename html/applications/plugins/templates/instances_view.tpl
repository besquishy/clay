<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('plugins','admin');?>" class="dashlink" title="Dashboard">Plugins</a> / Plugin Instances</h1>
	</div>
	<div class="c-app__content">
		<h4><a href="<?= \clay\application::url('plugins','instance','add'); ?>" title="Add a new Plugin Instance">Add a Plugin Instance</a></h4>	
		<table class="c-table c-table--border c-table--striped">
			<tr class="c-table__row">
				<th class="c-table__head">Plugin : ID</th><th class="c-table__head">Plugin Type : ID</th><th class="c-table__head">State</th><th class="c-table__head">Groups : ID</th><th class="c-table__head">Options</th>
			</tr>
		<?php if(!empty($instances)){ foreach($instances as $instance){ ?>	
			<tr class="c-table__row">
				<td class="c-table__cell"><?= $instance['name'].' : '.$instance['iid']?></td>
				<td class="c-table__cell"><?= $instance['plugin']['app'].'/'.$instance['plugin']['name'].' : '.$instance['pid']?></td>
				<td class="c-table__cell"><?= $instance['state']?></td>
				<td class="c-table__cell"><?php foreach($instance['groups'] as $group){ ?> <a href="<?= \Clay\Application::URL('plugins','group','edit', array('gid' => $group['gid'])); ?>" class="dashlink" title="Edit Group"><?= $group['name'].' : '.$group['gid']; ?></a> <?php }?></td>
				<td class="c-table__cell"><a href="<?= \Clay\Application::URL('plugins','instance','edit', array('iid' => $instance['iid'])); ?>" title="Edit Plugin">Edit</a> 
				<a href="<?= \Clay\Application::URL('plugins','instance','delete', array('iid' => $instance['iid'])); ?>" class="dashlink" title="Delete Plugin">Delete</a></td>
			</tr>
		<?php } } else { ?>
			<tr class="c-table__row">
				<td  class="c-table__cell" colspan="5">
					None
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
</section>