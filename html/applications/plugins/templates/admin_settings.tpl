<section class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('plugins', 'admin');?>" class="dashlink" title="Dashboard">Plugins Administration</a> / Settings</h1>
  </div>
  <div class="c-app__content user-admin-dash">
    No configuration options are currently available.
  </div>
</section>