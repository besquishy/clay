<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url( 'dashboard' );?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url( 'plugins', 'admin' );?>" class="dashlink" title="Dashboard">Plugins</a> / 
	<a href="<?= \clay\application::url( 'plugins', 'admin', 'pluginhooks' );?>" class="dashlink" title="Dashboard">Plugin Hooks</a> / <?= $ptype.' : '.$pname; ?></h1>
	</div>
	<div class="c-app__content user-admin-dash">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th><th>Application</th><th>Item Type</th><th>Item ID</th><th>Options</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($hooks as $hook) { ?>
				<tr>
					<td><?= $hook['hid']; ?></td>
					<td><?= \Clay\Application::getName($hook['appid']); ?></td>
					<td><?= !empty($hook['itemtype']) ? $hook['itemtype'] : 'All'; ?></td>
					<td><?= !empty($hook['itemid']) ? $hook['itemid'] : 'All'; ?></td>
					<td><a class="dashlink" href="<?= \Clay\Application::URL('plugins','hook','edit',array('hid' => $hook['hid'])); ?>" title="Edit">Edit</a> 
						<a class="dashlink" href="<?= \Clay\Application::URL('plugins','hook','delete',array('hid' => $hook['hid'])); ?>" title="Delete">Delete</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>	
	</div>
</section>