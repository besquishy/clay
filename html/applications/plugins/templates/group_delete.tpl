<style type="text/css">
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.1em; }
	#sortable li span { position: absolute; margin-left: -1.3em; }
</style>
<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('plugins','admin');?>" class="dashlink" title="Dashboard">Plugins</a> / 
	<a href="<?= \clay\application::url('plugins','groups');?>" class="dashlink" title="Dashboard">Groups</a> / 
	Delete Plugin Group</h1>
	</div>
	<div class="c-app__content">
		<form id="dashForm" action="<?= \Clay\Application::URL('plugins','group','remove'); ?>" method="post">
			<fieldset class="c-fieldset">
				<legend>Delete a Plugin Group</legend>
				<div class="u-p u-bgcolor-danger">Delete the <?= $plugingroup['name']; ?> Plugin Group?</div>
				<div>
					<input type="hidden" name="gid" id="gid" value="<?= $plugingroup['gid']; ?>" />
					<input type="submit" name="submit" id="submit" class="c-button" value="Yes, Delete Plugin Group" />
				</div>
			</fieldset>
		</form>
	</div>
</section>