<?php if($edit == TRUE) { ?><a class="c-button c-button--sm" href="<?= \Clay\Application::URL('plugins','group','edit', array('gid' => $group['gid'])); ?>" title="Edit Group">Edit <?= $group['name']; ?> Blocks</a><?php } ?>
<?php if(!empty($instances)){
foreach($instances as $instance){
    $this->template(array( 'data' => $instance, 'application' => 'plugins', 'template' => 'groups/default' ), array( 'application' => 'plugins', 'template' => 'groups/'.$groupTemplate ));
}} ?>