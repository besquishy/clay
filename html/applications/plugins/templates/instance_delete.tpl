<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('plugins','admin');?>" class="dashlink" title="Dashboard">Plugins</a> / 
	<a href="<?= \clay\application::url('plugins','instances');?>" class="dashlink" title="Dashboard">Plugin Instances</a> / 
	Delete a Plugin Instance</h1>
	</div>
	<div class="c-app__content">
		<form id="dashForm" action="<?= \Clay\Application::URL('plugins','instance','remove'); ?>" method="post">
			<fieldset class="c-fieldset">
				<legend>Delete a Plugin Instance</legend>
				<div class="c-table">
					<div class="c-table__row">
						<span class="c-table__head" style="width:100px;">Application</span>
						<span class="c-table__head" style="width:100px;">Plugin</span>
						<span class="c-table__head" style="width:250px;">Description</span>
					</div>
					<div class="c-table__row u-bgcolor-danger">
						<span class="c-table__cell" style="width:100px"><?= $plugin['app']; ?></span>
						<span class="c-table__cell" style="width:100px"><?= $plugin['name']; ?></span>
						<span class="c-table__cell" style="width:250px"><?= $plugin['descr']; ?></span>
						<input type="hidden" name="iid" id="iid" value="<?= $instance['iid']; ?>" />
					</div>
				</div>
				<div>
					<label title="Unique System Name">Name</label>
					<input class="c-field" type="text" name="name" id="name" value="<?= $instance['name']; ?>" disabled />
				</div>
				<div>
					<label title="Display Name">Title</label>
					<input class="c-field" type="text" name="title" id="title" value="<?= $instance['options']['title']; ?>" disabled />
				</div>
				<div>
					<label title="Assign to a Group">Plugin Groups</label>
					<select class="c-field" multiple name="group[]" id="group" disabled>
						<?php foreach($groups as $group){ ?>
						<option value="<?= $group['gid']; ?>" <?php if(!empty($group['selected'])) echo 'selected="selected"';?>><?= $group['name']; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<label title="Plugin Status">State</label>
					<select class="c-field" name="state" id="state" disabled>
						<?php foreach($states as $key => $state){ ?>
						<option value="<?= $key; ?>" <?php if($instance['state'] == $key) echo 'selected="selected"';?>><?= $state; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<label title="Output Template">Template</label>
					<input class="c-field" type="text" name="template" id="template" value="<?= $instance['options']['template']; ?>" disabled />
				</div>
				<?php $instance['object']->Plugin('edit', $instance['content'] ) ?>
				<div>
					<input type="submit" name="submit" id="submit" class="c-button" value="Delete Plugin Instance" />
				</div>
			</fieldset>
		</form>
	</div>
</section>