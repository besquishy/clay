<!-- Default Group Template -->
<div class="c-card">
	<?php if(!empty($options['title'])){ ?>
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<?= $options['title']; ?>
		</div>
	</div>
	<?php } ?>
	<div class="c-card__content">
		<?= $instance->Plugin( 'view', $content );?>
	</div>
</div>