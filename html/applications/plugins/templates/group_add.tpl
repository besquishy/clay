<section class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('plugins','admin');?>" class="dashlink" title="Dashboard">Plugins</a> / 
    <a href="<?= \clay\application::url('plugins','groups');?>" class="dashlink" title="Dashboard">Groups</a> / 
    Create a New Plugin Group</h1>
  </div>
  <div class="c-app__content">
    <?php $form->template(); ?>
  </div>
</section>