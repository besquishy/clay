<section class="c-app">	
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url( 'plugins', 'admin' );?>" class="dashlink" title="Dashboard">Plugins</a> / 
	<a href="<?= \clay\application::url( 'plugins', 'admin', 'hooks' );?>" class="dashlink" title="Dashboard">Hooks</a> / 
	<a class="dashlink" href="<?= \Clay\Application::URL('plugins','hooks','view',array('appid' => $app['id'])); ?>" title="Return to Hooks"><?= $app['title']; ?></a> / Edit Hook</h1> 
	</div>
	<div class="c-app__content">
		<h4><?= $app['title']; ?></h4>
		<h4><a class="dashlink" href="<?= \Clay\Application::URL('plugins','hooks','view',array('appid' => $app['id'])); ?>" title="Return to Hooks">Back to Hooks</a></h4>
		<form id="dashForm" action="<?= \Clay\Application::URL('plugins','hook','update'); ?>" method="post">
			<fieldset class="c-fieldset">
				<legend>Edit a Hook in <?= $app['title']; ?></legend>
				<div class="c-table c-table--border">
					<div class="c-table__row">
						<span class="c-table__head" style="width:100px;">Application</span>
						<span class="c-table__head" style="width:100px;">Hook Type</span>
						<span class="c-table__head" style="width:100px;">Hook</span>
						<?php if(!empty($itemtypes) OR !empty($itemtype)){ ?>
						<span class="c-table__head" style="width:200px;">Item Type</span>
						<?php }		
						if(!empty($items) OR !empty($item)){ ?>
						<span class="c-table__head" style="width:200px;"><?= !empty($itemtype) ? $itemtype['name'] : 'Items'; ?></span>
						<?php } ?>
					</div>
					<div class="c-table__row u-bgcolor-success">
						<span class="c-table__cell" style="width:100px"><?= $app['name']; ?></span>
						<span class="c-table__cell" style="width:100px"><?= $hook['plugin_type']; ?></span>
						<span class="c-table__cell" style="width:100px"><?= $hook['plugin_name']; ?></span>
						<input type="hidden" name="pid" id="pid" value="<?= $hook['pid']; ?>" />
						<?php if(!empty($itemtypes)){ ?>	
						<span class="c-table__cell" style="width:200px;">
							<select class="c-field" name="itemtype" id="itemtypes">
								<option value="">Select an Item Type</option>
								<option value="0" <?php if(!empty($itemtype) AND ($itemtype == '0')) echo 'selected="selected"';?>>ALL</option>
								<?php foreach($itemtypes as $itemType){ ?>
								<option value="<?= $itemType['id']; ?>" <?php if(!empty($itemtype) AND ($itemtype['name'] == $itemType['name'])) echo 'selected="selected"';?>><?= $itemType['name']; ?></option>
								<?php } ?>
							</select>
						</span>
						<?php }
						if(!empty($items) OR !empty($item)){ ?>
						<span class="c-table__cell" style="width:200px;">
							<select class="c-field" name="item" id="items">
								<option v-on:click="opt = false" value="">Select from <?= !empty( $itemtype ) ? $itemtype['name'] : 'Items'; ?></option>
								<option v-on:click="opt = true" value="0" <?= empty($hook['itemid']) ? 'selected="selected"' : ''; ?>>ALL</option>
								<?php foreach( $items as $Item ){ ?>
								<option v-on:click="opt = true" value="<?= !empty( $Item['id'] ) ?  $Item['id'] : $Item['name']; ?>" <?php if(!empty($hook['itemid']) AND ($hook['itemid'] == $Item['id'])) echo 'selected="selected"';?>>ID: <?= $Item['id']; ?> - <?= !empty($Item['title']) ? $Item['title'] : $Item['name']; ?><?php if(!empty($Item['uid'])){ ?> - UserID: <?= $Item['uid']; } ?></option>
								<?php } ?>
							</select>
							<fieldset v-if="opt" class="c-fieldset__controls">
								<div class="o-grid">
									<div class="o-grid__col u-3/3@sm">
										<input v-if="saved !== 'appHook'" type="submit" class="c-button" name="submit" id="submit" value="Update Hook" /> 
										<input v-on:click="toEdit('appHook', $event)" v-if="saved == 'appHook'" type="button" class="c-button" name="edit" id="edit" value="Add More" />
									</div>
									<div class="o-grid__col u-3/3@sm">
										<div v-if="response == 'appHook'" id="appHook_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
										<div v-if="error == 'appHook'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
									</div>
								</div>
							</fieldset>
						</span>
						<?php } ?>
					</div>
				</div>
				<?php if(!empty($itemtype)) {?>
				<br />
				<div class="c-table">
					<div class="c-table__row">
						<span class="c-table__head" style="width:200px;">New Hook</span>
					</div>
					<div class="c-table__row">
						<div class="c-table__cell" style="width:200px; text-align:center">
							<span id="hookDisplay"> </span>
						</div>
					</div>
				</div>
				<?php } ?>
				<input type="hidden" name="hid" id="hid" value="<?= $hook['hid']; ?>" />
				<input type="hidden" name="appid" id="appid" value="<?= $app['id']; ?>" />
			</fieldset>
		</form>
	</div>
</section>