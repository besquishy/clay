<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		<a href="<?= \clay\application::url('plugins','admin');?>" class="dashlink" title="Dashboard">Plugins</a> / Groups</h1>
	</div>
	<div class="c-app__content">
		<h4><a class="dashlink" href="<?= \clay\application::url('plugins','group','add'); ?>" title="Add a new Role">Add a Group</a></h4>	
		<table class="c-table c-table--border c-table--striped">
			<tr class="c-table__row">
				<th class="c-table__head">Group : ID</th><th class="c-table__head">State</th><th class="c-table__head">Blocks</th><th class="c-table__head">Options</th>
			</tr>
		<?php if(!empty($plugingroups)){ foreach($plugingroups as $group){ ?>	
			<tr class="c-table__row">
				<td class="c-table__cell"><?= $group['name'].' : '.$group['gid']?></td>
				<td class="c-table__cell"><?= $group['state']?></td>
				<td class="c-table__cell">
					<ul>
					<?php if(!empty($group['plugins'])){ foreach($group['plugins'] as $plugin){ ?>
						<li id="iid_<?= $plugin['iid']; ?>"><a href="<?= \Clay\Application::URL('plugins','instance','edit', array('iid' => $plugin['iid'])); ?>" class="dashlink" title="Edit Block"><?= $plugin['name']; ?></a></li>
					<?php } } ?>
					</ul>			
				</td>
				<td class="c-table__cell"><a href="<?= \Clay\Application::URL('plugins','group','edit', array('gid' => $group['gid'])); ?>" title="Edit Group">Edit</a> 
						<a href="<?= \Clay\Application::URL('plugins','group','delete', array('gid' => $group['gid'])); ?>" title="Delete Group">Delete</a></td>
			</tr>
		<?php } } else { ?>
			<tr class="c-table__row">
				<td class="c-table__cell" colspan="4">
					None
				</td>
			</tr>
		<?php } ?>
		</table>
	</div>
</section>