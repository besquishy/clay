<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> /
	<a href="<?= \clay\application::url( 'plugins', 'admin' );?>" class="dashlink" title="Dashboard">Plugins</a> /
	<a href="<?= \clay\application::url( 'plugins', 'admin', 'hooks' );?>" class="dashlink" title="Dashboard">Hooks</a> /
	<?= $app['title']; ?> / Add Hook</h1>
	</div>
	<div class="c-app__content">
		<h4><?= $app['title']; ?></h4>
		<h4><a class="dashlink" href="<?= \Clay\Application::URL('plugins','hooks','view',array('appid' => $app['id'])); ?>" title="Return to Role">Back to Hooks</a></h4>
		<form id="dashForm" action="<?= \Clay\Application::URL('plugins','hook','create'); ?>" method="post">
			<fieldset class="c-fieldset"><legend>Add a Hook to <?= $app['title']; ?></legend>
			<?php if(!empty($plugins)){ ?>
				<div class="c-table">
					<div class="c-table__row">
						<span class="c-table__head" style="width:100px;">Application</span>
						<span class="c-table__head" style="width:100px;">Hook Type</span>
						<span class="c-table__head" style="width:100px;">Hook</span>
						<?php if(!empty($itemtypes) OR !empty($itemtype)){ ?>
						<span class="c-table__head" style="width:200px;">Item Type</span>
						<?php }
						if(!empty($items) OR !empty($item)){ ?>
						<span class="c-table__head" style="width:200px;"><?= !empty($itemtype) ? $itemtype['name'] : 'Items'; ?></span>
						<?php } ?>
					</div>
				<?php foreach($plugins as $plugin) {
					if(!empty($hook) AND ($plugin['pid'] == $hook['pid'])){ ?>
					<div class="c-table__row u-bgcolor-success">
						<span class="c-table__cell" style="width:100px"><?= $plugin['app']; ?></span>
						<span class="c-table__cell" style="width:100px"><?= $plugin['type']; ?></span>
						<span class="c-table__cell" style="width:100px"><?= $plugin['name']; ?></span>
						<input type="hidden" name="pid" id="pid" value="<?= $hook['pid']; ?>" />
						<?php if(!empty($itemtypes)){ ?>
						<span class="c-table__cell" style="width:200px;">
							<select class="c-field" name="itemtype" id="itemtypes">
								<option value="">Select an Item Type</option>
								<option value="0" <?php if(!empty($itemtype) AND ($itemtype == '0')) echo 'selected="selected"';?>>ALL</option>
								<?php foreach($itemtypes as $itemType){ ?>
								<option value="<?= $itemType['id']; ?>" <?php if(!empty($itemtype) AND ($itemtype['name'] == $itemType['name'])) echo 'selected="selected"';?>><?= $itemType['name']; ?></option>
								<?php } ?>
							</select>
						</span>
						<?php }
						#if(!empty($items) OR !empty($item)){ ?>
						<span class="c-table__cell" style="width:200px;">
							<select class="c-field" name="item" id="items">
								<option v-on:click="opt = false" value="">Select from <?= !empty( $itemtype ) ? $itemtype['name'] : 'Items'; ?></option>
								<option v-on:click="opt = true" value="0">ALL</option>
								<?php if(!empty($items)){ foreach( $items as $Item ){ ?>
								<option v-on:click="opt = true" value="<?= !empty( $Item['id'] ) ?  $Item['id'] : $Item['name']; ?>" <?php if(!empty($item) AND ($item == $Item['id'])) echo 'selected="selected"';?>>ID: <?= $Item['id']; ?> - <?= !empty($Item['title']) ? $Item['title'] : $Item['name']; ?><?php if(!empty($Item['uid'])){ ?> - UserID: <?= $Item['uid']; } ?></option>
								<?php } }?>
							</select>
							<fieldset v-if="opt" class="c-fieldset__controls">
								<div class="o-grid">
									<div class="o-grid__col u-3/3@sm">
										<input v-if="saved !== 'appHook'" type="submit" class="c-button" name="submit" id="submit" value="Add Hook" /> 
										<input v-on:click="toEdit('appHook', $event)" v-if="saved == 'appHook'" type="button" class="c-button" name="edit" id="edit" value="Add More" />
									</div>
									<div class="o-grid__col u-3/3@sm">
										<div v-if="response == 'appHook'" id="appHook_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
										<div v-if="error == 'appHook'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
									</div>
								</div>
							</fieldset>
						</span>
						<?php #} ?>
					</div>
					<?php } else { ?>
					
						<a class="c-table__row" href="<?= \clay\application::url('plugins','hook','add',array('appid' => $app['id'], 'pid' => $plugin['pid'])); ?>" title="Assign to <?= $app['name']; ?>">
							<span class="c-table__cell" style="width:100px"><?= $plugin['app']; ?></span>
							<span class="c-table__cell" style="width:100px"><?= $plugin['type']; ?></span>
							<span class="c-table__cell" style="width:100px"><?= $plugin['name']; ?></span>
							<?php if(!empty($itemtypes) OR !empty($itemtype)){ ?>
							<span class="c-table__cell" style="width:100px;"></span>
							<?php }
							if(!empty($items) OR !empty($item)){ ?>
							<span class="c-table__cell" style="width:100px;"></span>
							<?php } ?>
						</a>
				
				<?php } }?>
				</div>
				<?php if(!empty($itemtype)) {?>
				<br />
				<div class="c-table">
					<div class="c-table__row">
						<span class="c-table__head" style="width:200px;">New Hook</span>
					</div>
					<div class="c-table__row">
						<div class="c-table__cell" style="width:200px; text-align:center">
							<span id="hookDisplay"> </span>
						</div>
					</div>
				</div>
				<?php } } ?>
				<input type="hidden" name="appid" id="appid" value="<?= $app['id']; ?>" />
			</fieldset>
		</form>
	</div>
	<?php /* if(!empty($hook)){ ?>
	<script type="text/javascript">
		$('document').ready(function(){
			$("#itemtypes").change(function(){
				$.ajax({
					url: "<?= \Clay\Application::URL('plugins','hook','add', array('appid' => $app['id'], 'pid' => $hook['pid'], 'pageName' => 'app'));?>" + "&itemtype=" + this.value,
					cache: false,
					success: function(html){
						$("#dashbox").empty();
						$("#dashbox").append(html).hide();
						$("#dashbox").show();
					}
				});
				return false;
			});
			var itemtype = $('#itemtypes').prop('value');
			$("#hookDisplay").html(itemtype).show();
			<?php if(!empty($itemtype) AND empty($items)){ ?>
			$(".dashSubmit").show();
			<?php } ?>
		});
	</script>
	<?php //} ?>
	<script type="text/javascript">
		$('document').ready(function(){
			$("#items").change(function(){
				var itemtype = $('#itemtypes').prop('value');
				var item = $('#items').prop('value');

				if(item){
					$("#hookDisplay").html(itemtype + '::' + item).show();
					$(".dashSubmit").show();
				} else {
					$("#hookDisplay").html(itemtype).show();
					$(".dashSubmit").hide();
				}
				return false;
			});
		});
	</script>
	*/ ?>
</section>