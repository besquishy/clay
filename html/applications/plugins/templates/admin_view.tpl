<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		Plugins Administration</h1>
	</div>
	<div class="c-app__content">
		<div class="c-tabs c-tooltip--container">
			<a href="<?= $settings; ?>" class="c-tabs__tab c-tooltip--block">
				Plugins Settings
			</a>
			<div class="c-tooltip__block">View and Manage global plugins settings and configuration options.</div>
			<a href="<?= $pluginTypes; ?>" class="c-tabs__tab c-tooltip--block">
				Plugin Types
			</a>
			<div class="c-tooltip__block">View and Manage registered plugin types.</div>
			<a href="<?= $plugins; ?>" class="c-tabs__tab c-tooltip--block">
				Plugins
			</a>
			<div class="c-tooltip__block">View and Manage registered plugins.</div>
			<a href="<?= $groups; ?>" class="c-tabs__tab c-tooltip--block">
				Groups
			</a>
			<div class="c-tooltip__block">View and Manage your site's plugin groups. Add and remove plugin instances to/from block groups.</div>
			<a href="<?= $instances; ?>" class="c-tabs__tab c-tooltip--block">
				Instances
			</a>
			<div class="c-tooltip__block">View, add, modify, and delete plugin instances.</div>
			<a href="<?= $hooks; ?>" class="c-tabs__tab c-tooltip--block">
				Hooks
			</a>
			<div class="c-tooltip__block">Manage and configure how plugins interact with applications.</div>
			<!--<a href="<?= $help; ?>" class="c-tabs__tab c-tooltip--block">
				Help
			</a>-->
			<div class="c-tooltip__block">Documentation and Support</div>
		</div>
	</div>
</section>