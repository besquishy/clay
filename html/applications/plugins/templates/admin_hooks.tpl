<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		<a href="<?= \clay\application::url('plugins', 'admin');?>" class="dashlink" title="Dashboard">Plugins Administration</a> / Hooks</h1>
	</div>
	<div class="c-app__content">
		<p class="u-bgcolor-info u-p u-text-center">
			Browse and Update Hooks assigned to Applications
		</p>
		<?php foreach( $applications as $app ) {?>
		<a href="<?= \clay\application::url( 'plugins', 'hooks', 'view', array( 'appid' => $app[ 'appid' ] ));?>" class="c-button" title="<?= $app[ 'name' ]; ?> Hooks"><?= $app[ 'name' ]; ?></a>
		<?php }?>
	</div>
</section>