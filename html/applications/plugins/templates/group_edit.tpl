<style type="text/css">
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.1em; }
	#sortable li span { position: absolute; margin-left: -1.3em; }
</style>
<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('plugins','admin');?>" class="dashlink" title="Dashboard">Plugins</a> / 
	<a href="<?= \clay\application::url('plugins','groups');?>" class="dashlink" title="Dashboard">Groups</a> / 
	Edit Plugin Group</h1>
	</div>
	<div class="c-app__content">
		<div>
			<?php $form->template(); ?>
		</div>
		<div class="well well-lite">
			<script>
				$(function() {
					$( "#sortable" ).sortable();
					$( "#sortable" ).disableSelection();
				});
			</script>
			<script>
				$(document).ready(function() {
					$("#sortable").sortable({
						update : function () {
							serial = $('#sortable').sortable('serialize');
							$.ajax({
								url: "<?= \Clay\Application::URL('plugins','group','position', array('gid' => $plugingroup['gid'])); ?>",
								type: "POST",
								data: serial,
								cache: false,
								success: function(html){
									var timer = setInterval(function(){$("#orderUpdate").empty()}, 5500);
									$("#orderUpdate").append("Updated!").timer;
								},
								error: function(){
									alert("Plugin order update failed!");
								}
							});
						}
					});
				});
			</script>
			<form>
				<fieldset>
					<legend>Plugins Order <span id="orderUpdate"> </span></legend>
					<ul id="sortable">
					<?php if(!empty($instances)){ foreach($instances as $instance){ ?>
					<li id="iid_<?= $instance['iid']; ?>" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?= $instance['name']; ?></li>
					<?php } } ?>
					</ul>
				</fieldset>
			</form>
		</div>
	</div>
</section>