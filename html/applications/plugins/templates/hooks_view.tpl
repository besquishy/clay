<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url( 'dashboard' );?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url( 'plugins', 'admin' );?>" class="dashlink" title="Dashboard">Plugins</a> / 
	<a href="<?= \clay\application::url( 'plugins', 'admin', 'hooks' );?>" class="dashlink" title="Dashboard">Hooks</a> / <?= $app['title']; ?></h1>
	</div>
	<div class="c-app__content user-admin-dash">
		<a class="c-button" href="<?= \clay\application::url( 'plugins', 'hook', 'add', array( 'appid' => $app['id'] ));?>" role="button">Add Hook</a>
		<table class="c-table c-table--border c-table--striped">
			<thead>
				<tr class="c-table__row">
					<th class="c-table__head">ID</th>
					<th class="c-table__head">Type</th>
					<th class="c-table__head">Name</th>
					<th class="c-table__head">Description</th>
					<th class="c-table__head">Item Type</th>
					<th class="c-table__head">Item ID</th>
					<th class="c-table__head">Options</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($hooks as $hook) { ?>
				<tr class="c-table__row">
					<td class="c-table__cell"><?= $hook['hid']; ?></td>
					<td class="c-table__cell"><?= $hook['plugin_type']; ?></td>
					<td class="c-table__cell"><?= $hook['plugin_name']; ?></td>
					<td class="c-table__cell"><?= $hook['descr']?></td>
					<td class="c-table__cell"><?= !empty($hook['itemtype']) ? $hook['itemtype'] : 'ALL'; ?></td>
					<td class="c-table__cell"><?= !empty($hook['itemid']) ? $hook['itemid'] : 'ALL'; ?></td>
					<td class="c-table__cell">
						<a class="c-button" href="<?= \Clay\Application::URL('plugins','hook','edit',array('hid' => $hook['hid'])); ?>" title="Edit">Edit</a> 
						<a class="c-button" href="<?= \Clay\Application::URL('plugins','hook','delete',array('hid' => $hook['hid'])); ?>" title="Delete">Delete</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>	
	</div>
</section>