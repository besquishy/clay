<div class="c-app__head">
  <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
  <a href="<?= \clay\application::url( 'plugins', 'admin' );?>" class="dashlink" title="Dashboard">Plugins</a> / 
  <a href="<?= \clay\application::url( 'plugins', 'admin', 'hooks' );?>" class="dashlink" title="Dashboard">Hooks</a> / 
  <a class="dashlink" href="<?= \Clay\Application::URL('plugins','hooks','view',array('appid' => $app['id'])); ?>" title="Return to Hooks"><?= $app['title']; ?></a> / Delete Hook</h1> 
</div>
<div class="c-app__content">
	<h4><?= $app['title']; ?></h4>
	<h4><a class="dashlink" href="<?= \Clay\Application::URL('plugins','hooks','view',array('appid' => $app['id'])); ?>" title="Return to Hooks">Back to Hooks</a></h4>
	
	<form id="dashForm" action="<?= \Clay\Application::URL('plugins','hook','remove'); ?>" method="post">
		<fieldset class="c-fieldset">
			<legend>Delete a Hook in <?= $app['title']; ?></legend>
			<div class="c-table c-table--border">
				<div class="c-table__row">
					<span class="c-table__head" style="width:100px;">Application</span>
					<span class="c-table__head" style="width:100px;">Hook Type</span>
					<span class="c-table__head" style="width:100px;">Hook</span>
					<?php if(!empty($itemtype)){ ?>
					<span class="c-table__head" style="width:200px;">Item Type</span>
					<?php }		
					if(!empty($items) OR !empty($item)){ ?>
					<span class="c-table__head" style="width:200px;"><?= !empty($itemtype) ? $itemtype['name'] : 'Items'; ?></span>
					<?php } ?>
				</div>
				<div class="c-table__row">
					<span class="c-table__cell" style="width:100px"><?= $app['name']; ?></span>
					<span class="c-table__cell" style="width:100px"><?= $hook['plugin_type']; ?></span>
					<span class="c-table__cell" style="width:100px"><?= $hook['plugin_name']; ?></span>
					<input type="hidden" name="pid" id="pid" value="<?= $hook['pid']; ?>" />
					<?php if(!empty($itemtypes)){ ?>	
					<span class="c-table__cell" style="width:200px;">
						<?php if(!empty($itemtype) AND ($itemtype == '0')){
							echo 'ALL';
						} else {
			      			foreach($itemtypes as $itemType){
			      				if(!empty($itemtype) AND ($itemtype['name'] == $itemType['name'])) echo $itemType['name'];
			    		} } ?>
					</span>
					<?php }
					if(!empty($items) OR !empty($item)){ ?>
					<span class="c-table__cell" style="width:200px;">
						<?php if(!empty($itemtype) AND ($itemtype == '0')){
							echo 'ALL';
						} else { 
							foreach( $items as $Item ){
			      			!empty( $Item['id'] ) ?  $Item['id'] : $Item['name']; ?>
							<?php if(!empty($hook['itemid']) AND ($hook['itemid'] == $Item['id'])){ ?>
								ID: <?= $Item['id']; ?> - <?= !empty($Item['title']) ? $Item['title'] : $Item['name']; ?><?php if(!empty($Item['uid'])){ ?> - UserID: <?= $Item['uid']; }
			    		} } } ?>
					</span>
					<?php } ?>
				</div>
			</div>
			<div>
				<input type="hidden" name="hid" id="hid" value="<?= $hook['hid']; ?>" />
				<input type="hidden" name="appid" id="appid" value="<?= $app['id']; ?>" />
			</div>
		</fieldset>
		<fieldset class="c-fieldset__controls">
			<div class="o-grid">
				<div class="o-grid__col u-1/3@sm">
					<input type="submit" class="c-button" name="submit" id="submit" value="Delete Hook" /> 
				</div>
			</div>
		</fieldset>
	</form>
</div>

<script type="text/javascript">
	$('document').ready(function(){
		$(".dashSubmit").hide();
		$("body").delegate(".dashConfirm",'click', function(){
			$(".dashConfirm").hide();
			$(".dashSubmit").show();
		});
	});
</script>