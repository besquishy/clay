<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('plugins','admin');?>" class="dashlink" title="Dashboard">Plugins</a> / 
	<a href="<?= \clay\application::url('plugins','instances');?>" class="dashlink" title="Dashboard">Plugin Instances</a> / 
	Create a New Plugin Instance</h1>
	</div>
	<div class="c-app__content">
		<form id="dashForm" action="<?= \Clay\Application::URL('plugins','instance','create'); ?>" method="post">
			<fieldset class="c-fieldset">
				<legend>Add a Plugin Instance</legend>
				<div class="c-table c-table--border c-table--striped">
					<div class="c-table__row">
						<span class="c-table__head">Application</span>
						<span class="c-table__head">Plugin</span>
						<span class="c-table__head">Description</span>
					</div>
					<?php foreach($plugins as $plugintype) {
					if(!empty($plugin['pid']) AND ($plugintype['pid'] == $plugin['pid'])){ ?>
					<div class="c-table__row u-bgcolor-success">
						<span class="c-table__cell"><?= $plugintype['app']; ?></span>
						<span class="c-table__cell"><?= $plugintype['name']; ?></span>
						<span class="c-table__cell"><?= $plugintype['descr']; ?></span>
						<input type="hidden" name="pid" id="typeid" value="<?= $plugintype['pid']; ?>" />
						<input type="hidden" name="type" id="type" value="<?= $plugintype['name']; ?>" />
						<input type="hidden" name="app" id="app" value="<?= $plugintype['app']; ?>" />
					</div>
					<?php } else { ?>
					<a class="c-table__row" href="<?= \clay\application::url('plugins','instance','add',array('pid' => $plugintype['pid'])); ?>" title="Add Plugin">
						<span class="c-table__cell"><?= $plugintype['app']; ?></span>
						<span class="c-table__cell"><?= $plugintype['name']; ?></span>
						<span class="c-table__cell"><?= $plugintype['descr']; ?></span>
					</a>
					<?php } } ?>
				</div>
				<?php if(!empty($plugin['pid'])){ ?>
				<div>
					<label title="Unique System Name">Name</label>
					<input class="c-field" type="text" name="name" id="name" value="<?= !empty($name) ? $name : ''; ?>" />
				</div>
				<div>
					<label title="Display Name">Title</label>
					<input class="c-field" type="text" name="title" id="title" />
				</div>
				<div>
					<label title="Assign to a Group">Plugin Groups</label>
					<select class="c-field" multiple name="group[]" id="group">
						<?php foreach($groups as $group){ ?>
						<option value="<?= $group['gid']; ?>"><?= $group['name']; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<label title="Plugin Status">State</label>
					<select class="c-field" name="state" id="state">
						<?php foreach($states as $key => $state){ ?>
						<option value="<?= $key; ?>"><?= $state; ?></option>
						<?php } ?>
					</select>
				</div>
				<div>
					<label title="Output Template">Template</label>
					<input class="c-field" type="text" name="template" id="template" value="<?= $plugin['name']; ?>" />
				</div>
				<?php $instance->Plugin('add') ?>
				<div>
					<input type="submit" name="submit" id="submit" class="c-button" value="Create Plugin Instance" />
				</div>
				<?php } ?>
			</fieldset>
		</form>
	</div>
</section>