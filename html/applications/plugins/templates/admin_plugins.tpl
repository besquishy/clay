<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		<a href="<?= \clay\application::url('plugins', 'admin');?>" class="dashlink" title="Dashboard">Plugins Administration</a> / Plugins</h1>
	</div>
	<div class="c-app__content user-admin-dash">
		<table class="c-table c-table--border c-table--striped">
		<thead>
			<tr class="c-table__row">
				<th class="c-table__head">ID</th><th class="c-table__head">Type</th><th class="c-table__head">Application</th><th class="c-table__head">Name</th><th class="c-table__head">Description</th><?php /*<th class="c-table__head">Options</th>*/ ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach($plugins as $plugin) { ?>
			<tr class="c-table__row">
				<td class="c-table__cell"><?= $plugin['pid']; ?></td>
				<td class="c-table__cell"><?= $plugin['type']['name']; ?></td>
				<td class="c-table__cell"><?= $plugin['app']; ?></td>
				<td class="c-table__cell"><?= $plugin['name']; ?></td>
				<td class="c-table__cell"><?= $plugin['descr']?></td>
				<?php /*
				<td class="c-table__cell"><a class="dashlink" href="<?= \clay\application::url('plugins','admin','edit',array('pid' => $plugin['pid'])); ?>" title="Edit">Edit</a> 
					<a class="dashlink" href="<?= \clay\application::url('plugins','admin','delete',array('pid' => $plugin['pid'])); ?>" title="Delete">Delete</a>
				</td>
				*/ ?>
			</tr>
			<?php } ?>
		</tbody>
		</table>
	</div>
</section>