<?php
/**
 * Plugins Hook Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2012
 */

namespace application\plugins\privilege;

\Library('Clay/Application/Privilege');

/**
 * Plugins Hook Privilege
 */
class Hook extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var array
	 */
	public $Request;
	
	/**
	 * Hook Multimask Privilege
	 * @param string $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. POSTID::1 = array('POSTID',1)
		//$request = extract("::",$this->Request);
		
		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
				
			case 'HOOKID':
				
				if($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
		}
		
		return FALSE;
	}
	
	/**
	 * Administration tool
	 * @param string $privilege
	 * @param string $scope
	 */
	public function Scopes($privilege, $scope = NULL){
		
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'HOOKID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
				
			case 'HOOKID':
				
				$hooks = \Clay\Module::Object( 'Plugins' )->Get()->Hooks();
				
				foreach($hooks as $hook){
					# Get Plugin Info
					$plugin = \Clay\Module::Object( 'Plugins' )->Get()->PluginByID($hook['pid']);
					$plugin['app'] = \Clay\Application::getName($plugin['appid']);
					$plugin['type'] = \Clay\Module::Object( 'Plugins' )->Get()->TypeName( $plugin['ptype'] );
					# Application Info
					$APP = \Clay\Application::getName($hook['appid']);
					$ITEMTYPE = !empty( $hook['itemtype'] ) ? ':ITEMTYPE('.$hook['itemtype'].')' : '';
					$ITEMID = !empty( $hook['itemid'] ) ? ':ITEMID('.$hook['itemid'].')' : '';
					$FIELD = !empty( $hook['field'] ) ? ':FIELD('.$hook['field'].')' : '';
					# Scope Info
					$item['id'] = $hook['hid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					//$item['title'] = $APP.$ITEMTYPE.$ITEMID.$FIELD;
					$item['name'] = $plugin['app'].'\\'.$plugin['type'].'\\'.$plugin['name'];
					$item['title'] = $item['name'].' &#10145; '.$APP.$ITEMTYPE.$ITEMID.$FIELD;

					$option['items'][] = $item;
				}
				
				unset($hooks);
				$option['type'] = 'Plugin Hooks';
				return $option;
		}
	}
}