<?php
/**
 * Plugins Group Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2012
 */

namespace application\plugins\privilege;

\Library('Clay/Application/Privilege');

/**
 * Plugins Group Privilege
 */
class Group extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var array
	 */
	public $Request;
	
	/**
	 * Blockgroup Multimask Privilege
	 * @param string $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. POSTID::1 = array('POSTID',1)
		//$request = explode("::",$this->Request);
		
		switch($scope[0]){

			case 'ALL':

				return TRUE;
				
			case 'GROUPID':
				
				if($scope[1] == $this->Request){
					
					return TRUE;
				}

			case 'HOOK':

				if($scope[1] == $this->Request){

					return TRUE;
				}
				
				return FALSE;
		}
		
		return FALSE;
	}
	
	/**
	 * Scope Masks to Items
	 * @param string $privilege
	 * @param string $scope
	 */
	public function Scopes($privilege, $scope = NULL){
		
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL', 'HOOK');
					
			}
			
			return array('ALL', 'GROUPID', 'HOOK');
		}

		$groups = \Clay\Module::Object( 'Plugins' )->Get()->Groups();
		
		foreach($groups as $group){
			
			$item['id'] = $group['gid'];
			$item['date'] = NULL;
			$item['uid'] = NULL;
			$item['title'] = NULL;
			$item['name'] = $group['name'];
			
			$option['items'][] = $item;
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
				
			case 'GROUPID':				
				
				$option['type'] = 'Plugin Groups';
				return $option;
			
			case 'HOOK':

				$option['type'] = 'Plugin Groups';
				return $option;
		}
	}
}