<?php
namespace application\plugins\privilege;

\Library('Clay/Application/Privilege');

/**
 * Plugins Instance Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2012
 */
class Instance extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var unknown_type
	 */
	public $Request;
	
	/**
	 * Instance Multimask Privilege
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. POSTID::1 = array('POSTID',1)
		//$request = extract("::",$this->Request);
		
		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
				
			case 'INSTANCEID':
				
				if($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
		}
		
		return FALSE;
	}
	
	/**
	 * Scope Masks
	 * @param string $privilege
	 * @param string $scope
	 */
	public function Scopes($privilege, $scope = NULL){
		
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'INSTANCEID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
				
			case 'INSTANCEID':
				
				$instances = \Clay\Module::Object( 'Plugins' )->Get()->Instances();
				
				foreach($instances as $instance){

					# Get Plugin Info
					$plugin = \Clay\Module::Object( 'Plugins' )->Get()->PluginByID($instance['pid']);
					$plugin['app'] = \Clay\Application::getName($plugin['appid']);
					$plugin['type'] = \Clay\Module::Object( 'Plugins' )->Get()->TypeName( $plugin['ptype'] );
					
					$item['id'] = $instance['iid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					$item['name'] = $instance['name'];
					$item['title'] = $plugin['app'].'\\'.$plugin['type'].'\\'.$plugin['name'].' &#10174; '.$item['name'];
					
					$option['items'][] = $item;
				}
				
				unset($instances);
				$option['type'] = 'Plugin Instances';
				return $option;
		}
	}
}