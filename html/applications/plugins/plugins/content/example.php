<?php
/**
 * Plugins Application
 *
 * Plugins application Example content plugin
 *
 * @copyright (C) 2014-2015 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\plugins\plugin\content;

/**
 * Plugins Content Example Plugin
 */
class example extends \Clay\Application\Plugin {
	/**
	 * Plugin Application
	 * @var string
	 */
	public $PluginApp = 'plugins';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'content';
	/**
	 * Plugin Name
	 * @var string
	 */
	public $Plugin = 'example';
	/**
	 * Stats Example
	 */
	public function stats(){
		
	}
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function view(){
		$data['msg'] = "This is the example plugin for view!";
		return $data;
	}
	/**
	 * Display - to be displayed on an item page
	 * @param unknown $args
	 * @return string
	 */
	public function display(){
		$data['msg'] = "This is the example plugin for display!";
		return $data;
	}
	/**
	 * Add Item
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		
		
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		
	}
}