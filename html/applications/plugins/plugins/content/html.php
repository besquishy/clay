<?php
/**
 * Plugins HTML Block
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\plugins\plugin\content;

/**
 * Plugins Content HTML Block (Instance)
 */
class html extends \Clay\Application\Plugin {
	/**
	 * Plugin App
	 * @var string
	 */
	public $PluginApp = 'plugins';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'content';
	/**
	 * Plugin Name
	 */
	public $Plugin = 'html';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function view( $args ){
		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}
	/**
	 * Display - to be displayed on an item page
	 * @param unknown $args
	 * @return string
	 */
	public function display( $args ){

		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}
	/**
	 * Add
	 */
	public function add(){

		$data = array(true);
		return $data;
	}
	/**
	 * Add Item
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit
	 * @param array $args
	 */
	public function edit( $args ){

		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		
		
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		
	}
}