<?php
/**
 * Plugins Application
 *
 * @copyright (C) 2013-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Installer
 */
$data = array('title' => 'Plugins',
			'name' => 'plugins',
			'version' => '1.1.0',
			'date' => 'October 14, 2017',
			'description' => 'Clay Plugins Manager',
			'class' => 'System',
			'category' => 'Administration',
			'core' => TRUE,
			);
?>