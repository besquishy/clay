<?php
/**
* Plugins Application
*
* @copyright (C) 2013-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\plugins\library;

/**
 * Plugins Installer
 * @author David
 *
 */
class Setup {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	# use \ClayDB\Connection;
	
	/**
	 * Install
	 */
	public static function Install(){

		# Content Plugin Type
		$content = array('type', array('name' => 'content', 'descr' => 'Includes content from another application.'));
		\Clay\Module::Object('Plugins')->Setup($content);

		# Plugins Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'plugins', 'name' => 'plugins', 'descr' => 'Plugins Administration.'));
		\Clay\Module::Object('Plugins')->Setup($dashboard);

		# Hook Plugins Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'plugins', 'name' => 'plugins'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );

		# Example Plugin
		$exPlugin = array('plugin', array('type' => 'content', 'app' => 'plugins', 'name' => 'example', 'descr' => 'An example content plugin (for developers).'));
		\Clay\Module::Object('Plugins')->Setup($exPlugin);

		# HTML Block Plugin
		$htmlp = array('plugin', array('type' => 'content', 'app' => 'plugins', 'name' => 'html', 'descr' => 'HTML Content Block.','inst' => '1'));
		\Clay\Module::Object('Plugins')->Setup($htmlp);

		# Text Block Plugin
		$textp = array('plugin', array('type' => 'content', 'app' => 'plugins', 'name' => 'text', 'descr' => 'Plain Text Content Block.', 'inst' => '1'));
		\Clay\Module::Object('Plugins')->Setup($textp);

		# Register Privileges
		\Clay\Module::Object('Privileges')->register('plugins','Hook','Create');
		\Clay\Module::Object('Privileges')->register('plugins','Hook','Update');
		\Clay\Module::Object('Privileges')->register('plugins','Hook','Delete');
		\Clay\Module::Object('Privileges')->register('plugins','Hook','View');
		\Clay\Module::Object('Privileges')->register('plugins','Instance','Create');
		\Clay\Module::Object('Privileges')->register('plugins','Instance','Update');
		\Clay\Module::Object('Privileges')->register('plugins','Instance','Delete');
		\Clay\Module::Object('Privileges')->register('plugins','Instance','View');
		\Clay\Module::Object('Privileges')->register('plugins','Group','Create');
		\Clay\Module::Object('Privileges')->register('plugins','Group','Update');
		\Clay\Module::Object('Privileges')->register('plugins','Group','Delete');
		\Clay\Module::Object('Privileges')->register('plugins','Group','View');

		return true;
	}
	
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function Upgrade($version){
		
		switch($version){
			
			case ($version <= '0.1'):

				$content = array('type', array('name' => 'content', 'descr' => 'Includes content from another application.'));
				\Clay\Module::Object('Plugins')->Setup($content);
				$exPlugin = array('plugin', array('type' => 'content', 'app' => 'plugins', 'name' => 'example', 'descr' => 'An example content plugin (for developers).'));
				\Clay\Module::Object('Plugins')->Setup($exPlugin);
			
			case ($version <= '1.0'):
				# Plugins Dashboard Plugin
				$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'plugins', 'name' => 'plugins', 'descr' => 'Plugins Administration.'));
				\Clay\Module::Object('Plugins')->Setup($dashboard);

			case ($version <= '1.0.1'):
				# Hook Plugins Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'plugins', 'name' => 'plugins'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );

			case($version <= '1.0.2'):
				# HTML Block Plugin
				$htmlp = array('plugin', array('type' => 'content', 'app' => 'plugins', 'name' => 'html', 'descr' => 'HTML Content Block.', 'inst' => '1'));
				\Clay\Module::Object('Plugins')->Setup($htmlp);
				# Text Block Plugin
				$textp = array('plugin', array('type' => 'content', 'app' => 'plugins', 'name' => 'text', 'descr' => 'Plain Text Content Block.', 'inst' => '1'));
				\Clay\Module::Object('Plugins')->Setup($textp);
				# Drop Old Privileges
				\Clay\Module::Object('Privileges')->remove('plugins');
				# Register New Privileges
				\Clay\Module::Object('Privileges')->register('plugins','Hook','Create');
				\Clay\Module::Object('Privileges')->register('plugins','Hook','Update');
				\Clay\Module::Object('Privileges')->register('plugins','Hook','Delete');
				\Clay\Module::Object('Privileges')->register('plugins','Hook','View');
				\Clay\Module::Object('Privileges')->register('plugins','Instance','Create');
				\Clay\Module::Object('Privileges')->register('plugins','Instance','Update');
				\Clay\Module::Object('Privileges')->register('plugins','Instance','Delete');
				\Clay\Module::Object('Privileges')->register('plugins','Instance','View');
				\Clay\Module::Object('Privileges')->register('plugins','Group','Create');
				\Clay\Module::Object('Privileges')->register('plugins','Group','Update');
				\Clay\Module::Object('Privileges')->register('plugins','Group','Delete');
				\Clay\Module::Object('Privileges')->register('plugins','Group','View');
				break;
		}
		return true;
	}
	
	/**
	 * Delete
	 * @param string $version
	 */
	public static function Delete($version){
		# Drop Privileges
	    \Clay\Module::Object('Privileges')->remove('plugins');
		# Drop Plugins
		//\Clay\Module::Object( 'Plugins' )->Delete( array( 'plugin', array( 'app' => 'plugins', 'type' => 'content', 'name' => 'example' )));
		# Drop Hooks
		//\Clay\Module::Object( 'Plugins' )->Unhook( array( 'app' => 'plugins' ));
		\Clay\Module::Object('Plugins')->remove('plugins');

	    return true;
	}
}