<?php
/**
 * Plugins Application
 *
 * @copyright (C) 2013-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\plugins\library;

/**
 * Plugins Core Library
 */
class Core {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Plugins Item Types (Groups)
	 * @return array
	 */
	public static function ItemTypes(){
	   # Get all Plugin Groups
	   $groups = \Clay\Module::Object( 'Plugins' )->Get()->Groups();
	   foreach( $groups as $group ){

		   $itemtypes[] = array( 'id' => $group['gid'],
								 'name' => $group['name'],
								 'title' => $group['name'] );
	   }
		return !empty($itemtypes) ? $itemtypes : array();
	}
	/**
	 * Plugins Item Type (Group)
	 * @param integer $itemType
	 * @return array
	 */
	public static function ItemType( $itemType ){
	   # Get a Plugin Group
	   $group = \Clay\Module::Object( 'Plugins' )->Get()->Group( $itemType );
	   
		return array( 'id' => $group['gid'],
					   'name' => $group['name'],
					   'title' => $group['name'] );
	}
	/**
	 * Plugin Items (Instances)
	 * @return array
	 */
	public static function Items(){
		# Get all Plugin Instances
		$instance = \Clay\Module::Object( 'Plugins' )->Get()->Instances();

		return array( 'id' => $instance['iid'],
					  'name' => $instance['name'],
					  'title' => $instance['name']);
	}
	/**
	 * Plugin Item (Instance)
	 * @param integer $id
	 * @return array
	 */
	public static function Item( $id ){
		# Get a Plugin Instances
		$instance = \Clay\Module::Object( 'Plugins' )->Get()->Instance( $id );
		return array( 'id' => $instance['iid'],
					  'name' => $instance['name'],
					  'title' => $instance['name']);
	}
	/**
	 * Plugins Fields
	 * @param array $args - ([itemtype],[id])
	 * @return array
	 */
	public static function Fields( $args ){
		
		return array();
	}
	/**
	 * Plugins Labels
	 * @param string $type - itemtype or item
	 * @return array
	 */
	public static function Labels( $type ){
		
		switch( $type ){

			case 'itemtype':
				return 'Group';

			case 'item':
				return 'Instance';
				
			default:
			break;
		}
		return;
	}

}