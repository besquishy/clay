<section class="c-app">
    <div class="c-app__content">
        <h3>Response: <?= $exception->getMessage(); ?></h3>
        <?php if(\Clay\Module::API('User','Instance')->privilege('system','Admin','User')) { ?>
        <p>Exception in File: <?= $exception->getFile(); ?> on Line: <?= $exception->getLine(); ?></p>
        <pre><?= $exception->getTraceAsString(); ?></pre>
        <?php } ?>
    </div>
</section>