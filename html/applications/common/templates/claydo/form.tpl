<form role="form" <?php if(!empty($this->attributes)){ foreach($this->attributes as $attr => $value) { if($attr == 'class'){ $value = 'o-container '.$value; } echo $attr.'="'.$value.'" '; } } ?>>
<?php if(!empty($this->properties)){ foreach($this->properties as $name){
	$name->template();
} } else { ?>
<h4>This form has no assigned object containers!</h4>
<?php } ?>
</form>