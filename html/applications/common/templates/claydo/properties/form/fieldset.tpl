<fieldset class="c-fieldset" <?php if(!empty($this->attributes)){ foreach($this->attributes as $attr => $value) { echo $attr.'="'.$value.'" '; } } ?>>
<?php if(!empty($this->legend)){ echo '<legend>'.$this->legend.'</legend>';}?>
<div class="c-fieldset__fields">
<?php if(!empty($this->intro)) echo '<p>'.$this->intro.'</p>'; ?>
<?php if(!empty($this->properties)){ foreach($this->properties as $name){ ?>
<?php $name->template(); ?>
<?php } } else { ?>
<h4>This fieldset has no assigned object properties!</h4>
<?php } ?>
</div>
</fieldset>
