<?php if (!empty($this->label)){ ?>
<label for="<?= $this->attributes['id']; ?>" title="<?= $this->attributes['title']; ?>"><?= $this->label; ?></label>
<?php } ?>
<button <?php if(!empty($this->attributes)){ foreach($this->attributes as $attr => $value) { 
	if($attr == 'class'){ echo 'class="c-button '.$value.'"'; $class = TRUE; } echo $attr.'="'.$value.'" '; } } 
	if(empty($class)){ echo 'class="c-button"'; }?>><?php if(!empty($this->icon)){ ?><i class="<?=$this->icon?>"></i>&emsp;<?php } ?><?= $this->attributes['value'] ?></button>