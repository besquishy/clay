<div class="c-field--checkbox">
    <input type="checkbox" <?php if(!empty($this->attributes)){ foreach($this->attributes as $attr => $value) { if($attr == 'class'){ echo 'class="c-field '.$value.'"'; $class = TRUE; } echo $attr.'="'.$value.'" '; } } 
	if(empty($class)){ echo 'class="c-field c-field__checkbox"'; }?> />
    <?php if (!empty($this->label)){ ?>
    <label for="<?= $this->attributes['id']; ?>" title="<?= $this->attributes['title']; ?>">
        <?= $this->label; ?>
    </label>
    <?php } ?>
</div>