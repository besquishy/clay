<?php if (!empty($this->label)){ ?>
<label for="<?= $this->attributes['id']; ?>" title="<?= $this->attributes['title']; ?>"><?= $this->label; ?></label>
<?php } 
if(!empty($this->options)){
?>
<select <?php if(!empty($this->attributes)){ foreach($this->attributes as $attr => $value) { if($attr == 'class'){ echo 'class="c-field u-1/6@sm '.$value.'"'; $class = TRUE; } echo $attr.'="'.$value.'" '; } } 
	if(empty($class)){ echo 'class="c-field u-1/6@sm"'; }?> >
	<?php foreach($this->options as $option){ ?>
	<option value="<?php if(!empty($option['value'])){ echo $option['value']; } else { echo ''; } ?>" <?php if(isset($this->selected) && ($this->selected == $option['value'])){ echo 'selected="selected"';}?>><?= $option['content'];?></option>
<?php } ?>
</select>
<?php } else { echo '<h4>No Option tags assigned</h4>'; }?>