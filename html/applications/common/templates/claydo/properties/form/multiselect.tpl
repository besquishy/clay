<?php if (!empty($this->label)){ ?>
<label for="<?= $this->attributes['id']; ?>" title="<?= $this->attributes['title']; ?>"><?= $this->label; ?></label>
<?php } 
if(!empty($this->options)){
?>
<select <?php if(!empty($this->attributes)){ foreach($this->attributes as $attr => $value) { echo $attr.'="'.$value.'" '; } } ?> class="form-control">


<?php 
	foreach($this->options as $option){ 
?>
	<option value="<?php if(!empty($option['value'])){ echo $option['value']; } else { echo ''; } ?>" <?php if(!empty($option['selected'])){ echo 'selected="selected"';}?>><?= $option['content'];?></option>

<?php } ?>
</select>
<?php } else { echo '<h4>No Option tags assigned</h4>'; }?>