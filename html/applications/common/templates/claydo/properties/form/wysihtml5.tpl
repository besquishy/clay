<?php if (!empty($this->label)){ ?><label for="<?= $this->attributes['id']; ?>" title="<?= $this->attributes['title']; ?>"><?= $this->label; ?></label> <?php } ?>
<textarea <?php if(!empty($this->attributes)){ foreach($this->attributes as $attr => $value) { echo $attr.'="'.$value.'" '; } } ?>>
<?php if(!empty($this->content)) echo $this->content; ?>
</textarea>
<?php $editor = \Clay\Application('wysihtml5','editor'); $this->template($editor->action('view')); ?>