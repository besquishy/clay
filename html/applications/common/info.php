<?php
/**
 * Common Application
 *
 * @copyright (C) 2015-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Common',
			'name' => 'common',
			'version' => '1.0',
			'date' => 'September 10, 2017',
			'description' => 'Common is a utility application to use as a base for general templates, styles, scripts, etc.',
			'class' => 'System',
			'category' => 'Utility',
			'depends' => NULL,
			'core' => FALSE,
			'install' => FALSE
			);
?>
