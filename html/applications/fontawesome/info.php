<?php
/**
 * Fontawesome Application
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 * Font Awesome by Dave Gandy - http://fontawesome.io
 */
$data = array('title' => 'Fontawesome',
			'name' => 'fontawesome',
			'version' => '4.7.0',
			'date' => 'July 16, 2017',
			'description' => 'Fontawesome Application adds the ability to use Fontawesome icons in Clay applications and themes. Font Awesome by Dave Gandy - http://fontawesome.io',
			'class' => 'System',
			'category' => 'Utility',
			'depends' => NULL,
			'core' => FALSE,
			'install' => FALSE
			);
?>
