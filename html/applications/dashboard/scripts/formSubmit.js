$(document).ready(function() {
     
    //if submit button is clicked
    $('#submit').click(function () {        
       
       var formID = $(this).attr('class');

       var pageURL = $('#' + formID).attr('action');
         
        //disabled all the text fields
       // $('.text').attr('disabled','true');
         
        //show the loading sign
       // $('.loading').show();
         
        //start the ajax
        $.ajax({
            //this is the php file that processes the data and send mail
            //url: '<?= \clay\application::url('system','admin','save');?>', 
            url: pageURL, 
            //GET method is used
            type: "POST",
 
            //pass the data         
            //data: $('#system_settings').serialize(),
            data: $('#' + formID).serialize(), 
            //Do not cache the page
            cache: false,
             
            //success
            success: function(response) {
            //    $('#system_settings').find('#system_settings-notify').html(response);
            	$('#' + formID).find('#' + formID + '-notify').html(response);
            } 
        }); 
         
        //cancel the submit button default behaviours
        return false;
    }); 
});