// Dashboard AJAX Form Handler
var dashVue = new Vue({
    el: '#dashboard',
    data: function() {
        return {
            saved: false,
            response: false,
            error: false,
            newInput: false,
            tags: { },
            data: [],
            counter: 0,
            opt: false
        }
    },
    methods: {
        onSubmit: function (id, e) {
            //e.preventDefault();
            data = new FormData(e.currentTarget);
            data.append('dashForm', 'true');
            this.response = id;
            var self = this;
            axios.post(e.currentTarget.getAttribute('action') + '&pageName=app', data)
            .then(function (response) {
                //console.log(response.data);
                axios.get('?app=dashboard&com=main&act=response&pageName=app', {transformResponse: []})
                .then(message => {
                    console.log(message);
                    if( message.data == "" ) {
                    self.response = false;
                    self.error = id;
                    } else {
                    console.log(id + '_submitResponse');
                    window.document.getElementById(id + '_submitResponse').innerHTML = message.data;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            })
            .catch(function (error) {
                console.log(error);
            });
            this.saved = id;
            //console.log('this.saved = true');
        },
        toEdit: function (id, e) {
            e.preventDefault();
            this.saved = false;
            this.response = false;
            //console.log('this.saved = false');
        },
        addArray: function(key, index, value) {
            return key +'['+ index +']['+ value + ']';
        },
        removeData: function(index){
            /*Vue.delete(dashVue.data, index);
            //this.tags[index] = false;
            //Vue.set(dashVue.tags, index, false)
            //dashVue.tags.splice(index,1);
            */
            this.data.splice(index, 1);
            console.log(this.data)
        },
        addData: function(type,fields){
            /*if( this.counter == 0) this.counter++;
            this.counter++;
            Vue.set(dashVue.data, this.counter, {"type": type})
            //this.tags[this.counter] = true;
            */
            this.data.push({
                type: type,
                fields: fields,
                subdata: []
            });
            console.log(this.data)
        },
        addSubData: function(index, type, fields){

            this.data[index].subdata.push({
                type: type,
                fields: fields,
                subdata: []
            })
        }
    }
})