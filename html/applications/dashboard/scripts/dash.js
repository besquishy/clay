$('document').ready(function(){

	$('.dashclose').hide();

	$('.dashErrorClose').hide();

    //$("[rel=tooltip]").tooltip();

    $("body").delegate('a.dashInfo', 'mouseenter', function ( event ) {

		$('span.' + $(this).attr('rel')).toggle();
    });

    $("body").delegate('a.dashInfo', 'mouseleave', function ( event ) {

		$('span.' + $(this).attr('rel')).toggle();
    });

    $("body").delegate('.dashopen','click' ,function(){
    	$.ajax({
    		//url: this.href + "&pageName=app",
    		url: "?app=dashboard" + "&pageName=app",
    		cache: false,
    		success: function(html){
    			$("#dashmenu").append(html);
    			return false;
    		}
    	});
    	$('.dashopen').hide();
    	$('.dashclose').show();
    	return false;
    });
    $("body").delegate(".dashSubmit", "click", function(event) {
		//$('.dashSubmit').toggle();
		$(this).toggle();
    	$('.dashEdit').toggle();
    	var formid;
    	if(typeof $(this).attr('form') !== 'undefined'){
        	formid = 'form#' + $(this).attr('form');
    	} else {
        	formid = 'form#dashForm';
    	}
    	$(formid).prepend('<input type="hidden" name="dashForm" value="true" />');
    	$.ajax({
    		type:'POST',
    		url: $(formid).attr('action') + "&pageName=app",
    		data: $(formid).serialize(),
    		cache: false,
    		success: function(html){
    			return false;
    		}
    	}).done(function(){
    		$.ajax({
    			//url: this.href + "&pageName=app",
    			url: "?app=dashboard&com=main&act=messages" + "&pageName=app",
    			cache: false,
    			success: function(html){
    				$('#dashMessageBox').show().append(html);
    				return false;
    			}
    		});
    	});
    	return false;
    });

    $('.dashclose').click(function(){
    	$('.dashclose').hide();
    	$('.dashback').hide();
    	$('.dashopen').show();
    	$("#dashmenu").empty();
    	$("#dashbox").empty();
    	//$('body').removeClass('dashbox_bg')
    	return false;
    });

    $('.dashback').click(function(){
    	$('.dashback').hide();
    	$("#dashbox").empty();
    	$("#dashmenu").show();
    	return false;
    });

    $("body").delegate('.dashlink','click' ,function(){
		$('.dashopen').hide();
    	$.ajax({
    		url: this.href + "&pageName=app",
    		cache: false,
    		success: function(html){
    			$("#dashbox").empty().append(html);
    			$('.dashEdit').hide();
    			return false;
    		}
    	});
    	$('#dashmenu').empty();
    	$('.dashback').show();
    	$('.dashclose').show();
    	//$('html,body').animate({scrollTop:$('#dashbar').offset().top}, 500);
    	return false;
    });

    $("body").delegate('.dashbutton','click' ,function(){
    	$('.dashopen').hide();
    	$.ajax({
    		url: this.value + "&pageName=app",
    		cache: false,
    		success: function(html){
    			$("#dashbox").empty().append(html);
    			$('.dashEdit').hide();
    			return false;
    		}
    	});
    	$('#dashmenu').empty();
    	$('.dashback').show();
    	$('.dashclose').show();
    	return false;
    });

    $('body').delegate(".dashEdit", "click", function(event) {
    	$('.dashEdit').toggle();
    	$('.dashSubmit').toggle();
    });
	
	$("body").delegate('.dashErrorOpen','click' ,function(){
    	$.ajax({
    		//url: this.href + "&pageName=app",
    		url: "?app=dashboard&pageName=app&act=errors",
    		cache: false,
    		success: function(html){
    			$("#dashErrorBox").append(html);
    			return false;
    		}
    	});
    	$('.dashErrorOpen').hide();
    	$('.dashErrorClose').show();
			$('#dashErrorBox').show();
    	return false;
	});
	
	$('.dashErrorClose').click(function(){
    	$('.dashErrorClose').hide();
    	$('.dashErrorOpen').show();
    	$("#dashErrors").empty();
    	$("#dashErrorBox").empty();
    	return false;
    });
	$('body').delegate(".dashErrorRemove", "click", function() {
    	//$('.dashErrorClear').toggle();
			$.ajax({
				url: this.href,
				cache: false,
				success: function(){
					return false;
				}
    		});
			$('#' + $(this).attr('rel')).toggle();
			return false;
    });
	$('body').delegate(".dashErrorClear", "click", function() {
    	//$('.dashErrorClear').toggle();
			$.ajax({
				url: this.href,
				cache: false,
				success: function(){
					return false;
				}
    		});
			$('#' + $(this).attr('rel')).toggle();
			$('#dashErrorBox').toggle();
			$('.dashErrorClose').toggle();
			return false;
    });
	// Load Embedded Content
	$.each($('.embed-data'), function(){
		$(this).html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>');
		var dataelement = this;
		$.ajax({
			type: 'GET',
			url: 'https://noembed.com/embed?url=' + dataelement.dataset.url,
			data: {
					format: 'json'
			},
			dataType: 'jsonp',

			success: function(data) {
					if(data.error){
						dataelement.innerHTML = '<p>An error has occurred</p>';
					} else {
					dataelement.innerHTML = data.html;
				}
			},
			error: function() {
					dataelement.innerHTML = '<p>An error has occurred</p>';
			}
		});
	});

});
