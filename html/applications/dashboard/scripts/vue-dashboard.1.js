// Dashboard AJAX Form Handler
var dashVue = new Vue({
    el: '#dashboard',
    data: function() {
        return {
        saved: false,
        response: false,
        error: false,
        newInput: false,
        tags: [],
        counter: 0,
        opt: false
        }
    },
    methods: {
        onSubmit: function (id, e) {
            //e.preventDefault();
            data = new FormData(e.currentTarget);
            data.append('dashForm', 'true');
            this.response = id;
            var self = this;
            axios.post(e.currentTarget.getAttribute('action') + '&pageName=app', data)
            .then(function (response) {
                //console.log(response.data);
                axios.get('?app=dashboard&com=main&act=response&pageName=app', {transformResponse: []})
                .then(message => {
                    console.log(message);
                    if( message.data == "" ) {
                    self.response = false;
                    self.error = id;
                    } else {
                    console.log(id + '_submitResponse');
                    window.document.getElementById(id + '_submitResponse').innerHTML = message.data;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
            })
            .catch(function (error) {
                console.log(error);
            });
            this.saved = id;
            //console.log('this.saved = true');
        },
        toEdit: function (id, e) {
            e.preventDefault();
            this.saved = false;
            this.response = false;
            //console.log('this.saved = false');
        },
        addArray: function(key, index, value) {
            return key +'['+ index +']['+ value + ']';
        },
        removeTag: function(index){
            Vue.delete(dashVue.tags, index);
            //this.tags[index] = false;
            //Vue.set(dashVue.tags, index, false)
            //dashVue.tags.splice(index,1);
        },
        addTag: function(){
            if(this.tags[0] !== undefined) this.counter++;
            Vue.set(dashVue.tags, this.counter, true)
            //this.tags[this.counter] = true;
        }
    }
})