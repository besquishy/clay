<?php
/**
* Dashboard Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\dashboard\component;

/**
 * Dashboard Application Main Component
 * @author David
 *
 */
class main extends \Clay\Application\Component {
	/**
	 * View
	 * @param array $args
	 */
	 public function view ($args){
	
			\Library('Clay/Scripts');
			\Library('Clay/Styles');
	
			$this->pageTitle = "Dashboard";
			$data = array();
	
			$user = \Clay\Module::API('User','Instance');
	
			$user->info = $user->info();
	
			$data['user'] = $user;
			$data['member'] = $user->isMember;
			# Get Dashboard Plugins
			$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'dashboard' );
			# Define Template Variable for $hooks (assigned as Dashboard plugin type)
			$data['hooks'] = !empty( $hooks ) ? $hooks['dashboard'] : array();

			$this->template = 'dashboard';
			
			return $data;
		}

	/**
	 * Dashboard Toolbar
	 * 
	 * Output for the toolbar and message box
	 * @param array $args
	 */
	public function toolbar($args){

		$data = array();
		# Load Message Bar (used for visual confirmation during AJAX submissions)
		$data['messagebar'] = $this->action('messagebar');

			
		$this->template = !empty($args['template']) ? $args['template'] : 'toolbar';

		\Library('Clay/Scripts');

		\Clay\Scripts::addApplication('common','vue.js','head');
		\Clay\Scripts::addApplication('common','axios.min.js','head');
	
		return $data;
	}

	/**
	 * Dashboard Message Center
	 */
	public function messagebar(){

		$data = array();
		$messages = \clay\application::api('system','message','get');

		if(!empty($messages)){

			$data['messages'] = $this->action('messages',$messages);
		}

		$errors = \Clay\Application::API('system','error','get');

		if(!empty($errors)){

			$data['errors'] = $this->action('errors',$errors);
		}

		$this->template = 'messagebar';
		\clay\styles::addApplication('dashboard','messages.css');
		unset($_SESSION['message']);
		return $data;
	}

	/**
	 * Dashboard User Messages
	 * @param array $messages
	 */
	public function messages($messages){
		$this->template = 'messages';

		if(!empty($messages)){

			return array('messages' => $messages);
		}

		$data['messages'] = \clay\application::api('system','message','get');
		unset($_SESSION['message']);
		return $data;
	}

	/**
	 * Request Response Messages
	 * @param array $messages
	 */
	public function response($messages){
		$this->template = 'response';
		$data['messages'] = \clay\application::api('system','message','get');
		unset($_SESSION['message']);
		return $data;
	}

	/**
	 * Dashboard Error Messages
	 */
	public function errors(){

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			return FALSE;
		}

		$this->template = 'errors';

		$errors = \clay\application::api('system','error','get');

		if(empty($errors)) return;
		return array('errors' => $errors);
	}
	/**
	 * Optional Loading Screen
	 */
	public function preload(){

		$this->template = 'preload';
		return true;
	}
	/**
	 * Clear (delete) Dashboard Error Messages
	 */
	public function clear() {

		$type = \Clay\Data\Get('ntype', 'string');
		$id = \Clay\Data\Get('nid', 'string', '', NULL);

		\Clay\Application::API('system','error','clear',$id);

		\Clay\Application::Redirect('dashboard','main','errors');

		return;
	}
}