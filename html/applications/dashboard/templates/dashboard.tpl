<section class="c-app">
	<div class="c-app__head">
	<h1>Dashboard</h1>
	</div>
	<div class="c-app__content">	
		<?php if(empty($member)){ ?>
		<div class="o-grid">
		<?php if( !empty( $hooks )){ ?>
			<?php foreach( $hooks as $hook ){ $hook['object']->Plugin('view', array('app' => 'dashboard', 'itemtype' => NULL, 'itemid' => NULL )); }?>
		<?php } ?>
		</div>
		<?php } else { ?>
		<div class="o-grid o-grid--equal-height">			
			<?php if( !empty( $hooks )){ ?>	
				<?php foreach( $hooks as $hook ){ ?>
				<div class="o-grid__col u-1/6@sm u-mb-x3">
				<?php $hook['object']->Plugin('view', array('app' => 'dashboard', 'itemtype' => NULL, 'itemid' => NULL ));?>			
				</div>
			<?php } } ?>
		</div>
		<?php } ?>
		<br style="clear:both">
		<?= empty( $hooks ) ? "No Dashboard Plugins have been configured!" : ''; ?>
	</div>
</section>