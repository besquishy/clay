<script type="text/javascript">
	$('document').ready(function(){
		$('.dashclose').hide();
		$("body").delegate('.dashopen','click' ,function(){
			$('.dashclose').hide();
			$.ajax({
				//url: this.href + "&pageName=app",
				url: "<?= \clay\application::url('dashboard');?>" + "&pageName=app",
				cache: false,
				success: function(html){
				$("#dashmenu").empty();
				$("#dashmenu").append(html).hide();
				$("#dashmenu").show();
				}
			});
			$('.dashopen').hide();
			$('.dashclose').show();
			return false;
		});
		$("body").delegate('.dashlink','click' ,function(){
			$('.dashopen').hide();
			$.ajax({
				url: this.href + "&pageName=dashapp",
				cache: false,
				success: function(html){
					$("#dashbox").empty();
					$("#dashbox").append(html).hide();
					$("#dashbox").show();
					$('input.dashEdit').hide();
				}
			});
			$('#dashmenu').hide();
			$('.dashback').show();
			$('.dashclose').show();
			$('html,body').animate({scrollTop:$('#dashbar').offset().top}, 500);
			return false;
		});
		$("body").delegate('.dashbutton','click' ,function(){
			$('.dashopen').hide();
			$.ajax({
				url: this.value + "&pageName=app",
				cache: false,
				success: function(html){
					$("#dashbox").empty();
					$("#dashbox").append(html).hide();
					$("#dashbox").show();
					$('input.dashEdit').hide();
				}
			});
			$('#dashmenu').hide();
			$('.dashback').show();
			$('.dashclose').show();
			return false;
		});
		$('.dashclose').click(function(){
			$('.dashclose').hide();
			$('.dashback').hide();
			$('.dashopen').show();
			$("#dashmenu").empty().hide();
			$("#dashbox").empty().hide();
			//$('body').removeClass('dashbox_bg')
			return false;
		});
		$('.dashback').click(function(){
			$('.dashback').hide();
			$("#dashbox").empty().hide();
			$("#dashmenu").show();
			return false;
		});
		
		$("body").delegate("input.dashSubmit", "click", function(event) {
			$('input.dashSubmit').toggle();
        	$('span.loading').toggle();
        	$('input.dashEdit').toggle();
        	$('form#dashForm').prepend('<input type="hidden" name="dashForm" value="true" />');
        	$.ajax({
        		type:'POST',
				url: $('form#dashForm').attr('action') + "&pageName=app",
				data: $('form#dashForm').serialize(),
				cache: false,
				success: function(html){
					//var alertdata = $('form#dashForm').attr('action');
					//alert();
				//$('#show-time').show().showtime();
				}
				//return false;
			}).done(function(){
				$.ajax({
					//url: this.href + "&pageName=app",
					url: "<?= \clay\application::url('dashboard','main','messages');?>" + "&pageName=app",
					cache: false,
					success: function(html){
					$('#dashMessageBox').show().append(html);
					//$('#show-time').show().showtime();
					}
				})});
        	return false;
		});
		
		
		$('body').delegate("input.dashEdit", "click", function(event) {
        	$('input.dashEdit').toggle();
        	$('input.dashSubmit').toggle();
        });

	});
	
</script>

<div id="dashbar">
	<?php $this->template($messagebar);?>
	<!-- <input type="button" class="dashopen" value="<?= $user->isMember ? 'Dashboard' : 'Login or Register'; ?>" /> -->
	
	<span class="dashright">	 
	<?= $user->isMember 
		? '<img src="'.$user->gravatar.'" />  
		<a class="dashlink" href="'.\clay\application::url('users','settings').'" title="Account Settings"><img class="dashicon" src="'.\clay\application::image('dashboard','gear_24.png').'" alt="Dashboard" title ="Account Settings" /></a> 
		<a href="'.\clay\application::url('users','main','logout').'" title="Log Out"><img class="dashicon" src="'.\clay\application::image('dashboard','cancel_24.png').'" alt="Settings" title ="Log Out" /></a> 
				</span> 
				<img class="dashopen dashicon" src="'.\clay\application::image('dashboard','app_globe_24.png').'" alt="Dashboard" title ="Dashboard" />' 
		: '<button id="login" class="dashbutton entry" value="'.\clay\application::url('dashboard').'">Login or Register</button></span><img class="dashopen dashicon" src="'.\clay\application::image('dashboard','man_24.png').'" alt="Login or Register" title ="Login or Register" />';?>
	<!-- <a href="<?= \clay\application::url('dashboard');?>" class="dashopen" rel="dashbox"><?= $user->isMember ? 'Dashboard' : 'Login or Register'; ?></a> -->
	<!-- <input type="button" class="dashback" style="font-size:1.1em;" value="Back" /> --> <!-- <input type="button" class="dashclose" style="font-weight:900;padding:0px;font-size:1.5em;" value=" X " /> -->
	<?= '<img class="dashclose closeicon" src="'.\clay\application::image('dashboard','close_24.png').'" alt="Close" title="Close" />'; ?>
	</span>
</div>
<div id="dashmenu"> </div>
<div id="dashbox"> </div>