	<nav class="c-navbar" id="toolbar">
		<div class="o-grid u-jc-between">
			<ul class="c-navbar__menu">
				<li class="c-navbar__brand">
					<a href="index.php"><?= $this->siteName; ?></a>
				</li>
				<li class="c-navbar__item"><a href="#news">News</a></li>
				<li class="c-navbar__item"><a href="<?= \clay\application::url('blog') ?>">Blog</a></li>
				<li class="c-navbar__item"><a href="<?= \clay\application::url('contact') ?>">Contact</a></li>
				<li class="c-navbar__item"><a href="#news">About</a>
					<div class="c-navbar__subitem">
							<a href="#news">Our People</a>
							<a href="#news">Our Mission</a>
					</div>
				</li>
			</ul>
			<ul class="c-navbar__menu">
			<?php if($user->isMember){ ?>
				<li class="c-navbar__item">
          <a><i class="fa fa-user"></i> <strong><?= strtoupper($user->info['uname']); ?></strong> <span class="caret"></span></a>
						<div class="c-navbar__subitem c-navbar__subitem--bl">
								<img style="text-align:center;" src="<?= $user->gravatar.'?s=175'; ?>" />
								<a class="dashlink" href="<?= \clay\application::url('users','account','edit') ?>">Account Settings</a>
								<a href="<?= \clay\application::url('users','main','logout') ?>">Log Out</a>
						</div>
        </li>
				<?php if(!empty($errors)) { ?>
				<li class="c-navbar__item">
					<a class="dashErrorOpen btn btn-danger btn-xs navbar-btn" style="" title="Error Console"><i class="fa fa-exclamation-triangle"></i></a>
				</li>
				<li class="c-navbar__item">
					<a class="dashErrorClose btn btn-danger btn-xs navbar-btn" style="" title="Close Error Console"><i class="fa fa-window-close"></i></a>
				</li>
				<?php } ?>
				<li class="c-navbar__item">
					<a class="dashOpen btn btn-info btn-xs navbar-btn" v-if="showOpen" v-on:click="open" style="" title="Dashboard"><i class="fa fa-chevron-down"></i></a>
				</li>
				<?php } else { ?><li class="c-navbar__item"><a class="dashOpen" v-if="showOpen" v-on:click="open" href="?app=dashboard"><i class="fa fa-user"></i> Login or Register</a></li><?php } ?>
				<li class="c-navbar__item"><a class="dashClose" v-if="showClose" v-on:click="close" title="Close Dashboard"><i class="fa fa-window-close"></i></a></li>
		</ul>
		</div>
	</nav>
	<?php $this->template($messagebar);?>
	<div id="dashmenu" class="u-bgcolor-container"> </div>
	<div id="dashbox" class="u-bgcolor-container"> </div>
	<div id="dashErrorBox" class="u-bgcolor-container"> </div>
	<script>
		var dashOpen = new Vue({
			el: '.dashOpen',
			data: {
				url: '?app=dashboard&pageName=app',
				showOpen: true,
			},
			methods: {
				open: function (e) {
					e.preventDefault();
					axios.get('?app=dashboard&pageName=app', {transformResponse: []})
					.then(response => {
						window.document.getElementById('dashmenu').innerHTML = response.data;
					});
					this.showOpen = false;
					dashClose.showClose = true;
				}
			}
		})
		var dashClose = new Vue({
			el: '.dashClose',
			data: {
				showClose: false
			},
			methods: {
				close: function (e) {
					e.preventDefault();
					var el = window.document.getElementById('dashmenu');
					while (el.firstChild) el.removeChild(el.firstChild);
					this.showClose = false;
					dashOpen.showOpen = true;
				}
			}
		})
	</script>
