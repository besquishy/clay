<div id="dashMessages">
	<messages v-if="showMessage" @close="showMessage = false">
		
	</messages>
	<noscript>
		<div class="c-alert u-bgcolor-info">
			<?php if(!empty($messages)){ 
				if(is_array($messages)) {
					foreach($messages as $msg) {
						echo '<p>'.$msg.'</p>';
					}
				} else {
					echo '<p>'.$messages.'</p>';
					?>
				<?php }?>
			<?php } ?>
		</div>
	</noscript>
</div>
<script type="text/x-template" id="message-template">
	<div class="c-alert u-bgcolor-info">
		<button type="button" class="c-alert__close js-close" @click="$emit('close')">&times;</button>
		<?php if(!empty($messages)){ 
				if(is_array($messages)) {
					foreach($messages as $msg) {
						echo '<p>'.$msg.'</p>';
					}
				} else {
					echo '<p>'.$messages.'</p>';
					?>
			<?php }?>
			<?php }
		?>
	</div>
</script>
<script>
// register messages component
	Vue.component('messages', {
		template: '#message-template'
	})
	// start app
	new Vue({
	el: '#dashMessages',
	data: {
			showMessage: true
	}
	})
</script>