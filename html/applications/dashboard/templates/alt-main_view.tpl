<style type="text/css">
    .box-title {font-weight:bold;}
    .exit {position:absolute;right:3px;top:3px;cursor:pointer;font-size:10px;color:#ffffff;}
    .box {background:#5C94A1;border:2px solid #cccccc;-moz-border-radius: 5%;}
 </style>
<script type="text/javascript">
var zindex = 1;
var nextz = function(){return zindex++};
$('document').ready(function(){
  	$("#loading").hide();
  	$("#loading").ajaxStart(function(){ $(this).show(); }).ajaxStop(function(){ $(this).hide(); });
    $('a.boxit').click(function(){
  	    //$("#boxed_content").empty();
  	    var thisID = 'boxed-' + this.rev;
  	    var thisTitle = this.title;
    		$.ajax({
    			url: this.rel + '&pageName=html',
    			cache: false,
    			success: function(html){
    			if ($('div#'+thisID)){
    			  $('div#'+ thisID).remove();
    			}

    			  $('#boxed').append('<div id="'+ thisID +'" class="porthole" rel="boxes"></div>');
    				$('#'+thisID).append('<div class="box-title">'+ thisTitle +'<a class="box-close exit" title="Close"><img src="cross.png" /></a></div>').append('<div class="box-content">'+ html +'</div>').hide();
    				var boxHeight = $('#'+thisID).height();
    				var boxWidth = $('#'+thisID).width();
    				var docHeight = $(document).height();
    				var docWidth = $(document).width();
            var horiz = docWidth / 2 - boxWidth / 2;
            var vert = docHeight / 2 - boxHeight / 2;
            $('#'+thisID).css({background: "#ffffff", position: "fixed", left: horiz + "px", top: vert + "px",padding: "1px", border: "5px solid #5C94A1", "z-index": nextz()});
            //$('#'+thisID).css({position: "fixed", left: horiz + "px", top: vert + "px", padding: "2px"});
            $('.box-content').css({background: "#ffffff", padding: "5px", border: "2px solid #cccccc",opacity: "1"});
            $('.box-title').css({padding: "5px", background: "#494941", opacity: "1", color: "#ffffff"});
    				$('#'+thisID).fadeIn(200);
    				//$('#info').text("docWidth: " + docWidth + ", boxWidth: " + boxWidth + ", horiz: " + horiz + ", docHeight: " + docHeight + ", boxHeight: " + boxHeight + ", vert: " + vert);
    				$('#'+thisID).draggable({handle: '.box-title'});

    			}
    		});
    		return false;
    	});
  		$('.box-close').livequery('click', function(event) {
  		  $(this).parent().parent().remove();
  		});
  		$('.box-title').livequery('mousedown', function(event) {
  		  var zindex = $(this).parent().css("z-index");
  		  var _zindex = zindex++;
  		  //alert('z-index is ' + nextz());
  		  $(this).parent().css({"z-index":nextz()});

  		}, function(){$('.box-title').expire('mousedown');});
  		/*$('.resizable').livequery(function() {
  		  $(this).resizable({handles: "all"});
  		}, function() {
  		  $(this).css({zindex: "1"});
  		});*/
});
</script>

<style type="text/css">
div.app-data-grid { min-width:150px;max-width:300px;height:150px;}
</style>
<div class="c-app__head">
  <h1>Dashboard</h1>
</div>
<div class="c-app__content">
	<?php foreach($items as $dash){ ?><?php \clay\application\object::template($dash); ?><?php } ?>
	<br style="clear:both" />

<br style="clear:both" />
</div>
<div id="boxed"> </div>