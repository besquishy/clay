<div id="dashErrors">
	<errors>
		
	</errors>
</div>
<script type="text/x-template" id="errors-template">
	<div class="c-app c-error__list">
		<div class="c-app__head">
			<h1>Error Log</h1>
		</div>		
		<div id="c-app__content dashErrorList">
			<?php if(!empty($errors)){ ?>
			<p>
				<a class="dashErrorClear c-button c-button--danger c-button--sm" v-on:click="clear" href="<?= \Clay\Application::URL('dashboard','main','clear',array('ntype' => 'error')); ?>" rel="dashErrorList" title="Remove All Errors from the Log">Clear</a></p>
				<?php foreach($errors as $key => $error) { ?>
				<div id="eid-<?= $key; ?>">
					<hr />
					<p>
						<a class="dashErrorRemove c-button c-button--primary c-button--sm" v-on:click="remove" href="<?= \Clay\Application::URL('dashboard','main','clear',array('ntype' => 'error', 'nid' => $key)); ?>" rel="eid-<?= $key; ?>" title="Remove this Error from the Log">Remove</a>
					</p>
					<p>
						<span class="badge"> <?= $error['times']; ?></span> <strong>Times</strong> since <strong><?= $error['date']; ?></strong>
					</p>
					<strong><?= $error['error'];  ?></strong>: <?= $error['message']; ?> in File: <?= $error['file']; ?> on Line: <?= $error['line']; ?>
					<?= !empty($error['context']) ? '<pre>'.print_r($error['context'], true).'</pre>' : ''; ?>
				</div>
			</p>
				<?php }
			} else { ?>
				<p>No Errors in the Log</p>
			<?php } ?>
		</div>
	</div>
</script>
<script>
// register messages component
	Vue.component('errors', {
		template: '#errors-template',
		methods: {
			clear: function (e) {
				e.preventDefault();
				axios.get(e.currentTarget.getAttribute('href'), {transformResponse: []})
				.then(response => {
					var p = window.document.getElementById('dashErrorList');
					while (p.firstChild) p.removeChild(p.firstChild);
				});
				this.showClear = false;
			},
			remove: function (e) {
				e.preventDefault();
				var eid = e.currentTarget.getAttribute('rel');
				axios.get(e.currentTarget.getAttribute('href'), {transformResponse: []})
				.then(response => {
					var p = window.document.getElementById(eid);
					while (p.firstChild) p.removeChild(p.firstChild);
				});
			}
		}
	})
	// start app
	new Vue({
		el: '#dashErrors',
		data: {
			showClear: true
		}
	})
</script>