<?php
/**
* Dashboard Application
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\dashboard\library;

\Clay\Module('Privileges');

/**
 * Dashboard Application Setup
 */
class setup {
	/**
	 * Install
	 */
	public static function install(){
		# Dashboard Plugin Type
		$pluginType = array('type', array('name' => 'dashboard', 'descr' => 'Dashboard items for administrators and users.'));
		\Clay\Module::Object('Plugins')->Setup($pluginType);
		return TRUE;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function upgrade($version){
		
		switch($version){
			case ($version <= '0.1'):
				
			case ($version <= '1.0'):
				# Dashboard Plugin Type
				$pluginType = array('type', array('name' => 'dashboard', 'descr' => 'Dashboard items for administration.'));
				\Clay\Module::Object('Plugins')->Setup($pluginType);
			case($version <= '1.0.1'):
				# Unregister Privilege
				\Clay\Module::Object('Privileges')->unregister('dashboard','Item','View');
				break;
		}
		return TRUE;
	}
	/**
	 * Delete
	 */
	public static function delete(){
		\Clay\Module::Object('Privileges')->remove('dashboard');
	    return TRUE;
	}
}