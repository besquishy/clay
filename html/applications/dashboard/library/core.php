<?php
/**
 * Dashboard Application
 *
 * @copyright (C) 2014-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\dashboard\library;

/**
 * Dashboard Application Core Library
 */
 class Core {
 	
 	/**
 	 * Import self::db() using a Trait \ClayDB\Connection
 	 */
 	use \ClayDB\Connection;
 	
 	/**
 	 * Dashboard Item Types (currently none)
 	 * @return array
 	 */
 	public static function ItemTypes(){
 		
 		return array();
 	}
 	/**
 	 * Dashboard Item Type
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function ItemType( $itemType ){
 		
 		return array();
 	}
 	/**
 	 * Dashboard Items (current none)
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function Items( $itemType = NULL ){
 		# Get all Dashboard Posts
 		return array();
 	}
 	/**
 	 * Dashboard Item
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Item( $args ){
 		
 		return array();
 	}
 	/**
 	 * Dashboard Fields
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Fields( $args ){
 		
 		return array();
	}
	/**
	 * Dashboard Labels
	 * @param string $type - itemtype or item
	 * @return array
	 */
	public static function Labels( $type ){
		
		switch( $type ){

			case 'itemtype':
				return NULL;

			case 'item':
				return NULL;
				
			default:
			break;
		}
		return;
	} 
 }