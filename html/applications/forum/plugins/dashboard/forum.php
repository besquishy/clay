<?php
/**
* Forum Dashboard Plugin
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link %link%
* @author David L Dyess II
*/

namespace application\forum\plugin\dashboard;

/**
 * Forum Dashboard Plugin
 */
class forum extends \Clay\Application\Plugin
{

	public $PluginApp = 'forum';
	public $PluginType = 'dashboard';
	public $Plugin = 'forum';

	/**
	 * Forum Dashboard
	 */
	public static function view()
	{
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if ($user->privilege('system','Admin','User')) {
			
			$data['admin'] = \Clay\Application::url('forum','admin','view');
		}

		$data['view'] = \Clay\Application::url('forum');
		
		return $data;
	}

}