<?php
/**
 * Forum Application
 *
 * @copyright (C) 2018 David L Dyess II
 * @license GPL
 * @link %link%
 * @author David L Dyess II
 * @package Clay
 */
$data = array('title' => 'Forum',
			'name' => 'forum',
			'version' => '0.1',
			'date' => 'June 3, 2018',
			'description' => 'Simple message board system',
			'class' => 'User',
			'category' => 'Communication',
			'depends' => array('applications' => array('dashboard')),
			'core' => false,
			'install' => true
			);