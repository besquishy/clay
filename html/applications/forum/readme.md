# Clay Starter App

This is an application for the Clay CMS you use as a boilerplate to build your own app.

## Search, Replace, Customize, Run

Search and replace the following with what you want to use in your app:

forum - Lowercase name of your app (unique)

Forum - Uppercase name of your app

2018 - Copyright Year(s)

David L Dyess II

%link%

June 3, 2018

0.1

Simple message board system

User

Communication

false

true

David L Dyess II - Author's Name

GPL - License

%db_table1% - Database table your app will use

%db_table2% - Secondary database table your app will use

%plugin_type% - Plugin Type your app will register (unique)

%plugin_type_descr% - Description of the Plugin Type your app will register (unique)

%plugin1_type% - Type of the Plugin your app will register (content, navigation, meta, etc.)

%plugin1_name% - Name of the Plugin your app will register (unique)

%plugin1_descr% - Description of the Plugin your app will register

forum - Name of the Dashboard menu your app will register (unique)

%dashboard_title% - Title of the Dashboard menu

%dashboard_descr%

Forum - First Privilege mask

Topic - Second Privilege mask