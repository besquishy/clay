<?php
/**
* Forum Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL
* @link %link%
* @author David L Dyess II
*/

namespace application\forum\library;

/**
 * Comment API
 * @author David L Dyess II
 *
 */
class post
{
	
	private $INVALID_ID = 'A numeric Comment ID (cid) must be supplied';
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;

	/**
	 * Create a Forum Post
	 * @param array $args
	 */
	public static function create( $args )
	{		
		
		$cid = self::db()->add( \claydb::$tables['comments']." (status, depth, ccount, tid, parid, uid, cpub, cdate, points, title, base, options, body) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
							array( $args['status'],$args['depth'],$args['ccount'],$args['tid'],$args['parent'],$args['user'],$args['date'],NULL,$args['points'],$args['title'],'VOID',$args['options'],$args['body'] )
						);
		
		$pcbase = str_pad( $cid, 10, "0", STR_PAD_LEFT );
		
		if ( $args['parent'] > 0 ) {
		
			//$par = self::base( $args['parent'] );
			//$base = $par['base']. "." .$pcbase;
			$base = $args['base']. "." .$pcbase;
		
		} else {
		
			//$base = $pcbase;
		}
		
		self::rebase( array( 'base' => !empty( $base ) ? $base : $pcbase , 'cid' => $cid ));
		
		$thread = \Clay\Application::API( 'comments', 'thread', 'comment', array( 'tid' => $args['tid'], 'date' => $args['date'] ));
		
		if ( !empty( $args['parent'] )){
			
			$parent = self::reply( array( 'cid' => $args['parent'], 'date' => $args['date'] ));
		}
		
		return $cid;
	}
	
	/**
	 * Update a Forum Post's Base
	 * @param array $args
	 */
	public static function rebase( $args )
	{
	
		return self::db()->update( \claydb::$tables['comments']." SET base = ? WHERE cid = ?",
				array( $args['base'],$args['cid'] ),1
		);
	}
	/**
	 * 
	 * @param array $args
	 */
	public static function reply( $args )
	{
		
		$parentCCount = self::getCCount( array( 'cid' => $args['cid'] ));
		$ccount = $parentCCount + 1;
		
		return self::db()->update( \claydb::$tables['comments']." SET ccount = ?, cdate = ? WHERE cid = ?",
				array( $ccount, $args['date'], $args['cid'] ),1
		);
	}
	
	/**
	 * Update a Comment
	 * @param array $args
	 */
	public static function update( $args )
	{
		
		return self::db()->update( \claydb::$tables['comments']." SET cdate = ?, title = ?, body = ? WHERE cid = ?",
								array( $args['updated'],$args['title'],$args['body'],$args['cid'] ),1
						);
	}
	
	/**
	 * Change the Status of a Comment
	 * @param array $args
	 */
	public static function status( $args )
	{
	
		return self::db()->update( \claydb::$tables['comments']." SET status = ?, cdate = ? WHERE cid = ?",
				array( $args['status'],$args['updated'],$args['cid'] ), 1
		);
	}
	
	/**
	 * Delete a Forum Post
	 * @param array $args
	 */
	public static function delete( $args )
	{
		
		return self::db()->delete( \claydb::$tables['comments']." WHERE cid = ?", array( $args['cid'] ), 1
						);
	}

	/**
	 * Forum Post Input Form
	 * @param array $args
	 */
	public static function input( $args )
	{
		
		\Library('ClayDO/DataObject/form');
		$form = new \ClayDO\DataObject\form;
		
		if (!empty($args['data']['cid'])) {
			
			$action = \Clay\Application::url('comments','post','update');
			
		} else {
			
			$action = \Clay\Application::url('comments','post','create');
		}
		
		$form->attributes(array('method' => 'POST', 'action' => $action));
		$form->container('set1','fieldset')->legend = !empty($args['data']['cid']) ? 'Edit Comment' : 'New Comment';
		$title = array('id' => 'title', 'name' => 'title','title' => 'Enter a comment title.','class' => 'input-xlarge');
		$title['value'] = !empty($args['data']['title'])? $args['data']['title'] : '';
		$form->container('set1')->property('title','text')->attributes($title);
		$form->container('set1')->property('title')->label = "Title";
		$body = array('id' => 'body', 'name' => 'body','title' => 'Enter comment text.','class' => 'textarea', 'rows' => 15, 'style' => 'width:740px;');
		$form->container('set1')->property('body','textarea')->attributes($body);
		$form->container('set1')->property('body')->content = !empty($args['data']['body'])? $args['data']['body'] : '';
		$form->container('set1')->property('body')->label = "Text";
		
		if (!empty($args['data']['cid'])) {
			
			$cid = array('type' => 'hidden', 'name' => 'cid', 'id' => 'cid', 'value' => $args['data']['cid']);
			$form->container('set1')->property('cid','input')->attributes($cid);
		}
		$tid = array('type' => 'hidden', 'name' => 'tid', 'id' => 'tid', 'value' => $args['data']['tid']);
		$form->container('set1')->property('tid','input')->attributes($tid);
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Comment'));
		
		return $form;
	}
	
	/**
	 * Get a Forum Post
	 * @param array $args
	 * @throws \Exception
	 */
	public static function get( $args=array() )
	{
		
		if (empty($args['cid'])){
			
			Throw new \Exception( self::$INVALID_ID );
		}
		
		return self::db()->get( 'status, depth, ccount, tid, parid, uid, cpub, cdate, points, title, base, options, body FROM '.\claydb::$tables['comments'].' WHERE cid = ?', array( $args['cid'] ), '0,1' );
	}
	
	/**
	 * Get a Forum Post's Base
	 * @param int $cid
	 * @throws \Exception
	 */
	public static function base( $cid )
	{
	
		if (empty($cid)){
				
			Throw new \Exception( self::$INVALID_ID );
		}
	
		return self::db()->get( 'base FROM '.\claydb::$tables['comments'].' WHERE cid = ?', array( $cid ), '0,1' );
	}
	
	/**
	 * Get Forum Post Info
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getInfo($args=array())
	{
		
		if (empty($args['cid'])){
			
			Throw new \Exception(self::$INVALID_ID);
		}
		
		return self::db()->get( 'ccount, tid, parid, uid, cpub, cdate, points, title FROM '.\claydb::$tables['comments'].' WHERE cid = ?',array($args['cid']),'0,1');
	}
	
	public static function getCCount($args=array())
	{
	
		if (empty($args['cid'])){
				
			Throw new \Exception(self::$INVALID_ID);
		}
	
		$comment = self::db()->get( 'ccount FROM '.\claydb::$tables['comments'].' WHERE cid = ?',array($args['cid']),'0,1');
		return $comment['ccount'];
	}
	
	/**
	 * Get Comment's Author's User ID
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getAuthor($args=array())
	{
		
		if (empty($args['cid'])){
			
			Throw new \Exception(self::$INVALID_ID);
		}
		
		return self::db()->get( 'uid FROM '.\claydb::$tables['comments'].' WHERE cid = ?',array( $args['cid'] ), '0,1' );
	}
}