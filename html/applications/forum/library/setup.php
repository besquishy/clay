<?php
/**
* Forum Application
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link %link%
* @author David L Dyess II
*/

namespace application\forum\library;

/**
 * Forum Setup API
 * @author David L Dyess II
 *
 */
class setup
{
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	/**
	 * Install
	 * @return boolean
	 */
	public static function install()
	{
		# Use the DB trait
		$dbconn = self::db();
		# Datadict is Used for Table Manipulation
	    $datadict = $dbconn->datadict();
		# Forum tables
		# Table Alias => Table Name
	    $tables = array( 'forums' => array( 'table' => 'forums' ), 
	    				 'forum_topics' => array( 'table' => 'forum_topics' ));
	    # Add DB Table names to data/config/sites/[site]/database.tables.php configuration file
	    $datadict->registerTables( $tables );
	    # Prepare the Forums table for creation
	    $cols = array();
	    $cols['column'] = array( 'fid' => array( 'type' => 'ID' ),
								'status' => array( 'type' => 'TINYINT', 'size' => '1' ),
								'seq' => array( 'type' => 'TINYINT', 'size' => '1' ),
								'pfid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => TRUE ),
	    						'tdate' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'pdate' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
								'tcount' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE, 'default' => '0' ),
								'pcount' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE, 'default' => '0' ),
								'title' => array( 'type' => 'VARCHAR', 'size' => '100' ),
								'name' => array( 'type' => 'VARCHAR', 'size' => '100' ),
	    						'options' => array( 'type' => 'STRING' ));
	    # Forums table indices
		$cols['index'] = array( 'status' => array( 'type' => 'KEY', 'index' => 'status' ),
								'sequence' => array( 'type' => 'KEY', 'index' => 'seq' ),
								'parent' => array( 'type' => 'KEY', 'index' => 'pfid' ),
								'name' => array( 'type' => 'KEY', 'index' => 'name' ));
	    # Create Forums Table
	    $datadict->createTable( \claydb::$tables['forums'], $cols );
	    # Prepare the Topics table for creation
	    $cols = array();
	    $cols['column'] = array( 'tid' => array( 'type' => 'ID' ),
					    		'status' => array( 'type' => 'TINYINT', 'size' => '1' ),
	    						'depth' => array( 'type' => 'TINYINT', 'unsigned' => TRUE, 'size' => '2' ),
	    						'pcount' => array( 'type' => 'INT', 'size' => '4', 'unsigned' => TRUE, 'null' => FALSE, 'default' => '0' ),
	    						'fid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
					    		'uid' => array('type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
					    		'tpub' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'tdate' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
					    		'points' => array( 'type' => 'INT', 'size' => '11', 'null' => FALSE, 'default' => '0' ),
	    						'title' => array( 'type' => 'VARCHAR', 'size' => '100' ),
	    						'base' => array( 'type' => 'STRING', 'null' => FALSE ),
					    		'options' => array( 'type' => 'STRING' ),
	    						'body' => array( 'type' => 'text'));
	    # Topics indices
	    $cols['index'] = array( 'status' => array( 'type' => 'KEY', 'index' => 'status' ),
					    		'created' => array( 'type' => 'KEY', 'index' => 'tpub' ),
					    		'updated' => array( 'type' => 'KEY', 'index' => 'tdate' ),
					    		'userid' => array( 'type' => 'KEY', 'index' => 'uid' ),
								'thread' => array( 'type' => 'KEY', 'index' => 'fid' ),
								'body' => array( 'type' => 'FULLTEXT', 'index' => 'body' ));
	    # Create Topics Table
	    $datadict->createTable( \claydb::$tables['forum_topics'], $cols );
	    # Forum Dashboard Plugin
		$dash = array('plugin', array('type' => 'dashboard', 'app' => 'forum', 'name' => 'forum', 'descr' => 'Dashboard plugin for the Forum application.'));
		\Clay\Module::Object('Plugins')->Setup($plugin);
		# Hook Forum Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'forum', 'name' => 'forum'),
					  'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		# Forum Plugin Type - %plugin_type%
		/*$pluginType = array('type', array('name' => '%plugin_type%', 'descr' => '%plugin_type_descr%'));
		\Clay\Module::Object('Plugins')->Setup($pluginType);*/
		# Forum Plugin - %plugin1_name%
		/*$plugin = array('plugin', array('type' => '%plugin1_type%', 'app' => 'forum', 'name' => '%plugin1_name%', 'descr' => '%plugin1_descr%'));*/
		/*\Clay\Module::Object('Plugins')->Setup($plugin);*/
		# Privileges
		# Forum Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Forum', 'Create' );
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Forum', 'Update' );
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Forum', 'Delete' );
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Forum', 'View' );
		# Topic Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Topic', 'Create' );
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Topic', 'Update' );
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Topic', 'Delete' );
		\Clay\Module::Object( 'Privileges' )->register( 'forum', 'Topic', 'View' );

		return true;
	}
	/**
	 * Upgrade
	 * @param integer $version
	 * @return boolean
	 */
	public static function upgrade($version)
	{

		return true;
	}
	/**
	 * Uninstall
	 * @return boolean
	 */
	public static function delete()
	{
		# ClayDB Object
		$dbconn = self::db();
	    $datadict = $dbconn->datadict();
		# Drop Tables
	    $datadict->dropTable( \claydb::$tables['forums'] );
	    $datadict->dropTable( \claydb::$tables['forum_topics'] );
		# Drop Privileges
	    \Clay\Module::Object( 'Privileges' )->remove( 'forum' );
		# Drop Plugins
		\Clay\Module::Object('Plugins')->remove( 'forum' );
		# Drop Settings
		\Clay\Application::Unset( 'forum' );
	    return true;
	}
}
