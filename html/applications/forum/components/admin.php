<?php
/**
* Forum Application
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link %link%
* @author David L Dyess II
*/

namespace application\forum\component;

/**
 *  Forum Admin Component
 * @author David L Dyess II
 *
 */
class admin extends \Clay\Application\Component
{

	/**
	 * Forum Admin View
	 */
	public function view()
	{

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if (!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Heading
		$data['heading'] = \Clay\Application::Setting('forum','heading','');
		# Introduction
		$data['intro'] = \Clay\Application::Setting('forum','intro','');
		# Form Type
		$data['type'] = \Clay\Application::Setting('forum','form.type','frame');
		# Embedded Form HTML
		$data['frame'] = \Clay\Application::Setting('forum', 'form.frame','');
		# Forum Email Address
		$data['email'] = \Clay\Application::Setting('forum', 'email','');
		# Use Map?
		$data['map'] = \Clay\Application::Setting('forum','map','0');
		# Embedded Map HTML
		$data['mapFrame'] = \Clay\Application::Setting('forum','map.frame','');
		# Use Extra Info?
		$data['info'] = \Clay\Application::Setting('forum','info','0');
		# Extra Info
		$infoData = \Clay\Application::Setting('forum','info.data',serialize(array()));
		$data['infoData'] = unserialize($infoData);
		
		return $data;
	}

	/**
	 * Forum Admin Save
	 */
	public function save()
	{

		$user = \Clay\Module::API('User','Instance');
		
		if (!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Heading
		if ($_POST['heading'] !== $_POST['oldHeading']){
			
			\Clay\Application::set('forum','heading',\Clay\Data\post('heading','string','text'));
		}
		# Introduction
		if ($_POST['intro'] !== $_POST['oldIntro']){

			\Clay\Application::set('forum','intro',\Clay\Data\post('intro','string','text'));
		}
		# Forum Email Address
		if ((!empty($_POST['email'])) && ($_POST['email'] !== $_POST['oldEmail'])){

			\Clay\Application::set('forum','email',\Clay\Data\post('email','email','email'));
		}
		# Form Type
		if ($_POST['type'] !== $_POST['oldType']){

			\Clay\Application::set('forum','form.type',\Clay\Data\post('type','string','text'));
		}
		# Embedded Form HTML
		if ((!empty($_POST['frame'])) && ($_POST['frame'] !== $_POST['oldFrame'])){
		
			\Clay\Application::set('forum','form.frame',\Clay\Data\post('frame','string'));
		}
		# Use Extra Info?
		if ($_POST['info'] !== $_POST['oldInfo']){
		
			\Clay\Application::set('forum','info',\Clay\Data\post('info','string','text'));
		}

		# Forum Information Extra Data
		# Reset Info Data
		\Clay\Application::set('forum','info.data','');
		# Save New Info Data
		\Clay\Application::set('forum','info.data',serialize(\Clay\Data\post('infoData','isArray','',array())));

		# Use Map?
		if ($_POST['map'] !== $_POST['oldMap']){
			
			\Clay\Application::set('forum','map',\Clay\Data\post('map','string','text'));
		}
		# Embedded Map HTML
		if ((!empty($_POST['mapFrame'])) && ($_POST['mapFrame'] !== $_POST['oldMapFrame'])){
		
			\Clay\Application::set('forum','map.frame',\Clay\Data\post('mapFrame','string'));
		
		}
		# Show Update Notification
		\Clay\Application::API('system','message','send','Forum Settings Updated');
		# Redirect if not using AJAX submit
		if (empty($_POST['dashForm'])){
			
			\clay::redirect($_SERVER['HTTP_REFERER']);
		}
	}
}