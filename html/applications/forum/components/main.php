<?php
/**
* Forum Application
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link %link%
* @author David L Dyess II
*/

namespace application\forum\component;

/**
 *  Forum Main Component
 * @author David L Dyess II
 *
 */
class main extends \Clay\Application\Component
{

	/**
	 * Forum View
	 */
	public function view()
	{

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');

		/* # Privilege Check
		if (!$user->privilege('forum','Main','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		*/
		
		$data['user'] = $user->Info();
		
		return $data;
	}
}