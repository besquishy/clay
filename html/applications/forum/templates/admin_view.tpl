<div class="c-app__head">
  <h1><a href="<?php echo \Clay\Application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
  Forum Settings</h1>
</div>
<div class="c-app__content">
	<form v-on:submit.prevent="onSubmit('forumSettings', $event)" role="form" id="dashForm" class="forum_settings" action="<?= \Clay\Application::url('forum','admin','save');?>" method="post">
	  <div>
        <fieldset class="c-fieldset">
			<legend>Heading</legend>
            <div class="c-fieldset__fields">
                <div>
                    <label>Application Title (Optional)</label>
                    <input type="text" name="heading" id="heading" class="c-field" placeholder="Forum Us" value="<?= $heading ?>" />
                    <input type="hidden" name="oldHeading" id="oldHeading" value="<?= $heading ?>" />
                </div>
                <div>
                    <label>Introduction (Optional)</label>
                    <textarea name="intro" id="intro" class="c-field" placeholder="Get in touch with us!"><?= $intro ?></textarea>
                    <input type="hidden" name="oldIntro" id="oldIntro" value="<?= $intro ?>" />
                </div>
            </div>
		</fieldset>
		<fieldset class="c-fieldset">
			<legend>Form Settings</legend>
            <div class="c-fieldset__fields">
                <div class="c-alert c-alert--primary">
                    Only Used With Basic Form
                </div>
                <div>
                    <label>Forum Email Address</label>
                    <input type="email" name="email" id="email" class="c-field" value="<?= $email ?>" />
                    <input type="hidden" name="oldEmail" id="oldEmail" value="<?= $email ?>" />
                </div>
                <div>
                    <label>Form Type</label>
                    <select name="type" id="type" class="c-field">
                        <option value="form" <?php if ($type === 'form') echo 'selected="selected"'?> disabled>Basic</option>
                        <option value="frame" <?php if ($type === 'frame') echo 'selected="selected"'?>>Embedded</option>
                    </select>
                    <input type="hidden" name="oldType" id="oldType" value="<?= $type ?>" />
                </div>
                <div class="c-alert c-alert--primary">
                    Paste an Embed Code For Your Form
                </div>
                <div>
                    <label>Embedded Form HTML</label>
                    <textarea name="frame" id="frame" class="c-field"><?= $frame ?></textarea>
                    <textarea name="oldFrame" id="oldFrame" style="display:none;"><?= $frame ?></textarea>
                </div>
            </div>
		</fieldset>
	  </div>
		<fieldset class="c-fieldset">
			<legend>Info</legend>
            <div class="c-fieldset__fields">
                <div>
                    <label>Enable Extra Info?</label>
                    <select name="info" id="info" class="c-field">
                        <option value="0" <?php if ($info === '0') echo 'selected="selected"'?>>No</option>
                        <option value="1" <?php if ($info === '1') echo 'selected="selected"'?>>Yes</option>
                    </select>
                    <input type="hidden" name="oldInfo" id="oldInfo" value="<?= $info?>" />
                </div>
                <div>
                    <label>Office Telephone</label>
                    <div>
                        <input type="telephone" name="infoData[office.phone]" id="office-phone" class="c-field" value="<?= !empty($infoData['office.phone']) ? $infoData['office.phone'] : ''; ?>" />
                        <input type="hidden" name="oldData[office.phone]" id="oldOffice-phone" value="<?= !empty($infoData['office.phone']) ? $infoData['office.phone'] : ''; ?>" />
                    </div>
                </div>
                <div>
                    <label>Fax</label>
                    <div>
                        <input type="telephone" name="infoData[fax]" id="fax" class="c-field" value="<?= !empty($infoData['fax']) ? $infoData['fax'] : ''; ?>" />
                        <input type="hidden" name="oldData[fax]" id="oldFax" value="<?= !empty($infoData['fax']) ? $infoData['fax'] : ''; ?>" />
                    </div>
                </div>
                <div>
                    <label>Cell Phone</label>
                    <div>
                        <input type="telephone" name="infoData[cell]" id="cell" class="c-field" value="<?= !empty($infoData['cell']) ? $infoData['cell'] : ''; ?>" />
                        <input type="hidden" name="oldData[cell]" id="oldCell" value="<?= !empty($infoData['cell']) ? $infoData['cell'] : ''; ?>" />
                    </div>
                </div>
                <div>
                    <label>Twitter</label>
                    <div>
                        <input type="text" name="infoData[twitter]" id="twitter" class="c-field" value="<?= !empty($infoData['twitter']) ? $infoData['twitter'] : ''; ?>" />
                        <input type="hidden" name="oldData[twitter]" id="oldTwitter" value="<?= !empty($infoData['twitter']) ? $infoData['twitter'] : ''; ?>" />
                    </div>
                </div>
                <div>
                    <label>Facebook</label>
                    <div>
                        <input type="text" name="infoData[facebook]" id="facebook" class="c-field" value="<?= !empty($infoData['facebook']) ? $infoData['facebook'] : ''; ?>" />
                        <input type="hidden" name="oldData[facebook]" id="oldFacebook" value="<?= !empty($infoData['facebook']) ? $infoData['facebook'] : ''; ?>" />
                    </div>
                </div>
            </div>
		</fieldset>	
		<fieldset class="c-fieldset">
			<legend>Map</legend>
            <div class="c-fieldset__fields">
                <div>
                    <label>Enable Embedded Map?</label>
                    <select name="map" id="map" class="c-field">
                        <option value="0" <?php if ($map === '0') echo 'selected="selected"'?>>No</option>
                        <option value="1" <?php if ($map === '1') echo 'selected="selected"'?>>Yes</option>
                    </select>
                    <input type="hidden" name="oldMap" id="oldMap" value="<?= $map; ?>" />
                </div>
                <div class="c-alert c-alert--primary">
                    Paste an Embed Code For Your Map
                </div>
                <div>
                    <label>Embedded Map HTML</label>
                    <textarea name="mapFrame" id="mapFrame" class="c-field"><?= $mapFrame; ?></textarea>
                    <textarea name="oldMapFrame" id="oldMapFrame" style="display:none;"><?= $mapFrame ?></textarea>
                </div>
            </div>
		</fieldset>
        <fieldset class="c-fieldset__controls">
            <input type="hidden" name="settings" id="settings" value="1" />
            <button type="submit" name="submit" id="submit" class="c-button c-button--primary dashSubmit"><i class="fa fa-check-circle fa-lg "></i>&emsp;Save Settings</button> 
            <a href="<?= \Clay\Application::URL('forum','admin','view'); ?>" class="dashlink btn btn-default dashEdit">Edit Settings</a>
        </fieldset>
        <fieldset class="c-fieldset__controls">
            <div class="o-grid">
                <div class="o-grid__col u-1/3@sm">
                    <button v-if="saved !== 'forumSettings'" type="submit" class="c-button" name="submit" id="submit"><i class="fa fa-check-circle fa-lg "></i>&emsp;Save Settings</button> 
                    <button v-on:click="toEdit('forumSettings', $event)" v-if="saved == 'forumSettings'" type="button" class="c-button" name="edit" id="edit">Edit Settings</button>
                </div>
                <div class="o-grid__col u-1/3@sm">
                    <div v-if="response == 'forumSettings'" id="forumSettings_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
                    <div v-if="error == 'forumSettings'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
                </div>
            </div>
        </fieldset>
	</form>
</div>