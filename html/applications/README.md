# Clay

## Applications (default)

`/applications` is the default folder used for Clay Applications.

Applications are independent or dependent modules that can interact with other Applications via Plugins or Application APIs.

Applications utilize Classes called Components, which serve as the Controller in Clay's MVC pattern. 

* Components reside in the `\application\[application]\component` namespace. 
* Components extend the `\Clay\Application\Component` Class
* Components reside in the `/components` folder
* Components have corresponding templates located in the `/templates` folder

### Files & Folders

#### info.php

* `info.php`
	* A data file, similar to NPM's package.json
	* Formatted as an array, which is used by the `Apps` application and `\Clay\Application` Library to determine an application's `name`, `version`, `Core` status, and other pertinent organizational data.
	
##### Example

Example `info.php` file from the `Apps` application:

	$data = array('title' => 'Applications',
				'name' => 'apps',
				'version' => '1.0.3',
				'date' => 'October 3, 2017',
				'description' => 'Applications Manager',
				'class' => 'System',
				'category' => 'Administration',
			# Depends on the Setup Module, but declaring it is redundant.
				'depends' => array('applications' => array('dashboard')),
				'core' => TRUE,
				);
				
#### Application Components
				
* `/components`
	* Contains `Application Components`
	* `Application Component` is a single class, in a file with the same name as the class.
	* Application Component Class extends the `\Clay\Application\Component` Class
		* `\Clay\Application\Component` resides in the `/library/Clay/Application/Component.php` file
		* An abstract class
	* Application Component resides in the `\application\[application]\component` namespace
	* An Application Component is declared as &com in a URL
	* A Public Class Function of an Application Component acts as the Action
		* Declared as &act in a URL
	* `\Clay\Application( 'apps', 'main' );`
		* Calls `\application\apps\component\main` class
			* `'main'` is optional and is the default parameter
		* Returns an Object
			* `$object->action( 'view' )` calls the function `\application\apps\component\main->view()`
	* Can specify custom `Application Template` names using `$this->template = 'mytemplate';`
	* Can specify `Theme Page Template` name using `$this->page = 'pagetemplate';`
	* Provides `Application Tempate` data by returning an `Array` with `Key => Value` pairs
		* `$data['name'] = 'This Name';`
		* `return( $data );`
			* Provides `$name` to the template
		
##### Example

Example `main.php` file from the `Apps` application:

	namespace application\apps\component;
	
		/**
		* Apps Application
		*
		* @copyright (C) 2007-2012 David L Dyess II
		* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
		* @link http://clay-project.com
		* @author David L Dyess II (david.dyess@gmail.com)
		*/
		
		/**
		 * Apps Main
		 * @author David
		 *
		 */
		class main extends \Clay\Application\Component {
		
			/**
			 * View
			 */
			public function view(){
				
				return array();
			}
		}
	
#### Application APIs (Libraries)
	
* `/library`
	* Contains `Application APIs`
	* `Application API` is a single Class, in a file with the same name as the class
	* Application API classes are Static classes, which means they are called without creating an object
	* Application API classes are not required to extend another class, which gives you the option to extend other classes if desired
	* Application API resides in the `application\[application]\library` namespace
	* `\Clay\Application::API( 'apps', 'get', 'app', 'dashboard' );`
		* Calls `\application\apps\library\get::app( 'dashboard' );
	* Contains the Installer Class for the Application
		* Class must be named `Setup`
		* File must be named `setup.php`
		* Must include the following Public Static functions:
			* public static function install()
			* public static function upgrade($version)
				* $version == Installed version of the application
			* public static function delete($version)
				* $version == Installed version of the application
	* Contains the Core Class for the Application
		* A Class which allows other parts of Clay to obtain Itemtype and Item data from the application, without knowing that application's specific APIs
		* Class must be named `Core`
		* File must be named `core.php`
	* Application APIs must not circumvent other Application APIs
		
##### Exampe Application Setup API

Example `setup.php` Application Setup API from the Blog application

	namespace application\blog\library;
	
	\Clay\Module('Privileges');
	
	/**
	* ClayCMS
	*
	* @copyright (C) 2007-2017 David L Dyess II
	* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
	* @link http://clay-project.com
	* @author David L Dyess II (david.dyess@gmail.com)
	*/
	class setup {
	
		public static function install(){
			\Library('ClayDB');
			$dbconn = \claydb::connect();
		    $datadict = $dbconn->datadict();
		    $tables = array('blog_posts' => array('table' => 'blog_posts'));
		    $datadict->registerTables($tables);
		    //\claydb::tables('flush');
		    
		    $cols = array();
		    $cols['column'] = array('pid' => array('type' => 'ID'),
		    						'status' => array('type' => 'TINYINT', 'size' => '1'),
		    						'pubdate' => array('type' => 'INT', 'size' => '10', 'null' => FALSE),
		    						'uid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
		    						'title' => array('type' => 'VARCHAR', 'size' => '255'),
		    						'body' => array('type' => 'TEXT'));
		    
		    $cols['index'] = array('status' => array('type' => 'KEY', 'index' => 'status'),
		    						'date' => array('type' => 'KEY', 'index' => 'pubdate'),
		    						'userid' => array('type' => 'KEY', 'index' => 'uid'));
		    
		    $datadict->createTable(\claydb::$tables['blog_posts'],$cols);
	
			# Blog Dashboard Plugin
			$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog', 'descr' => 'Blog Administration.'));
			\Clay\Module::Object('Plugins')->Setup($dashboard);
	
			# Hook Blog Dashboard Plugin to the Dashboard
			$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog'),
			'app' => 'dashboard');
			\Clay\Module::Object('Plugins')->Hook ( $hook );
	
			\Clay\Module::Object('Privileges')->register('blog','Post','Create');
			\Clay\Module::Object('Privileges')->register('blog','Post','Update');
			\Clay\Module::Object('Privileges')->register('blog','Post','Delete');
			\Clay\Module::Object('Privileges')->register('blog','Post','View');
	
			\clay\application::set('blog','title',"Blog");
	
			return true;
		}
		public static function upgrade($version){
	
			switch($version){
				
				case ($version <= '0.1.5'):
					\clay\application::set('blog','title',"Blog");
					
				case ($version <= '1.0'):
					# Blog Dashboard Plugin
					$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog', 'descr' => 'Blog Administration.'));
					\Clay\Module::Object('Plugins')->Setup($dashboard);
				
				case ($version <= '1.0.1'):
					# Hook Blog Dashboard Plugin to the Dashboard
					$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'blog', 'name' => 'blog'),
					'app' => 'dashboard');
					\Clay\Module::Object('Plugins')->Hook ( $hook );
					break;
			}
			return true;
		}
		public static function delete(){
			\Library('ClayDB');
			$dbconn = \claydb::connect();
		    $datadict = $dbconn->datadict();
	
		    $datadict->dropTable(\claydb::$tables['blog_posts']);
	
		    \Clay\Module::Object('Privileges')->remove('blog');
	
		    return true;
		}
	}
		
##### Example Application Core API

Example `core.php` Application Core API from the Blog application

	namespace application\blog\library;
	
	/**
	 * Blog Application
	 *
	 * @copyright (C) 2014-2015 David L Dyess II
	 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
	 * @link http://clay-project.com
	 * @author David L Dyess II (david.dyess@gmail.com)
	 */
	
	/**
	 * Blog Core Library
	 */
	 class Core {
	 	
	 	/**
	 	 * Import self::db() using a Trait \ClayDB\Connection
	 	 */
	 	use \ClayDB\Connection;
	 	
	 	/**
	 	 * Blog Item Types (currently none)
	 	 * @return array
	 	 */
	 	public static function ItemTypes(){
	 		
	 		return array();
	 	}
	 	/**
	 	 * Blog Item Type
	 	 * @param integer $itemType
	 	 * @return array
	 	 */
	 	public static function ItemType( $itemType ){
	 		
	 		return array();
	 	}
	 	/**
	 	 * Blog Posts
	 	 * @param integer $itemType
	 	 * @return array
	 	 */
	 	public static function Items( $itemType = NULL ){
	 		# Get all Blog Posts
	 		$posts = self::db()->get( 'pid, pubdate, uid, title, body FROM '.\claydb::$tables['blog_posts'].' ORDER BY pid ASC' );
	 		
	 		foreach( $posts as $post ){
	 			# Titles are optional, make sure we have a filler
	 			if( empty( $post['title'] )){
	 				
	 				$post['title'] = 'Untitled';
	 			}
	 			$items[] = array( 'id' => $post['pid'],
	 						      'name' => $post['title'],
	 						      'title' => $post['title'],
	 						      'uid' => $post['uid'],
	 							  'itemtype' => NULL,
	 							  'date' => $post['pubdate'],
								   'content' => $post['body']
	 							 );
	 		}
	 		
	 		return !empty($items) ? $items : array();
	 	}
	 	/**
	 	 * Blog Post
	 	 * @param array $args - ([itemtype],[id])
	 	 * @return array
	 	 */
	 	public static function Item( $args ){
	 		
	 		$post = self::db()->get( 'pid, pubdate, uid, title, body FROM '.\claydb::$tables['blog_posts'].' WHERE pid = ? ORDER BY pid ASC', array($args[1]), '0,1' );
	 		
	 		return array( 'id' => $post['pid'],
	 					   'name' => $post['title'],
	 					   'title' => $post['title'],
	 					   'uid' => $post['uid'],
	 				 	   'itemtype' => NULL,
	 					   'date' => $post['pubdate'],
						   'content' => $post['body']
	 					  );
	 	}
	 	/**
	 	 * Blog Fields
	 	 * @param array $args - ([itemtype],[id])
	 	 * @return array
	 	 */
	 	public static function Fields( $args ){
	 		
	 		return array( 'uid', 'pubdate', 'title', 'body' );
		 }
		 /**
		 * Blog Labels
		 * @param string $type - itemtype or item
		 * @return array
		 */
		public static function Labels( $type ){
			
			switch( $type ){
	
				case 'itemtype':
					return NULL;
	
				case 'item':
					return 'POST';
					
				default:
				break;
			}
			return;
		}
	 }
				
##### Example Application API

Example get.php Application API file from the Apps application

	namespace application\apps\library;
	/**
	* Apps Application
	*
	* @copyright (C) 2007-2012 David L Dyess II
	* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
	* @link http://clay-project.com
	* @author David L Dyess II (david.dyess@gmail.com)
	*/
	
	/**
	 * Apps Get API
	 * @author David
	 *
	 */
	class get {
		
		/**
		 * Import self::db() using a Trait \ClayDB\Connection
		 */
		use \ClayDB\Connection;
		
		/**
		 * 
		 * Get All Apps table data
		 * @param NULL
		 * @return array(array) [appid,state,version,name]
		 */
		public static function all(){
	
			return self::db()->get('appid, state, version, name FROM '.\claydb::$tables['apps'].' ORDER BY name ASC');
		}
		
		/**
		 * 
		 * Get All Apps table data from App Name
		 * @param string $name
		 * @return array [appid,state,version,name]
		 */
		public static function app($name){
			
			return self::db()->get('appid, state, version, name FROM '.\claydb::$tables['apps'] .'WHERE name = ?',array($name),'0,1');
		}
		
		/**
		 * 
		 * Get App Name from ID
		 * @param integer $id
		 * @return string name
		 */
		public static function appID($id){
			
			$app = self::db()->get('name FROM '.\claydb::$tables['apps'] .'WHERE appid = ?',array($id),'0,1');
			return $app['name'];
		}
		
		/**
		 * 
		 * Get App ID from Name
		 * @param string $name
		 * @return integer $id
		 */
		public static function appName($name){
			
			$app = self::db()->get('appid FROM '.\claydb::$tables['apps'] .'WHERE name = ?',array($name),'0,1');
			return $app['appid'];
		
		}
		
		public static function itemtypes($app){
			
		}
	}
		
#### Application Plugins

* `/plugins`
	* Contains `Application Plugin Types` folders
		* Each folder contains `Application Plugins` provided by the application of that Plugin Type
	* `Application Plugin` is a single class, in a file with the same name as the class
	* Application Plugin classes extend the `\Clay\Application\Plugin` class
	* Application Plugin resides in the`\application\[application]\plugin\[plugin type]` namespace
	* Application Plugin must be registered in the Application Setup API when an application is installed or upgraded
	
#### Application Privileges

* `/privileges`
	* Contains `Application Privileges` files
	* `Application Privilege` is a single class, in a file with the same name as the class
	* Application Privilege classes extend the `\Clay\Application\Privilege` class
	* Application Privilege resides in the `application\[application]\privilege` namespace
	* Application Privilege must be registered in the Application Setup API when an application is installed or upgraded
	* Application Privileges must not circumvent other Application Privileges
	* Application Privileges are assigned to Roles in the Users application administration
	* Application Privileges are required to provide two class functions
		* `public function Scope($scope)`
			* Used by User Module for checking the current user's assigned privileges compared to this privilege
		* `public function Scopes($privilege, $scope = NULL)`
			* Used by the Privileges Module to assign this privilege to a user
			* References Itemtypes and Items from this Application's Core API to allow the Users Application administration when assigning Privileges to Roles
			
#### Application Templates

* `/templates`
	* Contains `Application Templates` files and `Application Plugin Templates` folders and files
	* `Application Template` is a single `.tpl` file, with the naming schema `class_function.tpl`
	* `Application Plugin Template` resides in a folder named `plugins`, segregated by `Plugin Type` and follows the same naming convention as `Application Templates`
	* Names can be customized by `Application Component` by declaring `$this->template = 'mytemplate'`
		* `mytemplate.tpl`
		* Names can incude a folder name, which is useful for larger applications to use a Class name for the folder and the Function name for the file
			* `$this->template = 'roles/view'` becomes `/roles/view.tpl`
	* The $data array returned by `Application Component` is converted to Variables to be used within it's template
		* `$data['name']` becomes `$name`
		
#### Application Stylesheets

* `/styles`
	* Contains CSS files
	* Used by `\Clay\Styles` Library to include CSS files in the `Theme Page Template` from the application
	
#### Application Scripts

* `/scripts`
	* Contains JavaScript files
	* Used by `\Clay\Scripts` Library to include JavaScript files in the `Theme Page Template` from the application
	
	
