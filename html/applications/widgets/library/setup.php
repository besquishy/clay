<?php
/**
* ClayCMS
*
* @copyright (C) 2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\widgets\library;

\Clay\Module('Privileges');

/**
 * Widgets Application Setup
 */
class setup {
	/**
	 * Install
	 */
	public static function install(){

		# Widgets Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'widgets', 'name' => 'widgets', 'descr' => 'Widgets and 3rd Party API Administraion.'));
		\Clay\Module::Object('Plugins')->Setup($dashboard);

		# Hook Widgets Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'widgets', 'name' => 'widgets'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );

		return true;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function upgrade($version){

		switch($version){
				
			case ($version <= '1.0.1'):
				# Widgets Dashboard Plugin
				$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'widgets', 'name' => 'widgets', 'descr' => 'Widgets and 3rd Party API Administraion.'));
				\Clay\Module::Object('Plugins')->Setup($dashboard);
				# Hook Widgets Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'widgets', 'name' => 'widgets'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );
				break;
		}
		return true;
	}
	/**
	 * Delete
	 */
	public static function delete(){
		
		\Clay\Module::Object('Privileges')->remove('widgets');

	    return true;
	}
}