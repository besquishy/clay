<?php
/**
* Widgets Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\widgets\library;

/**
 * Zillow Wrapper API
 * @author David
 *
 */
class zillow {
    /**
     * Zillow API URL
     * @var string
     */
    public static $zillowURL = 'http://www.zillow.com/webservice/';
    /**
     * Zillow Reviews URL
     * @var string
     */
    public static $reviewsURL = 'ProReviews.htm';
    /**
     * Reviews
     * @param array $args
     */
    public static function reviews($args){

        $args['zws-id'] = \Clay\Application::Setting('widgets','zillow.key');
        
        $url = self::$zillowURL.self::$reviewsURL.'?'.http_build_query($args);
        
        $data = simplexml_load_string(file_get_contents($url));

        return $data;
    }
}