<section class="c-app">
	<div class="c-app__head">
		<h1>
			<a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
			Widgets Settings
		</h1>
	</div>
	<div class="c-app__content">
		<form v-on:submit.prevent="onSubmit('widgetSettings', $event)" role="form" id="dashForm" class="widgets_settings" action="<?= \clay\application::url('widgets','admin','save');?>" method="post">
			<div class="form-group">
			<fieldset class="c-fieldset">
				<legend>API Settings</legend>
					<div class="c-fieldset__fields">
						<label>Zillow API Key</label>
						<input type="text" name="zillow" id="zillow" class="c-field" value="<?= $zillow ?>" />
						<input type="hidden" name="oldZillow" id="oldZillow" value="<?= $zillow ?>" />
					</div>
			</fieldset>
			<fieldset class="c-fieldset__controls">
				<div class="o-grid">
					<div class="o-grid__col u-1/3@sm">
						<button v-if="saved !== 'widgetSettings'" type="submit" class="c-button" name="submit" id="submit"><i class="fa fa-check-circle fa-lg "></i>&emsp;Save Settings</button> 
						<button v-on:click="toEdit('widgetSettings', $event)" v-if="saved == 'widgetSettings'" type="button" class="c-button" name="edit" id="edit">Edit Settings</button>
					</div>
					<div class="o-grid__col u-1/3@sm">
						<div v-if="response == 'widgetSettings'" id="widgetSettings_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
						<div v-if="error == 'widgetSettings'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</section>