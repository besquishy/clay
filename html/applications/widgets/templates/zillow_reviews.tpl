<div>
    <?php if($branding == 'true') { ?>
    <a href="<?= $profile->proInfo->profileURL; ?>"><img style="display:block;margin:0 auto;" src="http://www.zillow.com/widgets/GetVersionedResource.htm?path=/static/logos/Zillowlogo_200x50.gif" width="150" alt="Zillow Real Estate Search"></a>
    <?php } ?>
    <h2 style="text-align:center;"><?= $profile->proInfo->name; ?></h3>
    <div style="float: left;text-align:right;vertical-align:middle;width:50%;margin:auto"><img src="<?= $profile->proInfo->photo; ?>" /></div>
    <div style="float:left;text-align:left;vertical-align:middle;width:50%;margin:auto"><span style="color:gold; font-size:60px;">🌠</span> <span style="font-weight:bold; font-size: 50px"><?= $profile->proInfo->avgRating; ?></span><span> / 5</span> <br> <span style="font-size:20px;"><a href="<?= $profile->proInfo->profileURL; ?>/Reviews"><?= $profile->proInfo->reviewCount; ?> Reviews on Zillow</a></span></div>
    <br style="clear:both;">
</div>
<div>
    <h3>Reviews on Zillow</h4>
    <?php foreach($profile->proReviews->review as $review){ ?>
    <div>
        <h4><span style="color:#74c005;"><?= $stars["$review->rating"]; ?></span><br><?= $review->reviewSummary; ?></h4>
        <span><?= $review->reviewDate; ?></span> | <a href="<?= $review->reviewerLink; ?>" target="_blank"><?= $review->reviewer; ?></a>
        <p>
            <span>Local Knowledge:</span> <span style="color:#74c005;"><?php $rating = intval($review->localknowledgeRating); echo $stars["$rating"]; ?></span> &nbsp; &nbsp;
            <span>Process Expertise:</span> <span style="color:#74c005;"><?php $rating = intval($review->processexpertiseRating); echo $stars["$rating"]; ?></span> &nbsp; &nbsp;
            <span>Responsiveness:</span> <span style="color:#74c005;"><?php $rating = intval($review->responsivenessRating); echo $stars["$rating"]; ?></span> &nbsp; &nbsp;
            <span>Negotiation Skills:</span> <span style="color:#74c005;"><?php $rating = intval($review->negotiationskillsRating); echo $stars["$rating"]; ?></span>
        </p>
        <p><?= $review->description; ?> <span><a href="<?= $review->reviewURL; ?>" target="_blank">Read More</a></span></p>
    </div>
    <hr>
    <?php } ?>
</div>
<div>
    <h4><a href="<?= $profile->proInfo->reviewRequestURL; ?>" target="_blank">Submit a Review for <?= $profile->proInfo->name; ?></a> <?php if((int)$profile->proInfo->reviewCount > (int)$profile->count){?>| <a href="<?= $profile->proInfo->profileURL; ?>/Reviews" target="_blank">Read <?= $profile->proInfo->reviewCount - 10; ?> More Reviews</a><?php } ?></h4>
    <?php if($branding == 'true') { ?>
    <a href="<?= $profile->proInfo->profileURL; ?>"><img src="http://www.zillow.com/widgets/GetVersionedResource.htm?path=/static/logos/Zillowlogo_200x50.gif" width="150" alt="Zillow Real Estate Search"></a> © Zillow, Inc., 2006-2016. Use is subject to <a href="https://www.zillow.com/corp/Terms.htm">Terms of Use</a>. <a href="https://www.zillow.com/wikipages/What-is-a-Zestimate/">What's a Zestimate?</a>
    <?php } ?>
</div>