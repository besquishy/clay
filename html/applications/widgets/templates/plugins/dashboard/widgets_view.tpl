<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<i class="fa fa-gears dash-icon-lg"></i> Widgets
		</div>
	</div>
	<div class="c-card__content">
		<?php if(!empty($admin))?><a href="<?= $admin; ?>" class="dashlink">Settings</a>
	</div>
</div>