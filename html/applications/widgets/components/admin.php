<?php
/**
* Widgets Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\widgets\component;

/**
 *  Widgets Admin Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component {

    /**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
    /**
     * View
     */
    public function view(){

        if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

        $this->pageTitle = 'Widgets Settings';
		$data = array();
		# Zillow API Key
		$data['zillow'] = \clay\application::setting('widgets','zillow.key');

        return $data;
    }
    /**
     * Save
     */
    public function save(){

        # Zillow API Key
        if($_POST['zillow'] !== $_POST['oldZillow']){
        
            \clay\application::set('widgets','zillow.key',\clay\data\post('zillow','string','text'));
        }

        \clay\application::api('system','message','send','Widgets Settings Updated');
			
		if(empty($_POST['dashForm'])){
			
			\clay::redirect($_SERVER['HTTP_REFERER']);
		}
    }
}