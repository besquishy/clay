<?php
/**
* Widgets Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\widgets\component;

/**
 *  Widgets Zillow Component
 * @author David
 *
 */
class zillow extends \Clay\Application\Component {
    /**
     * Reviews
     */
    public function reviews(){

        header("Access-Control-Allow-Origin: *");

        $this->page = 'app';
        
        $args['screenname'] = \Clay\Data\Get('screenname','string','string');
        $args['count'] = \Clay\Data\Get('count','int','int', '10');
        $args['returnTeamMemberReviews'] = \Clay\Data\Get('returnTeamMemberReviews','bool','', 'false');
        
        $data['branding'] = \Clay\Data\Get('branding','string','string','true');

        if(empty($args['screenname'])) throw new \Exception('Zillow Username Required (&zuser=)');

        $data['profile'] = \Clay\Application::API('widgets', 'zillow', 'reviews', $args)->response->result;

        $data['stars'] = array('0' => '', '1' => '★', '2' => '★★', '3' => '★★★', '4' => '★★★★', '5' => '★★★★★');

        if($args['returnTeamMemberReviews'] == 'true') $this->template = 'zillow_team';

        return $data;
    }
    /**
     * Test
     */
    public function test(){

        return;
    }
}