<?php
/**
 * Widgets Application
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\widgets\plugin\dashboard;

/**
 * Widgets Application Dashboard Plugin
 */
class widgets extends \Clay\Application\Plugin {
	/**
	 * Plugin Application
	 * @var string
	 */
	public $PluginApp = 'widgets';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'dashboard';
	/**
	 * Plugin Name
	 * @var string
	 */
	public $Plugin = 'widgets';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function view( $args ){
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if(\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			$data['admin'] = \clay\application::url('widgets','admin');
		
		} else {
			
			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}
		
		return $data;
	}
	/**
	 * Stats - Info - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function stats( $args ){
	
		
	}
	/**
	 * Display - to be displayed on an item page
	 * @param array $args
	 * @return string
	 */
	public function display( $args ){

		
	}
	/**
	 * Add Item
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		
		
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		
	}
}