<?php
/**
 * Widgets Application
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Widgets',
			'name' => 'widgets',
			'version' => '1.0.2',
			'date' => 'September 16, 2017',
			'description' => 'Widgets to be used within Clay or on external pages.',
			'class' => 'User',
			'category' => 'Publishing',
			'depends' => array('applications' => array('dashboard')),
			'core' => FALSE,
			'install' => TRUE
			);
?>