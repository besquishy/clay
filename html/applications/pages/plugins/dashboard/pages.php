<?php

/**
* Pages Dashboard Service
*
* @copyright (C) Pages David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\pages\plugin\dashboard;

class pages extends \Clay\Application\Plugin
{
	/**
	 * Plugin App
	 * @var string
	 */
	public $PluginApp = 'pages';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'dashboard';
	/**
	 * Plugin Name
	 * @var string
	 */
	public $Plugin = 'pages';
	/**
	 * Pages Dashboard
	 */
	public static function view($args)
	{
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('pages','Page','View')) {
			
			$data['view'] = \clay\application::url('pages','pages','view');
		}
		
		if($user->privilege('pages','Page','Create')) {
			
			$data['new'] = \clay\application::url('pages','pages','create');
		}
		
		if($user->privilege('pages','Page','Manage')) {
			
			$data['manage'] = \clay\application::url('pages','pages','manage');
		}
		
		if($user->privilege('pages','Page','Admin')) {
			
			$data['admin'] = \clay\application::url('pages','admin');
		}
		
		if(empty($data)){
			
			$data['count'] = \clay\application::api('pages','pages','count');
		}
		
		return $data;
	}

}