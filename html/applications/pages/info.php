<?php
/**
 * Clay Framework
 *
 * @copyright (C) 2007-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Installer
 */
$data = array('title' => 'Pages',
			'name' => 'pages',
			'version' => '0.9',
			'date' => 'March 31, 2018',
			'description' => 'Dynamic and Static Page Creator',
			'class' => 'User',
			'category' => 'Publishing',
			'core' => FALSE,
			);
?>