<?php
/**
* Pages Application
*
* @copyright (C) 2013-2018 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\pages\library;

/**
 * Page API
 * @author David
 *
 */
class Page
{
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;

	/**
	 * Create a Page
	 * @param array $args
	 */
	public static function create($args)
	{
		
		if(empty($args['options'])){
			
			$args['options'] = array();
		}
		
		$args['options'] = serialize($args['options']);
		
		if(empty($args['parent'])){
			
			$args['parent'] = null;
		}

		if(empty($args['ptid'])){
			
			$args['ptid'] = '1';
		}
		
		return self::db()->add(\claydb::$tables['pages']." (status, pubdate, uid, parent, ptid, title, options, body) VALUES (?,?,?,?,?,?,?,?)",
							array($args['status'],$args['date'],$args['userid'],$args['parent'],$args['ptid'],$args['title'],$args['options'],$args['body'])
						);
	}
	
	/**
	 * Update a Page
	 * @param array $args
	 */
	public static function update($args)
	{
		
		if(empty($args['options'])){
			
			$args['options'] = array();
		}
		
		$args['options'] = serialize($args['options']);

		if(empty($args['parent'])){
			
			$args['parent'] = null;
		}
		
		if(empty($args['ptid'])){
			
			$args['ptid'] = '1';
		}
		
		return self::db()->update(\claydb::$tables['pages']." SET status = ?, pubdate = ?, uid = ?, parent = ?, title = ?, options = ?, body = ? WHERE pgid = ?",
								array($args['status'],$args['date'],$args['userid'],$args['parent'],$args['title'],$args['options'],$args['body'], $args['pgid']),1
						);
	}
	
	/**
	 * Delete a Page
	 * @param array $args
	 */
	public static function delete($args)
	{
		
		return self::db()->delete(\claydb::$tables['pages']." WHERE pgid = ?",
								array($args['pgid']),1
						);
	}

	/**
	 * Page Input Form
	 * @param array $args
	 */
	public static function input($args)
	{
		
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		
		if(!empty($args['data']['pgid'])) {
			
			$action = \clay\application::url('pages','page','update');
			
		} else {
			
			$action = \clay\application::url('pages','page','create');
		}
		
		$form->attributes(array('method' => 'POST', 'action' => $action));
		$form->container('set1','fieldset')->legend = !empty($args['data']['pgid']) ? 'Edit Page' : 'New Page';
		$title = array('id' => 'title', 'name' => 'title','title' => 'Enter a page title.','class' => 'input-xlarge');
		$title['value'] = !empty($args['data']['title'])? $args['data']['title'] : '';
		$form->container('set1')->property('title','text')->attributes($title);
		$form->container('set1')->property('title')->label = "Title";
		$body = array('id' => 'body', 'name' => 'body','title' => 'Enter page text.','class' => 'c-field', 'rows' => 15);
		$form->container('set1')->property('body','textarea')->attributes($body);
		$form->container('set1')->property('body')->content = !empty($args['data']['body'])? $args['data']['body'] : '';
		$form->container('set1')->property('body')->label = "Text";
		
		if(!empty($args['data']['pgid'])) {
			
			$pgid = array('type' => 'hidden', 'name' => 'pgid', 'id' => 'pgid', 'value' => $args['data']['pgid']);
			$form->container('set1')->property('pgid','input')->attributes($pgid);
		}
		
		$form->container('footer','fieldset')->property('submit','submit')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'class' => 'c-button--primary', 'value' => !empty($args['data']['pgid']) ? 'Update Page' : 'Create Page'));
		
		return $form;
	}
	
	/**
	 * Get a Page
	 * @param array $args
	 * @throws \Exception
	 */
	public static function get($args=array())
	{
		
		if(empty($args['pgid'])){
			
			Throw new \Exception('A numeric Page ID (pgid) must be supplied');
		}
		
		$page = self::db()->get('pgid, status, pubdate, uid, parent, ptid, title, options, body FROM '.\claydb::$tables['pages'].' WHERE pgid = ?',array($args['pgid']),'0,1');
	
		return $page;
	}
	
	/**
	 * Get Page Info
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getInfo($args=array())
	{
		
		if(empty($args['pgid'])){
			
			Throw new \Exception('A numeric Page ID (pgid) must be supplied');
		}
		
		$page = self::db()->get('pubdate, uid, title, ptid FROM '.\claydb::$tables['pages'].' WHERE pgid = ?',array($args['pgid']),'0,1');
		
		return $page;
	}
	
	/**
	 * Get Page Author's User ID
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getAuthor($args=array())
	{
		
		if(empty($args['pgid'])){
			
			Throw new \Exception('A numeric Page ID (pgid) must be supplied');
		}
		
		$page = self::db()->get('uid FROM '.\claydb::$tables['pages'].' WHERE pgid = ?',array($args['pgid']),'0,1');
		
		return $page;
	}
}