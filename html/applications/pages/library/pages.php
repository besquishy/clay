<?php
/**
* Page Application
*
* @copyright (C) 2013-2018 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\pages\library;

/**
 * Pages API
 * @author David
 *
 */
class Pages
{
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All (or some) Pages
	 * @param array $args
	 * @return array
	 */
	public static function getAll($args=array())
	{
		
		# Select a range starting number using 'startnum'
		$offset = !empty($args['startnum']) ? $args['startnum'] : '0';
		# Fetch 'items' number of Pages, offset by 'startnum' if applicable
		if(!empty($args['items'])){
			
			if(!empty($args['startnum'])){
				
				$limit = $args['startnum'].','.$args['items'];
				
			} else {
				
				$limit = '0,'.$args['items'];
			}
			
		} else {
			
			$limit = '';
		}
		# Return associative array
		return self::db()->get('pgid, status, pubdate, uid, parent, ptid, title, options, body FROM '.\claydb::$tables['pages'].' WHERE status = ? ORDER BY pubdate DESC',array(1),$limit);
	}
	
	/**
	 * Count Pages
	 * @param array $args - inert
	 * @return int
	 */
	public static function count($args)
	{

		$count = self::db()->get('COUNT(*) as pages FROM '.\claydb::$tables['pages']);
		return $count[0]['pages'];
	}
}