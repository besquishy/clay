<?php
/**
 * Pages Application
 *
 * @copyright (C) 2013-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\pages\library;

/**
 * Pages Application Core Library
 */
 class Core {
 	
 	/**
 	 * Import self::db() using a Trait \ClayDB\Connection
 	 */
 	use \ClayDB\Connection;
 	
 	/**
 	 * Pages Item Types (currently none)
 	 * @return array
 	 */
 	public static function ItemTypes(){
 		
 		return array();
 	}
 	/**
 	 * Pages Item Type
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function ItemType( $itemType ){
 		
 		return array();
 	}
 	/**
 	 * Pages Posts
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function Items( $itemType = NULL ){
        $pages = \Clay\Application::API('pages','pages','getAll');
 		
 		foreach( $pages as $page ){
 			# Titles are optional, make sure we have a filler
 			if( empty( $page['title'] )){
 				
 				$page['title'] = 'Untitled';
 			}
 			$items[] = array( 'id' => $page['pgid'],
 						      'name' => $page['title'],
 						      'title' => $page['title'],
 						      'uid' => $page['uid'],
 							  'itemtype' => $page['ptid'],
 							  'date' => $page['pubdate'],
							   'content' => $page['body']
 							 );
 		}
 		
 		return !empty($items) ? $items : array();
 	}
 	/**
 	 * Pages Post
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Item( $args ){
 		
 		$page = \Clay\Application::API('pages','page','get', array('pgid' => $args[1]));
 		
 		return array( 'id' => $page['pgid'],
 					   'name' => $page['title'],
 					   'title' => $page['title'],
 					   'uid' => $page['uid'],
 				 	   'itemtype' => $page['ptid'],
 					   'date' => $page['pubdate'],
					   'content' => $page['body']
 					  );
 	}
 	/**
 	 * Pages Fields
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Fields( $args ){
 		
 		return array( 'uid', 'pubdate', 'title', 'body' );
	 }
	 /**
	 * Pages Labels
	 * @param string $type - itemtype or item
	 * @return array
	 */
	public static function Labels( $type ){
		
		switch( $type ){

			case 'itemtype':
				return 'Page Type';

			case 'item':
				return 'Page';
				
			default:
			break;
		}
		return;
	}
 }