<?php
namespace application\pages\library;

/**
* Pages Application
*
* @copyright (C) 2013-2018 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * Pages Installer
 * @author David
 *
 */
class Setup {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Install
	 */
	public static function Install(){
		
		$datadict = self::db()->datadict();
		# Register Database Tables
	    $tables = array('pages' => array('table' => 'pages'), 'page_types' => array('table' => 'page_types'));
	    $datadict->registerTables($tables);
	    \claydb::tables('flush');
	    # Pages Database Table
	    $pages = array();
	    $pages['column'] = array('pgid' => array('type' => 'ID'),
	    						'status' => array('type' => 'TINYINT', 'size' => '1'),
	    						'pubdate' => array('type' => 'INT', 'size' => '10', 'null' => FALSE),
	    						'uid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    						'parent' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => TRUE),
	    						'ptid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE, 'default' => '1'),
	    						'title' => array('type' => 'VARCHAR', 'size' => '255'),
	    						'options' => array('type' => 'VARCHAR', 'size' => '255'),
	    						'body' => array('type' => 'TEXT'));
	    
	    $pages['index'] = array('status' => array('type' => 'KEY', 'index' => 'status'),
	    						'date' => array('type' => 'KEY', 'index' => 'pubdate'),
	    						'userid' => array('type' => 'KEY', 'index' => 'uid'),
	    						'title' => array('type' => 'KEY', 'index' => 'title'),
	    						'type' => array('type' => 'KEY', 'index' => 'ptid'),
								'parent' => array('type' => 'KEY', 'index' => 'parent'),
								'body' => array( 'type' => 'FULLTEXT', 'index' => 'body' ));
	    
	    $datadict->createTable(\claydb::$tables['pages'],$pages);
	    # Page Types Database Table
	    $ptypes = array();
	    $ptypes['column'] = array('ptid' => array('type' => 'ID'),
	    						  'title' => array('type' => 'VARCHAR', 'size' => '50'),
	    						  'descr' => array('type' => 'VARCHAR', 'size' => '255'),
	    						  'settings' => array('type' => 'VARCHAR', 'size' => '255'));
	    
	    $datadict->createTable(\claydb::$tables['page_types'],$ptypes);
		# Register Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'pages', 'name' => 'pages', 'descr' => 'Pages Administration.'));
		# Hook Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'pages', 'name' => 'pages'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		\Clay\Module::Object('Plugins')->Setup($dashboard);
		# Register Privileges
		\Clay\Module::Object('Privileges')->register('pages','Page','Create');
		\Clay\Module::Object('Privileges')->register('pages','Page','Update');
		\Clay\Module::Object('Privileges')->register('pages','Page','Delete');
		\Clay\Module::Object('Privileges')->register('pages','Page','View');

		return true;
	}
	
	/**
	 * Upgrade
	 * @param string $version
	 */
	public static function Upgrade($version){
		
		
	}
	
	/**
	 * Delete
	 * @param string $version
	 */
	public static function Delete(){
		
		$datadict = self::db()->datadict();
		# Drop Tables
	    $datadict->dropTable(\claydb::$tables['pages']);
		$datadict->dropTable(\claydb::$tables['page_types']);
		# Drop Privileges
	    \Clay\Module::Object('Privileges')->remove('pages');
	    # Drop Hooks
		\Clay\Module::Object( 'Plugins' )->Remove( 'pages' );

	    return true;
	}
}