<script type="text/javascript">
$('document').ready(function(){
	$('.carousel').carousel({
	  interval: 5000
	});
});
</script>
<div id="myCarousel" class="carousel slide">
  <!-- Carousel items -->
  <div class="carousel-inner">
    <div class="active item">
    	<img src="<?= \Clay\Application::Image('soho','outer_verge_pd.png',true); ?>" alt="" />
    	<div class="carousel-caption">
    		<h4>Professional Photography and Graphic Design</h4>
    	</div>
    </div>
    <div class="item">
    	<img src="<?= \Clay\Application::Image('soho','ov_nightscape.jpg',true); ?>" alt="" />
    	<div class="carousel-caption">
    		<h4>Artistic Photography</h4>
    	</div>
    </div>
    <div class="item">
    	<img src="<?= \Clay\Application::Image('soho','ov_babes.jpg',true);?>" alt="" />
    	<div class="carousel-caption">
    		<h4>Portraits and Events</h4>
    	</div>
    </div>
    <div class="item">
    	<img src="<?= \Clay\Application::Image('soho','ov_bizcard.jpg',true);?>" alt="" />
    	<div class="carousel-caption">
    		<h4>Graphic Design and Branding</h4>
    	</div>
    </div>
  </div>
  <!-- Carousel nav -->
  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
</div>