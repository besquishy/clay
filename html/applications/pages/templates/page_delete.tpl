<div class="c-app__head">
  <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
  <a href="<?= \clay\application::url('pages','pages','manage');?>" class="dashlink" title="Manage Users">Manage Pages</a> / 
  Delete Page</h1>
</div>
<div class="c-app__content pagesEdit">
<form class="c-form" id="pagesDelete" action="<?= \clay\application::url('pages','page','delete'); ?>" method="POST">
<fieldset class="c-fieldset">
<legend>Are you sure you want to delete this page? (Page ID: <?= $pgid; ?>) <?= $title; ?></legend>
<input type="hidden" name="pgid" id="pgid" value="<?= $pgid;?>" />
<input type="submit" name="submit" id="submit" class="c-button c-button--danger" value="Yes, Delete it." /> <a class="c-button c-button--secondary" href="<?= \clay\application::url('pages','pages','manage'); ?>">No, Cancel.</a>
</form>
</div>