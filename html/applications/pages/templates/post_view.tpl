<div class="app-head">
  <h1>Blog</h1>
</div>
<?php //if(!empty($new)) echo '<div style="width:300px;">'.$new->template($post).'</div>'; ?>
<div class="app-content">
<h2><?php echo $post['title']; ?></h2>
<?php $date = date('d F Y',$post['pubdate']); ?>	
	<div class="accent accent-outline">Posted by <?php echo $user;?> on <?php echo $date.' at '.date('g:i a',$post['pubdate']); ?></div>
	<div class="accent-outline blog-post">		
		<div class="item-text">
			<?php echo $post['body']; ?>
		</div>
	</div>
</div>