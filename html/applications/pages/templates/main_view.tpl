<div class="c-app__head">
  <h1>Pages</h1>
</div>
<div class="c-app__content">
	<?php if (!empty($pages)) { foreach ($pages as $page){ ?>
	<div class="accent">
		<h2><a href="<?= \Clay\Application::URL('pages','page','view',array('pgid' => $page['pgid']));?>"><?= $page['title'] ?></a></h2>
	</div>
	<?php } } else { ?>
	<h3>No pages exist!</h3>
	<?php } ?>
</div>