<div class="c-app__head">
  <h1><a href="<?php echo \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
  <a href="<?php echo \clay\application::url('pages','pages','manage');?>" class="dashlink" title="Manage Users">Manage Pages</a> / 
  Page Editor</h1>
</div>
<div class="c-app__content pageEdit">
<?php $form->template(); ?>
</div>