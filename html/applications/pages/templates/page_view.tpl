<div class="c-app">
	<div class="u-p@sm u-bgcolor-accent-alt c-app__head">
		<h1><?= $title ?></h1>
	</div>
	<div class="c-app__content">
			<?= $body; ?>
		<div class="u-mt@sm u-p@sm u-bgcolor-info">Published on <?= $date; ?></div>
	</div>
</div>