<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
		<i class="fa fa-file dash-icon-lg"></i> Pages
		</div>
	</div>
	<div class="c-card__content">
		<?php if(!empty($manage))?><a href="<?php echo $manage; ?>" class="dashlink">Manage Pages</a><br>	
		<?php if(!empty($new))?><a href="<?php echo $new; ?>" class="dashlink">Create a Page</a><br>
		<?php if(!empty($view))?><a href="<?php echo $view; ?>">View Pages</a><br>
		<?php if(!empty($admin))?><a href="<?php echo $admin; ?>" class="dashlink">Settings</a><br>
	</div>
</div>