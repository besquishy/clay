<div class="c-app__head">
  <h1><a href="<?php echo \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
  Manage Pages Pages</h1>
</div>
<div class="c-app__content">
	<p><a class="c-button c-button--primary" href="<?php echo \clay\application::url('pages','pages','create');?>">Add Page</a></p>
<?php if(!empty($pages)) { ?>
	<table class="c-table c-table--border c-table--striped">
		<tr class="c-table__row">
			<th class="c-table__head">ID</th>
			<th class="c-table__head">Author</th>
			<th class="c-table__head">Published</th>
			<th class="c-table__head">Title</th>
			<th class="c-table__head">options</th>
		</tr>
		<?php foreach($pages as $page) { ?>
		<tr class="c-table__row">
			<td class="c-table__cell"><?php echo $page['pgid']; ?></td>
			<td class="c-table__cell"><?php echo $page['author'] . ' (id:' . $page['uid'] . ')'; ?></td>
			<td class="c-table__cell"><?php echo date('d M y g:i a',$page['pubdate']); ?></td>
			<td class="c-table__cell"><?php echo empty($page['title']) ? 'untitled' : $page['title']; ?></td>
			<td class="c-table__cell"><a class="dashlink" href="<?php echo \Clay\Application::url('pages','page','edit',array('pgid' => $page['pgid'])); ?>" title="Edit">Edit</a> 
				<a class="dashlink" href="<?php echo \Clay\Application::url('pages','page','delete',array('pgid' => $page['pgid'])); ?>" title="Delete">Delete</a>
			</td>
		</tr>
		<?php } ?>
	</table>
<?php } else { ?>
<h3>No pages have been created yet!</h3>
<?php } ?>
</div>


