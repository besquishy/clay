<?php
namespace application\pages;
/**
* ClayCMS
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

class tests extends \Clay\Application\Component {

	public function view(){
		$this->pageTitle = 'Pages';
		$data['url'] = \clay\application::url('pagex');
		\Library('Clay/Styles');
		/*$styles = \clay\styles::getInstance();
		$styles->addApplication('pagex','styles.css');
		$styles->addApplication('pagex','stylx.css');*/
		\clay\styles::addApplication('pagex','styles.css');
		\clay\styles::addApplication('pagex','stylx.css');
		\Library('Clay/Scripts');
		//$scripts = \clay\scripts::getInstance();
		\clay\scripts::addApplication('common','jquery/jquery-1.4.2.min.js');

		//$db = \clay\database::connect('default');
		//$data['test'] = $db->get('* FROM profiles');
		$user = \Clay\Module\User::Instance();
		//$_SESSION['userid'] = 3;
		if($user->privilege('users','view','users')) $data['priv_test'] = true;
		return $data;
	}

}