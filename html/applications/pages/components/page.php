<?php
/**
* Pages Application
*
* @copyright (C) 2013-2018 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\pages\component;

/**
 * Page Component
 * @author David
 *
 */
class page extends \Clay\Application\Component
{

	/**
	 * View Action
	 * @param array $args - inert
	 */
	public function view($args)
	{
		$this->pageTitle = 'Pages';		
		$user =\Clay\Module::API('User','Instance');
		$pgid = \Clay\Data\Get('pgid','int','int');
		$page = \Clay\Application::API('pages','page','get',array('pgid' => $pgid));
		$data['title'] = $page['title'];
		$data['date'] = date('d M y',$page['pubdate']);
		$data['body'] = $page['body'];
		$data['author'] = \Clay\Module::API('Users','getUser',$page['uid']);
		return $data;
	}
	
	/**
	 * Create Page
	 */
	public function create()
	{
		
		$user = \Clay\Module::API('User','Instance');

		if($user->privilege('pages','Page','Create')) {

			$args['status'] = \Clay\Data\post('status','int','int',1);
			$args['ptid'] = \Clay\Data\post('ptid','int','int',1);
			$args['date'] = \Clay\Data\post('date','num','num',time());
			$args['userid'] = $user->id();
			$args['title'] = substr(\Clay\Data\post('title','string'),0,155);
			$args['body'] = \Clay\Data\post('body','string');

			if(!empty($args['body'])) {

				# Preserve original content for hooks
				$params = $args;
				# Load up assigned hooks
				$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'pages' );
				# Call the Transform hook in case plugins have processing
				foreach( $hooks as $tkey => $type ){

					foreach ( $type as $pkey => $plugin ){

						$proc[$tkey] = $plugin['object']->Process( 'transform', array('app' => 'pages', 'itemtype' => NULL, 'itemid' => NULL, 'content' => $args['body'] ));
					}
				}
				# This can be expanded later to look for different types of processing
				# If an editor is hooked and supplied processed text, we'll update our text
				if( !empty( $proc['editor'] ) AND ( $proc['editor']['content'] !== $args['body'] )){
					# Update body to match processed content
					$args['body'] = $proc['editor']['content'];
				}
				
				$pgid = \Clay\Application::api('pages','page','create',$args);

				# Creates the Markdown copy of the unstyled text
				# Run this after the page is created so we don't have to worry about duplicates
				foreach( $hooks as $type ){

					foreach ( $type as $plugin ){

						$plugin['object']->Process( 'create', array('app' => 'pages', 'itemtype' => $params['ptid'], 'itemid' => $pgid, 'content' => $params['body'] ));
					}
				}
				
			} else {
				
				\Clay\Application::api('system','message','send','Although a Title is optional, all pages pages must have Text.');
				
				return \Clay\Application::redirect('pages','pages','create');
			}
			
			\Clay\Application::api('system','message','send',"Page (ID: ".$pgid.') '.substr($args['title'],0,50).' - Created Successfully! <p>Go to <a href="'.\Clay\Application::url('pages','pages','manage').'" class="dashlink">Pages Manager</a>');
		}
		
		return \Clay\Application::redirect('pages');
	}
	
	/**
	 * Page Edit
	 * @param array $args
	 * @throws \Exception
	 */
	public function edit($args)
	{
		
		$user = \Clay\Module::API('User','Instance');
	
		$pgid = \Clay\Data\get('pgid','int','int',!empty($args) ? $args : '');
	
		if(!$user->privilege('pages','Page','Update',$pgid)) throw new \Exception('Additional Privileges Are Required to View This Page.');
		
		$this->page = 'dashboard';

		$page = \Clay\Application::api('pages','page','get',array('pgid' => $pgid));
		
		$page['title'] = \Clay\Data\filter('text',$page['title']);

		$page['hooks'] = \Clay\Module::Object( 'Plugins' )->Hooks( 'pages' );

		if( !empty( $page['hooks']['editor']['markdown/markdown'] )){
			# We'll use the preformatted Markdown text instead of the processed HTML
			$body = $page['hooks']['editor']['markdown/markdown']['object']->Process( 'editor',
				array( 'app' => 'pages', 'itemtype' => $page['ptid'], 'itemid' =>$pgid ));
			# Preformatted Markdown text is stored
			if( !is_null( $body['content'] )){
				# Markdown has a preformatted version of this body
				$page['body'] = $body['content'];
			# No Preformatted Markdown text is stored
			} else {
				# If Markdown is enabled, strip all tags from the body so it can be formatted and stored as Markdown -> HTML
				$page['body'] = \clay\data\filter('noTags', $page['body']);
			}
		}
		
		$page['body'] = $page['body'];
		
		return array('page' => $page, 'form' => \Clay\Application::api('pages','page','input',array('data' => $page)));
	}
	
	/**
	 * Page Update
	 * @param array $args
	 * @throws \Exception
	 */
	public function update($args)
	{
	
		$user = \Clay\Module::API('User','Instance');
	
		$args['pgid'] = \Clay\Data\post('pgid','int','int');
		
		if(!$user->privilege('pages','Page','Update',$args['pgid'])) throw new \Exception('Additional Privileges Are Required to Update This Item.');
		$args['status'] = \Clay\Data\post('status','int','int',1);
		$args['date'] = \Clay\Data\post('date','num','num',time());
		$args['userid'] = $user->id();
		$args['ptid'] = \Clay\Data\post('ptid','int','int',1);
		$args['title'] = \Clay\Data\post('title','string');
		$args['body'] = \Clay\Data\post('body','string');

		if(!empty($args['body'])) {

			# Preserve original content for hooks
			$params = $args;
			# Load up assigned hooks
			$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'pages' );
			# Call the Transform hook in case plugins have processing
			foreach( $hooks as $tkey => $type ){

				foreach ( $type as $pkey => $plugin ){

					$proc[$tkey] = $plugin['object']->Process( 'transform', array('app' => 'pages', 'itemtype' => NULL, 'itemid' => NULL, 'content' => $args['body'] ));
				}
			}
			# This can be expanded later to look for different types of processing
			# If an editor is hooked and supplied processed text, we'll update our text
			if( !empty( $proc['editor'] ) AND ( $proc['editor']['content'] !== $args['body'] )){
				# Update body to match processed content
				$args['body'] = $proc['editor']['content'];
			}
			
			\Clay\Application::api('pages','page','update',$args);

			foreach( $hooks as $type ){

				foreach ( $type as $plugin ){

					$plugin['object']->Process( 'update', array('app' => 'pages', 'itemtype' => $params['ptid'], 'itemid' => $params['pgid'], 'content' => $params['body'] ));
				}
			}
			
		} else {
			
			\Clay\Application::api('system','message','send','Although a Title is optional, all pages pages must have Text.');
			
			return \Clay\Application::redirect('pages','page','edit',array('pgid' => $args['pgid']));
		}
		
		\Clay\Application::api('system','message','send',"Page (ID: ".$args['pgid'].') '.$args['title'].' - Updated Successfully! <p>Go to <a href="'.\Clay\Application::url('pages','pages','manage').'" class="dashlink">Pages Manager</a>');
		
		return \Clay\Application::redirect('pages');
	}
	
	/**
	 * Page Delete
	 * @param array $args
	 * @throws \Exception
	 */
	public function delete($args)
	{
		
		$pgid = \Clay\Data\fetch('pgid','int','int',!empty($args)?$args:'');
		
		if(empty($pgid)) throw new \Exception('A Page ID (int) is Required to Perform This Action.');
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('pages','Page','Delete',$pgid)){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		
		$this->page = 'dashboard';

		$page = \Clay\Application::api('pages','page','get',array('pgid' => $pgid));
		
		$page['title'] = \Clay\Data\filter('text',$page['title']);
		
		if(!empty($_GET['pgid']) || !empty($args)){
			
			if(empty($page['title'])) $page['title'] = 'untitled';
			
			return array('pgid' => $page['pgid'], 'title' => $page['title']);
		}
		$title = !empty($page['title']) ? $page['title'] : 'untitled';

		# Delete Items that may have been created by Plugins (Hooks)
		# Load up assigned hooks
		/** @TODO Use the itemid here Hooks('blog', null, $pid); */
		$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'pages' );
		foreach( $hooks as $tkey => $type ){

			foreach ( $type as $pkey => $plugin ){

				$proc[$tkey] = $plugin['object']->Process( 'delete', array('app' => 'pages', 'itemtype' => $page['ptid'], 'itemid' => $pgid ));
			}
		}
		
		\Clay\Application::api('pages','page','delete',array('pgid' => $pgid));
		\Clay\Application::api('system','message','send',"Page (ID: $pgid) $title - Deleted Successfully! <p>Go to <a href=".'"'.\Clay\Application::url('pages','pages','manage').'" class="dashlink">Pages Manager</a>');	
		
		return \Clay\Application::redirect('pages');
	}
}