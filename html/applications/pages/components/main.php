<?php
namespace application\pages\component;
/**
* Pages Application
*
* @copyright (C) 2013-2018 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * Main Component
 * @author David
 *
 */
class main extends \Clay\Application\Component
{
	/**
	 * View Action
	 * @param array $args - inert
	 */
	public function view($args)
	{		
		$data = array();
		$this->pageTitle = 'Pages';		
		$user =\Clay\Module::API('User','Instance');
		if (!$user->privilege('pages','Page','View')){
			
			#throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		
		$startnum = \clay\data\get('startnum','int','int',1) - 1;
		$pagesCount = \Clay\Application::api('pages','pages','count');
		# Items per page @todo: Make items per page a Pages setting in the Dashboard
		$perPage = 10;
		$pages = \Clay\Application::api('pages','pages','getAll',array('items' => $perPage, 'startnum' => $startnum));
		//die(var_dump($pages));
		if ($user->privilege('pages','Page','Create')) {
			$data['add'] = \Clay\Application::url('pages','pages','create');
		}
		if (!empty($pages)){
			foreach($pages as $page){
				
				if($user->privilege('pages','Page','Update',$page['pgid'])) {
					
					$page['edit'] = \Clay\Application::url('pages','page','edit',array('pgid' => $page['pgid']));
				}
				
				if($user->privilege('pages','Page','Delete',$page['pgid'])) {
					
					$page['delete'] = \Clay\Application::url('pages','page','delete',array('pgid' => $page['pgid']));
				}
				
				$page['title'] = \clay\data\filter('text',$page['title']);
				$page['body'] = \clay\data\filter('html',$page['body']);
				$page['pubdate'] = date('d M y',$page['pubdate']);
				$data['pages'][] = $page;
			}
		}		
		if (($pagesCount > 10) && !empty($perPage)){
			
			$data['pager'] = \Clay\Application::api('apps','pager','view',array('items' => $perPage,
																				'url' => array('pages','main','view'),
																				'count' => $pagesCount,
																				'first' => 'Newest',
																				'last' => 'Oldest'));
		}
		
		$data['pagesCount'] = $pagesCount;
		return $data;
	}
}