<?php
namespace application\pages\component;
/**
* Pages Application
*
* @copyright (C) 2013 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * Slideshow Component
 * @author David
 *
 */
class slideshow extends \Clay\Application\Component {

	/**
	 * View Action
	 * @param array $args - inert
	 */
	public function view($args){
		$this->template = 'slideshow';
		$this->pageTitle = 'Slideshow';
		return array();
	}

}