<?php
/**
* Pages Application
*
* @copyright (C) 2013-2018 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\pages\component;

/**
 * Pages
 * @author David
 *
 */
class pages extends \Clay\Application\Component
{

	/**
	 * View
	 */
	public function view()
	{
		
		return \Clay\Application::Redirect('pages');
	}

	/**
	 * Create
	 */
	public function create()
	{
		
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('pages','Page','Create')) {

			$this->page = 'dashboard';
			$data = array();

			$data['form'] = \Clay\Application::API('pages','page','input');
			$data['hooks'] = \Clay\Module::Object( 'Plugins' )->Hooks( 'pages' );
			return $data;
		}
		
		return \Clay\Application::Redirect('pages');
	}

	/**
	 * Manager
	 */
	public function manage(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('pages','Page','Manage')) {
			
			$this->page = 'dashboard';
			
			$data = array();

			$pages = \Clay\Application::API('pages','pages','getAll');
			
			if(is_array($pages)){
				
				foreach($pages as $key => $value){
					
					$pages[$key]['author'] = \Clay\Module::API('Users','getUser',$pages[$key]['uid']);
					$pages[$key]['title'] = \Clay\Data\Filter('text',$pages[$key]['title']);
				}
				
			} else {
				
				$pages = array();
			}
			
			$data['pages'] = $pages;
			return $data;
		}
		
		return \Clay\Application::Redirect('pages');
	}
}