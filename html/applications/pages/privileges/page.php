<?php
/**
 * Page Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2013-2018
 * @todo Add Status field in pages table for use with 'PUBLIC', 'PRIVATE', 'DRAFT', etc
 */

namespace application\pages\privilege;

class Page extends \Clay\Application\Privilege
{
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var unknown_type
	 */
	public $Request;
	
	/**
	 * Multimask Privilege
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope)
	{
		
		# Extract the requested scope into array - e.g. POSTID::1 = array('POSTID',1)

		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
			
			case 'AUTHOR':
				
				# Fetch some info about the requested Page
				$page = \Clay\Application::API('pages','page','getInfo',array('pgid' => $this->Request));

				switch($scope[1]){
					
					case 'MYSELF':
						
						# Pseudo User - If assigned and User created the Page, returns TRUE
						if($page['uid'] == \Clay\Module::Object('User')->id()){
							
							return TRUE;
						}
						
						return FALSE;
						
					case $page['uid']:
						
						return TRUE;
				}
				
				return FALSE;
				
			case 'PAGEID':
				
				if($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
				
			case 'PUBLIC':
				
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Administration tool
	 */
	public function Scopes($privilege, $scope = NULL)
	{
		
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'AUTHOR', 'PAGEID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
			
			case 'AUTHOR':
				
				$users = \Clay\Module::API('Users','getALL');
				
				# Pseudo User - allows privileges for the User that created a Post
				$users[] = array('uid' => 'MYSELF', 'name' => 'MYSELF');
				
				# do a foreach loop here to make it match PAGEID below
				
				foreach($users as $user){
					
					$item['id'] = $user['uid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					$item['title'] = NULL;
					$item['name'] = !empty($user['uname']) ? $user['uname'] : $user['name'];
					
					$option['items'][] = $item;
				}
				unset($users);
				$option['type'] = 'Users';
				return $option;
				
			case 'PAGEID':
				
				$pages = \Clay\Application::API('pages','page','getAll');
				
				foreach($pages as $page){
					
					$item['id'] = $page['pgid'];
					$item['date'] = $page['pubdate'];
					$item['uid'] = $page['uid'];
					$item['title'] = !empty($page['title']) ? $page['title'] : 'Untitled';
					$item['name'] = NULL;
					
					$option['items'][] = $item;
				}
				
				unset($pages);
				$option['type'] = 'Pages';
				return $option;
		}
	}
}