<style type="text/css">
hr.short {
margin: 8px 0 8px 0;
}
</style>
<?php /*
<script type="text/javascript">
$("body").delegate('.comment-reply','click' ,function(){
	var url = "<?= \Clay\Application::URL('comments','post','reply'); ?>";
	var parid = "&parid=" + $(this).data("parent");
	var tid = "&tid=" + $(this).data("thread");
	var base = "&base=" + $(this).data("base");
	var cid = "#c" + $(this).data("parent");
	var rid = ".r" + $(this).data("parent");
	$(rid).hide();
	$('.reply-container').empty();
	$.ajax({
		url: url + parid + tid + base + "&pageName=dashapp",
		cache: false,
		success: function(html){
			$(cid).empty().append(html);
			return false;
		}
	});
	return false;
});
</script> */ ?>
<div class="row">
<div class="col-md-12">
	<div class="c-app__head">
	<?php if(!empty($admin)) { ?>
		<div class="c-button-group c-button--default">
			<div class="c-dropdown"><a class="c-button c-button--sm c-button--default" href="#news">Options&emsp;<i class="fa fa-caret-down"></i></a>
				<div class="c-dropdown__item">
					<a class="dashlink" href="<?= \Clay\Application::URL('comments','posts','manage', array('tid' => $tid));?>"><i class="fa fa-sliders"></i>&emsp;Manage Comments</a>
					<a class="dashlink" href="<?= \Clay\Application::URL('comments','admin');?>"><i class="fa fa-cog"></i>&emsp;Settings</a>
				</div>
			</div>
		</div>
	<?php } else { ?>
	<p>Log in to comment</p>
	<?php } ?>
	</div>
	<?php //if(!empty($new)) echo '<div style="width:300px;">'.$new->template($post).'</div>'; ?>
	<div class="c-app__content" id="comments">
		
	<div>
	<?php 
	
	 if(!empty($posts)) {
		
		foreach($posts as $post) {
		
		$padding = ($post['depth'] * 50) - 50; $padding = $padding.'px'; ?>
		
		<div style="margin-left: <?= $padding; ?>; padding: 15px; border-left: 1px solid #eee; border-bottom: 2px solid #eee;">
		<?php //if(($date != $lastdate) AND ($post['depth'] === '1')) echo '<h2>'.$date.'</h2> <hr>'; 
		//echo '<div class="post-info"><div class="post-time accent accent-outline">'.date('g:i a',$post['cpub']).'</div>'; if(!empty($post['edit'])) { echo '<div class="post-options accent accent-outline"><a href="'.$post['edit'].'" class="dashlink">Edit</a><br /><a href="'.$post['delete'],'" class="dashlink">Delete</a></div>'; } echo '</div>'; 
		?>	        
			<?php $this->Template( array( 'application' => 'comments', 'template' => 'comment', 'data' => $post ));?>
		</div>
		
		<?php }
		if(!empty($pager)) $this->template($pager);
	} else { echo "No comments yet!"; }?>
	</div>
	
		</div>
	</div>		
</div>


<script>
	// start app
	new Vue({
		el: '#comments',
		data:  {
			cid: null,
			tid: null,
			parid: null,
			base: null			
		}
	})
</script>