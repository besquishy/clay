<div class="c-app__content blogEdit">
  <form class="form-horizontal" method="post" action="<?= \Clay\Application::URL( 'comments', 'post', 'create' ); ?>">
    <fieldset>
      <legend>Reply</legend>
      <div class="control-group">
        <div class="controls">                     
          <textarea id="body" name="body" class="form-control reply-text" placeholder="Comment"><?= !empty( $body ) ? $body : ''; ?></textarea>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="submit"></label>
        <div class="controls">
          <button id="submit" name="submit" class="btn btn-default">Comment</button>
        </div>
      </div>
    </fieldset>
  <input id="" name="tid" type="hidden" value="<?= $thread; ?>">
  <input id="" name="parid" type="hidden" value="<?= $parent; ?>">
  <input id="" name="base" type="hidden" value="<?= $base; ?>">
  <input id="" name="<?= $authid['field']; ?>" type="hidden" value="<?= $authid['id']; ?>">
  </form>
</div>
<?php $this->template($editor->action('view', 'reply-text')); ?>