<div class="c-app__content" id="addComment">
	<p>
		<button v-if="add == null" class="c-button c-button--md c-button--primary" @click="add = true">
			Comment
		</button>
	</p>
	
	<form v-if="add == true" class="form-horizontal" method="post" action="<?= \Clay\Application::URL( 'comments', 'post', 'create' ); ?>">
		<fieldset class="c-fieldset">
			<legend>New Comment</legend>
			<div class="c-fieldset__fields">
				<div class="control-group">
					<label class="control-label" for="title">Title</label>
					<div class="controls">
						<input id="title" name="title" type="text" placeholder="Title" class="c-field" value="<?= !empty( $title ) ? $title : ''; ?>">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="body">Comment</label>
					<div class="controls">                     
						<textarea id="body" name="body" class="c-field" placeholder="Comment"><?= !empty( $body ) ? $body : ''; ?></textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="submit"></label>
					<div class="controls">
						<button id="submit" name="submit" class="c-button c-button--success"><i class="fa fa-check"></i> Comment</button>
						<button type="button" @click="add = null" class="c-button c-button--danger"><i class="fa fa-times"></i> Cancel</button>
					</div>
				</div>			
				<input id="" name="tid" type="hidden" value="<?= $tid; ?>">
				<input id="" name="<?= $authid['field']; ?>" type="hidden" value="<?= $authid['id']; ?>">
			</div>				
		</fieldset>
	</form>
</div>

<script>
	// start app
	new Vue({
		el: '#addComment',
		data:  {
			add: null		
		}
	})
</script>