<div class="c-app__content blogEdit">
  <form class="form-horizontal" method="post" action="<?= \Clay\Application::URL( 'comments', 'post', 'create' ); ?>">
  <fieldset>
  <legend>New Comment</legend>
  <div class="control-group">
    <label class="control-label" for="title">Title</label>
    <div class="controls">
      <input id="title" name="title" type="text" placeholder="Title" class="form-control" value="<?= !empty( $title ) ? $title : ''; ?>">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="body">Comment</label>
    <div class="controls">                     
      <textarea id="body" name="body" class="form-control textarea" placeholder="Comment"><?= !empty( $body ) ? $body : ''; ?></textarea>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="submit"></label>
    <div class="controls">
      <button id="submit" name="submit" class="btn btn-default">Comment</button>
    </div>
  </div>
  </fieldset>
  <input id="" name="tid" type="hidden" value="<?= $tid; ?>">
  <input id="" name="parid" type="hidden" value="<?= $parent; ?>">
  <input id="" name="base" type="hidden" value="<?= $base; ?>">
  </form>
</div>
<?php $editor = \Clay\Application('wysihtml5','editor'); $this->template($editor->action('view')); ?>