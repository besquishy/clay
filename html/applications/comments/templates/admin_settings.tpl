<section class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('comments','admin');?>" class="dashlink" title="Dashboard">Comments</a> / 
    Comments Settings</h1>
  </div>
  <div class="c-app__content">
  <?php $form->template(); ?>
    <div class="u-bgcolor-container u-p@sm">
      Note: Depth Limit of "0" means there is NO DEPTH LIMIT
    </div>
  </div>
</section>