<div class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('comments','admin','view');?>" class="dashlink" title="Manage Threads">Comments</a> / 
    <a href="<?= \clay\application::url('comments','threads','manage');?>" class="dashlink" title="Manage Threads">Threads</a> / 
    <a href="<?= \clay\application::url('comments','posts','manage', array('tid' => $post['tid']));?>" class="dashlink" title="Manage Threads">Manage Thread</a> / 
    Comment Editor</h1>
  </div>
  <div class="c-app__content blogEdit">
    <form class="c-form" method="post" action="<?= \Clay\Application::URL( 'comments', 'post', 'update' ); ?>">
      <fieldset class="c-fieldset">
        <legend>Edit Comment</legend>
        <?php if(!empty($title)){ ?>
        <div class="control-group">
          <label class="control-label" for="title">Title</label>
          <div class="controls">
            <input id="title" name="title" type="text" placeholder="Title" class="c-field" value="<?= !empty( $post['title'] ) ? $post['title'] : ''; ?>">
          </div>
        </div>
        <?php } ?>
        <div class="control-group">
          <div class="controls">                     
            <textarea id="body" name="body" class="c-field" placeholder="Comment"><?= !empty( $post['body'] ) ? $post['body'] : ''; ?></textarea>
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="submit"></label>
          <div class="controls">
            <button id="submit" name="submit" class="c-button">Update Comment</button>
          </div>
        </div>
      </fieldset>
    <input id="" name="tid" type="hidden" value="<?= $post['tid']; ?>">
    <input id="" name="cid" type="hidden" value="<?= $post['cid']; ?>">
    <input id="" name="parid" type="hidden" value="<?= $post['parid']; ?>">
    <input id="" name="base" type="hidden" value="<?= $post['base']; ?>">
    <input id="" name="<?= $post['auth']['field']; ?>" type="hidden" value="<?= $post['auth']['id']; ?>">
    </form>
  </div>
</div>