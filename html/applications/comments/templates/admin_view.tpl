<section class="c-app">
		<div class="c-app__head">
			<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
			Comments Administration</h1>
		</div>
		<div class="c-app__content">
			<div class="c-tabs c-tooltip--container">
				<a href="<?= $threads; ?>" class="c-tabs__tab c-tooltip--block">
					Comments Threads
				</a>
				<div class="c-tooltip__block">View and Manage threads of Comments throughout the site.</div>
				<a href="<?= $settings; ?>" class="c-tabs__tab c-tooltip--block">
					Comments Settings
				</a>
				<div class="c-tooltip__block">View and Manage global comments settings and configuration options.</div>
				<a href="<?= $hooks; ?>" class="c-tabs__tab c-tooltip--block">
					Application Hooks
				</a>
				<div class="c-tooltip__block">Manage and configure how comments interact with applications.</div>
				<!--<a href="<?= $help; ?>" class="c-tabs__tab c-tooltip--block">
					Help
				</a>-->
				<div class="c-tooltip__block">Documentation and Support</div>
			</div>
		</div>
</section>