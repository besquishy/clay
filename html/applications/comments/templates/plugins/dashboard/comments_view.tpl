<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
		<i class="fa fa-comments dash-icon-lg"></i> Comments
		</div>
	</div>
	<div class="c-card__content">
		<a href="<?= $link; ?>" class="dashlink">Comments</a>
	</div>
</div>