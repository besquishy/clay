<style type="text/css">
.post-time {
 padding:6px;vertical-align:middle;text-align:center;font-size:1.2em;width:90px;
}
.post-options {
 padding:6px;vertical-align:middle;text-align:center;font-size:1em;width:90px;margin-top: 3px;
}
.blog-post{
 margin-left:107px;padding:7px;
}
.post-info{
float:left;width:90px;
}
</style>
<div class="c-app__head">
<?php if(!empty($add)) { ?>
 <div class="dropdown pull-right">
  <div class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
    <span class="fa-stack">
     <i class="fa fa-square-o fa-stack-2x"></i>
     <i class="fa fa-cogs fa-stack-1x"></i>
	</span>
    <span class="caret"></span>
  </div>
  <ul class="dropdown-menu" role="menu" aria-labelledby="blogdropdownMenu1">
    <li role="presentation"><a class="dashlink" role="menuitem" tabindex="-1" href="<?= $add; ?>"><i class="fa fa-plus"></i>&emsp;Add Post</a></li>
    <li role="presentation"><a class="dashlink" role="menuitem" tabindex="-1" href="<?= \Clay\Application::URL('blog','posts','manage');?>"><i class="fa fa-sliders"></i>&emsp;Manage Posts</a></li>
    <li role="presentation"><a class="dashlink" role="menuitem" tabindex="-1" href="<?= \Clay\Application::URL('blog','admin');?>"><i class="fa fa-cog"></i>&emsp;Settings</a></li>
  </ul>
</div>
<?php } ?>
  <h1>Blog - Plugins Test</h1>
</div>
<?php //if(!empty($new)) echo '<div style="width:300px;">'.$new->template($post).'</div>'; ?>
<div class="c-app__content">
<?php
 //if(!empty($add)) { echo '<a href="'.$add.'" class="dashlink">Add New Post</a>';}
 if(!empty($posts)) {
	$lastdate = NULL;
	foreach($posts as $post) {
	$date = date('d M y',$post['pubdate']);
	if($date != $lastdate) echo '<h2>'.$date.'</h2>';
	//echo '<div class="post-info"><div class="post-time accent accent-outline">'.date('g:i a',$post['pubdate']).'</div>'; if(!empty($post['edit'])) { echo '<div class="post-options accent accent-outline"><a href="'.$post['edit'].'" class="dashlink">Edit</a><br /><a href="'.$post['delete'],'" class="dashlink">Delete</a></div>'; } echo '</div>'; 
	?> 
  <div class="post-info">
	<div class="post-time accent accent-outline"><?= date('g:i a',$post['pubdate']); ?></div>
	<?php if(!empty($post['edit'])) { ?>
	<div class="post-options accent-outline">
		<div class="dropdown">
		  <div class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
		     <i class="fa fa-pencil-square-o"></i>			
		    <span class="caret"></span>
		  </div>
		  <ul class="dropdown-menu" role="menu" aria-labelledby="blogPostdropdownMenu1">
		    <li role="presentation"><a class="dashlink" role="menuitem" tabindex="-1" href="<?= $post['edit']; ?>"><i class="fa fa-pencil"></i>&emsp;Edit</a></li>
		    <li role="presentation"><a class="dashlink" role="menuitem" tabindex="-1" href="<?= $post['delete'];?>"><i class="fa fa-trash"></i>&emsp;Delete</a></li>
		  </ul>
		</div>
	</div>
	<?php } ?>
</div>
	<div class="accent-outline blog-post">
		<?php if(!empty($post['title'])){ ?>
		<h3><?= $post['title']; ?></h3>
		<?php } ?>
		<div class="item-text"><?= $post['body']; ?></div>
		<div>
		<?php foreach( $post['hooks'] as $hook ){ $hook['object']->Plugin('view'); }?>
		</div>
	</div>
	<br style="clear:both" />
	<?php $lastdate = $date; }
	if(!empty($pager)) $this->template($pager);
} else { echo "No Blog posts have been created yet!"; }?>
</div>