<section class="c-app">	
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('comments','admin');?>" class="dashlink" title="Dashboard">Comments</a> / 
	<a href="<?= \clay\application::url('comments','admin','threads');?>" class="dashlink" title="Dashboard">Threads</a> / 
		Manage Thread Comments</h1>
	</div>
	<div class="c-app__content">
	<?php if(!empty($posts)) { ?>
		<table class="c-table c-table--border c-table--striped">
			<tr class="c-table__row">
				<th class="c-table__head">ID</th>
				<th class="c-table__head">Author</th>
				<th class="c-table__head">Published</th>
				<th class="c-table__head">Title</th>
				<th class="c-table__head">options</th>
			</tr>
			<?php foreach($posts as $post) { ?>
			<tr class="c-table__row">
				<td class="c-table__cell"><?= $post['cid']; ?></td>
				<td class="c-table__cell"><?= $post['author'] . ' (id:' . $post['uid'] . ')'; ?></td>
				<td class="c-table__cell"><?= date('d M y g:i a',$post['cpub']); ?></td>
				<td class="c-table__cell"><?= empty($post['title']) ? 'untitled' : $post['title']; ?></td>
				<td class="c-table__cell">
					<a class="c-button" href="<?= \Clay\Application::url('comments','post','edit',array('cid' => $post['cid'])); ?>" title="Edit">Edit</a> 
					<a class="c-button" href="<?= \Clay\Application::url('comments','post','delete',array('cid' => $post['cid'])); ?>" title="Delete">Delete</a>
				</td>
			</tr>
			<?php } ?>
		</table>
	<?php } else { echo "No Comments have been created yet!"; }?>

	</div>
</section>