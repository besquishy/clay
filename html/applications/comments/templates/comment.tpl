<?php $date = date('d M y',$cpub);?>
<div class="pull-left">
	<a href="#">
		<img src="<?= $user['image']; ?>" class="thumbnail" alt="User Image">
	</a>
</div>
<div style="margin-left: 65px;" id="cid-<?= $cid; ?>">
	<h5><?= $date; ?></h5>
	<?php if( !empty( $title )) ?><h4 class="media-heading"><?= $title; ?></h4>
	<?= "<strong>".$user['uname']."</strong>&nbsp;&nbsp;&nbsp;" . $body; ?>
	<h6><i><?= date('g:i a',$cpub); ?></i></h6>
	<?php if( !empty( $reply )) { ?>
	<form  v-if="cid == '<?= $cid; ?>'" class="form-horizontal" method="post" action="<?= \Clay\Application::URL( 'comments', 'post', 'create' ); ?>">
		<fieldset class="c-fieldset">
		<legend>Reply</legend>
		<div class="c-fieldset__fields">
			<textarea id="body" name="body" class="c-field" placeholder="Comment"></textarea>
		</div>
		<div class="c-fieldset__controls">
			<button id="submit" name="submit" class="c-button c-button--md c-button--info">Comment</button> 
			<button type="button" class="c-button c-button--md c-button--danger" @click="cid = null">Cancel</button>
		</div>
		</fieldset>
		<input name="tid" type="hidden" value="<?= $tid; ?>">
		<input name="parid" type="hidden" value="<?= $cid; ?>">
		<input name="base" type="hidden" value="<?= $base; ?>">
		<input name="<?= $auth['field']; ?>" type="hidden" value="<?= $auth['id']; ?>">
	</form>
	<button v-if="cid == null" @click="cid = '<?= $cid; ?>'" class="c-button c-button--md c-button--info">Reply</button>
	<?php } ?>
</div>