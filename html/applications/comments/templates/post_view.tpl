<section class="c-app">
	<div class="c-app__head">
		<h1>Blog</h1>
	</div>
	<?php //if(!empty($new)) echo '<div style="width:300px;">'.$new->template($post).'</div>'; ?>
	<div class="c-app__content">
	<h2><?= $post['title']; ?></h2>
	<?php $date = date('d F Y',$post['pubdate']); ?>	
		<div class="accent accent-outline">Posted by <?= $user['uname'];?> on <?= $date.' at '.date('g:i a',$post['pubdate']); ?></div>
		<div class="accent-outline">		
			<div class="item-text">
				<?= $post['body']; ?>
			</div>
		</div>
	</div>
</section>