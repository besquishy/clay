<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('comments','admin');?>" class="dashlink" title="Dashboard">Comments</a> / Threads</h1>
	</div>
	<div class="c-app__content user-admin-dash">
		<table class="c-table c-table--border c-table--striped">
			<thead>
				<tr class="c-table__row">
					<th class="c-table__head">Thread ID</th>
					<th class="c-table__head">Comments</th>
					<th class="c-table__head">Last Comment</th>
					<th class="c-table__head">Status</th>
					<th class="c-table__head">Application</th>
					<th class="c-table__head">Item Type</th>
					<th class="c-table__head">Item ID</th>
					<th class="c-table__head">Options</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($threads as $thread) { 
					if( empty( $thread['tcount'] )) $thread['tcount'] = 0; ?>
				<tr class="c-table__row">
					<td class="c-table__cell"><?= $thread['tid']; ?></td>
					<td class="c-table__cell"><?= $thread['tcount']; ?></td>
					<td class="c-table__cell"><?= date('d M y g:i a',$thread['tdate']); ?></td>
					<td class="c-table__cell"><?= $thread['status']; ?></td>
					<td class="c-table__cell"><?= \Clay\Application::getName($thread['appid']); ?></td>
					<td class="c-table__cell"><?= !empty($thread['itemtype']) ? $thread['itemtype'] : 'All'; ?></td>
					<td class="c-table__cell"><?= !empty($thread['itemid']) ? $thread['itemid'] : 'All'; ?></td>
					<td class="c-table__cell text-center">
						<a class="c-button" href="<?= \Clay\Application::URL('comments','posts','manage',array('tid' => $thread['tid'])); ?>" title="Edit">Manage</a> 
						<a class="c-button" href="<?= \Clay\Application::URL('comments','thread','view',array('tid' => $thread['tid'])); ?>" title="Edit">Comments</a> 
						<?php /*<a class="c-button" href="<?= \Clay\Application::URL('comments','thread','edit',array('tid' => $thread['tid'])); ?>" title="Edit">Edit</a> 
						<a class="c-button" href="<?= \Clay\Application::URL('comments','thread','delete',array('tid' => $thread['tid'])); ?>" title="Delete">Delete</a>
						*/?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>	
	</div>
</section>