<?php
/**
 * Comments Application
 *
 * @copyright (C) 2015-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Comments',
			'name' => 'comments',
			'version' => '1.0.2',
			'date' => 'September 16, 2017',
			'description' => 'Comment system for application items.',
			'class' => 'User',
			'category' => 'Content',
			'depends' => array('applications' => array('dashboard')),
			'core' => FALSE,
			);
?>