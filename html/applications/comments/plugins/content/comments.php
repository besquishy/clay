<?php
/**
 * Comments Plugin
 *
 * Comments application "Comments" content plugin
 *
 * @copyright (C) 2015-2018 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\comments\plugin\content;

/**
 * Comments Application Content Comments Plugin
 */
class comments extends \Clay\Application\Plugin {
	/**
	 * Plugin App
	 * @var string
	 */
	public $PluginApp = 'comments';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'content';
	/**
	 * Plugin Name
	 * @var string
	 */
	public $Plugin = 'comments';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function view( $args ){
		
		$data['thread'] = \Clay\Application::API( 'comments', 'thread', 'get', $args );
		
		if( empty( $data['thread'] )){
			
			$data['thread']['tcount'] = "0";
		}
		
		return $data;
	}
	/**
	 * Stats - Info - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function stats( $args ){
	
		$data['thread'] = \Clay\Application::API( 'comments', 'thread', 'get', $args );
	
		if( empty( $data['thread'] )){
				
			$data['thread']['tcount'] = "0";
		}
	
		return $data;
	}
	/**
	 * Display - to be displayed on an item page
	 * @param array $args
	 * @return string
	 */
	public function display( $args ){

		$user = \Clay\Module::API('User','Instance');
		
		$thread = \Clay\Application::API( 'comments', 'thread', 'get', $args );
		
		if( empty( $thread )){
			$args['hook'] = TRUE;
			# If there isn't a thread for this item, we'll get it
			$thread = \Clay\Application::API( 'comments', 'thread', 'create', $args );
			$thread = \Clay\Application::API( 'comments', 'thread', 'get', array('tid' => $thread ));
		}
		//$data['comments'] = \Clay\Application::API( 'comments', 'posts', 'thread', array('tid' => $thread['tid']) );
		$data['thread'] = $thread;

		if( $user->privilege('comments','Comment','Create') ) {

			$new = \Clay\Application( 'comments', 'post' );
			$new->template = 'comment_add';
			$data['new'] = $new->action( 'add', $thread['tid'] );

		}
				
		$comments = \Clay\Application( 'comments', 'thread' );
		$data['comments'] = $comments->action( 'view', $thread['tid'] );

		return $data;
	}
	/**
	 * Add Item
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		
		
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		$thread = \Clay\Application::API('comments','thread','get',$args);

		\Clay\Application::API('comments','posts','delete', $thread['tid']);
		\Clay\Application::API('comments','thread','delete', $thread['tid']);
	}
}