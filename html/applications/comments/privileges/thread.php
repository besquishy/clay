<?php
/**
 * Comment - Thread Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2012
 * @todo Add Status field in blog_posts table for use with 'PUBLIC', 'PRIVATE', 'DRAFT', etc
 */

namespace application\comments\privilege;

\Library('Clay/Application/Privilege');

/**
 * Comments Application Thread Privileges
 */
class Thread extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var array
	 */
	public $Request;
	
	/**
	 * Thread Multimask Privilege
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. THREADID::1 = array('THREADID',1)

		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
			
			case 'AUTHOR':
				
				# Fetch some info about the requested Thread
				$thread = \Clay\Application::API('comments','thread','getAuthor',array('tid' => $this->Request));

				switch($scope[1]){
					
					case 'MYSELF':
						
						# Pseudo User - If assigned and User created the Post, returns TRUE
						if($thread['uid'] == \Clay\Module::Object('User')->id()){
							
							return TRUE;
						}
						
						return FALSE;
						
					case $thread['uid']:
						
						return TRUE;
				}
				
				return FALSE;
				
			case 'THREADID':
				
				if($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
				
			case 'PUBLIC':
				
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Administration tool
	 * @param string $privilege
	 * @param string $scope
	 */
	public function Scopes($privilege, $scope = NULL){
		
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'AUTHOR', 'THREADID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
			
			case 'AUTHOR':
				
				$users = \Clay\Module::API('Users','getALL');
				
				# Pseudo User - allows privileges for the User that created a Thread
				$users[] = array('uid' => 'MYSELF', 'name' => 'MYSELF');
				
				# do a foreach loop here to make it match THREADID below
				
				foreach($users as $user){
					
					$item['id'] = $user['uid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					$item['title'] = NULL;
					$item['name'] = !empty($user['uname']) ? $user['uname'] : $user['name'];
					
					$option['items'][] = $item;
				}
				unset($users);
				$option['type'] = 'Users';
				return $option;
				
			case 'THREADID':
				
				$threads = \Clay\Application::API('comments','threads','getAll');
				
				foreach($threads as $thread){
					
					$item['id'] = $thread['cid'];
					$item['date'] = $thread['cpub'];
					$item['uid'] = $thread['uid'];
					$item['title'] = !empty($thread['title']) ? $thread['title'] : 'Untitled';
					$item['name'] = NULL;
					
					$option['items'][] = $item;
				}
				
				unset($threads);
				$option['type'] = 'Threads';
				return $option;
		}
	}
}