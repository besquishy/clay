<?php
/**
* Comments Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\library;

/**
 * Comments Application Posts API
 * @author David
 *
 */
class posts {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All (or some) Comments
	 * @param array $args
	 * @return array
	 */
	public static function get($args=array()){
		
		# Select a range starting number using 'startnum'
		$offset = !empty($args['startnum']) ? $args['startnum'] : '0';
		# Fetch 'items' number of Blog Posts, offset by 'startnum' if applicable
		if(!empty($args['items'])){
			
			if(!empty($args['startnum'])){
				
				$limit = $args['startnum'].','.$args['items'];
				
			} else {
				
				$limit = '0,'.$args['items'];
			}
			
		} else {
			
			$limit = '';
		}
		$status = !empty($args['status']) ? $args['status'] : 1;
		# Return associative array
		return self::db()->get('cid, status, depth, tid, parid, uid, cpub, cdate, points, title, base, options, body FROM '.\claydb::$tables['comments'].' WHERE tid = ? ORDER BY base ASC',array($args['tid']),$limit);
	}
	
	/**
	 * Get a Thread's Comments
	 * @param array $args
	 * @return array
	 */
	public static function thread($args=array()){
	
		# Select a range starting number using 'startnum'
		$offset = !empty($args['startnum']) ? $args['startnum'] : '0';
		# Fetch 'items' number of Blog Posts, offset by 'startnum' if applicable
		if(!empty($args['items'])){
					
			if(!empty($args['startnum'])){
	
				$limit = $args['startnum'].','.$args['items'];
	
			} else {
	
				$limit = '0,'.$args['items'];
			}
		
		} else {
					
			$limit = '';
		}
		
		$status = !empty($args['status']) ? $args['status'] : 1;
		# Return associative array
		return self::db()->get('cid, status, depth, tid, parid, uid, cpub, cdate, points, title, base, options, body FROM '.\claydb::$tables['comments'].' WHERE status = ? AND tid = ? ORDER BY base ASC',array($status, $args['tid']),$limit);
	}
	
	/**
	 * Count Comments
	 * @param array $args - inert
	 * @return int
	 */
	public static function count($args){

		$count = self::db()->get('COUNT(*) as posts FROM '.\claydb::$tables['comments']);
		return $count[0]['posts'];
	}

	/**
	 * Delete Comments in a Thread
	 * @param int $tid
	 */
	public static function delete( $tid ){

		$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'comments' );

		foreach( $hooks as $type ){

			foreach ( $type as $plugin ){

				$plugin['object']->Process( 'delete', array('app' => 'comments', 'itemtype' => $tid ));
			}
		}

		return self::db()->delete( \claydb::$tables['comments']." WHERE tid = ?", array( $tid ));
	}
}