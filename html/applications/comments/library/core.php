<?php
/**
 * Comments Application
 *
 * @copyright (C) 2015 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\comments\library;

/**
 * Comments Application Core Library
 */
 class Core {
 	
 	/**
 	 * Import self::db() using a Trait \ClayDB\Connection
 	 */
 	use \ClayDB\Connection;
 	
 	/**
 	 * Comments Item Types (Threads)
 	 * @return array
 	 */
 	public static function ItemTypes(){

		$itemtypes = array();
 		/*
 		# Get all Threads (itemtype)
		$threads = \Clay\Application::API('comments', 'threads', 'get');
 		
 		foreach( $threads as $thread ){
			
			if (empty($itemtypes[$thread['appid']])){

				$app = \Clay\Application::getName($thread['appid']);
				$name = !empty($thread['itemtype']) ? ':: '.$thread['itemtype'].' ::' : '';

				$itemtypes[$thread['appid']] = array( 'id' => $thread['tid'],
								'name' => $app.$name,
								'title' => $app.$name,
								'itemtype' => $thread['tid'],
								'date' => $thread['tpub'],
								);
				unset($app);unset($name);
			}
 		}
 		*/
 		return $itemtypes;
 	}
 	/**
 	 * Comments Item Type (Thread)
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function ItemType( $itemType ){
 		
 		/*# Get a Thread (itemtype)
 		$thread = \Clay\Application::API('comments', 'thread', 'get', array('tid' => $itemType)); 		
		
		return array( 'id' => $thread['tid'],
							'name' => $thread['appid'].'::'.$thread['itemtype'].'::'.$thread['itemid'],
							'title' => $thread['appid'].'::'.$thread['itemtype'].'::'.$thread['itemid'],
							'itemtype' => $thread['tid'],
							'date' => $thread['tpub'],
						);
		*/
		return array();
 	}
 	/**
 	 * Comments Posts
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function Items( $itemType ){
 		# Get all Comments (items) in a Thread (itemtype)
 		$comments = \Clay\Application::API('comments', 'posts', 'get', array('tid' => $itemType));
 		$items = array();
 		foreach( $comments as $comment ){
 			# Titles are optional, make sure we have a filler
 			if( empty( $comment['title'] )){
 				
 				$comment['title'] = 'Untitled';
 			}
 			$items[] = array( 'id' => $comment['cid'],
 						      'name' => $comment['title'],
 						      'title' => $comment['title'],
							  'uid' => $comment['uid'],
							  'itemtype' => $comment['tid'],
							  'date' => $comment['cpub'],
					   		  'content' => $comment['body']
 							);
 		}
 		
 		return $items;
 	}
 	/**
 	 * Comments Post
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Item( $args ){
 		
 		$comment = \Clay\Application::API('comments', 'post', 'get', array('cid' => $args[1]));
 		
 		return array( 'id' => $comment['cid'],
 					   'name' => $comment['title'],
 					   'title' => $comment['title'],
					   'uid' => $comment['uid'],
					   'itemtype' => $comment['tid'],
					   'date' => $comment['cpub'],
					   'content' => $comment['body']
 					);
 	}
 	/**
 	 * Comments Fields
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Fields( $args ){
 		
 		return array( 'uid', 'cpub', 'title', 'body' );
	 }
	 /**
	 * Comments Labels
	 * @param string $type - itemtype or item
	 * @return array
	 */
	public static function Labels( $type ){
		
		switch( $type ){

			case 'itemtype':
				return 'Thread';

			case 'item':
				return 'Comment';
				
			default:
			break;
		}
		return;
	}
 }