<?php
/**
* Comments Application
*
* @copyright (C) 2015-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\library;

\Clay\Module( 'Privileges' );

/**
 * Comments Application Setup
 */
class setup {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	/**
	 * Install
	 * @return boolean
	 */
	public static function install(){
		# Use the DB trait
		$dbconn = self::db();
	    $datadict = $dbconn->datadict();
	    # Comments tables
	    $tables = array( 'comment_threads' => array( 'table' => 'comment_threads' ), 
	    				 'comments' => array( 'table' => 'comments' ));
	    
	    $datadict->registerTables( $tables );
	    # Refresh the table cache
	    //\claydb::tables( 'flush' );
	    # Create the Threads table
	    $cols = array();
	    $cols['column'] = array( 'tid' => array( 'type' => 'ID' ),
	    						'status' => array( 'type' => 'TINYINT', 'size' => '1' ),
	    						'appid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'itemtype' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
	    						'itemid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'uid' => array('type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
	    						'tpub' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'tdate' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
	    						'points' => array( 'type' => 'INT', 'size' => '11', 'null' => FALSE, 'default' => '0' ),
	    						'tcount' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE, 'default' => '0' ),
	    						'title' => array( 'type' => 'VARCHAR', 'size' => '100' ),
	    						'options' => array( 'type' => 'STRING' ));
	    # Threads table indices
	    $cols['index'] = array( 'status' => array( 'type' => 'KEY', 'index' => 'status' ),
	    						'created' => array( 'type' => 'KEY', 'index' => 'tpub' ),
	    						'updated' => array( 'type' => 'KEY', 'index' => 'tdate' ),
	    						'userid' => array( 'type' => 'KEY', 'index' => 'uid' ),
	    						'thread' => array( 'type' => 'UNIQUE', 'index' => 'appid, itemtype, itemid' ));
	    # Complete it
	    $datadict->createTable( \claydb::$tables['comment_threads'], $cols );
	    # Create the Comments table
	    $cols = array();
	    $cols['column'] = array( 'cid' => array( 'type' => 'ID' ),
					    		'status' => array( 'type' => 'TINYINT', 'size' => '1' ),
	    						'depth' => array( 'type' => 'TINYINT', 'unsigned' => TRUE, 'size' => '2' ),
	    						'ccount' => array( 'type' => 'INT', 'size' => '4', 'unsigned' => TRUE, 'null' => FALSE, 'default' => '0' ),
	    						'tid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'parid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
					    		'uid' => array('type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE),
					    		'cpub' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'cdate' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
					    		'points' => array( 'type' => 'INT', 'size' => '11', 'null' => FALSE, 'default' => '0' ),
	    						'title' => array( 'type' => 'VARCHAR', 'size' => '100' ),
	    						'base' => array( 'type' => 'STRING', 'null' => FALSE ),
					    		'options' => array( 'type' => 'STRING' ),
	    						'body' => array( 'type' => 'text'));
	    # Comments indices
	    $cols['index'] = array( 'status' => array( 'type' => 'KEY', 'index' => 'status' ),
					    		'created' => array( 'type' => 'KEY', 'index' => 'cpub' ),
					    		'updated' => array( 'type' => 'KEY', 'index' => 'cdate' ),
					    		'userid' => array( 'type' => 'KEY', 'index' => 'uid' ),
					    		'thread' => array( 'type' => 'KEY', 'index' => 'tid, parid' ));
	    # Complete it
	    $datadict->createTable( \claydb::$tables['comments'], $cols );
		# Comments Plugin
		$plugin = array('plugin', array('type' => 'content', 'app' => 'comments', 'name' => 'comments', 'descr' => 'Provides a comments system to application items.'));
		\Clay\Module::Object('Plugins')->Setup($plugin);
		# Comments Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'comments', 'name' => 'comments', 'descr' => 'Manage Comments and Settings.'));
		\Clay\Module::Object('Plugins')->Setup($dashboard);
		# Hook Comments Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'comments', 'name' => 'comments'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		# Privileges
		# Thread Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Thread', 'Create' );
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Thread', 'Update' );
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Thread', 'Delete' );
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Thread', 'View' );
		# Comment Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Comment', 'Create' );
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Comment', 'Update' );
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Comment', 'Delete' );
		\Clay\Module::Object( 'Privileges' )->register( 'comments', 'Comment', 'View' );

		return true;
	}
	/**
	 * Upgrade
	 * @param integer $version Installed version
	 * @return boolean
	 */
	public static function upgrade($version){
		
		switch ($version) {

			case ($version <= '1.0'):
				# Comments Dashboard Plugin
				$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'comments', 'name' => 'comments', 'descr' => 'Manage Comments and Settings.'));
				\Clay\Module::Object('Plugins')->Setup($dashboard);

			case ($version <= '1.0.1'):
				# Hook Comments Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'comments', 'name' => 'comments'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );
				break;
		}
		return true;
	}
	/**
	 * Uninstall
	 * @return boolean
	 */
	public static function delete(){
		# ClayDB Object
		$dbconn = self::db();
	    $datadict = $dbconn->datadict();
		# Drop Tables
	    $datadict->dropTable( \claydb::$tables['comment_threads'] );
	    $datadict->dropTable( \claydb::$tables['comments'] );
		# Drop Privileges
	    \Clay\Module::Object( 'Privileges' )->remove( 'comments' );
		# Drop Plugins /  Drop Hooks
		\Clay\Module::Object( 'Plugins' )->Remove( 'comments' );
	    return true;
	}
}
