<?php
/**
* Comments Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\library;

/**
 * Comments Application Threads API
 * @author David
 *
 */
class threads {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All (or some) Comment Threads
	 * @param array $args
	 * @return array
	 */
	public static function get($args=array()){
		
		# Select a range starting number using 'startnum'
		$offset = !empty($args['startnum']) ? $args['startnum'] : '0';
		# Fetch 'items' number of Comment Threads, offset by 'startnum' if applicable
		if(!empty($args['items'])){
			
			if(!empty($args['startnum'])){
				
				$limit = $args['startnum'].','.$args['items'];
				
			} else {
				
				$limit = '0,'.$args['items'];
			}
			
		} else {
			
			$limit = '';
		}
		# @TODO: In the future we should create an API (perhaps system level?) for status codes, for now leave it user select
		$status = !empty($args['status']) ? $args['status'] : 1;
		# Return associative array
		return self::db()->get('tid, status, appid, itemtype, itemid, uid, tpub, tdate, points, tcount, options FROM '.\claydb::$tables['comment_threads'].' WHERE status = ? ORDER BY tdate DESC',array($status),$limit);
	}
	
	/**
	 * Count Blog Posts
	 * @param array $args - inert
	 * @return int
	 */
	public static function count($args){

		$count = self::db()->get('COUNT(*) as threads FROM '.\claydb::$tables['comment_threads']);
		return $count[0]['threads'];
	}
}