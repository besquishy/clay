<?php
/**
* Comments Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\component;

/**
 * Comments Post Component
 * @author David
 *
 */
class post extends \Clay\Application\Component {

	/**
	 * Comments Post View
	 * @param array $args
	 * @throws \Exception
	 */
	public function view( $args ){
		
		$cid = !empty($args['cid']) ? $args['cid'] : \Clay\Data\Get('cid','int','int');
		
		if(empty($cid)){
			
			throw new \Exception('Comments COMMENT ID (cid) must be NUMERIC');
		}
		
		$user = \Clay\Module::API('User','Instance');

		if(!$user->privilege( 'comments','Comment','View',"COMMENTID::$cid" )) {			
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		$post = \Clay\Application::API('comments','post','get',array('cid' => $cid));
		
		return array('post' => $post,
					 'date' => date('d M y',$post['pubdate']),
					 'user' => \Clay\Module::API('Users','Get',$post['uid']));		
	}
	
	/**
	 * Add a new Comments Post
	 * @param integer $tid Thread ID
	 */
	public function add( $tid ){
	
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('comments','Comment','Create')) {
			
			return;
		}
		
		$data = array();
		$data['tid'] = $tid;
		//$data['editor'] = \Clay\Application('summernote','editor');
		# Initiate Authentication (POST Request)
		$data['authid'] = $user->authId('comment.add');
		$data['hooks'] = \Clay\Module::Object( 'Plugins' )->Hooks( 'comments' );
		return $data;
	}
	
	/**
	 * Add a new Comments Reply
	 * @param array $args
	 * @sample array('tid' => Thread ID, 'parid' => Parent Comment, 'base' => Parent Comment Base)
	 */
	public function reply( $args ){
	
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('comments','Comment','Create')) {
				
			return;
		}
	
		$data = array();
		$data['thread'] = !empty($args['tid'])? $args['tid'] : \Clay\Data\Get('tid','int','int') ;
		$data['parent'] = !empty($args['parid']) ? $args['parid'] : \Clay\Data\Get('parid','int','int');
		$data['base'] = !empty($args['base']) ? $args['base'] : \Clay\Data\Get('base','string','string');
		
		if(empty($data)){
			
			throw new \Exception('Parent data must be supplied for a Comment Reply (tid, parid, base).');
		}
		# Initiate Authentication (POST Request)
		$data['authid'] = $user->authId('comment.reply');
		$data['editor'] = \Clay\Application('summernote','editor');
		$this->template = 'comment_reply';			
		return $data;
	}

	/**
	 * Comments Post Create
	 */
	public function create(){
		
		$user = \Clay\Module::API('User','Instance');

		if($user->privilege('comments','Comment','Create')) {

			$args['status'] = \Clay\Data\Post( 'status', 'int', 'int', 1);
			$args['date'] = \Clay\Data\Post( 'date', 'num', 'num', time());
			$args['user'] = $user->id();
			$args['title'] = substr(\Clay\Data\Post( 'title', 'string' ),0,155);
			$args['body'] = \Clay\Data\Post( 'body', 'string' );
			$args['tid'] = \Clay\Data\Post( 'tid', 'int', 'int' );
			$args['ccount'] = '0';
			$args['points'] = '0';
			$args['parent'] = \Clay\Data\Post( 'parid', 'int', 'int', NULL );

			if(is_null($args['parent'])){
				# New Comment
				$user->auth('comment.add');
			
			} else {
				# Comment Reply
				$user->auth('comment.reply');
			}

			$args['base'] = \Clay\Data\Post( 'base', 'string' );
			$args['depth'] = \Clay\Data\Post( 'depth', 'int', 'int', 1 );
			$args['options'] = \Clay\Data\Post( 'options', 'isarray', '', NULL );

			if(!empty($args['body'])) {
				# Preserve original content for hooks
				$params = $args;

				if( !empty( $args['base'] )){
					
					$depth = explode( '.', $args['base'] );
					$args['depth'] = count($depth);
					$args['depth']++;
				}
				
			} else {
				
				\Clay\Application::api('system','message','send','Although a Title is optional, all comments must have Text.');
				
				return \Clay\Application::redirect('comments','post','add');
			}

			# Run Hooks once we know we have 'body' text
			$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'comments' );
			# Call the Transform hook in case plugins have processing
			foreach( $hooks as $tkey => $type ){

				foreach ( $type as $pkey => $plugin ){

					$proc[$tkey] = $plugin['object']->Process( 'transform', array('app' => 'comments', 'itemtype' => NULL, 'itemid' => NULL, 'content' => $args['body'] ));
				}
			}
			# This can be expanded later to look for different types of processing
			# If an editor is hooked and supplied processed text, we'll update our text
			if( !empty( $proc['editor'] ) AND ( $proc['editor']['content'] !== $args['body'] )){
				# Update body to match processed content
				$args['body'] = $proc['editor']['content'];
			}
			$cid = \Clay\Application::api('comments','post','create',$args);
			# Creates the Markdown copy of the unstyled text
			# Run this after the comment is created so we don't have to worry about duplicates
			foreach( $hooks as $type ){

				foreach ( $type as $plugin ){

					$plugin['object']->Process( 'create', array('app' => 'comments', 'itemtype' => $args['tid'], 'itemid' => $cid, 'content' => $params['body'] ));
				}
			}
			\Clay\Application::api('system','message','send',"Comment (ID: ".$cid.') '.substr($args['title'],0,50).' - Created Successfully! <p>Go to <a href="'.\Clay\Application::url('comments','posts','manage',array('tid' => $args['tid'])).'" class="dashlink">Comments Manager</a>');
		}
		
		return \Clay::Redirect($_SERVER["HTTP_REFERER"].'#cid-'.$cid);
	}
	
	/**
	 * Comments Post Edit
	 * @param array $args
	 * @throws \Exception
	 */
	public function edit($args){
		
		$user = \Clay\Module::API('User','Instance');
	
		$cid = \clay\data\get('cid','int','int',!empty($args) ? $args : '');
	
		if(!$user->privilege('comments','Comment','Update',$cid)) throw new \Exception('Additional Privileges Are Required to View This Page.');

		$this->template = 'comment_edit';
				
		$post = \Clay\Application::api('comments','post','get',array('cid' => $cid));
		
		$post['title'] = \clay\data\filter('text',$post['title']);

		$data['hooks'] = \Clay\Module::Object( 'Plugins' )->Hooks( 'comments' );

		if( !empty( $data['hooks']['editor']['markdown/markdown'] )){
			# We'll use the preformatted Markdown text instead of the processed HTML
			$body = $data['hooks']['editor']['markdown/markdown']['object']->Process( 'editor',
				array( 'app' => 'comments', 'itemtype' => $post['tid'], 'itemid' =>$cid ));
			# Preformatted Markdown text is stored
			if( !is_null( $body['content'] )){
				# Markdown has a preformatted version of this body
				$post['body'] = $body['content'];
			# No Preformatted Markdown text is stored
			} else {
				# If Markdown is enabled, strip all tags from the body so it can be formatted and stored as Markdown -> HTML
				$post['body'] = \clay\data\filter('noTags', $post['body']);
			}
		}

		$post['auth'] = $user->authId('comment.update');

		$this->page = 'dashboard';
		
		return array('post' => $post);
	}
	
	/**
	 * Comments Post Update
	 * @param array $args
	 * @throws \Exception
	 */
	public function update($args){
	
		$user = \Clay\Module::API('User','Instance');
	
		$args['cid'] = \clay\data\post('cid','int','int');

		$args['updated'] = time();
		
		if(!$user->privilege('comments','Comment','Update',$args['cid'])) throw new \Exception('Additional Privileges Are Required to Update This Item.');

		$args['tid'] = \clay\data\post('tid','int','int');
		$args['title'] = \clay\data\post('title','string');
		$args['body'] = \clay\data\post('body','string');

		if(!empty($args['body'])) {

			# Preserve original content for hooks
			$params = $args;
			# Load up assigned hooks
			$hooks = \Clay\Module::Object( 'Plugins' )->Hooks( 'comments' );
			# Call the Transform hook in case plugins have processing
			foreach( $hooks as $tkey => $type ){

				foreach ( $type as $pkey => $plugin ){

					$proc[$tkey] = $plugin['object']->Process( 'transform', array('app' => 'comments', 'itemtype' => NULL, 'itemid' => NULL, 'content' => $args['body'] ));
				}
			}
			# This can be expanded later to look for different types of processing
			# If an editor is hooked and supplied processed text, we'll update our text
			if( !empty( $proc['editor'] ) AND ( $proc['editor']['content'] !== $args['body'] )){
				# Update body to match processed content
				$args['body'] = $proc['editor']['content'];
			}

			\Clay\Application::api('comments','post','update',$args);

			foreach( $hooks as $type ){

				foreach ( $type as $plugin ){

					$plugin['object']->Process( 'update', array('app' => 'comments', 'itemtype' => $params['tid'], 'itemid' => $params['cid'], 'content' => $params['body'] ));
				}
			}
			
		} else {
			
			\Clay\Application::api('system','message','send','Although a Title is optional, all comments posts must have Text.');
			
			return \Clay\Application::redirect('comments','post','edit',array('cid' => $args['cid']));
		}
		
		\Clay\Application::api('system','message','send',"Comments Post (ID: ".$args['cid'].') '.$args['title'].' - Updated Successfully! <p>Go to <a href="'.\Clay\Application::url('comments','posts','manage',array('tid' => $args['tid'])).'" class="dashlink">Comments Manager</a>');
		
		return \Clay::Redirect($_SERVER["HTTP_REFERER"]);
	}
	
	/**
	 * Comments Post Delete
	 * @param array $args
	 * @throws \Exception
	 */
	public function delete($args){
		
		$cid = \clay\data\fetch('cid','int','int',!empty($args)?$args:'');
		
		if(empty($cid)) throw new \Exception('A COMMENT ID (int) is Required to Perform This Action.');
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('comments','Comment','Delete',$cid)){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		
		
		$post = \Clay\Application::api('comments','post','get',array('cid' => $cid));
		
		$post['title'] = \clay\data\filter('text',$post['title']);
		
		if(!empty($_GET['cid']) || !empty($args)){
			
			if(empty($post['title'])) $post['title'] = 'untitled';
			
			return array('cid' => $post['cid'], 'title' => $post['title']);
		}
		$title = !empty($post['title']) ? $post['title'] : 'untitled';
		
		\Clay\Application::api('comments','post','delete',array('cid' => $cid));
		\Clay\Application::api('system','message','send',"Comments Post (ID: $cid) $title - Deleted Successfully! <p>Go to <a href=".'"'.\Clay\Application::url('comments','posts','manage',array('tid' => $post['tid'])).'" class="dashlink">Comments Manager</a>');	
		
		return \Clay\Application::redirect('comments');
	}
}