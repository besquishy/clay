<?php
/**
* Comments Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\component;

/**
 *  Comments Application Main Component
 * @author David
 *
 */
class main extends \Clay\Application\Component {

	/**
	 * Comments View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API( 'User','Instance' );
		
		if(!$user->privilege( 'comments', 'Comment', 'View' )){
			
			#throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		
		$startnum = \Clay\Data\Get( 'startnum', 'int', 'int', 1 ) - 1;
		$postsCount = \Clay\Application::API( 'comments', 'posts', 'count' );
		# Items per page @todo: Make items per page a Blog setting in the Dashboard
		$perPage = 10;
		$posts = \Clay\Application::API( 'comments', 'posts', 'getAll', array( 'items' => $perPage, 'startnum' => $startnum ));
		
		if($user->privilege( 'comments', 'Comment', 'Create' )) {
			$data['add'] = \Clay\Application::URL( 'comments', 'posts', 'create' );
		}
		
		foreach( $posts as $post ){
			
			if( $user->privilege( 'comments', 'Comment', 'Update', $post['pid'] )) {
				
				$post['edit'] = \Clay\Application::URL( 'comments', 'post', 'edit', array('cid' => $post['cid'] ));
			}
			
			if( $user->privilege( 'comments', 'Post', 'Delete', $post['pid'] )) {
				
				$post['delete'] = \Clay\Application::URL( 'comments', 'post', 'delete', array( 'pid' => $post['pid'] ));
			}
			
			$post['title'] = \Clay\Data\Filter( 'text', $post['title'] );
			$post['body'] = \Clay\Data\Filter( 'html', $post['body'] );
			$data['posts'][] = $post;
		}
		
		if(( $postsCount > 10 ) && !empty( $perPage )){
			
			$data['pager'] = \Clay\Application::API( 'apps', 'pager', 'view', array( 'items' => $perPage,
																					'url' => array('comments','main','view'),
																					'count' => $postsCount,
																					'first' => 'Newest',
																					'last' => 'Oldest'
																					));
		}
		
		$data['postsCount'] = $postsCount;
		return $data;
	}
}