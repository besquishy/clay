<?php
/**
* Comments Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\component;

/**
 *  Comments Thread Component
 * @author David
 *
 */
class thread extends \Clay\Application\Component {

	/**
	 * Thread View
	 * @param integer $tid Thread ID
	 */
	public function view( $tid ){

		$data = array();
		
		$user = \Clay\Module::API( 'User','Instance' );
		
		if(!$user->privilege( 'comments', 'Comment', 'View' )){
			
			throw new \Exception('Additional Privileges Are Required to View Comments.');
			//return false;
		}
		# Thread ID - passed via parameter or GET: tid
		$tid = !empty( $tid ) ? $tid : \Clay\Data\Get( 'tid', 'int', 'int' );
		# $tid could be array passed from another method, see if we already have the thread
		if(is_array($tid)){
			# Set tid to the array value and split off the thread
			if(!empty($tid['tid'])){
				
				$thread = $tid;
				$tid = $thread['tid'];
			}
		}
		# Thread ID required
		if( empty( $tid )){
			
			throw new \Exception( 'Comment Thread ID (tid) must be NUMERIC!' );
		}
		# Get the Thread info and the attached comments
		if(!empty($thread)){
		
			$thread = \Clay\Application::API( 'comments', 'thread', 'get', array( 'tid' => $tid ));
		}

		$data['tid'] = $tid;
		$comments = \Clay\Application::API( 'comments', 'posts', 'get', array( 'tid' => $tid ));
		
		if($user->privilege( 'comments', 'Comment', 'Create' )) {
			
			$data['add'] = \Clay\Application::URL( 'comments', 'posts', 'create' );
		}

		if($user->privilege( 'system', 'Admin', 'User' )) {

			$data['admin'] = true;
		}
		
		$defaultDepth = \Clay\Application::Setting('comments','depth',100);

		$auth = $user->authId('comment.reply');
		
		foreach( $comments as $post ){

			$post['auth'] = $auth;
			
			if( $user->privilege( 'comments', 'Comment', 'Update', $post['cid'] )) {
				
				$post['edit'] = \Clay\Application::URL( 'comments', 'post', 'edit', array('cid' => $post['cid'] ));
			}
			
			if( $user->privilege( 'comments', 'Comment', 'Delete', $post['cid'] )) {
				
				$post['delete'] = \Clay\Application::URL( 'comments', 'post', 'delete', array( 'cid' => $post['cid'] ));
			}
			
			if($defaultDepth < $post['depth']) {
				
				$post['depth'] = $defaultDepth;
			}

			if( !empty( $data['add'] )) {

				$post['reply'] = true;
			}
			
			$post['title'] = \Clay\Data\Filter( 'text', $post['title'] );
			$post['body'] = \Clay\Data\Filter( 'html', $post['body'] );
			$post['user'] = \Clay\Module::API('Users','Get',$post['uid']);
			$post['user']['image'] = 'https://www.gravatar.com/avatar/' . md5( strtolower( trim( \Clay\Module::API( 'Users', 'Email', $post['uid'] )))) . '?s=40';
			$data['posts'][] = $post;
		}
		
		return $data;
	}
}