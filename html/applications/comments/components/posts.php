<?php
/**
* Comments Application
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\component;

/**
 * Comment Posts
 * @author David
 *
 */
class posts extends \Clay\Application\Component {

	/**
	 * View
	 */
	public function view(){
		
		return \Clay\Application::Redirect('comments');
	}

	/**
	 * Manager
	 */
	public function manage(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('comments','Comment','Manage')) {
			
			$data = array();

			$posts = \Clay\Application::API('comments','posts','get', array('tid' => \Clay\Data\Get('tid','int','int')));
			
			if(is_array($posts)){
				
				foreach($posts as $key => $value){
					
					$posts[$key]['author'] = \Clay\Module::API('Users','getUser',$posts[$key]['uid']);
					$posts[$key]['title'] = \Clay\Data\Filter('text',$posts[$key]['title']);
				}
				
			} else {
				
				$posts = array();
			}
			
			$data['posts'] = $posts;
			$this->page = 'dashboard';
			return $data;
		}
		
		return \Clay\Application::Redirect('comments');
	}
}