<?php
/**
* Comments Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\comments\component;

/**
 * Comments Application Admin Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * Comments Dashboard Overview
	 * @return array $data
	 */
	public function view(){
		
		$data = array();		
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' )){
				
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		$data['threads'] = \Clay\Application::URL('comments', 'admin', 'threads');
		$data['settings'] = \Clay\Application::URL('comments', 'admin', 'settings');
		$data['hooks'] = \Clay\Application::URL('plugins', 'hooks', 'view', array('appid' => \Clay\Application::getID('comments')));
		$data['help'] = \Clay\Application::URL('comments', 'admin', 'help');
		
		return $data;
	}
	
	/**
	 * Comments Threads
	 * @return array $data
	 */
	public function threads(){
	
		$data = array();
		$user = \Clay\Module::API( 'User', 'Instance' );
	
		if( !$user->privilege( 'system', 'Admin', 'User' ) ){
	
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$data['threads'] = \Clay\Application::API( 'comments', 'threads', 'get' );
	
		//$plugins = \Clay\Module::Object('Plugins');
	
		//$plugin = $plugins->Get()->Plugin( 'content', 'comments' );
	
		//\Clay\Application::Redirect( 'plugins', 'hook', 'view', array( 'pageName' => 'app', 'ptype' => 'content', 'plugin' => 'comments' ));
	
		return $data;
	}
	
	/**
	 * Comments Dashboard Settings
	 * @return array $data
	 */
	public function settings(){
		
		$data = array();
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' )){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		# Use a ClayDO object form for our input form.
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('comments','admin','updateSettings'), 'id' => 'dashForm'));
		$form->container('set1','fieldset')->legend = 'Comments Settings';
		# Allow User Registration?
		# Define attributes for 'register'
		$threadDepth = array('type' => 'number', 'min' => 0, 'max' => 100, 'id' => 'depth', 'name' => 'depth','title' => 'Thread Depth Limit?', 'value' => \Clay\Application::Setting('comments','depth',0), 'size' => 4 );
		$form->container('set1')->property('depth','input')->attributes($threadDepth);
		$form->container('set1')->property('depth')->label = "Comments Thread Depth";
		# Hidden input for 'register' reference
		$depthSetting = array('type' => 'hidden', 'name' => 'depthSetting', 'id' => 'depthSetting', 'value' => $threadDepth['value']);
		$form->container('set1')->property('pid','input')->attributes($depthSetting);
		# Submit
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Save Settings', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	}
	
	/**
	 * Update Settings
	 * @throws \Exception
	 * @return
	 */
	public function updateSettings(){
	
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('system','Admin','User')){
				
			throw new \Exception($this->xpriv);
		}
	
		$depth = \clay\data\post('depth','int','int', 0);
		$depthSetting = \clay\data\post('depthSetting','int','int');
	
		if($depth != $depthSetting){
				
			\clay\application::set('comments','depth',$depth);
		}
	
		\clay\application::api('system','message','send','Comments Settings Updated!');
	}
	
	/**
	 * Comments Hooks
	 * @return array $data
	 */
	public function hooks(){
		
		$data = array();
		$user = \Clay\Module::API( 'User', 'Instance' );
		
		if( !$user->privilege( 'system', 'Admin', 'User' ) ){
		
			throw new \Exception( 'Additional Privileges are Required to View This Page!' );
		}
		
		$plugins = \Clay\Module::Object('Plugins');
		
		//$plugin = $plugins->Get()->Plugin( 'content', 'comments' );
		
		\Clay\Application::Redirect( 'plugins', 'hook', 'view', array( 'ptype' => 'content', 'plugin' => 'comments' ));
		
		//return $data;
	}
	/**
	 * Plugins Help
	 * @return array $data
	 */
	public function help(){
		
		
	}
}