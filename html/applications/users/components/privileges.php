<?php
namespace application\users\component;

/**
* Clay
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * Users Application Privileges Component
 * @author David
 *
 */
class privileges extends \Clay\Application\Component {
	
	# Exception Messages
	private $xpriv = 'Additional Privileges are Required to View This Page!';
	private $xuid = 'User ID (uid) must be Numeric!';
	private $xuidn = 'User ID (uid) does not exist!';
	private $xrid = 'Role ID (rid) must be Numeric!';
	private $xpid = 'Privilege ID (pid) must be Numeric!';
	private $xprid = 'Privilege Role ID (prid) must be Numeric!';
	
	/**
	 * View Action
	 * @throws \Exception
	 * @return array
	 */
	public function view(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$privs = \Clay\Module::Object('Privileges')->getAll();
		
		foreach($privs as $priv){
			
			$priv['app'] = \clay\application::getName($priv['appid']);
			$data['privileges'][] = $priv;
		}
		
		return $data;
	}
}