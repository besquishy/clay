<?php
namespace application\users\component;

/**
* Clay
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * User Application Roles Component
 * @author David
 *
 */
class roles extends \Clay\Application\Component {
	
	# Exception Messages
	private $xpriv = 'Additional Privileges are Required to View This Page!';
	private $xuid = 'User ID (uid) must be Numeric!';
	private $xuidn = 'User ID (uid) does not exist!';
	private $xrid = 'Role ID (rid) must be Numeric!';
	private $xpid = 'Privilege ID (pid) must be Numeric!';
	private $xprid = 'Privilege Role ID (prid) must be Numeric!';
	/**
	 * Uses Dashboard Page template by default
	 */
	public $page = 'dashboard';
	
	/**
	 * View Roles Action
	 * @throws \Exception
	 * @return array
	 */
	public function view(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$roles = \Clay\Module::Object('Roles');
		
		$states = array(1 => 'Active', 2 => 'Disabled');
		
		foreach($roles->getAll() as $role){
			
			$role['parent'] = $roles->get($role['prid']);
			
			$role['state'] = $states[$role['state']];
			
			$data['roles'][] = $role;
		}
		
		return $data;
	}
	
	/**
	 * Add Role Action
	 * @throws \Exception
	 * @return array
	 */
	public function add(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		
		$form = new \ClayDO\DataObject\Form;
		
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','roles','create'), 'id' => 'dashForm'));
		# Begin Fieldset for 'New Role'
		$addInfo = $form->container('roleInfo','fieldset');
		$addInfo->legend = 'New Role';
		# Role Name input
		$rolename = array('type' => 'text', 'name' => 'role', 'id' => 'role', 'title' => 'Role Name');
		$addInfo->property('role','input')->attributes($rolename);
		$addInfo->property('role')->label = 'Role Name';
		# Role Description input
		$descr = array('type' => 'text', 'name' => 'descr', 'id' => 'descr','title' => 'Role Description');
		$addInfo->property('descr','input')->attributes($descr);
		$addInfo->property('descr')->label = 'Short Description';
		# Role State input
		$state = array('name' => 'state', 'id' => 'state','title' => 'Role Status');
		$addInfo->property('state','select')->attributes($state);
		$addInfo->property('state')->label = 'Role State';
		$addInfo->property('state')->options[] = array('value' => 1, 'content' => 'Active');
		$addInfo->property('state')->options[] = array('value' => 2, 'content' => 'Disabled');
		# Role Parent input
		$parents = \Clay\Module::Object('Roles')->getAll();
		$parent = array('name' => 'parent', 'id' => 'parent','title' => 'Parent Role');
		$addInfo->property('parent','select')->attributes($parent);
		$addInfo->property('parent')->label = 'Parent Role';
		$addInfo->property('parent')->options[] = array('value' => NULL, 'content' => 'No Parent');
		
		foreach($parents as $parent){
			
			$addInfo->property('parent')->options[] = array('value' => $parent['rid'], 'content' => $parent['name']);
		}
		
		# Fieldset for Submit button
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Create Role', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	}
	
	/**
	 * Create a Role Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function create(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$role = \Clay\Data\Post('role','string','string');
		$descr = \Clay\Data\Post('descr','string','string', NULL);
		$state = \Clay\Data\Post('state','int:1','int');
		$parent = \Clay\Data\Post('parent','int:1','int', NULL);
		
		\Clay\Module::Object('Roles')->Add($role,$state,$descr,$parent);
		
		\clay\application::api('system','message','send',"Role: $role, Successfully Added!");

		\Clay\Application::Redirect('users','roles','view');
		
		return TRUE;
	}
	
/**
	 * Edit Role Action
	 * @throws \Exception
	 * @return array
	 */
	public function edit(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$data['rid'] = \Clay\Data\Get('rid','int:1','int');
		
		$roles = \Clay\Module::Object('Roles');
		
		$role = $roles->Get($data['rid']);
		
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		
		$form = new \ClayDO\DataObject\Form;
		
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','roles','update'), 'id' => 'dashForm'));
		# Begin Fieldset for 'Edit Role'
		$editInfo = $form->container('roleInfo','fieldset');
		$editInfo->legend = 'Edit Role';
		# Role Name input
		$rolename = array('type' => 'text', 'name' => 'role', 'id' => 'role', 'title' => 'Role Name', 'value' => $role['name']);
		$editInfo->property('role','input')->attributes($rolename);
		$editInfo->property('role')->label = 'Role Name';
		# Role Description input
		$descr = array('type' => 'text', 'name' => 'descr', 'id' => 'descr','title' => 'Role Description', 'value' => $role['descr']);
		$editInfo->property('descr','input')->attributes($descr);
		$editInfo->property('descr')->label = 'Short Description';
		# Role State input
		$state = array('name' => 'state', 'id' => 'state','title' => 'Role Status');
		$editInfo->property('state','select')->attributes($state);
		$editInfo->property('state')->label = 'Role State';
		$editInfo->property('state')->options[] = array('value' => 1, 'content' => 'Active');
		$editInfo->property('state')->options[] = array('value' => 2, 'content' => 'Disabled');
		$editInfo->property('state')->selected = $role['state'];
		# Role Parent input
		$parents = $roles->getAll();
		$parent = array('name' => 'parent', 'id' => 'parent','title' => 'Parent Role');
		$editInfo->property('parent','select')->attributes($parent);
		$editInfo->property('parent')->label = 'Parent Role';
		$editInfo->property('parent')->selected = $role['prid'];
		$editInfo->property('parent')->options[] = array('value' => '', 'content' => 'No Parent');
		
		foreach($parents as $parent){
			
			# Role can't be its own Parent Role
			if($parent['rid'] == $data['rid']){
				
				continue;
			}
			
			$editInfo->property('parent')->options[] = array('value' => $parent['rid'], 'content' => $parent['name']);
		}
		
		# Role ID input
		$roleid = array('type' => 'hidden', 'name' => 'rid', 'id' => 'rid', 'value' => $data['rid']);
		$editInfo->property('roleid','input')->attributes($roleid);		
		# Fieldset for Submit button
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Update Role', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		$data['role'] = $role['name'];
		
		return $data;
	}
	
	/**
	 * Update a Role Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function update(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$rid = \Clay\Data\Post('rid','int:1','int');
		$role = \Clay\Data\Post('role','string','string');
		$descr = \Clay\Data\Post('descr','string','string', NULL);
		$state = \Clay\Data\Post('state','int:1','int');
		$parent = \Clay\Data\Post('parent','int:1','int', NULL);
		
		if($rid == $parent){
			
			throw new \Exception('A Role cannot be assigned as its own Parent Role!');
		}
		
		try{
			
			\Clay\Module::Object('Roles')->Update($rid,$role,$state,$descr,$parent);
						
		} catch (\Exception $e) {
			
			throw new \Exception($e);
		}
		
		\clay\application::api('system','message','send',"Role: $role, Successfully Updated!");

		\Clay\Application::Redirect('users','roles','view');
		
		return TRUE;
	}
	
/**
	 * Delete Role Action
	 * @throws \Exception
	 * @return array
	 */
	public function delete(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$data['rid'] = \Clay\Data\Get('rid','int:1','int');
		
		$data['role'] = \Clay\Module::Object('Roles')->getName($data['rid']);
		
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		
		$form = new \ClayDO\DataObject\Form;
		
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','roles','remove'), 'id' => 'dashForm'));
		# Begin Fieldset for 'New Role'
		$removeInfo = $form->container('roleInfo','fieldset');
		$removeInfo->legend = 'Delete Role - '.$data['role'].'?';
		# Role ID input
		$roleID = array('type' => 'hidden', 'name' => 'rid', 'id' => 'rid', 'value' => $data['rid']);
		$removeInfo->property('role','input')->attributes($roleID);
		# Submit button
		$removeInfo->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Delete Role', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	}
	
	/**
	 * Remove Role Action
	 * POST
	 * @throws \Exception
	 */
	public function remove(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$rid = \Clay\Data\Post('rid','int:1','int');
		
		$roles = \Clay\Module::Object('Roles');
		
		$role = $roles->getName($rid);
		
		$trace = $roles->Unregister($rid);
		$message = "Role: $role, Successfully Deleted! <br /> Removed ".$trace['role_privileges']." Role Privileges. <br /> Unassigned ".$trace['user_roles']." Users from $role. <br /> Orphanned ".$trace['child_roles']." Child Roles.";
		
		\clay\Application::API('system','message','send',$message);

		\Clay\Application::Redirect('users','roles','view');
				
		return TRUE;
	}
}