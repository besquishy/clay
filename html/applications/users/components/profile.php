<?php
namespace application\users\component;
/**
* ClayCMS
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

class profile extends \Clay\Application\Component {

	public function view(){

		$data = array();

		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->isMember) throw new \Exception('Guests are not allowed to have profiles.');
		
		
		

		return $data;

	}

	public function edit(){

		$data = array();

		return $data;

	}
}
?>