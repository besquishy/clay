<?php
namespace application\users\component;
/**
* Clay
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * User Account Component
 * @author David
 *
 */
class account extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * User Settings
	 * 
	 */
	public function view(){

		$data = array();

		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->isMember){
			
			throw new \Exception('Only Registered Accounts are allowed to edit Account Settings!');
		}

		
	}
	

	/**
	 * Edit Action
	 * @throws \Exception
	 */
	public function edit(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->isMember){

			throw new \Exception('Only Registered Accounts are allowed to edit Account Settings!');
		}
		
		$authid = $user->authId('user.account');

		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','account','update'), 'id' => 'dashForm'));
		# Begin Fieldset for 'Edit User Info'
		$editInfo = $form->container('userInfo','fieldset');
		$editInfo->legend = 'Edit Account Settings';
		/*# Username input
		$uname = array('type' => 'text', 'name' => 'uname', 'id' => 'uname', 'title' => 'Username', 'value' => $userinfo['uname']);
		$editInfo->property('uname','input')->attributes($uname);
		$editInfo->property('uname')->label = 'Username';*/
		# Email input
		$email = array('type' => 'text', 'name' => 'email', 'id' => 'email','title' => 'Email Address', 'placeholder' => $user->info()['email']);
		$editInfo->property('email','input')->attributes($email);
		$editInfo->property('email')->label = 'Email';
		# Password input
		$passw = array('type' => 'password', 'name' => 'passw', 'title' => 'User Password', 'id' => 'passw');
		$editInfo->property('passw','input')->attributes($passw);		
		$editInfo->property('passw')->label = 'Password';
		$passconf = array('name' => 'passconf', 'id' => 'passconf', 'title' => 'Change Password?', 'value' => 'true');
		$editInfo->property('passconf','checkbox')->attributes($passconf);
		$editInfo->property('passconf')->label = 'Change Password?';
		# User ID
		$userid = array('type' => 'hidden', 'name' => 'uid', 'id' => 'uid', 'value' => $user->id());
		$editInfo->property('userid','input')->attributes($userid);
		# Authorization
		$auth = array('type' => 'hidden', 'name' => $authid['field'], 'id' => $authid['field'], 'value' => $authid['id']);
		$editInfo->property('authid','input')->attributes($auth);
		/*# User State input
		$state = array('name' => 'state', 'id' => 'state','title' => 'User Status');
		$editInfo->property('state','select')->attributes($state);
		$editInfo->property('state')->label = 'User Status';*/
		# Fieldset for Submit button
		$form->container('footer','fieldset')->property('submit','button')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Save User Info', 'class' => 'btn btn-primary dashSubmit'));
		$form->container('footer')->property('submit')->icon = "fa fa-check-circle fa-lg";
		# Pass form object into the template data.
		$data['form'] = $form;

		return $data;
	}

	/**
	 * Update Action
	 */
	public function update(){

		$user = \Clay\Module::API('User','Instance');

		if(!$user->auth('user.account')){

			throw new \exception('Unauthorized request.');
		}

		if(!$user->isMember){

			throw new \Exception('Only Registered Accounts are allowed to edit Account Settings!');
		}

		# Check for User ID - must be INTEGER
		$userid = \clay\data\post('uid','string','int');

		if(empty($userid) || ($userid !== $user->id())){
			
			throw new \Exception('Invalid User ID');
		}

		$email = \clay\data\post('email','email','email');
		
		if(empty($email)) {
			
			# Retain current Email
			$email = FALSE;
		
		} else {
			
			$email = strtolower($email);

			$Audit = \Clay\Module::API('Users','Audit',array('email' => $email));
			
			if(!empty($Audit['email'])){
				
				$msg[] = 'User Email: '.$email.' already exists under a differet User ID (uid)!';
			}
		}
		
		$passconf = \clay\data\post('passconf','bool');
		
		if(!empty($passconf)){
			
			$password = \clay\data\post('passw','string','string');
			
			if(!empty($password)){
				
				$userkey = md5(time().mt_rand());
				$password = hash_hmac('sha512', $password , $userkey);
				
				# Set the User's Key		
				\Clay\Module\Users::Set($user->id(), 'key', $userkey);
				
			} else {
				
				$msg[] = 'You must enter a new Password to perform a Password Change!';
			}
		} else {
			
			$password = FALSE;
		}

		if(!empty($msg)){
			
			\clay\application::api('system','message','send',$msg);			
			return;
		}

		if(($password == false) && ($email == false)){
			
			\clay\application::api('system','message','send','No Changes Were Submitted!');			
			return;
		}

		# Update Account Info.
		$userID = \Clay\Module::API('Users','Update',array('userid' => $user->id(), 'password' => $password,'email' => $email));
	
		\clay\application::api('system','message','send',"Account successfully updated!");
		return TRUE;
	}
}