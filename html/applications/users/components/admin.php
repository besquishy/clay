<?php
namespace application\users\component;

/**
* Clay
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * User Application Admin Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component {
	
	# Exception Messages
	private $xpriv = 'Additional Privileges are Required to View This Page!';
	private $xuid = 'User ID (uid) must be Numeric!';
	private $xuidn = 'User ID (uid) does not exist!';
	private $xrid = 'Role ID (rid) must be Numeric!';
	private $xpid = 'Privilege ID (pid) must be Numeric!';
	private $xprid = 'Privilege Role ID (prid) must be Numeric!';

	private $states = array(1 => 'Active', 2 => 'Inactive', 3 => 'Disabled', 4 => 'Admin Deleted', 5 => 'User Deleted');

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';
	
	/**
	 * View Action
	 * @throws \Exception
	 * @return array
	 */
	public function view(){
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data['help'] = \clay\application::url('users','help','view');
		$data['settings'] = \clay\application::url('users','admin','settings');
		$data['privileges'] = \clay\application::url('users','privileges','view');
		$data['roles'] = \clay\application::url('users','roles','view');
		$data['manage'] = \clay\application::url('users','admin','manage');
		
		return $data;
	}

	/**
	 * Manage Action
	 * @throws \Exception
	 * return array
	 */
	public function manage(){
		
		# User Module Object
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		# Get Users
		$users = \Clay\Module::API('Users','getAll');
		
		# Roles Module Object
		$roles = \Clay\Module::Object('Roles');
		
		$data['status'] = $this->states;
		
		foreach($users as $User){
			
			# Get Assigned Roles (not inherited)
			$userRoles = $roles->User($User['uid']);
			
			# Get Info about Assigned Roles
			foreach($userRoles as $role){
				
				# Get Role Name from Role ID
				$roleInfo['name'] = $roles->getName($role['rid']);
				$roleInfo['rid'] = $role['rid'];
				
				$User['roles'][] = $roleInfo;
			}
			
			$data['users'][] = $User;
		}
		
		return $data;
	}
	
	/**
	 * Settings Action
	 * @throws \Exception
	 * @return array
	 */
	public function settings(){
	
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
	
		$data = array();
	
		# Use a ClayDO object form for our input form.
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','admin','updateSettings'), 'id' => 'dashForm'));
		$form->container('set1','fieldset')->legend = 'User Settings';
		# Allow User Registration?
		# Define attributes for 'register'
		$allowReg = array('id' => 'reg', 'name' => 'reg','title' => 'Allow Users to Register?');
		$form->container('set1')->property('register','select')->attributes($allowReg);
		$form->container('set1')->property('register')->label = "Allow Users to Register?";
		# Define options for our select field
		$form->container('set1')->property('register')->options[0] = array('value' => 0, 'content' => 'No');
		$form->container('set1')->property('register')->options[1] = array('value' => 1, 'content' => 'Yes');
		$form->container('set1')->property('register')->selected = \clay\application::setting('users','registration',0);
		# Hidden input for 'register' reference
		$regSetting = array('type' => 'hidden', 'name' => 'regSetting', 'id' => 'regSetting', 'value' => $form->container('set1')->property('register')->selected);
		$form->container('set1')->property('pid','input')->attributes($regSetting);
		# Submit
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Save Settings', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	
	}
	
	/**
	 * Update Settings
	 * @throws \Exception
	 * @return
	 */
	public function updateSettings(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$registration = \clay\data\post('reg','int','int', 0);
		$regSetting = \clay\data\post('regSetting','int','int');
		
		if($registration != $regSetting){
			
			\clay\application::set('users','registration',$registration);
		}
		
		\clay\application::api('system','message','send','User Settings Updated!');
	}
	
	/**
	 * Edit User Action
	 * @param array $args
	 * @throws \Exception
	 * @return array
	 */
	public function edit($args){
		
		# Check for Admin Privilege
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		# Check for User ID - must be INTEGER
		$userid = !empty($args['uid']) ? \clay\data\validate('int', $args['uid']) : \clay\data\get('uid','int','int');
		
		if(empty($userid)){
			
			throw new \Exception($this->xuid);
		}
		
		# Get this User ID's infor
		$userinfo = \Clay\Module::API('Users','Get',$userid);
		
		# User ID doesn't exist
		if(empty($userinfo)){
			
			throw new \Exception($this->xuidn);
		}
		
		$data = array();
		$data['userinfo'] = $userinfo;
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','admin','update'), 'id' => 'dashForm'));
		# Begin Fieldset for 'Edit User Info'
		$editInfo = $form->container('userInfo','fieldset');
		$editInfo->legend = 'Edit User Info';
		# Username input
		$uname = array('type' => 'text', 'name' => 'uname', 'id' => 'uname', 'title' => 'Username', 'value' => $userinfo['uname']);
		$editInfo->property('uname','input')->attributes($uname);
		$editInfo->property('uname')->label = 'Username';
		# Email input
		$email = array('type' => 'text', 'name' => 'email', 'id' => 'email','title' => 'Email Address', 'value' => $userinfo['email']);
		$editInfo->property('email','input')->attributes($email);
		$editInfo->property('email')->label = 'Email';
		# Password input
		$passw = array('type' => 'password', 'name' => 'passw', 'title' => 'User Password', 'id' => 'passw');
		$editInfo->property('passw','input')->attributes($passw);		
		$editInfo->property('passw')->label = 'Password';
		$passconf = array('name' => 'passconf', 'id' => 'passconf', 'title' => 'Change Password?', 'value' => 'true');
		$editInfo->property('passconf','checkbox')->attributes($passconf);
		$editInfo->property('passconf')->label = 'Change Password?';
		$userid = array('type' => 'hidden', 'name' => 'uid', 'id' => 'uid', 'value' => $userid);
		$editInfo->property('userid','input')->attributes($userid);
		# User State input
		$state = array('name' => 'state', 'id' => 'state','title' => 'User Status');
		$editInfo->property('state','select')->attributes($state);
		$editInfo->property('state')->label = 'User Status';
				
		foreach($this->states as $key => $state){
			
			$option = array('value' => $key, 'content' => $state);
			
			if($userinfo['state'] == $key){
				
				$editInfo->property('state')->selected = $key;
			}
			
			$editInfo->property('state')->options[] = $option;
		}
		
		# User Roles input
		$Roles = \Clay\Module::Object('Roles');
		$roles = $Roles->getAll();
		$assignedRoles = $Roles->User($userinfo['uid']);
		$role = array('name' => 'rid[]', 'id' => 'rid', 'multiple' => 'multiple', 'title' => 'Assign Roles');
		$editInfo->property('roles','multiselect')->attributes($role);
		$editInfo->property('roles')->label = 'Assign Roles';
		$editInfo->property('roles')->options[] = array('value' => NULL, 'content' => 'No Role');
		
		foreach($roles as $role){
			
			$option = array('value' => $role['rid'], 'content' => $role['name']);
			
			foreach($assignedRoles as $assignedRole){
				
				if($assignedRole['rid'] == $role['rid']){
					
					$option['selected'] = 'selected';
				}
			}
			
			$editInfo->property('roles')->options[] = $option;
		}
		
		# Fieldset for Submit button
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Save User Info', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	}
	
	/**
	 * Update User Action
	 * @throws \Exception
	 */
	public function update(){
		
		# Check for Admin Privilege
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		# Check for User ID - must be INTEGER
		$userid = \clay\data\post('uid','int','int');
		
		if(empty($userid)){
			
			throw new \Exception($this->xuid);
		}
		
		# Get current User Info
		$userinfo = \Clay\Module::API('Users','Get', $userid);
		# Username: POST - uname
		$username = \clay\data\post('uname','alnum','string | alnum');
		
		if(empty($username)){
			
			# Retain current Username
			$username = FALSE;
		
		} else {
			
			$username = strtolower($username);
			
			if($username !== $userinfo['uname']){
				
				$Audit = \Clay\Module::API('Users','Audit',array('uname' => $username));
				
				if(!empty($Audit['uname'])){
					
					$msg[] = 'Username: '.$username.' already exists under a differet User ID (uid)!';
				}
			} else {
				
				$username = FALSE;
			}
		}
		
		$email = \clay\data\post('email','email','email');
		
		if(empty($email)) {
			
			# Retain current Email
			$email = FALSE;
		
		} else {
			
			$email = strtolower($email);
			
			if($email !== $userinfo['email']){
				
				$Audit = \Clay\Module::API('Users','Audit',array('email' => $email));
				
				if(!empty($Audit['email'])){
					
					$msg[] = 'User Email: '.$email.' already exists under a differet User ID (uid)!';
				}
			} else {
				
				$email = FALSE;
			}
		}
		
		$passconf = \clay\data\post('passconf','bool');
		
		if(!empty($passconf)){
			
			$password = \clay\data\post('passw','string','string');
			
			if(!empty($password)){
				
				$userkey = md5(time().mt_rand());
				$password = hash_hmac('sha512', $password , $userkey);
				
				# Set the User's Key		
				\Clay\Module\Users::Set($userid, 'key', $userkey);
				
			} else {
				
				$msg[] = 'You must enter a new Password to perform a Password Change!';
			}
		} else {
			
			$password = FALSE;
		}
		
		if(!empty($msg)){
			
			\clay\application::api('system','message','send',$msg);			
			return;
		}
		
		$state = \Clay\Data\Post('state','int:1','int', 1);
		
		$Roles = \Clay\Module::Object('Roles');
		
		# Lazy way - Drop all User's Roles and Add Selected Roles
		# If nothing else, this prevents the possibility of an error in processing
		$Roles->dropUser($userid);
		
		$roles = \Clay\Data\Post('rid');
		
		if(is_array($roles)){
			# Only add Roles if Roles were selected
			foreach($roles as $role){
				
				if(!empty($role)){
					$Roles->addUser($userid, $role);
				}
			}
		}
		
		# Add the user to the database @todo: Make the 'state' configurable / add user confirmation option.
		$userID = \Clay\Module::API('Users','Update',array('userid' => $userid, 'username' => $username,'password' => $password,'email' => $email,'state' => $state));
	
		\clay\application::api('system','message','send',"User: ".$userinfo['uname']." - successfully updated!");
		return TRUE;
	}
	
	/**
	 * Add User Action
	 * @throws \Exception
	 */
	public function add(){
		
		# Check for Admin Privilege
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','admin','create'), 'id' => 'dashForm'));
		# Begin Fieldset for 'Edit User Info'
		$addInfo = $form->container('userInfo','fieldset');
		$addInfo->legend = 'New User Info';
		# Username input
		$uname = array('type' => 'text', 'name' => 'uname', 'id' => 'uname', 'title' => 'Username');
		$addInfo->property('uname','input')->attributes($uname);
		$addInfo->property('uname')->label = 'Username';
		# Email input
		$email = array('type' => 'text', 'name' => 'email', 'id' => 'email','title' => 'Email Address');
		$addInfo->property('email','input')->attributes($email);
		$addInfo->property('email')->label = 'Email';
		# Password input
		$passw = array('type' => 'password', 'name' => 'passw', 'title' => 'User Password', 'id' => 'passw');
		$addInfo->property('passw','input')->attributes($passw);
		$addInfo->property('passw')->label = 'Password';
		# User State input
		$state = array('name' => 'state', 'id' => 'state','title' => 'User Status');
		$addInfo->property('state','select')->attributes($state);
		$addInfo->property('state')->label = 'User Status';
		$addInfo->property('state')->options[] = array('value' => 1, 'content' => 'Active');
		$addInfo->property('state')->options[] = array('value' => 2, 'content' => 'Inactive');
		$addInfo->property('state')->options[] = array('value' => 3, 'content' => 'Disabled');
		# User Roles input
		$roles = \Clay\Module::Object('Roles')->getAll();
		$role = array('name' => 'rid[]', 'id' => 'rid', 'multiple' => 'multiple', 'title' => 'Assign Roles');
		$addInfo->property('roles','select')->attributes($role);
		$addInfo->property('roles')->label = 'Assign Roles';
		$addInfo->property('roles')->options[] = array('value' => NULL, 'content' => 'No Role');
		
		foreach($roles as $role){
			
			$addInfo->property('roles')->options[] = array('value' => $role['rid'], 'content' => $role['name']);
		}
		# Fieldset for Submit button
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Create User', 'class' => 'dashSubmit'));
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	}
	
	/**
	 * Create User Action
	 * POST
	 */
	public function create(){
		# Check for Admin Privilege
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
	
		$username = strtolower(\Clay\Data\Post('uname','string','string'));
		
		$email = strtolower(\Clay\Data\Post('email','email','email'));
		
		$audit = \Clay\Module::API('Users','Audit',array('email' => $email, 'uname' => $username));
		
		# Check for existing username			
		if(!empty($audit['uname'])){
			
			$msg[] = 'The Username <strong>'.$username.'</strong> is already registered. ';
		}
		
		# Check for existing email address
		if(!empty($audit['email'])){
			
			$msg[] = 'The Email Address <strong>'.$email.'</strong> is already registered. ';
		}
		
		if(!empty($msg)){ 
			
			\clay\application::api('system','message','send',$msg);
			return;
		}
		
		$password = \Clay\Data\Post('passw','string','string');
		
		# Setup the User Key and Password
		$userkey = md5(time().mt_rand());
		$password = hash_hmac('sha512', $password , $userkey);
		
		$state = \Clay\Data\Post('state','int:1','int', 1);
		
		# Add the user to the database @todo: Make the 'state' configurable / add user confirmation option.
		$userID = \Clay\Module::API('Users','Add',array('username' => $username,'password' => $password,'email' => $email,'state' => $state));
		
		# Set the User's Key		
		\Clay\Module\Users::Set($userID, 'key', $userkey);
		
		# Add a Role (user) @TODO: Make the default role configuration.
		$Roles = \Clay\Module::Object('Roles');
		
		$roles = \Clay\Data\Post('rid');
		
		foreach($roles as $role){
		
			$Roles->addUser($userID, $role);
		}
		
		\clay\application::api('system','message','send',"User: $username - successfully registered!");
		
		return TRUE;
	}
	
	/**
	 * Delete User Action
	 * @param array $args
	 * @throws \Exception
	 */
	public function delete($args){
		
		# Check for Admin Privilege
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		# Check for User ID - must be INTEGER
		$userid = !empty($args['uid']) ? \clay\data\validate('int', $args['uid']) : \clay\data\get('uid','int','int');
		
		if(empty($userid)){
			
			throw new \Exception($this->xuid);
		}
		
		# Get this User ID's infor
		$userinfo = \Clay\Module::API('Users','Get',$userid);
		
		# User ID doesn't exist
		if(empty($userinfo)){
			
			throw new \Exception($this->xuidn);
		}
		
		$data = array();
		$data['userinfo'] = $userinfo;
		# Build the Form with ClayDO
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		# Initialize Form attributes
		$form->attributes(array('method' => 'POST', 'action' => \clay\application::url('users','admin','remove'), 'id' => 'dashForm'));
		# Begin Fieldset for 'Delete User'
		$form->container('delete','fieldset');
		$form->container('delete')->property('uid','input')->attributes(array('type' => 'hidden', 'id' => 'uid',  'name' => 'uid', 'value' => $userinfo['uid']));
		$form->container('delete')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit',  'name' => 'submit', 'value' => 'Delete User', 'class' => 'dashSubmit'));
		$form->container('delete')->legend = 'Delete User';
		$form->container('delete')->intro = 'Are you sure you want to delete '.$userinfo['uname']."?<br />At this time the User won't be physically deleted from the system, but will be set in a deleted Status";
		# Pass form object into the template data.
		$data['form'] = $form;
		
		return $data;
	}
	
	/**
	 * Remove User Action
	 * POST
	 * @throws \Exception
	 */
	public function remove(){
		
		# Check for Admin Privilege
		$user = \Clay\Module::API('User','Instance');
	
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$uid = \Clay\Data\Post('uid','int','int');
		
		\Clay\Module::API('Users','Update', array('userid' => $uid, 'state' => 4));
		
		\clay\application::api('system','message','send','User ID: '.$uid.' has been placed in Admin Deleted status. The User still exists, but is disabled.');
	}
}