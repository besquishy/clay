<?php
namespace application\users\component;

/**
* Clay
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * User Application Role Component
 * @author David
 *
 */
class role extends \Clay\Application\Component {
	
	# Exception Messages
	private $xpriv = 'Additional Privileges are Required to View This Page!';
	private $xuid = 'User ID (uid) must be Numeric!';
	private $xuidn = 'User ID (uid) does not exist!';
	private $xrid = 'Role ID (rid) must be Numeric!';
	private $xpid = 'Privilege ID (pid) must be Numeric!';
	private $xprid = 'Privilege Role ID (prid) must be Numeric!';
	/**
	 * Uses Dashboard Page template by default
	 */
	public $page = 'dashboard';
	
	/**
	 * View Role Action
	 * @throws \Exception
	 * @return array
	 */
	public function view(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$data['roleID'] = \Clay\Data\Get('rid','int:1','int');
		
		$roles = \Clay\Module::Object('Roles');
		
		$data['roleDescription'] = $roles->getDescription($data['roleID']);		
		$data['roleName'] = $roles->getName($data['roleID']);
		
		$priv = \Clay\Module::API('Privileges','Instance');
		
		$rolesTree = $roles->Tree($data['roleID']);
		
		foreach($rolesTree as $branch){
			
			$role['name'] = $roles->getName($branch);			
			$role['rid'] = $branch;
			
			$rolePrivs = $priv->getRole($branch);
			
			foreach($rolePrivs as $privilege){
				
				$role['privileges'][$privilege['rpid']] = $priv->getInfo($privilege['pid']);
				$role['privileges'][$privilege['rpid']]['rpid'] = $privilege['rpid'];
				$role['privileges'][$privilege['rpid']]['scope'] = $privilege['scope'];
				$role['privileges'][$privilege['rpid']]['app'] = \Clay\Application::getName($role['privileges'][$privilege['rpid']]['appid']);
				$role['privileges'][$privilege['rpid']]['pid'] = $privilege['pid'];
				
			}
			
			$data['roles'][] = $role;
			unset($role);
		}

		return $data;
	}
	
	/**
	 * Add Role Privilege Action
	 * Add a Privilege to a Role
	 * @param array $args
	 * @throws \Exception
	 * @return array
	 */
	public function add($args){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$role = array();
		
		$roles = \Clay\Module::Object('Roles');
		
		$data['role'] = \Clay\Data\Get('rid','int:1','int');
		$data['role'] = $roles->get($data['role']);		
		$data['pid'] = \Clay\Data\Get('pid','int:1','int');		
			
		$privileges = \Clay\Module::Object('Privileges')->getAll();
		
		foreach($privileges as $privilege){
			
			$privilege['app'] = \Clay\Application::getName($privilege['appid']);
			
			$data['privileges'][] = $privilege;
			
			if(!empty($data['pid']) AND ($privilege['pid'] == $data['pid'])){

				$data['privilege'] = $privilege;				
			}
		}
		
		unset($privileges);	
		
		if(!empty($data['pid'])){
			
			$app = \Clay\Application::getName($data['privilege']['appid']);
			
			$data['privilege']['app'] = $app;
			
			$data['scope'] = \Clay\Data\Get('scope','string','string');
			
			$appPrivilege = \Clay\Application_Privilege($app, $data['privilege']['component']);
			
			$data['scopes'] = $appPrivilege->Scopes($data['privilege']['name']);
			
			if(!empty($data['scope'])){
				
				$options = $appPrivilege->Scopes($data['privilege']['name'], $data['scope']);
				
				if(empty($options)){
					
					throw new \Exception('Privilege Scope '.$data['scope'].' selection is not available in the requested Application Component.');
				}
				
				if(is_string($options)){
					
					$data['final_scope'] = $options;
					
				} else {
					
					$data['itemtype'] = !empty($options['type']) ? $options['type'] : 'Items';
					$data['items'] = !empty($options['items']) ? $options['items'] : NULL;
					$data['item'] = \Clay\Data\Get('item','string','string', NULL);
				}
				
			} else {
				
				$data['scopes'] = $appPrivilege->Scopes($data['privilege']['name']);
			}
		}
		
		return $data;
	}
	
	/**
	 * Create Role Privilege Action
	 * POST
	 * @throws \Exception
	 * @return boolean
	 */
	public function create(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$rid = \Clay\Data\Post('rid','int:1','int');
		$pid = \Clay\Data\Post('pid','int:1','int');
		$scope = \Clay\Data\Post('scope','string','string');
		$item = \Clay\Data\Post('item','string','string');
		
		$rights = !empty($item) ? $scope.'::'.$item : $scope;
		
		\Clay\Module::API('Privileges','Instance')->addRole($rid, $pid, $rights);
		
		\clay\application::api('system','message','send',"Privilege Successfully Added to Role ID: $rid!");
		
		return TRUE;
	}
	
	/**
	 * Edit Role Privilege Action
	 * @throws \Exception
	 * @return array
	 */
	public function edit(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$role = array();
		
		$roles = \Clay\Module::Object('Roles');
		
		$rpid = \Clay\Data\Get('rpid','int:1','int');

		$data['privilege'] = \Clay\Module::API('Privileges','Instance')->getRolePrivilege($rpid);
		$data['roleName'] = \Clay\Module::Object('Roles')->getName($data['privilege']['rid']);
		$data['privilege']['info'] = \Clay\Module::API('Privileges','Instance')->getInfo($data['privilege']['pid']);
		$data['privilege']['info']['app'] = \Clay\Application::getName($data['privilege']['info']['appid']);
		
		
		$data['role'] = $data['privilege']['rid'];
		$data['role'] = $roles->get($data['role']);		
		$data['pid'] = $data['privilege']['pid'];	
		
		$scope = explode('::', $data['privilege']['scope']);
		
		$data['scope'] = \Clay\Data\Get('scope','string','string', $scope[0]);
		
		$appPrivilege = \Clay\Application_Privilege($data['privilege']['info']['app'], $data['privilege']['info']['component']);
		
		$data['scopes'] = $appPrivilege->Scopes($data['privilege']['info']['name']);
			
		$options = $appPrivilege->Scopes($data['privilege']['info']['name'], $data['scope']);
		
		if(empty($options)){
			
			throw new \Exception('Privilege Scope '.$data['scope'].' selection is not available in the requested Application Component.');
		}
		
		if(is_string($options)){
			
			$data['final_scope'] = $options;
			
		} else {
			
			$data['itemtype'] = !empty($options['type']) ? $options['type'] : 'Items';
			$data['items'] = !empty($options['items']) ? $options['items'] : NULL;
			$data['item'] = \Clay\Data\Get('item','string','string', !empty($scope[1]) ? $scope[1] : '');
		}
		
		return $data;
	}
	
	/**
	 * Update Role Privilege Action
	 * POST
	 * @throws \Exception
	 */
	public function update(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$rpid = \Clay\Data\Post('rpid','int:1','int');
		$scope = \Clay\Data\Post('scope','string','string');
		$item = \Clay\Data\Post('item','string','string');
		$rid = \Clay\Data\Post('rid','int:1','int');
		$rights = !empty($item) ? $scope.'::'.$item : $scope;
		
		\Clay\Module::API('Privileges','Instance')->updateRole($rpid, $rights);
		
		\clay\application::api('system','message','send',"Privilege Successfully Updated in Role ID: $rid!");
		
		return TRUE;
	}
	
	/**
	 * Delete Role Privilege Action
	 * @throws \Exception
	 * @return array
	 */
	public function delete(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$rpid = \Clay\Data\Get('rpid','int:1','int');

		$data['privilege'] = \Clay\Module::API('Privileges','Instance')->getRolePrivilege($rpid);
		$data['roleName'] = \Clay\Module::Object('Roles')->getName($data['privilege']['rid']);
		$data['privilege']['info'] = \Clay\Module::API('Privileges','Instance')->getInfo($data['privilege']['pid']);
		$data['privilege']['info']['app'] = \Clay\Application::getName($data['privilege']['info']['appid']);
		
		return $data;
	}
	
	/**
	 * Remove Role Privilege Action
	 * POST
	 * @throws \Exception
	 */
	public function remove(){
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception($this->xpriv);
		}
		
		$data = array();
		
		$rpid = \Clay\Data\Post('rpid','int:1','int');
		$role = \Clay\Data\Post('role','string','string');
		
		\Clay\Module::API('Privileges','Instance')->removeRole($rpid);
		
		\clay\Application::API('system','message','send',"Role Privilege (ID: $rpid) Successfully Removed from Role $role!");
				
		return TRUE;
	}
}