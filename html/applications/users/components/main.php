<?php
namespace application\users\component;

/**
* Clay
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * Users Application Main Component
 * @author David
 *
 */
class main extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * View Action
	 */
	public function view(){

		$data = array();

		$user = \Clay\Module::API('User','Instance');

		if($user->isMember) goto end;

		$authid = $user->authId('user.login');

		\Library('ClayDO/DataObject/Form');

		$form = new \ClayDO\DataObject\form;

		$form->attributes(array('method' => 'post', 'action' => \Clay\Application::URL('users','main','authenticate'),'class' => 'login-form'));
		$form->container('set1','fieldset')->legend = 'User Login';
		$form->container('set1')->intro = 'Please enter your Username and Password.';
		$form->container('set1')->property('username','text')->attributes(array('id' => 'uname', 'name' => 'uname','title' => 'Enter your username.'));
		$form->container('set1')->property('username')->label = "User Name";

		$form->container('set1')->property('password','input')->attributes(array('type' => 'password', 'id' => 'passw', 'name' => 'passw','title' => 'Enter your password.'));
		$form->container('set1')->property('password')->label = "Password";
		$auth = array('type' => 'hidden', 'name' => $authid['field'], 'id' => $authid['field'], 'value' => $authid['id']);
		$form->container('set1')->property('authid','input')->attributes($auth);
		$form->container('footer','fieldset')->property('submit','submit')->attributes(array('type' => 'submit', 'id' => 'submit', 'name' => 'submit', 'value' => 'Login'));

		$data['form'] = $form;

		end:
		return $data;
	}

	/**
	 * Authenticate()
	 * Action for logging in a user
	 * @return NULL
	 * @todo Allow login with email address
	 */
	public function authenticate(){

		$user = \Clay\Module::API('User','Instance');

		if($user->isMember){

			throw new \exception('You Are Already Logged In.');
		}

		if(!$user->auth('user.login')){

			throw new \exception('Unauthorized Request.');
		}

		# Fetch the POST vars
		$username = \clay\data\post('uname','alnum','string | alnum');
		$password = \clay\data\post('passw','string','string');

		# Check validations
		if(empty($username) || empty($password)){
			# Failed Check
			\Clay\Application::API('system','message','send','Invalid Username or Password');
			return \Clay::Redirect($_SERVER['HTTP_REFERER']);
		}
		# Authenticate with the Security Module
		return \Clay\Module::Object( 'Security' )->Authenticate( $username, $password );
	}

	/**
	 * Logout Action
	 */
	public function logout(){
		
		# User object
		$user = \Clay\Module::API('User','Instance');
		# Reset the User ID to 1 (guest)
		$user->id(1);
		# The reset should give the user the guest User ID and create a blank session
		# Send a System Message notification
		\Clay\Application::API('system','message','send','You have been logged out!');
		# Redirect to the Login page
		return \Clay\Application::Redirect('users');
	}

	/**
	 * Registration Action
	 * @throws \Exception
	 */
	public function registration(){
		
		$data = array();
		$allowReg = \Clay\Application::Setting('users','registration');
		
		if(empty($allowReg)){
			
			return $data;
		}
		
		$user = \Clay\Module::API('User','Instance');

		if($user->isMember){
			
			throw new \Exception('Silly! You are already registered!');
		}
		
		$authid = $user->authId('user.register');

		\Library('ClayDO/DataObject/Form');

		$form = new \ClayDO\DataObject\Form;

		$form->attributes(array('method' => 'post', 'action' => \Clay\Application::URL('users','','register'),'class' => 'registration-form'));
		$form->container('set1','fieldset')->legend = 'Registration';
		$form->container('set1')->intro = 'Please enter your desired Username and Password.';
		$form->container('set1')->property('username','text')->attributes(array('id' => 'uname', 'name' => 'uname','title' => 'Enter a username.'));
		$form->container('set1')->property('username')->label = "User Name";

		$form->container('set1')->property('password1','input')->attributes(array('type' => 'password', 'id' => 'passw1', 'name' => 'passw1','title' => 'Enter a password.'));
		$form->container('set1')->property('password1')->label = "Password";

		$form->container('set1')->property('password2','input')->attributes(array('type' => 'password', 'id' => 'passw2', 'name' => 'passw2','title' => 'Re-enter your password.'));
		$form->container('set1')->property('password2')->label = "Re-enter Password";

		$form->container('set1')->property('email1','text')->attributes(array('id' => 'email1', 'name' => 'email1','title' => 'Enter your email address.'));
		$form->container('set1')->property('email1')->label = "Email Address";

		$form->container('set1')->property('email2','text')->attributes(array('id' => 'email2', 'name' => 'email2','title' => 'Re-enter your email address.'));
		$form->container('set1')->property('email2')->label = "Re-enter Email Address";

		$auth = array('type' => 'hidden', 'name' => $authid['field'], 'id' => $authid['field'], 'value' => $authid['id']);
		$form->container('set1')->property('authid','input')->attributes($auth);

		$form->container('footer','fieldset')->property('submit','submit')->attributes(array('type' => 'submit', 'id' => 'submit', 'name' => 'submit', 'value' => 'Register'));
		$form->container('footer')->intro = 'The information you have provided is intended for communication between this site and you. Email addresses associated with user accounts are not shared.';
		$data['form'] = $form;

		return $data;
	}

	/**
	 * Register Action
	 * POST
	 * @throws \Exception
	 */
	public function register(){
		
		$allowReg = \Clay\Application::Setting('users','registration');

		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->auth('user.register')){

			throw new \exception('Unauthorized Request.');
		}

		if(empty($allowReg)){
			
			throw new \Exception('User registration has been disabled (possibly temporary).');
		}
		
		$msg = array();
		$username = \Clay\Data\Post('uname','alnum','string | alnum');
		
		if(empty($username)){
			
			$msg[] = 'Please enter a valid username. ';
			
		} else {
			
			$username = strtolower($username);
			
			$Audit = \Clay\Module::API('Users','Audit',array('uname' => $username));
			# Check for existing username			
			if(!empty($Audit['uname'])){
				
				$msg[] = 'The Username <strong>'.$username.'</strong> is already registered. ';
			}
		}
		
		$password = \clay\data\post('passw1','string','string');
		
		if(empty($password)){
			
			$msg[] = 'Please enter a password. ';
		}
		
		$password2 = \clay\data\post('passw2','string','string');
		
		if(empty($password2)){
			
			$msg[] = 'Please enter the password and a confirmation. ';
		}
		
		if($password !== $password2){
			
			$msg[] = 'The entered passwords did not match. ';
		}
		
		$email = strtolower(\clay\data\post('email1','email','email'));
		
		if(empty($email)){
			
			$msg[] = 'Please enter a valid email address. ';
		}
		
		$Audit = \Clay\Module::API('Users','Audit',array('email' => $email));
		
		# Check for existing email address
		if(!empty($Audit['email'])){
			
			$msg[] = 'The Email Address <strong>'.$email.'</strong> is already registered. ';
		}
		
		$email2 = strtolower(\clay\data\post('email2','email','email'));
		
		if(empty($email2)){
			
			$msg[] = 'Please enter the email address and a confirmation. ';
		}
		
		if($email !== $email2){
			
			$msg[] = 'The entered email addresses did not match. ';
		}

		if(!empty($msg)){ 
			
			\clay\application::api('system','message','send',$msg);
			\clay::redirect($_SERVER['HTTP_REFERER']); 
			return;
		}
		
		# Setup the User Key and Password
		$userkey = md5(time().mt_rand());
		$password = hash_hmac('sha512', $password , $userkey);
		
		# Add the user to the database @todo: Make the 'state' configurable / add user confirmation option.
		$userID = \Clay\Module::API('Users','Add',array('username' => $username,'password' => $password,'email' => $email,'state' => 1));
		
		# Set the User's Key
		\Clay\Module\Users::Set($userID,'key',$userkey);
		
		# Add a Role (user) @TODO: Make the default role configuration.
		$roles = \Clay\Module::Object('Roles');
		
		$roles->addUser($userID, $roles->getID('user'));
		
		\clay\application::api('system','message','send',"User: $username - successfully registered!");
		
		return \clay\application::redirect('users');
	}
}