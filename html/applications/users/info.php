<?php
/**
 * Clay Framework
 *
 * @copyright (C) 2007-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Installer
 */
$data = array('title' => 'Users',
			'name' => 'users',
			'version' => '1.0.2',
			'date' => 'September 16, 2017',
			'description' => 'Users',
			'class' => 'System',
			'category' => 'Administration',
			'depends' => array('modules' => array('User','Roles','Privileges','Session','Plugins'), 'applications' => array('dashboard')),
			'core' => TRUE,
			);
?>