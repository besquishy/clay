<?php
namespace application\users\plugin\dashboard;

/**
 * User Dashboard Plugin
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */
class user extends \Clay\Application\Plugin {
	
	public $PluginApp = 'users';
	public $PluginType = 'dashboard';
	public $Plugin = 'user';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function view( $args ){
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		# @todo: Use role privileges here.
		if(!$user->isMember){
			
			$regAllowed = \Clay\Application::Setting('users','registration');
			$guest = \Clay\Application('users');
			$guest->template = 'includes/login';
			$data['login'] = $guest->action('view');
			
			if(!empty($regAllowed)){
				
				$guest->template = 'includes/register';
				$data['register'] = $guest->action('registration');
			}			
			
			$data['guest'] = $guest;
			return $data;
		}
		
		$data['member'] = true;
		//$data['view'] = \Clay\Application::URL('users','profile','view');
		//$data['update'] = \Clay\Application::URL('users','profile','edit');
		//$data['updateImage'] = \Clay\Application::URL('users','profile','editImage');
		$data['settings'] = \Clay\Application::URL('users','account','edit');
		return $data;
	}
	/**
	 * Stats - Info - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function stats( $args ){
	
		
	}
	/**
	 * Display - to be displayed on an item page
	 * @param unknown $args
	 * @return string
	 */
	public function display( $args ){

		
	}
	
	public function addItem(){
		
		
	}
	
	public function create($args){
		
		
	}
	
	public function editItem($args){
		
		
	}
	
	public function update($args){
		
		
	}
	
	public function delete($args){
		
		
	}
}