<?php
namespace application\users\plugin\dashboard;

/**
 * Users Dashboard Plugin
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */
class users extends \Clay\Application\Plugin {
	
	public $PluginApp = 'users';
	public $PluginType = 'dashboard';
	public $Plugin = 'users';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function view( $args ){
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('system','Admin','User')) {
			
			$data['admin'] = \Clay\Application::URL('users','admin','view');
		}
		
		return $data;
	}
	/**
	 * Stats - Info - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function stats( $args ){
	
		
	}
	/**
	 * Display - to be displayed on an item page
	 * @param unknown $args
	 * @return string
	 */
	public function display( $args ){

		
	}
	
	public function addItem(){
		
		
	}
	
	public function create($args){
		
		
	}
	
	public function editItem($args){
		
		
	}
	
	public function update($args){
		
		
	}
	
	public function delete($args){
		
		
	}
}