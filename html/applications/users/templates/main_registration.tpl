<section class="c-app">
  <div class="c-app__head">
    <h1>Welcome! Please Sign Up</h1>
  </div>
  <div class="c-app__content">
  <?php if(!empty($message)) { foreach($message as $msg) { echo "<h4>$msg</h4>"; } } ?>
  <?php if(!empty($form)) {
    $form->template(); 
  } else {
    echo "Registration is currently disabled."; 
  } ?>
  </div>
</section>