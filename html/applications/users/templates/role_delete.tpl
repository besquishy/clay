<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
	<a href="<?= \clay\application::url('users','roles');?>" class="dashlink" title="Dashboard">Roles</a> / 
	<a href="<?= \clay\application::url('users','role','view',array('rid' => $privilege['rid']));?>" class="dashlink" title="Dashboard"><?= $roleName; ?></a> / 
	Delete Privilege</h1>
	</div>
	<div class="c-app__content">
	<form v-on:submit.prevent="onSubmit('rolePrivDelete', $event)" id="dashForm" method="post" action="<?= \clay\application::url('users','role','remove'); ?>">
		<fieldset class="c-fieldset">
			<legend>Delete this Privilege from <?= $roleName; ?>?</legend>
			<table class="c-table c-table--border">
				<tr class="c-table__row">
					<th class="c-table__head">Role Privilege ID</th>
					<th class="c-table__head">Privilege ID</th>
					<th class="c-table__head">Application : ID</th>
					<th class="c-table__head">Component</th>
					<th class="c-table__head">Name</th>
					<th class="c-table__head">Scope</th>
				</tr>
				<tr class="c-table__row">
					<td class="c-table__cell"><?= $privilege['rpid']; ?></td>
					<td class="c-table__cell"><?= $privilege['pid']; ?></td>
					<td class="c-table__cell"><?= $privilege['info']['app'] . ' : '.$privilege['info']['appid']; ?></td>
					<td class="c-table__cell"><?= $privilege['info']['component']; ?></td>
					<td class="c-table__cell"><?= $privilege['info']['name']; ?></td>
					<td class="c-table__cell"><?= $privilege['scope']; ?></td>
				</tr>
			</table>
			<input type="hidden" name="rpid" id="rpid" value="<?= $privilege['rpid']; ?>" />
			<input type="hidden" name="role" id="role" value="<?= $roleName; ?>" />
		</fieldset>
		<fieldset class="c-fieldset__controls">
			<div class="o-grid">
				<div class="o-grid__col u-1/3@sm">
					<input v-if="saved !== 'rolePrivDelete'" type="submit" class="c-button" name="submit" id="submit" value="Delete Privilege" /> 
				</div>
				<div class="o-grid__col u-1/3@sm">
					<div v-if="response == 'rolePrivDelete'" id="rolePrivDelete_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
					<div v-if="error == 'rolePrivDelete'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
				</div>
			</div>
		</fieldset>
	</form>
	</div>
</section>