<section class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
    <a href="<?= \clay\application::url('users','roles');?>" class="dashlink" title="Dashboard">Roles</a> / 
    Delete a Role</h1>
  </div>
  <div class="c-app__content">
    <p class="u-p u-bgcolor-danger"><b>Note:</b> When a Role is deleted, all of its assigned Privileges and associations to User Accounts are also deleted. If the Role is assigned as a Parent Role, the Child Role will no longer have an assigned Parent.</p>
    <?php $form->template(); ?>
  </div>
</section>