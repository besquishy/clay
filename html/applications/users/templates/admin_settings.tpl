<section class="c-app">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
    User Settings</h1>
  </div>
  <div class="c-app__content">
  <?php $form->template(); ?>
  </div>
</section>