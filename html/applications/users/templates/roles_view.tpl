<section class="c-app">
	<style>
	.roles-rid { width:50px; text-align:center; }
	.roles-name { width:100px; }
	.roles-rpid { width:150px; }
	.roles-descr { width:200px; }
	.roles-state { width:50px; text-align:center; }
	.roles-options { width:200px; text-align:center; }
	.roles-options button { margin:0px; }
	</style>
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
	Roles</h1>
	</div>
	<div class="c-app__content">
		<h4><a class="dashlink" href="<?= \clay\application::url('users','roles','add'); ?>" title="Add a new Role">Add a Role</a></h4>	
		<div class="c-table c-table--striped c-table--border">
			<div class="c-table__row">
				<span class="c-table__head roles-rid">Role ID</span>
				<span class="c-table__head roles-name">Name</span>
				<span class="c-table__head roles-rpid">Parent Role : Role ID</span>
				<span class="c-table__head roles-descr">Description</span>
				<span class="c-table__head roles-state">State</span>
				<span class="c-table__head roles-options">Options</span>
			</div>
			<?php foreach($roles as $role) { ?>
			<div class="c-table__row">
				<span class="c-table__cell roles-rid"><?= $role['rid']; ?></span>
				<span class="c-table__cell roles-name">
					<a class="dashlink" href="<?= \clay\application::url('users','role','view',array('rid' => $role['rid'])); ?>" title="View Role">
						<?= $role['name']; ?>
					</a>
				</span>
				<span class="c-table__cell roles-rpid"><?= !empty($role['prid']) ? $role['parent']['name'].' : '.$role['prid'] : 'n/a'; ?></span>
				<span class="c-table__cell roles-descr"><?= $role['descr']; ?></span>
				<span class="c-table__cell roles-state"><?= $role['state']; ?></span>
				<span class="c-table__cell roles-options"><a class="dashlink" href="<?= \clay\application::url('users','roles','edit',array('rid' => $role['rid'])); ?>" title="Edit">Edit</a> 
					<a class="dashlink" href="<?= \clay\application::url('users','roles','delete',array('rid' => $role['rid'])); ?>" title="Delete">Delete</a>
					<a class="dashlink" href="<?= \clay\application::url('users','role','view',array('rid' => $role['rid'])); ?>" title="Assigned Privileges">Privileges</a>
				</span>
			</div>
			<?php } ?>
		</div>
	</div>
</section>