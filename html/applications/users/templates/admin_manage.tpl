<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		<a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> /
		Manager</h1>
	</div>
	<div class="c-app__content">
		<h4><a class="dashlink" href="<?= \clay\application::url('users','admin','add'); ?>" title="Add a new User">Add User</a></h4>
		<table class="c-table c-table--border c-table--striped">
			<thead>
				<tr class="c-table__row">
					<th class="c-table__head">ID</th><th class="c-table__head">Username</th><th class="c-table__head">Email</th><th class="c-table__head">Status</th><th class="c-table__head">Roles</th><th class="c-table__head">options</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($users as $user) { ?>
				<tr class="c-table__row">
					<td class="c-table__cell"><?= $user['uid']; ?></td>
					<td class="c-table__cell"><?= $user['uname']; ?></td>
					<td class="c-table__cell"><?= $user['email']; ?></td>
					<td class="c-table__cell"><?= $status[$user['state']]; ?></td>
					<td class="c-table__cell"><?php if(!empty($user['roles'])) { foreach($user['roles'] as $role) { ?> 
						<a class="dashlink" href="<?= \clay\application::url('users','role','view',array('rid' => $role['rid'])); ?>" title="Edit"><?= $role['name']; ?></a> 
					<?php  } } ?> </td>
					<td class="c-table__cell"><a class="dashlink" href="<?= \clay\application::url('users','admin','edit',array('uid' => $user['uid'])); ?>" title="Edit">Edit</a> 
						<a class="dashlink" href="<?= \clay\application::url('users','admin','delete',array('uid' => $user['uid'])); ?>" title="Delete">Delete</a>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
</section>