<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		User Administration</h1>
	</div>
	<div class="c-app__content">
		<div class="c-tabs c-tooltip--container">
			<a href="<?= $settings; ?>" class="c-tabs__tab c-tooltip--block">
				User Settings
			</a>
			<div class="c-tooltip__block">
				View and Manage global user settings and configuration options for your site's users.
			</div>
			<a href="<?= $manage; ?>" class="c-tabs__tab c-tooltip--block">
				Manage Users
			</a>
			<div class="c-tooltip__block">
				View and Manage registered users; edit or delete registered users' account information.
			</div>
			<a href="<?= $privileges; ?>" class="c-tabs__tab c-tooltip--block">
				Privileges
			</a>
			<div class="c-tooltip__block">
				View and Manage your site's registered application privileges.
			</div>
			<a href="<?= $roles; ?>" class="c-tabs__tab c-tooltip--block">
				Roles
			</a>
			<div class="c-tooltip__block">
				View, add, modify, and delete registered Roles. Roles are privilege groups that allow users to perform specific tasks on your site.
			</div>
			<!--<a href="<?= $help; ?>" class="c-tabs__tab c-tooltip--block">
				Help
			</a>-->
			<div class="c-tooltip__block">
				Documentation and Support
			</div>
		</div>
	</div>
</section>