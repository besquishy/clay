<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
	<a href="<?= \clay\application::url('users','roles');?>" class="dashlink" title="Dashboard">Roles</a> / 
	<?= $role['name']; ?> / Edit Privilege</h1>
	</div>
	<div class="c-app__content">
		<h4><?= $role['descr']; ?></h4>
		<h4><a class="dashlink" href="<?= \clay\application::url('users','role','view',array('rid' => $role['rid'])); ?>" title="Return to Role">Back to Role</a></h4>
		
		<form v-on:submit.prevent="onSubmit('rolePrivs', $event)" id="dashForm" action="<?= \Clay\Application::URL('users','role','update'); ?>" method="post">
			<fieldset class="c-fieldset"><legend>Edit a Privilege in <?= $role['name']; ?></legend>
				<div class="c-table">
					<div class="c-table__row">
						<span class="c-table__head" style="min-width:100px;">Application</span>
						<span class="c-table__head" style="min-width:100px;">Component</span>
						<span class="c-table__head" style="min-width:100px;">Privilege</span>
						<?php if(!empty($scopes) OR !empty($scope)){ ?>
						<span class="c-table__head" style="min-width:200px;">Scope</span>
						<?php }		
						if(!empty($items) OR !empty($item)){ ?>
						<span class="c-table__head" style="width:200px;"><?= $itemtype; ?></span>
						<?php } ?>
					</div>
					<div class="c-table__row u-bgcolor-success">
						<span class="c-table__cell" style="min-width:100px"><?= $privilege['info']['app']; ?></span>
						<span class="c-table__cell" style="min-width:100px"><?= $privilege['info']['component']; ?></span>
						<span class="c-table__cell" style="min-width:100px"><?= $privilege['info']['name']; ?></span>
						<input type="hidden" name="pid" id="pid" value="<?= $privilege['info']['pid']; ?>" />
						<?php if(!empty($scopes)){ ?>
						<span class="c-table__cell" style="min-width:200px;">
							<h4>Select a Scope</h4>
							<?php foreach($scopes as $level){ ?>
							<a class="c-button <?php if(!empty($scope) && ($scope == $level)) echo 'c-button--default';?>" href="<?= \Clay\Application::URL('users','role','edit', array('rid' => $role['rid'], 'pid' => $privilege['pid'], 'scope' => $level, 'rpid' => $privilege['rpid']));?>"><?= $level; ?></a>
							<?php }
							if(!empty($scope) && empty($items)){ ?>
							<input type="hidden" value="<?= $scope; ?>" name="scope" />
							<fieldset class="c-fieldset__controls">
								<div class="o-grid">
									<div class="o-grid__col u-1/3@sm">
										<input v-if="saved !== 'rolePrivs'" type="submit" class="c-button" name="submit" id="submit" value="Update Privilege" /> 
										<input v-on:click="toEdit('rolePrivs', $event)" v-if="saved == 'rolePrivs'" type="button" class="c-button" name="edit" id="edit" value="Edit Privilege" />
									</div>
									<div class="o-grid__col u-1/3@sm">
										<div v-if="response == 'rolePrivs'" id="rolePrivs_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
										<div v-if="error == 'rolePrivs'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
									</div>
								</div>
							</fieldset>
							<?php } ?>
						</span>
						<?php }
						if(!empty($items) OR !empty($item)){ ?>
						<input type="hidden" value="<?= $scope; ?>" name="scope" />
						<span class="c-table__cell" style="min-width:200px;">
							<select name="item" id="items">
								<option v-on:click="opt = false" value="">Select from <?= $itemtype; ?></option>
								<?php foreach($items as $Item){ ?>
								<option v-on:click="opt = true" value="<?= !empty($Item['id']) ?  $Item['id'] : $Item['name']; ?>" <?php if(!empty($item) AND ($item == $Item['name']) OR ($item == $Item['id'])) echo 'selected="selected"';?>>ID: <?= $Item['id']; ?> - <?= !empty($Item['title']) ? $Item['title'] : $Item['name']; ?><?php if(!empty($Item['uid'])){ ?> - UserID: <?= $Item['uid']; } ?></option>
								<?php } ?>
							</select>
							<fieldset v-if="opt" class="c-fieldset__controls">
								<div class="o-grid">
									<div class="o-grid__col u-3/3@sm">
										<input v-if="saved !== 'rolePrivs'" type="submit" class="c-button" name="submit" id="submit" value="Update Privilege" /> 
										<input v-on:click="toEdit('rolePrivs', $event)" v-if="saved == 'rolePrivs'" type="button" class="c-button" name="edit" id="edit" value="Edit Privilege" />
									</div>
									<div class="o-grid__col u-3/3@sm">
										<div v-if="response == 'rolePrivs'" id="rolePrivs_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
										<div v-if="error == 'rolePrivs'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
									</div>
								</div>
							</fieldset>
						</span>
						<?php } ?>
					</div>
				</div>
				<input type="hidden" name="rid" id="rid" value="<?= $role['rid']; ?>" />
				<input type="hidden" name="rpid" id="rpid" value="<?= $privilege['rpid']; ?>" />
			</fieldset>
		</form>
	</div>
</section>