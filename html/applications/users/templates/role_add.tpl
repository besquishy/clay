<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	<a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
	<a href="<?= \clay\application::url('users','roles');?>" class="dashlink" title="Dashboard">Roles</a> / 
	<?= $role['name']; ?> / Add Privilege</h1>
	</div>
	<div class="c-app__content">
		<h4><?= $role['descr']; ?></h4>
		<h4><a class="dashlink" href="<?= \clay\application::url('users','role','view',array('rid' => $role['rid'])); ?>" title="Return to Role">Back to Role</a></h4>
		
		<form v-on:submit.prevent="onSubmit('rolePrivs', $event)" id="dashForm" action="<?= \Clay\Application::URL('users','role','create'); ?>" method="post">
			<fieldset class="c-fieldset"><legend>Add a Privilege to <?= $role['name']; ?></legend>
			<?php if(!empty($privileges)){ ?>
				<div class="c-table c-table--border c-table--striped">
					<div class="c-table__row">
						<span class="c-table__head">Application</span>
						<span class="c-table__head">Component</span>
						<span class="c-table__head">Privilege</span>
						<?php if(!empty($scopes) OR !empty($scope)){ ?>
						<span class="c-table__head">Scope</span>
						<?php }		
						if(!empty($items) OR !empty($item)){ ?>
						<span class="c-table__head"><?= $itemtype; ?></span>
						<?php } ?>
					</div>
				
				<?php foreach($privileges as $priv) {
					if(!empty($privilege) AND ($privilege['pid'] == $priv['pid'])){ ?>
					<a name="<?= $privilege['pid']; ?>"></a>
					<div class="c-table__row u-bgcolor-success">
						<span class="c-table__cell u-weight-bold"><?= $priv['app']; ?></span>
						<span class="c-table__cell u-weight-bold"><?= $priv['component']; ?></span>
						<span class="c-table__cell u-weight-bold"><?= $priv['name']; ?></span>
						<input type="hidden" name="pid" id="pid" value="<?= $privilege['pid']; ?>" />
						<?php if(!empty($scopes)){ ?>			
						<span class="c-table__cell">
							
							<span class="u-weight-bold">Select a Scope: </span>
							<?php foreach($scopes as $level){ ?>
							<a class="c-button <?php if(!empty($scope) && ($scope == $level)) echo 'c-button--default';?>" href="<?= \Clay\Application::URL('users','role','add', array('rid' => $role['rid'], 'pid' => $privilege['pid'], 'scope' => $level));?>#<?= $privilege['pid']; ?>"><?= $level; ?></a>
							<?php } 
							if(!empty($scope) && empty($items)){ ?>
							<input type="hidden" value="<?= $scope; ?>" name="scope" />
							<fieldset class="c-fieldset__controls">
								<div class="o-grid">
									<div class="o-grid__col u-1/3@sm">
										<input v-if="saved !== 'rolePrivs'" type="submit" class="c-button" name="submit" id="submit" value="Add Privilege" /> 
										<input v-on:click="toEdit('rolePrivs', $event)" v-if="saved == 'rolePrivs'" type="button" class="c-button" name="edit" id="edit" value="Add More" />
									</div>
									<div class="o-grid__col u-1/3@sm">
										<div v-if="response == 'rolePrivs'" id="rolePrivs_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
										<div v-if="error == 'rolePrivs'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
									</div>
								</div>
							</fieldset>
							<?php } ?>
						</span>
						<?php } 
						if(!empty($items) OR !empty($item)){ ?>
						<input type="hidden" value="<?= $scope; ?>" name="scope" />
						<span class="c-table__cell">
							<h4><?= $itemtype; ?></h4>
							<select name="item" id="items">
								<option v-on:click="opt = false" value="">Select from <?= $itemtype; ?></option>
								<?php foreach($items as $Item){ ?>
								<option v-on:click="opt = true" value="<?= !empty($Item['id']) ?  $Item['id'] : $Item['name']; ?>" <?php if(!empty($item) AND ($item == $Item)) echo 'selected="selected"';?>>ID: <?= $Item['id']; ?> - <?= !empty($Item['title']) ? $Item['title'] : $Item['name']; ?><?php if(!empty($Item['uid'])){ ?> - UserID: <?= $Item['uid']; } ?></option>
								<?php } ?>
							</select>
							<fieldset v-if="opt" class="c-fieldset__controls">
								<div class="o-grid">
									<div class="o-grid__col u-3/3@sm">
										<input v-if="saved !== 'rolePrivs'" type="submit" class="c-button" name="submit" id="submit" value="Add Privilege" /> 
										<input v-on:click="toEdit('rolePrivs', $event)" v-if="saved == 'rolePrivs'" type="button" class="c-button" name="edit" id="edit" value="Add More" />
									</div>
									<div class="o-grid__col u-3/3@sm">
										<div v-if="response == 'rolePrivs'" id="rolePrivs_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
										<div v-if="error == 'rolePrivs'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
									</div>
								</div>
							</fieldset>
						</span>
						<?php } ?>
					</div>
					<?php } else { ?>
					<a class="c-table__row" href="<?= \clay\application::url('users','role','add',array('rid' => $role['rid'], 'pid' => $priv['pid'])); ?>#<?= $priv['pid']; ?>" title="Assign to <?= $role['name']; ?>">
						<span class="c-table__cell"><?= $priv['app']; ?></span>
						<span class="c-table__cell"><?= $priv['component']; ?></span>
						<span class="c-table__cell"><?= $priv['name']; ?></span>
						<?php if(!empty($scopes) OR !empty($scope)){ ?>
						<span class="c-table__cell-empty"></span>
						<?php } 
						if(!empty($items) OR !empty($item)){ ?>
						<span class="c-table__cell-empty"></span>
						<?php } ?>
					</a>
				<?php } }?>
				</div>
				<?php } ?>
				<input type="hidden" name="rid" id="rid" value="<?= $role['rid']; ?>" />
			</fieldset>
		</form>
	</div>
</section>