<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		<a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Users">Users</a> / 
		<a href="<?= \clay\application::url('users','roles');?>" class="dashlink" title="User Roles">Roles</a> / 
		<?= $roleName; ?></h1>
	</div>
	<div class="c-app__content">
		<h3><?= $roleDescription; ?></h3>
		<h4><a href="<?= \clay\application::url('users','roles','edit',array('rid' => $roleID)); ?>" class="dashlink" title="Edit Role">Edit <?= $roleName; ?></a></h4>
		<h4><a class="dashlink" href="<?= \clay\application::url('users','role','add',array('rid' => $roleID)); ?>" title="Add a new Role">Assign a Privilege</a></h4>
		<?php foreach($roles as $role) { ?>
		<h3>Privileges <?php if($role['rid'] == $roleID) { echo 'Assigned to '; } else { echo 'Inherited from '; } echo $role['name']; ?></h3>
		<?php if (!empty($role['privileges'])){ ?>
		<table class="c-table c-table--striped c-table--border">
			<tr class="c-table__row">
				<th class="c-table__head">Role Privilege ID</th><th class="c-table__head">Privilege ID<th class="c-table__head">Application : ID</th><th class="c-table__head">Component</th><th class="c-table__head">Name</th><th class="c-table__head">Scope</th><th class="c-table__head">Options</th>
			</tr>
			<?php foreach($role['privileges'] as $privilege) { ?>
			<tr class="c-table__row">
				<td class="c-table__cell"><?= $privilege['rpid']; ?></td>
				<td class="c-table__cell"><?= $privilege['pid']; ?></td>
				<td class="c-table__cell"><?= $privilege['app'] . ' : '.$privilege['appid']; ?></td>
				<td class="c-table__cell"><?= $privilege['component']; ?></td>
				<td class="c-table__cell"><?= $privilege['name']; ?></td>
				<td class="c-table__cell"><?= $privilege['scope']; ?></td>
				<td class="c-table__cell"><a class="dashlink" href="<?= \Clay\Application::URL('users','role','edit',array('rpid' => $privilege['rpid'])); ?>">Edit</a> 
				<a class="dashlink" href="<?= \Clay\Application::URL('users','role','delete',array('rpid' => $privilege['rpid'])); ?>">Delete</a> 
				</td>
			</tr>
			<?php } ?>
		</table>
		<?php } else { ?>
		<h4>None</h4>
		<?php } } ?>
	</div>
</section>