<section class="c-app">
	<style>
		.privs-pid { width:75px; }
		.privs-app { width:100px; }
		.privs-com { width:100px; }
		.privs-priv { width:100px; }
		.privs-options { width:100px; }
	</style>
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		<a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
		Privileges</h1>
	</div>
	<div class="c-app__content">
		<h4><a class="dashlink" href="<?= \clay\application::url('users','privileges','add'); ?>" title="Add a new User">Add a Privilege</a></h4>
		<div class="c-table c-table--border c-table--striped">
			<div class="c-table__row">
				<span class="c-table__head privs-pid">Privilege ID</span>
				<span class="c-table__head privs-app">Application</span>
				<span class="c-table__head privs-com">Component</span>
				<span class="c-table__head privs-priv">Privilege</span>
				<span class="c-table__head privs-options">Options</span>
			</div>
			<?php foreach($privileges as $priv) { ?>
			<div class="c-table__row">
				<span class="c-table__cell privs-pid"><?= $priv['pid']; ?></span>			
				<span class="c-table__cell privs-app"><?= $priv['app']; ?></span>
				<span class="c-table__cell privs-com"><?= $priv['component'] ?></span>
				<span class="c-table__cell privs-priv"><?= $priv['name']; ?></span>
				<span class="c-table__cell privs-options">
					<a class="dashlink" href="<?= \clay\application::url('users','privileges','edit',array('pid' => $priv['pid'])); ?>" title="Edit">Edit</a> 
					<a class="dashlink" href="<?= \clay\application::url('users','privileges','delete',array('pid' => $priv['pid'])); ?>" title="Delete">Delete</a>
				</span>
			</div>
			<?php } ?>
		</div>
	</div>
</section>