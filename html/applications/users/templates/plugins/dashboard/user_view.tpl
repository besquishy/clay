<?php if(!empty($guest)) { ?>

<div class="o-grid__col u-1/2@sm"><?php $guest->template($login); ?></div>
<?php if(!empty($register)) { ?>
<div class="o-grid__col u-1/2@sm"><?php $guest->template($register); ?></div> <br style="clear:both;" />
<?php } ?>

<?php } ?>

<?php if(!empty($member)) { ?>
<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<i class="fa fa-user-circle-o dash-icon-lg"></i> Account Settings
		</div>
	</div>
	<div class="c-card__content">		
		<a href="<?= $settings; ?>" class="dashlink">Account Settings</a></li>
		<br />
		<?php if(!empty($view)){?><a href="<?= $view; ?>" class="dashlink">View My Profile</a> <br /><?php } ?>
		<?php if(!empty($updateImage)){?><a href="<?= $update; ?>" class="dashlink">Change Picture</a> <br /><?php } ?>
		<?php if(!empty($update)){?><a href="<?= $update; ?>" class="dashlink">Edit Profile</a> <br /><?php } ?>
		<?php if(!empty($xsettings)){?><a href="<?= $settings; ?>" class="dashlink">Account Settings</a> <br /><?php } ?>
	</div>
</div>
<?php } ?>