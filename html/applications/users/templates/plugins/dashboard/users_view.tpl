<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<i class="fa fa-users dash-icon-lg"></i> Users
		</div>
	</div>
	<div class="c-card__content">
		<a href="<?= $admin; ?>" class="dashlink">User Administration</a>
	</div>
</div>