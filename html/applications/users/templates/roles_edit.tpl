<section class="c-app"><section class="u-pl@sm u-pr@sm">
  <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    <a href="<?= \clay\application::url('users','admin');?>" class="dashlink" title="Dashboard">Users</a> / 
    <a href="<?= \clay\application::url('users','roles');?>" class="dashlink" title="Dashboard">Roles</a> / 
    <a href="<?= \clay\application::url('users','role','view', array('rid' => $rid));?>" class="dashlink" title="Dashboard"><?= $role; ?></a> / 
    Edit Role</h1>
  </div>
  <div class="c-app__content">
    <?php $form->template(); ?>
  </div>
</section>