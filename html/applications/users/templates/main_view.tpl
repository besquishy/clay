<section class="c-app">
  <div class="c-app__head">
    <h1>Welcome! <?php if(!empty($form)){ echo 'Please sign in'; }?></h1>
  </div>
  <div class="c-app__content">
    <?php if(!empty($message)){ if(!is_array($message)) { ?>
    <h4><?= $message; ?></h4> 
    <?php } else { foreach($message as $stmt) { ?>
    <h4><?= $stmt; ?></h4>
    <?php } } }?>
    <?php if(!empty($form)){ $form->template(); ?>
    <div>Not a Member? <a href="<?= \Clay\Application::URL('dashboard');?>" title="Register for an Account">Register.</a></div> 
    <?php } else { ?><a href="<?= \clay\application::url('users','main','logout');?>" title="Logout">Logout</a><?php } ?>
  </div>
</section>