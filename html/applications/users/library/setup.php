<?php
namespace application\users\library;
/**
* ClayCMS
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

\Clay\Module('Privileges');

class setup {

	public static function install(){
		# User Dashboard Plugin
		$udashboard = array('plugin', array('type' => 'dashboard', 'app' => 'users', 'name' => 'user', 'descr' => 'User account settings.'));
		\Clay\Module::Object('Plugins')->Setup($udashboard);
		# Users Dashboard Plugin
		$usdashboard = array('plugin', array('type' => 'dashboard', 'app' => 'users', 'name' => 'users', 'descr' => 'Users Administration.'));
		\Clay\Module::Object('Plugins')->Setup($usdashboard);
		# Hook User Dashboard Plugin to the Dashboard
		$uhook = array('plugin' => array('type' => 'dashboard', 'app' => 'users', 'name' => 'user'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $uhook );
		# Hook Users Dashboard Plugin to the Dashboard
		$ushook = array('plugin' => array('type' => 'dashboard', 'app' => 'users', 'name' => 'users'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $ushook );
		# Diasble Registration - Admin's can enable it.
		# @FIXME: This is to be privilege based... ? (David)
		\Clay\Application::Set('users','registration','0');
		return true;
	}
	public static function upgrade($version){

		switch($version){

			case ($version <= '1.0'):
				# User Dashboard Plugin
				$udashboard = array('plugin', array('type' => 'dashboard', 'app' => 'users', 'name' => 'user', 'descr' => 'User account settings.'));
				\Clay\Module::Object('Plugins')->Setup($udashboard);
				# Users Dashboard Plugin
				$usdashboard = array('plugin', array('type' => 'dashboard', 'app' => 'users', 'name' => 'users', 'descr' => 'Users Administration.'));
				\Clay\Module::Object('Plugins')->Setup($usdashboard);
			case ($version <= '1.0.1'):
				# Hook User Dashboard Plugin to the Dashboard
				$uhook = array('plugin' => array('type' => 'dashboard', 'app' => 'users', 'name' => 'user'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $uhook );
				# Hook Users Dashboard Plugin to the Dashboard
				$ushook = array('plugin' => array('type' => 'dashboard', 'app' => 'users', 'name' => 'users'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $ushook );
				break;
		}
		return TRUE;
	}
	public static function delete(){
		\Clay\Module::Object('Privileges')->remove('users');
		return TRUE;
	}
}
