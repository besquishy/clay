<?php
/**
 * Apps Application
 *
 * @copyright (C) 2007-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Applications',
			'name' => 'apps',
			'version' => '1.0.4',
			'date' => 'January 4, 2018',
			'description' => 'Applications Manager',
			'class' => 'System',
			'category' => 'Administration',
		# Depends on the Setup Module, but declaring it is redundant.
			'depends' => array('applications' => array('dashboard')),
			'core' => TRUE,
			);