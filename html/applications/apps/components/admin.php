<?php
/**
* Apps Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\apps\component;

/**
 * Apps Application Admin Component
 * 
 * Apps Administration
 * @author David
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Default Page Template
	 */
	public $page = 'dashboard';

	/**
	 * View
	 * @throws \Exception
	 */
	public function View(){

		$this->pageTitle = "Applications Administration";

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

	}

	/**
	 * Help
	 */
	public function Help(){

	}

	/**
	 * Add or Remove an Application
	 * @throws \Exception
	 */
	public function Manage(){

		$this->pageTitle = "Applications Manager";

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

		$data = array();

		/**
		 * @todo Move all of this to an API
		 */

		$dbconn = \ClayDB::Connect();

		$appsdb = \Clay\Application::getAll(); #$dbconn->get('appid, state, version, name FROM '.\claydb::$tables['apps']);

		$apps = file_exists(\clay\APPS_PATH) ? scandir(\clay\APPS_PATH) : array();

		foreach($apps as $app){
				# Hide these
				if(($app == '.') || ($app == '..')){
					unset($app);
					continue;
				}

				$appdata = \Clay\Application::Info('application',$app);

				if(!empty($appdata)){

					foreach($appsdb as $sysApp){

						if($sysApp['name'] == $appdata['name']){

							switch(true){

								case ($sysApp['state'] == 1) :
									$appdata['status'] = 'core';
									$appdata['css'] = 'info';
									break;

								case ($sysApp['state'] == 2) :
									$appdata['status'] = 'installed';
									$appdata['css'] = 'success';
									$appdata['action'] = array('type' => 'Remove', 'url' => \Clay\Application::URL('apps','admin','remove',array('appid' => $sysApp['appid'])));
									break;

								default :
									$appdata['status'] = 'not-installed';
									$appdata['action'] = array('type' => 'Install', 'url' => \Clay\Application::URL('apps','admin','install',array('name' => $sysApp['name'])));
									$appdata['css'] = '';
									break;
							}

							if(($sysApp['version'] < $appdata['version']) AND ($sysApp['state'] != 3)){

								$appdata['action'] = array('type' => 'Upgrade', 'url' => \Clay\Application::URL('apps','admin','upgrade',array('appid' => $sysApp['appid'])));
								$appdata['status'] = 'upgrade';
								$appdata['css'] = 'warning';
								$appdata['installed.version'] = $sysApp['version'];
							}

							$appdata['info'] = $sysApp;

						}
					}

					if(empty($appdata['status'])){

						$appdata['status'] = 'not-installed';
						$appdata['action'] = array('type' => 'Install', 'url' => \Clay\Application::URL('apps','admin','install',array('name' => $app)));
					}

					if(isset($appdata['install']) && $appdata['install'] === FALSE){

						$appdata['status'] = 'not-installable';
						$appdata['action'] = array('type' => 'Ready', 'url' => NULL);
					}

					if(empty($appdata['class'])){

						$appdata['class'] = 'Unclassed';
					}

					if(empty($appdata['category'])){

						$appdata['category'] = 'Uncategorized';
					}

					$data['apps'][$appdata['class']][$appdata['category']][] = $appdata;
				}
		}

		$data['upgradeURL'] = \Clay\Application::URL('apps','admin','upgrade');
		$data['installURL'] = \Clay\Application::URL('apps','admin','install');
		$data['removeURL'] = \Clay\Application::URL('apps','admin','remove');
		$data['appsdb'] = $appsdb;

		return $data;
	}

	/**
	 * Application Installer
	 * @throws \Exception
	 */
	public function Install(){

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

		if(!is_string($app = \Clay\Data\Post('appname','string','string', FALSE))){

			throw new \Exception("Input for 'appname' must be a string");
		}

		$setup = \Clay\Module::API('Setup','Type','Application');

		$setup->install($app,2); # 1 = CORE(INSTALLED), 2 = INSTALLED, 3 = INITIATED (for installations that require actions), 4 = NOT INSTALLED(REMOVED)

		\Clay\Application::API('system','message','send',"Application named $app installed successfully!");

		if(empty($_POST['dashForm'])){

			//\Clay::Redirect($_SERVER['HTTP_REFERER']);
		}
	}

	/**
	 * Application Uninstaller
	 * @throws \Exception
	 */
	public function Remove(){

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

		if(!is_string($app = \Clay\Data\Post('appname','string','string', FALSE))){

			throw new \Exception("Input for 'appname' must be a string");
		}

		\Library('Clay/Application/Setup');

		$setup = \Clay\Module::API('Setup','Type','Application');

		$setup->delete($app);

		\Clay\Application::API('system','message','send',"Application named $app removed successfully!");

		if(empty($_POST['dashForm'])){

			\Clay::Redirect($_SERVER['HTTP_REFERER']);
		}

	}

	/**
	 * Application Upgrader
	 * @throws \Exception
	 */
	public function Upgrade(){

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin', 'User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

		if(!is_string($app = \Clay\Data\Post('appname','string','string', FALSE))){

			throw new \Exception("Input for 'appname' must be a string");
		}

		$version = \Clay\Data\Post('appversion','string');
		\Library('Clay/Application/Setup');

		$setup = \Clay\Module::API('Setup','Type','Application');

		$setup->upgrade($app,2,$version); # 1 = CORE(INSTALLED), 2 = INSTALLED, 3 = INITIATED (for installations that require actions), 4 = NOT INSTALLED(REMOVED)

		\Clay\Application::API('system','message','send',"Application named $app upgraded!");

		if(empty($_POST['dashForm'])){

			\Clay::Redirect($_SERVER['HTTP_REFERER']);
		}

	}
	/**
	 * Applications' Settings
	 * Lists all settings and allows them to be altered.
	 * @return array
	 */
	public function Settings(){

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

		$data = array();

		$settings = \Clay\Application::API('apps','settings','getAll');

		$appSettings = array();

		foreach($settings as $setting){

			$setting['appname'] = \Clay\Application::getName($setting['appid']);
			$appSettings[] = $setting;
		}

		$data['settings'] = $appSettings;
		return $data;
	}

	/**
	 * Update Application Settings
	 */
	public function updateSettings(){

		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){

			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}

		$this->template = NULL;

		//$this->page = 'app';

		$settings = \Clay\Data\Post('settings','isarray','',array());

		if(!empty($settings)){

			\Clay\Application::API('apps','settings','update',$settings);
		}

		\Clay\Application::API('system','message','send',"Application Settings updated successfully!");

		if(empty($_POST['dashForm'])){

			\Clay::Redirect($_SERVER['HTTP_REFERER']);
		}

		return FALSE;
	}
}
