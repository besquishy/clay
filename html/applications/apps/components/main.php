<?php
/**
* Apps Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\apps\component;

/**
 * Apps Application Main Component
 * @author David
 *
 */
class main extends \Clay\Application\Component {

	/**
	 * View
	 */
	public function view(){
		
		return array();
	}
}