<?php
/**
 * Applications Dashboard Plugin
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\apps\plugin\dashboard;

/**
 * Application Dashboard Plugin
 */
class applications extends \Clay\Application\Plugin {
	/**
	 * Plugin App
	 * @var string
	 */
	public $PluginApp = 'apps';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'dashboard';
	/**
	 * Plugin name
	 * @var string
	 */
	public $Plugin = 'applications';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function view( $args ){
		
		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if($user->Privilege('system','Admin','User')) {
			
			$data['manage'] = \Clay\Application::URL('apps','admin','manage');
			$data['settings'] = \Clay\Application::URL('apps','admin','settings');
			//$data['services'] = \Clay\Application::URL('apps','admin','services');
		}
		
		return $data;
	}
	/**
	 * Stats - Info - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function stats( $args ){
	
		
	}
	/**
	 * Display - to be displayed on an item page
	 * @param array $args
	 * @return string
	 */
	public function display( $args ){

		
	}
	/**
	 * Add Item
	 * @param array $args
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		
		
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		
	}
}