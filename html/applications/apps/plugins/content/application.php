<?php
/**
 * Applications Application Block
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\apps\plugin\content;

/**
 * Application Plugin Content Application Block (Instance)
 */
class application extends \Clay\Application\Plugin {
	/**
	 * Plugin App
	 * @var string
	 */
	public $PluginApp = 'apps';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'content';
	/**
	 * Plugin name
	 * @var string
	 */
	public $Plugin = 'application';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function view( $args ){

		$data = array();
		$data['application'] = !empty($args['app']) ? $args['app'] : '';
		$data['component'] = !empty($args['com']) ? $args['com'] : 'main';
		$data['action'] = !empty($args['act']) ? $args['act'] : 'view';
		
		if(!empty($args['query'])){
			
			parse_str($args['query'],$data['query']);
			
		} else {
			
			$data['query'] = array();
		}
		
		return $data;
	}
	/**
	 * Display - to be displayed on an item page
	 * @param array $args
	 * @return string
	 */
	public function display( $args ){

		$data = array();
		$data['application'] = !empty($args['app']) ? $args['app'] : '';
		$data['component'] = !empty($args['com']) ? $args['com'] : 'main';
		$data['action'] = !empty($args['act']) ? $args['act'] : 'view';
		
		if(!empty($args['query'])){
			
			parse_str($args['query'],$data['query']);
			
		} else {
			
			$data['query'] = array();
		}
		
		return $data;
	}
	/**
	 * Add
	 */
	public function add(){

		$data = array();
		$data['apps'] = \Clay\Application::API('apps','get','all');
		return $data;
	}
	/**
	 * Add Item
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit
	 * @param array $args
	 */
	public function edit( $args ){

		$data = array();
		$data['apps'] = \Clay\Application::API('apps','get','all');
		$data['application'] = !empty($args['app']) ? $args['app'] : '';
		$data['component'] = !empty($args['com']) ? $args['com'] : 'main';
		$data['action'] = !empty($args['act']) ? $args['act'] : 'view';
		$data['query'] = !empty($args['query']) ? $args['query'] : '';
		return $data;
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		
		
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		
	}
}