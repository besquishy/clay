<?php
/**
 * Applications Menu Block
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */

namespace application\apps\plugin\navigation;

/**
 * Application Plugin Navigation
 *  Menu Block (Instance)
 */
class Menu extends \Clay\Application\Plugin {
	/**
	 * Plugin App
	 * @var string
	 */
	public $PluginApp = 'apps';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'navigation';
	/**
	 * Plugin name
	 * @var string
	 */
	public $Plugin = 'menu';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param array $args
	 * @return string
	 */
	public function view( $args ){

		if (!empty($args['siteName'])){

			$args['siteName'] = \Clay\Application::Setting( 'system','site.name' );
		}
		
		if (!empty($args['user'])){
			$user = \Clay\Module::API('User','Instance');

			$user->info = $user->info();

			if(!empty($user->info['email'])){
				# Get this user's Gravatar Icon for the toolbar
				$email = trim( $user->info['email'] );
				$email = strtolower( $email );
				$email = md5( $email );
				$user->gravatar = 'https://www.gravatar.com/avatar/'.$email;
			} else {
				# TODO: Allow this to be customized for a site-specific generic avatar.
				$user->gravatar = 'https://www.gravatar.com/avatar/';
			}
			$args['user'] = $user;
		}
		if (!empty($args['log'])){
			# Check for Logged Errors
			if (!empty(\Clay\Application('dashboard','main')->errors())){
				# Display the Error Log icon (or as determined by the template)
				$args['errors'] = TRUE;
			}
		}
		
		if ( !empty( $args['dashboard'] )){
			if ( \Clay\PAGE == 'dashboard'){
				$args['close'] = true;
				$args['back'] = !empty( $_SESSION['referer'] ) && ($_SESSION['referer'] !== 'app=dashboard') 
								? '?'.$_SESSION['referer'] 
								: !empty( $_SERVER['HTTP_REFERER'] )
									? $_SERVER['HTTP_REFERER']
									: $_SERVER['PHP_SELF'];

			} else {

					$_SESSION['referer'] = $_SERVER['QUERY_STRING'];
			}
		}
		return $args;
	}
	/**
	 * Add
	 */
	public function add(){

		$data = array();
		$data['apps'] = \Clay\Application::API('apps','get','all');
		return $data;
	}
	/**
	 * Add Item
	 */
	public function addItem(){
		
		
	}
	/**
	 * Create
	 * @param array $args
	 */
	public function create($args){
		
		
	}
	/**
	 * Edit
	 * @param array $args
	 */
	public function edit( $args ){

		$data = $args;
		$data['linksCount'] = \count($args['links']);
		$data['nextLink'] = $data['linksCount']++;
		$data['apps'] = \Clay\Application::API('apps','get','all');
		return $data;
	}
	/**
	 * Edit Item
	 * @param array $args
	 */
	public function editItem($args){
		
		
	}
	/**
	 * Update
	 * @param array $args
	 */
	public function update($args){
		$delete = \Clay\Data\Post('delete');
		if (!empty($delete)){
			foreach ($delete as $index){
				if (!empty($args['links'][$index])){
					unset($args['links'][$index]);
				}
			}
		}
		foreach ($args['links'] as $key => $link){
			if (empty($link['text'])) unset($args['links'][$key]);
		}
		return $args;
	}
	/**
	 * Delete
	 * @param array $args
	 */
	public function delete($args){
		
		
	}
}