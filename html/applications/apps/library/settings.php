<?php 
/**
* Apps Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\apps\library;

/**
 * Apps Application Settings API
 * @author David
 *
 */
class settings {
	
	/**
	 * Import $this->db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All Settings or All for an Application
	 * @param string $application
	 */
	public static function getAll($application){

		if(empty($application)){
			
			$setting = self::db()->get("appsid, appid, name, value FROM ".\claydb::$tables['app_settings']." ORDER BY appid");
		
		} else {
			
			$setting = self::db()->get("appsid, appid, name, value FROM ".\claydb::$tables['app_settings']." WHERE appid = ?", array(\Clay\Application::getID($application)));
		}	
		
		return !empty($setting) ? $setting : array();
	}
	
	/**
	 * Update Multiple Settings
	 * @param array $settings
	 */
	public static function update($settings){
		
		foreach($settings as $setting){
			
			self::db()->update(\claydb::$tables['app_settings']." SET appid = ?, name = ?, value = ? WHERE appsid = ?", array(\Clay\Application::getID($setting['appname']),$setting['name'],$setting['value'],$setting['appsid']), 1);
		}
		
		return true;
	}
}