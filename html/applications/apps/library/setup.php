<?php
/**
* Apps Application
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\apps\library;

/**
 * Apps Application Setup
 */
class setup {
	/**
	 * Install
	 */
	public static function install(){
		# Some parts of this application are installed by the Setup module during installation
		
		# Applications Dashboard Plugin
		$plugin = array('plugin', array('type' => 'dashboard', 'app' => 'apps', 'name' => 'applications', 'descr' => 'Manage Applications and Settings.'));
		\Clay\Module::Object('Plugins')->Setup($plugin);

		# Hook Applications Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'apps', 'name' => 'applications'),
					  'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );

		# Application Block Plugin
		$application = array('plugin', array('type' => 'content', 'app' => 'apps', 'name' => 'application', 'descr' => 'Display an Application in a Block.', 'inst' => '1'));
		\Clay\Module::Object('Plugins')->Setup($application);

		# Navigation Plugin Type
		$navType = array('type', array('name' => 'navigation', 'descr' => 'Menu and navigation related plugins.'));
		\Clay\Module::Object('Plugins')->Setup($navType);

		# Menu Block Plugin
		$menu = array('plugin', array('type' => 'navigation', 'app' => 'apps', 'name' => 'menu', 'descr' => 'A vertical menu.', 'inst' => '1'));
		\Clay\Module::Object('Plugins')->Setup($menu);

		return true;
	}
	/**
	 * Upgrade
	 * @param string version Installed version
	 */
	public static function upgrade($version){
		# Some parts of this application are upgraded by the Setup module during upgrade
		
		switch($version){
				
			case($version <= '1.0'):
				# Applications Dashboard Plugin
				$plugin = array('plugin', array('type' => 'dashboard', 'app' => 'apps', 'name' => 'applications', 'descr' => 'Manage Applications and Settings.'));
				\Clay\Module::Object('Plugins')->Setup($plugin);

			case($version <= '1.0.1'):
				# Hook Applications Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'apps', 'name' => 'applications'),
							  'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );

			case($version <= '1.0.2'):
				# Application Block Plugin
				$application = array('plugin', array('type' => 'content', 'app' => 'apps', 'name' => 'application', 'descr' => 'Display an Application in a Block.', 'inst' => '1'));
				\Clay\Module::Object('Plugins')->Setup($application);
			
			case($version <= '1.0.3'):
				# Navigation Plugin Type
				$navType = array('type', array('name' => 'navigation', 'descr' => 'Menu and navigation related plugins.'));
				\Clay\Module::Object('Plugins')->Setup($navType);

				# Menu Block Plugin
				$menu = array('plugin', array('type' => 'navigation', 'app' => 'apps', 'name' => 'menu', 'descr' => 'A vertical menu.', 'inst' => '1'));
				\Clay\Module::Object('Plugins')->Setup($menu);
				break;
		}
		return true;
	}
	/**
	 * Delete
	 * @param string $version Installed version
	 */
	public static function delete($version){
		# Core application; Can't be deleted
		return FALSE;
	}
}