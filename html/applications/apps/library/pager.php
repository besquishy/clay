<?php
/**
* Apps Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\apps\library;

/**
 * Application Content Pager API
 * @author David
 *
 */
class pager {

	/**
	 * Generate Pager
	 * @param array $args
	 * @return array
	 */
	public static function view($args){
		
		if(empty($args['items'])){
			
			$args['items'] = 10;
		}
		
		if(empty($args['first'])){
			
			$args['first'] = 'First';
		}
		
		if(empty($args['last'])){
			
			$args['last'] = 'Last';
		}
		
		$connector = !empty($args['url'][3]) ? '&amp;' : '?';
		
		$url = $args['url'];
		# almost same as !empty($_GET['startnum']) ? $_GET['startnum'] : 1;
		$currentnum = \clay\data\get('startnum','int','int',1);
		$startnum = 1;
		# divide total Count by Items per page
		$args['pageCount'] = ceil($args['count'] / $args['items']);

		$lastpage = ($args['pageCount'] * $args['items']) - ($args['items'] - 1);
		# reference for loop 
		$i = 1;
		
		if(($currentnum != 1) && empty($first)){
			
			$args['pages'][] = array('url' => \clay\application::url($url[0],$url[1],$url[2]),
												'num' => $args['first']);
			
			if($args['pageCount'] > 2){
				
				$url[3]['startnum'] = $currentnum - $args['items'];
				$args['pages'][] = array('start' => $currentnum - $args['items'],
													'url' => \clay\application::url($url[0],$url[1],$url[2],$url[3]),
																'num' => 'Previous');
			}
			
			$first = true;
		}
		# loop for each page link
		loop:
			
			if($startnum == $currentnum){
				$args['pages'][] = array('num' => $i);
				$startnum = $startnum + $args['items'];
				$i++;
				goto check;
			}

			$url[3]['startnum'] = $startnum;
			$args['pages'][] = array('start' => $startnum, 
									'url' => \clay\application::url($url[0],$url[1],$url[2],$url[3]),
									'num' => $i);

			$startnum = $startnum + $args['items'];
			
			$i++;
			
		# repeat loop as needed	
		check:
		
			if($i <= $args['pageCount']){
				
				goto loop;				
			}
			
		last:
		
			if(($currentnum != $lastpage)){
			
				$url[3]['startnum'] = $lastpage;
				$args['lastpage'] = array('startnum' => $lastpage,
										'url' => \clay\application::url($url[0],$url[1],$url[2],$url[3]),
										'num' => $args['last']);
				
				if($args['pageCount'] > 2){
					
					$startnum = $startnum - $args['items'];
					$url[3]['startnum'] = $currentnum + $args['items'];;
					$args['pages'][] = array('start' => $currentnum + $args['items'],
											'url' => \clay\application::url($url[0],$url[1],$url[2],$url[3]),
											'num' => 'Next');
				}
			}
			
		return array('application' => 'apps', 'template' => 'pager', 'data' => $args);
	}
}