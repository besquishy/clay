<?php 
/**
* Apps Application
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\apps\library;

/**
 * Apps Application Get API
 * @author David
 *
 */
class get {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All Apps table data
	 * @return array (array) [appid,state,version,name]
	 */
	public static function all(){

		return self::db()->get('appid, state, version, name FROM '.\claydb::$tables['apps'].' ORDER BY name ASC');
	}
	
	/**
	 * Get All Apps table data from App Name
	 * @param string $name
	 * @return array [appid,state,version,name]
	 */
	public static function app($name){
		
		return self::db()->get('appid, state, version, name FROM '.\claydb::$tables['apps'] .'WHERE name = ?',array($name),'0,1');
	}
	
	/**
	 * Get App Name from ID
	 * @param integer $id
	 * @return string name
	 */
	public static function appID($id){
		
		$app = self::db()->get('name FROM '.\claydb::$tables['apps'] .'WHERE appid = ?',array($id),'0,1');
		return $app['name'];
	}
	
	/**
	 * Get App ID from Name
	 * @param string $name
	 * @return integer $id
	 */
	public static function appName($name){
		
		$app = self::db()->get('appid FROM '.\claydb::$tables['apps'] .'WHERE name = ?',array($name),'0,1');
		return $app['appid'];
	
	}
}