<div class="c-pager u-text-center u-bgcolor-secondary">
<?php foreach($pages as $page) {
if(empty($page['url'])){ ?>
<span class="c-pager__active u-bgcolor-accent"><?= $page['num']?></span>
<?php } else { ?>
<a href="<?= $page['url'];?>"><span class="c-pager__num u-bgcolor-body"><?= $page['num']?></span></a>
<?php } } if(!empty($lastpage)) {?>
<a href="<?= $lastpage['url'];?>"><span class="c-pager__num u-bgcolor-body"><?= $lastpage['num']?></span></a>
<?php } ?>
</div>