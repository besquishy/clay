<section class="c-app">
	<div class="c-app__head">
	<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
	Applications Settings</h1>
	</div>
	<div class="c-app__content">
	<form v-on:submit.prevent="onSubmit('appSettings', $event)" id="dashForm" action="<?= \Clay\Application::url('apps','admin','updateSettings'); ?>" method="post">
		<fieldset class="c-fieldset">
			<legend>Application Settings</legend>
			<div class="c-fieldset__fields">
				<table class="c-table c-table--striped c-table--border">
					<tr class="c-table__row">
						<th class="c-table__head">Application</th><th class="c-table__head">Setting</th><th class="c-table__head">Value</th>
					</tr>
				<?php foreach($settings as $setting){ ?>
					<tr class="c-table__row">
						<td class="c-table__cell">
							<input type="text" class="c-field" id="as-<?= $setting['appsid']; ?>" name="settings[<?= $setting['appsid']; ?>][appname]" value="<?= $setting['appname'];?>" />
						</td>
						<td class="c-table__cell">
							<input type="text" class="c-field" id="an-<?= $setting['appsid']; ?>" name="settings[<?= $setting['appsid']; ?>][name]" value="<?= $setting['name']; ?>" />
						</td>
						<td class="c-table__cell">
							<textarea class="c-field" name="settings[<?= $setting['appsid']; ?>][value]" id="av-<?= $setting['appsid']; ?>"><?= $setting['value']; ?></textarea>
							
							<input type="hidden" id="ass-<?= $setting['appsid']; ?>" name="settings[<?= $setting['appsid']; ?>][appsid]" value="<?= $setting['appsid']; ?>" />
						</td>
					</tr>
					
				<?php } ?>
				</table>
			</div>
		</fieldset>
		<fieldset class="c-fieldset__controls">
			<div class="o-grid">
				<div class="o-grid__col u-1/3@sm">
					<input v-if="saved !== 'appSettings'" type="submit" class="c-button" name="submit" id="submit" value="Save Settings" /> 
					<input v-on:click="toEdit('appSettings', $event)" v-if="saved == 'appSettings'" type="button" class="c-button" name="edit" id="edit" value="Edit Settings" />
				</div>
				<div class="o-grid__col u-1/3@sm">
					<div v-if="response == 'appSettings'" id="appSettings_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
					<div v-if="error == 'appSettings'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
				</div>
			</div>
		</fieldset>
	</form>
	</div>
</section>		