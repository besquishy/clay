<section class="c-app">
	<div class="c-app__head"><h1>PHP.ini Settings</h1></div>
	<div class="c-app__content">
		<p>Here you can specify php.ini settings for your site.</p>
		<?php $form->template(); ?>
	</div>
</section>