<section class="c-app">
	<div class="c-app__head"><h1>ClayCMS</h1></div>
	<div class="c-app__content">
		This website is powered by ClayCMS, a content management system built on top of the Clay development platform.
	</div>
</section>