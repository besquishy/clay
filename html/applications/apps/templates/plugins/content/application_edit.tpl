<div>
	<label>Application</label>
	<select name="content[app]" id="app">
	<?php foreach($apps as $app){?>
	<option value="<?= $app['name']; ?>" <?php if((!empty($application)) AND ($application == $app['name'])) echo 'selected="selected"'; ?>><?= $app['name']; ?></option>
	<?php } ?>
	</select>
</div>
<div>
	<label>Component</label>
	<input type="text" name="content[com]" id="com" value="<?= $component; ?>" />
</div>
<div>
	<label>Action</label>
	<input type="text" name="content[act]" id="act" value="<?= $action; ?>" />
</div>
<div>
	<label>Query</label>
	<input type="text" name="content[query]" id="query" value="<?= $query; ?>" />
</div>