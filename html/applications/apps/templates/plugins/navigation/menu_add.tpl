<div>
	<label>Menu Type?</label>
	<select name="content[type]" id="type" title="Which type of menu is this plugin?">
		<option value="navbar">Horizontal Menu</option>
		<option value="sidebar">Side Menu</option>
	</select>
</div>
<div>
	<label>Display Site Name?</label>
	<select name="content[siteName]" id="sitename" title="Display the Site Name from Site Settings?">
		<option value="1">Yes</option>
		<option value="0">No</option>
	</select>
</div>
<div>
	<label>Display a Home link?</label>
	<select name="content[home]" id="home" title="Display a Homepage link?">
		<option value="1">Yes</option>
		<option value="0">No</option>
	</select>
</div>
<div>
	<label>Display User Options?</label>
	<select name="content[user]" id="user" title="Display User Login, User Menu?">
		<option value="1">Yes</option>
		<option value="0">No</option>
	</select>
</div>
<div>
	<label>Display Error Log Alerts?</label>
	<select name="content[log]" id="log" title="Display an icon to alert when an error has occurred?">
		<option value="1">Yes</option>
		<option value="0">No</option>
	</select>
</div>
<div>
	<label>Display Dashboard Button?</label>
	<select name="content[dashboard]" id="dashboard" title="Display an icon to open the Dashboard?">
		<option value="1">Yes</option>
		<option value="0">No</option>
	</select>
</div>
<div class="u-bgcolor-info u-p u-m">
	Additional links can be added on the instance edit page.
</div>