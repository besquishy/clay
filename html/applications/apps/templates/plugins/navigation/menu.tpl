<?php if ($type == 'navbar'){ ?>
<nav class="c-navbar" id="toolbar">
    <div class="o-grid u-jc-between">
        <ul class="c-navbar__menu">
            <?php if (!empty($siteName)){ ?>
            <li class="c-navbar__brand">
                <a href="index.php"><?= $siteName; ?></a>
            </li>
            <?php }
            if (!empty($home)){ ?>
            <li class="c-navbar__item">
                <a href="index.php">Home</a>
            </li>
            <?php } 
            if (!empty($links)){
            foreach ($links as $link){ ?>
            <li class="c-navbar__item">
                <a href="<?= \clay\application::url($link['app'],$link['com'],$link['act']) ?>"><?= $link['text']; ?></a>
            </li>
            <?php } } ?>
            <?php /*<li class="c-navbar__item"><a href="#news">About</a>
                <div class="c-navbar__subitem">
                        <a href="#news">Our People</a>
                        <a href="#news">Our Mission</a>
                </div>
            </li> */ ?>
        </ul>
        <ul class="c-navbar__menu">
        <?php if (!empty($log)){ ?>
            <?php if(!empty($errors)) { ?>
            <li class="c-navbar__item">
                <a title="Error Console" href="?app=dashboard&amp;act=errors"><i class="fa fa-exclamation-triangle" style="color:gold;"></i></a>
            </li>
            <?php } 
        } ?>
        <?php if (!empty($dashboard)){
            if( empty( $close )){ ?>
            <li class="c-navbar__item">
                <a style="" title="Dashboard" href="?app=dashboard"><i class="fa fa-chevron-down u-color-accent-alt"></i></a>
            </li>
            <?php } else { ?>
            <li class="c-navbar__item">
                <a title="Close Dashboard" href="<?= $back; ?>"><i class="fa fa-window-close u-color-danger"></i></a>
            </li>
            <?php }
        } ?>
        <?php if (!empty($user)){ ?>
        <?php if($user->isMember){ ?>
            <li class="c-navbar__item">
                <a><i class="fa fa-user"></i> <strong><?= strtoupper($user->info['uname']); ?></strong> <span class="caret"></span></a>
                <div class="c-navbar__subitem c-navbar__subitem--bl">
                    <img style="text-align:center;" src="<?= $user->gravatar.'?s=175'; ?>" />
                    <a class="dashlink" href="<?= \clay\application::url('users','account','edit') ?>">Account Settings</a>
                    <a href="<?= \clay\application::url('users','main','logout') ?>">Log Out</a>
                </div>
            </li>
        <?php } else { ?>
            <li class="c-navbar__item">
                <a href="?app=dashboard"><i class="fa fa-user"></i> Login or Register</a>
            </li>
        <?php } } ?>
        </ul>
    </div>
</nav>
<?php 
}
if ($type == 'sidebar'){ ?>
Sidebar
<?php } ?>