<div>
	<label>Menu Type?</label>
	<select name="content[type]" id="type" title="Which type of menu is this plugin?">
		<option value="navbar" <?= ($type == 'navbar') ? 'selected' : ''; ?>>Horizontal Menu (navbar)</option>
		<option value="sidebar" <?= ($type == 'sidebar') ? 'selected' : ''; ?>>Side Menu (sidebar)</option>
	</select>
</div>
<div>
	<label>Display Site Name?</label>
	<select name="content[siteName]" id="sitename" title="Display the Site Name from Site Settings?">
		<option value="1" <?= ($siteName == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($siteName == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display a Home link?</label>
	<select name="content[home]" id="home" title="Display a Homepage link?">
		<option value="1" <?= ($home == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($home == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display User Options?</label>
	<select name="content[user]" id="user" title="Display User Login, User Menu?">
		<option value="1" <?= ($user == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($user == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display Error Log Alerts?</label>
	<select name="content[log]" id="log" title="Display an icon to alert when an error has occurred?">
		<option value="1" <?= ($log == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($log == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display Dashboard Button?</label>
	<select name="content[dashboard]" id="dashboard" title="Display an icon to open the Dashboard?">
		<option value="1" <?= ($dashboard == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($dashboard == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<?php if (!empty($links)){ ?>
<ul class="c-navbar__menu">
<?php foreach ($links as $link){ ?>
	<li class="c-navbar__item"><a href="<?= \clay\application::url($link['app'], $link['com'], $link['act']) ?>"><?= $link['text'] ?></a></li>
	<?php /*<li class="c-navbar__item"><a href="#news">About</a>
		<div class="c-navbar__subitem">
				<a href="#news">Our People</a>
				<a href="#news">Our Mission</a>
		</div>
	</li> */ ?>
	<?php } ?>
</ul>
<?php } ?>
<?php if (!empty($links)) { 
	foreach ($links as $index => $link){ ?>
<div class="c-table__row">
	<div class="c-table__cell">
		<label>Display Text</label>
		<input type="text" name="content[links][<?= $index ?>][text]" value="<?= $link['text'] ?>" class="c-field">
	</div>
	<div class="c-table__cell">
		<label>Application</label>
		<select name="content[links][<?= $index ?>][app]" class="c-field">
		<?php foreach ($apps as $app){
			if ($link['app'] == $app['name']) { ?>
			<option value="<?= $app['name']; ?>" selected><?= $app['name']; ?></option>
			<?php } ?>
			<option value="<?= $app['name']; ?>"><?= $app['name']; ?></option>
		<?php } ?>
		</select>
	</div>
	<div class="c-table__cell">
		<label>Component</label>
		<input type="text" name="content[links][<?= $index ?>][com]" value="<?= $link['com'] ?>" class="c-field">
	</div>
	<div class="c-table__cell">
		<label>Action</label>
		<input type="text" name="content[links][<?= $index ?>][act]" value="<?= $link['act'] ?>" class="c-field">
	</div>
	<div class="c-table__cell">
		<label>Query String</label>
		<input type="text" name="content[links][<?= $index ?>][query]" value="<?= $link['query'] ?>" class="c-field">
	</div>
	<div class="c-table__cell">
		<label>Delete</label>
		<input type="checkbox" name="delete[<?= $index ?>]" class="c-field" value="<?= $index ?>" />
	</div>
</div>
<?php } } ?>
<div class="c-table__row">
	<div class="c-table__cell">
		<label>Display Text</label>
		<input type="text" name="content[links][<?= $nextLink; ?>][text]"  class="c-field">
	</div>
	<div class="c-table__cell">
		<label>Application</label>
		<select name="content[links][<?= $nextLink; ?>][app]" class="c-field">
			<option value="">Select Application</option>
		<?php foreach ($apps as $app){ ?>
			<option value="<?= $app['name']; ?>"><?= $app['name']; ?></option>
		<?php } ?>
		</select>
	</div>
	<div class="c-table__cell">
		<label>Component</label>
		<input type="text" name="content[links][<?= $nextLink; ?>][com]"  class="c-field">
	</div>
	<div class="c-table__cell">
		<label>Action</label>
		<input type="text" name="content[links][<?= $nextLink; ?>][act]"  class="c-field">
	</div>
	<div class="c-table__cell">
		<label>Query String</label>
		<input type="text" name="content[links][<?= $nextLink; ?>][query]"  class="c-field">
	</div>
</div>