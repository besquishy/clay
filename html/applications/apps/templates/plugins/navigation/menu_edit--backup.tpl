<div>
	<label>Menu Type?</label>
	<select name="content[type]" id="type" title="Which type of menu is this plugin?">
		<option value="navbar">Horizontal Menu</option>
		<option value="sidebar">Side Menu</option>
	</select>
</div>
<div>
	<label>Display Site Name?</label>
	<select name="content[siteName]" id="sitename" title="Display the Site Name from Site Settings?">
		<option value="1" <?= ($siteName == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($siteName == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display a Home link?</label>
	<select name="content[home]" id="home" title="Display a Homepage link?">
		<option value="1" <?= ($home == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($home == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display User Options?</label>
	<select name="content[user]" id="user" title="Display User Login, User Menu?">
		<option value="1" <?= ($user == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($user == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display Error Log Alerts?</label>
	<select name="content[log]" id="log" title="Display an icon to alert when an error has occurred?">
		<option value="1" <?= ($log == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($log == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<div>
	<label>Display Dashboard Button?</label>
	<select name="content[dashboard]" id="dashboard" title="Display an icon to open the Dashboard?">
		<option value="1" <?= ($dashboard == '1') ? 'selected' : ''; ?>>Yes</option>
		<option value="0" <?= ($dashboard == '0') ? 'selected' : ''; ?>>No</option>
	</select>
</div>
<pre>
<?= var_dump($links); ?>
</pre>
<ul class="c-navbar__menu">
<?php foreach ($links as $link){ ?>
	<li class="c-navbar__item"><a href="<?= \clay\application::url($link['app'], $link['com'], $link['act']) ?>"><?= $link['text'] ?></a></li>
	<?php /*<li class="c-navbar__item"><a href="#news">About</a>
		<div class="c-navbar__subitem">
				<a href="#news">Our People</a>
				<a href="#news">Our Mission</a>
		</div>
	</li> */ ?>
	<?php } ?>
</ul>


<div v:if="data" v-for="(input, index) in data" class="c-table c-table--striped u-mt">
	<div class="c-table__row" v-if="input.type == 'application'">
		<div class="c-table__cell">
			Application Link {{ index }}
			<input type="hidden" :name="addArray('content[links]', index, 'type')" v-model="input.type" />
		</div>
		<div class="c-table__cell">
			<label>Display Text</label>
			<input type="text" :name="addArray('content[links]', index, 'text')" class="c-field" placeholder="Link text" v-model="input.fields.title" />
		</div>
		<div class="c-table__cell">
			<label>Application</label>
			<select :name="addArray('content[links]', index, 'app')" class="c-field" v-model="input.fields.app">
			<?php foreach($apps as $app){?>
				<option value="<?= $app['name']; ?>"><?= $app['name']; ?></option>
			<?php } ?>
			</select>
		</div>
		<div class="c-table__cell">
			<label>Component</label>
			<input type="text" :name="addArray('content[links]', index, 'com')" class="c-field" placeholder="main" v-model="input.fields.com" />
		</div>
		<div class="c-table__cell">
			<label>Action</label>
			<input type="text" :name="addArray('content[links]', index, 'act')" class="c-field" placeholder="view" v-model="input.fields.act" />
		</div>
		<div class="c-table__cell">
			<label>Query</label>
			<input type="text" :name="addArray('content[links]', index, 'query')" class="c-field" v-model="input.fields.query" />
		</div>
		<div class="c-table__cell">
			<button v-on:click="removeData(index)" :data-index="index" type="button" class="c-button">Delete</button>
			<button v-on:click="addSubData(index, 'application', {title:'',app:'',com:'',act:'',query:''})" :data-index="index" type="button" class="c-button">Add Subitem</button>
		</div>
	</div>
	<div v:if="input.subdata" v-for="(input, index) in input.subdata" class="c-table c-table--striped u-mt">
		<div class="c-table__row" v-if="input.type == 'application'">
			<div class="c-table__cell">
				Application Link
				<input type="hidden" :name="addArray('content[links]', index, 'type')" v-model="input.type" />
			</div>
			<div class="c-table__cell">
				<label>Display Text</label>
				<input type="text" :name="addArray('content[links]', index, 'text')" class="c-field" placeholder="Link text" v-model="input.fields.title" />
			</div>
			<div class="c-table__cell">
				<label>Application</label>
				<select :name="addArray('content[links]', index, 'app')" class="c-field" v-model="input.fields.app">
				<?php foreach($apps as $app){?>
					<option value="<?= $app['name']; ?>"><?= $app['name']; ?></option>
				<?php } ?>
				</select>
			</div>
			<div class="c-table__cell">
				<label>Component</label>
				<input type="text" :name="addArray('content[links]', index, 'com')" class="c-field" placeholder="main" v-model="input.fields.com" />
			</div>
			<div class="c-table__cell">
				<label>Action</label>
				<input type="text" :name="addArray('content[links]', index, 'act')" class="c-field" placeholder="view" v-model="input.fields.act" />
			</div>
			<div class="c-table__cell">
				<label>Query</label>
				<input type="text" :name="addArray('content[links]', index, 'query')" class="c-field" v-model="input.fields.query" />
			</div>
			<div class="c-table__cell">
				<button v-on:click="removeData(index)" :data-index="index" type="button" class="c-button">Delete</button>
			</div>
			<?php /*<div v:if="tags[index]['subs']" v-for="(_tag, _index) in tags[index]['subs']">
				<div>
					<label>Application</label>
					<select name="content[index][subs][_index][app]" id="app">
					<?php foreach($apps as $app){?>
						<option value="<?= $app['name']; ?>"><?= $app['name']; ?></option>
					<?php } ?>
					</select>
				</div>
				<div>
					<label>Component</label>
					<input type="text" name="content[index][subs][_index][com]" id="com" />
				</div>
				<div>
					<label>Action</label>
					<input type="text" name="content[index][subs][_index][act]" id="act" />
				</div>
				<div>
					<label>Query</label>
					<input type="text" name="content[index][subs][_index][query]" id="query" />
				</div>		
			</div> */ ?>
		</div>
	</div>
</div>
<button v-on:click="addData('application',{title:'',app:'',com:'',act:'',query:''})" type="button" class="c-button">New Application Link</button>