<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<i class="fa fa-cube dash-icon-lg"></i> Applications
		</div>
	</div>
	<div class="c-card__content">
		<?php if(!empty($manage))?><a href="<?= $manage; ?>" class="dashlink">Add/Remove</a><br />
		<?php if(!empty($settings))?><a href="<?= $settings; ?>" class="dashlink">App Settings</a><br />
	</div>
</div>