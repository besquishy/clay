<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> /
		Applications Manager</h1>
	</div>
	<div class="c-app__content">
		<?php foreach($apps as $class => $categories){ ?>
		<h2><?= $class; ?></h2>
		<?php foreach($categories as $category => $apps){ ?>
		<h3><?= $category; ?></h3>
		<table class="c-table c-table--border">
			<thead>
				<tr class="c-table__row">
					<th  class="c-table__head">Application</th><th class="c-table__head">Description</th><th class="c-table__head">Version</th><th class="c-table__head">Release</th><th class="c-table__head">Namespace</th><th class="c-table__head">Options</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($apps as $app){ ?>
				<tr <?= !empty($app['css']) ? 'class="u-bgcolor-'.$app['css'].' c-table__row"' : 'class="c-table__row"';?>>
					<td class="c-table__cell"><?= $app['title']; ?></td>
					<td class="c-table__cell"><?= $app['description']; ?></td>
					<td class="c-table__cell"><?= $app['version']; ?></td>
					<td class="c-table__cell"><?= $app['date']; ?></td>
					<td class="c-table__cell"><?= $app['name']; ?></td>
					<td class="c-table__cell">
					<?php switch($app['status']){
						case 'not-installed': ?>
						<form v-on:submit.prevent="onSubmit('<?= $app['name']; ?>', $event)" id="<?= $app['name']; ?>-install" method="POST" action="<?= $installURL; ?>">
						<input type="hidden" form="<?= $app['name']; ?>-install" value="<?= $app['name']; ?>" name="appname" id="<?= $app['name']; ?>" />
						<input v-if="saved !== '<?= $app['name']; ?>'" type="submit" class="c-button c-button--success" form="<?= $app['name']; ?>-install" value="Install" name="submit" id="<?= $app['name']; ?>-submit" />
						<div v-if="response == '<?= $app['name']; ?>'" id="<?= $app['name']; ?>_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
						<div v-if="error == '<?= $app['name']; ?>'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
						</form>
					<?php break;
						case 'installed': ?>
						<form v-on:submit.prevent="onSubmit('<?= $app['name']; ?>', $event)" id="<?= $app['name']; ?>-remove" method="POST" action="<?= $removeURL; ?>">
						<input type="hidden" form="<?= $app['name']; ?>-remove" value="<?= $app['name']; ?>" name="appname" id="<?= $app['name']; ?>" />
						<input v-if="saved !== '<?= $app['name']; ?>'" type="submit" class="c-button c-button--danger" form="<?= $app['name']; ?>-remove" value="Remove" name="submit" id="<?= $app['name']; ?>-submit" />
						<div v-if="response == '<?= $app['name']; ?>'" id="<?= $app['name']; ?>_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
						<div v-if="error == '<?= $app['name']; ?>'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
						</form>
					<?php break;
						case 'upgrade': ?>
						<form v-on:submit.prevent="onSubmit('<?= $app['name']; ?>', $event)" id="<?= $app['name']; ?>-upgrade" method="POST" action="<?= $upgradeURL; ?>">
						<input type="hidden" form="<?= $app['name']; ?>-upgrade" value="<?= $app['name']; ?>" name="appname" id="<?= $app['name']; ?>" />
						<input type="hidden" form="<?= $app['name']; ?>-upgrade" value="<?= $app['installed.version']; ?>" name="appversion" id="<?= $app['name'].'-version'; ?>" />
						<input v-if="saved !== '<?= $app['name']; ?>'" type="submit" class="btn btn-warning btn-block dashSubmit" form="<?= $app['name']; ?>-upgrade" value="Upgrade" name="submit" id="<?= $app['name']; ?>-submit" />
						<div v-if="response == '<?= $app['name']; ?>'" id="<?= $app['name']; ?>_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
						<div v-if="error == '<?= $app['name']; ?>'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
						</form>
					<?php break;
					case 'not-installable': ?>
						<input type="button" class="c-button c-button--primary c-field--disabled" value="Ready" disabled />
					<?php break;
					case 'core': ?>
						<input type="button" class="c-button c-button--default c-field--disabled" value="Core" disabled />
					<?php break; } ?>
					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<?php } } ?>
	</div>
</section>