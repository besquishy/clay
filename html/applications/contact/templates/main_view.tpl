<section class="c-app">
	<div class="c-app__head">
	<?= !empty($c['heading']) ? "<h1>".$c['heading']."</h1>" : "<h1>Contact Us</h1>"; ?>
	</div>
	<div class="container-fluid c-app__content">
		<div class="row-fluid">
			<div class="col-md-6 col-sm-12">
			<?php if($c['form.type'] == 'form'){ ?>
				<?= !empty($c['intro']) ? "<p>".$c['intro']."</p>" : ''; ?>
				<form>
					<div class="form-group">
						<label for="contactName">Name</label>
						<input name="name" type="text" class="form-control" id="contactName" placeholder="Your Name">
					</div>
					<div class="form-group">
						<label for="contactEmail">Email address</label>
						<input name="email" type="email" class="form-control" id="contactEmail" placeholder="Your Email Address">
					</div>
					<div class="form-group">
						<label for="contactSubject">Subject</label>
						<input name="subject" type="text" class="form-control" id="contactSubject" placeholder="Your Subject">
					</div>
					<div class="form-group">
						<label for="contactMsg">Message</label>
						<textarea name="message" class="form-control" rows="3" placeholder="Your Message"></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Send</button>
				</form>
			<?php } ?>
			<?php if($c['form.type'] == 'frame'){ ?>
				<?= $c['form.frame']; ?>
			<?php } ?>
			</div>
			<div class="col-md-6 col-sm-12" style="text-align:center">
			<?php if ($c['map'] == '1') { ?>
				<?= $c['map.frame']; ?> 
			<?php } 
				if ($c['info'] == '1') { ?>

				<?php if(!empty($infoData['office.phone'])) { ?>
				<p>
					<i class="fa fa-building" aria-hidden="true"></i> Phone: <?= $infoData['office.phone']; ?>
				</p> <?php } ?>
				<?php if(!empty($infoData['fax'])) { ?>
				<p>
					<i class="fa fa-fax" aria-hidden="true"></i> Fax: <?= $infoData['fax']; ?>
				</p> <?php } ?>
				<?php if(!empty($infoData['cell'])) { ?>
				<p>
					<i class="fa fa-mobile" aria-hidden="true"></i> Cell: <?= $infoData['cell']; ?>
				</p> <?php } ?>
				<?php if(!empty($infoData['twitter'])) { ?>
				<p>
					<i class="fa fa-twitter" aria-hidden="true"></i> Twitter: <?= $infoData['twitter']; ?>
				</p> <?php } ?>
				<?php if(!empty($infoData['facebook'])) { ?>
				<p>
					<i class="fa fa-facebook" aria-hidden="true"></i> Facebook: <?= $infoData['facebook']; ?>
				</p> <?php } 
				} ?>
			</div>
		</div>
	</div>
</section>