<section class="c-app">
    <div class="c-app__head">
    <h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    Contact Settings</h1>
    </div>
    <div class="c-app__content">
        <form v-on:submit.prevent="onSubmit('contactSettings', $event)" role="form" id="dashForm" class="contact_settings" action="<?= \clay\application::url('contact','admin','save');?>" method="post">
        <div class="form-group">
            <fieldset class="c-fieldset">
                <legend>Heading</legend>
                <div class="c-fieldset__fields">
                    <label>Application Title (Optional)</label>
                    <input type="text" name="heading" id="heading" class="c-field u-1/4@sm" placeholder="Contact Us" value="<?= $heading ?>" />
                    <input type="hidden" name="oldHeading" id="oldHeading" value="<?= $heading ?>" />
                    <label>Introduction (Optional)</label>
                    <textarea name="intro" id="intro" class="c-field" placeholder="Get in touch with us!"><?= $intro ?></textarea>
                    <input type="hidden" name="oldIntro" id="oldIntro" value="<?= $intro ?>" />
                </div>
            </fieldset>
            <fieldset class="c-fieldset">
                <legend>Form Settings</legend>
                <div class="c-fieldset__fields">
                    <p class="c-alert u-bgcolor-primary u-text-center u-1/6@sm">
                        Only Used With Basic Form
                    </p>
                    <label>Contact Email Address</label>
                    <input type="email" name="email" id="email" class="c-field u-1/4@sm" value="<?= $email ?>" />
                    <input type="hidden" name="oldEmail" id="oldEmail" value="<?= $email ?>" />
                    <label>Form Type</label>
                    <select name="type" id="type" class="c-field u-1/12@sm">
                        <option value="form" <?php if($type === 'form') echo 'selected="selected"'?> disabled>Basic</option>
                        <option value="frame" <?php if($type === 'frame') echo 'selected="selected"'?>>Embedded</option>
                    </select>
                    <input type="hidden" name="oldType" id="oldType" value="<?= $type ?>" />
                    <p class="c-alert u-bgcolor-primary u-text-center u-1/6@sm">
                        Paste an Embed Code For Your Form
                    </p>
                    <label>Embedded Form HTML</label>
                    <textarea name="frame" id="frame" class="c-field"><?= $frame ?></textarea>
                    <textarea name="oldFrame" id="oldFrame" style="display:none;"><?= $frame ?></textarea>
                </div>
            </fieldset>
        </div>
            <fieldset class="c-fieldset">
                <legend>Info</legend>
                <div class="c-fieldset__fields">
                    <label>Enable Extra Info?</label>
                    <select name="info" id="info" class="c-field u-1/12@sm">
                        <option value="0" <?php if($info === '0') echo 'selected="selected"'?>>No</option>
                        <option value="1" <?php if($info === '1') echo 'selected="selected"'?>>Yes</option>
                    </select>
                    <input type="hidden" name="oldInfo" id="oldInfo" value="<?= $info?>" />
                    <label>Office Telephone</label>
                    <input type="telephone" name="infoData[office.phone]" id="office-phone" class="c-field u-1/4@sm" value="<?= !empty($infoData['office.phone']) ? $infoData['office.phone'] : ''; ?>" />
                    <input type="hidden" name="oldData[office.phone]" id="oldOffice-phone" value="<?= !empty($infoData['office.phone']) ? $infoData['office.phone'] : ''; ?>" />
                    <label>Fax</label>
                    <input type="telephone" name="infoData[fax]" id="fax" class="c-field u-1/4@sm" value="<?= !empty($infoData['fax']) ? $infoData['fax'] : ''; ?>" />
                    <input type="hidden" name="oldData[fax]" id="oldFax" value="<?= !empty($infoData['fax']) ? $infoData['fax'] : ''; ?>" />
                    <label>Cell Phone</label>
                    <input type="telephone" name="infoData[cell]" id="cell" class="c-field u-1/4@sm" value="<?= !empty($infoData['cell']) ? $infoData['cell'] : ''; ?>" />
                    <input type="hidden" name="oldData[cell]" id="oldCell" value="<?= !empty($infoData['cell']) ? $infoData['cell'] : ''; ?>" />
                    <label>Twitter</label>
                    <input type="text" name="infoData[twitter]" id="twitter" class="c-field u-1/4@sm" value="<?= !empty($infoData['twitter']) ? $infoData['twitter'] : ''; ?>" />
                    <input type="hidden" name="oldData[twitter]" id="oldTwitter" value="<?= !empty($infoData['twitter']) ? $infoData['twitter'] : ''; ?>" />
                    <label>Facebook</label>
                    <input type="text" name="infoData[facebook]" id="facebook" class="c-field u-1/4@sm" value="<?= !empty($infoData['facebook']) ? $infoData['facebook'] : ''; ?>" />
                    <input type="hidden" name="oldData[facebook]" id="oldFacebook" value="<?= !empty($infoData['facebook']) ? $infoData['facebook'] : ''; ?>" />
                </div>
            </fieldset>	
            <fieldset class="c-fieldset">
                <legend>Map</legend>
                <div class="c-fieldset__fields">
                    <label>Enable Embedded Map?</label>
                    <select name="map" id="map" class="c-field u-1/12@sm">
                        <option value="0" <?php if($map === '0') echo 'selected="selected"'?>>No</option>
                        <option value="1" <?php if($map === '1') echo 'selected="selected"'?>>Yes</option>
                    </select>
                    <input type="hidden" name="oldMap" id="oldMap" value="<?= $map; ?>" />
                    <p class="c-alert u-bgcolor-primary u-1/6@sm u-text-center">
                        Paste an Embed Code For Your Map
                    </p>
                    <label>Embedded Map HTML</label>
                    <textarea name="mapFrame" id="mapFrame" class="c-field"><?= $mapFrame; ?></textarea>
                    <textarea name="oldMapFrame" id="oldMapFrame" style="display:none;"><?= $mapFrame ?></textarea>
                </div>
            </fieldset>
            <fieldset class="c-fieldset">
                <div class="c-fieldset__fields">
                    <p v-if="response == 'contactSettings'" id="contactSettings_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></p>
                    <p v-if="error == 'contactSettings'" class="c-alert u-pl@sm u-color-error u-weight-bold">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></p>
                    <input type="hidden" name="settings" id="settings" value="1" />
                    <button v-if="saved !== 'contactSettings'" type="submit" name="submit" id="submit" class="c-button"><i class="fa fa-check-circle fa-lg "></i>&emsp;Save Settings</button> 
                    <input v-on:click="toEdit('contactSettings', $event)" v-if="saved == 'contactSettings'" type="button" class="c-button" name="edit" id="edit" value="Edit Settings" />
                </div>
            </fieldset>
        </form>
    </div>
</section>