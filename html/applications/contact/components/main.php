<?php
/**
* Contact Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\contact\component;

/**
 * Contact Application Main Component
 * @author David
 *
 */
class main extends \Clay\Application\Component {

	/**
	 * Contact View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		/*if(!$user->privilege('contact','Form','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}*/

		/*

		$type = \Clay\Application::Setting('contact','form.type');

		if($type == 'frame'){

			$data['frame'] = \Clay\Application::Setting('contact', 'form.frame');
		
		} else {

			$data['form'] = true;
		}

		$map = \Clay\Application::Setting('contact','map');

		if(!empty($map)){

			$data['mapFrame'] = \Clay\Application::Setting('contact','map.frame');
		}

		$info = \Clay\Application::Setting('contact','info.data',serialize(array()));
		
		$data['infoData'] = unserialize($info);

		*/

		$settings = \Clay\Application::Settings('contact');

		foreach($settings as $set){

			$data['c'][$set['name']] = $set['value'];
		}

		$data['infoData'] = unserialize($data['c']['info.data']);
		
		return $data;
	}
}