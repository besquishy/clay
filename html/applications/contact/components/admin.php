<?php
/**
* Contact Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\contact\component;

/**
 * Contact Application Admin Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * Contact Admin View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Heading
		$data['heading'] = \Clay\Application::Setting('contact','heading','');
		# Introduction
		$data['intro'] = \Clay\Application::Setting('contact','intro','');
		# Form Type
		$data['type'] = \Clay\Application::Setting('contact','form.type','frame');
		# Embedded Form HTML
		$data['frame'] = \Clay\Application::Setting('contact', 'form.frame','');
		# Contact Email Address
		$data['email'] = \Clay\Application::Setting('contact', 'email','');
		# Use Map?
		$data['map'] = \Clay\Application::Setting('contact','map','0');
		# Embedded Map HTML
		$data['mapFrame'] = \Clay\Application::Setting('contact','map.frame','');
		# Use Extra Info?
		$data['info'] = \Clay\Application::Setting('contact','info','0');
		# Extra Info
		$infoData = \Clay\Application::Setting('contact','info.data',serialize(array()));
		$data['infoData'] = unserialize($infoData);
		
		return $data;
	}

	/**
	 * Contact Admin Save
	 */
	public function save(){

		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Heading
		if($_POST['heading'] !== $_POST['oldHeading']){
			
			\clay\application::set('contact','heading',\clay\data\post('heading','string','text'));
		}
		# Introduction
		if($_POST['intro'] !== $_POST['oldIntro']){

			\clay\application::set('contact','intro',\clay\data\post('intro','string','text'));
		}
		# Contact Email Address
		if((!empty($_POST['email'])) && ($_POST['email'] !== $_POST['oldEmail'])){

			\clay\application::set('contact','email',\clay\data\post('email','email','email'));
		}
		# Form Type
		if($_POST['type'] !== $_POST['oldType']){

			\clay\application::set('contact','form.type',\clay\data\post('type','string','text'));
		}
		# Embedded Form HTML
		if((!empty($_POST['frame'])) && ($_POST['frame'] !== $_POST['oldFrame'])){
		
			\clay\application::set('contact','form.frame',\clay\data\post('frame','string'));
		}
		# Use Extra Info?
		if($_POST['info'] !== $_POST['oldInfo']){
		
			\clay\application::set('contact','info',\clay\data\post('info','string','text'));
		}

		# Contact Information Extra Data
		# Reset Info Data
		\Clay\Application::set('contact','info.data','');
		# Save New Info Data
		\clay\application::set('contact','info.data',serialize(\clay\data\post('infoData','isArray','',array())));

		# Use Map?
		if($_POST['map'] !== $_POST['oldMap']){
			
			\clay\application::set('contact','map',\clay\data\post('map','string','text'));
		}
		# Embedded Map HTML
		if((!empty($_POST['mapFrame'])) && ($_POST['mapFrame'] !== $_POST['oldMapFrame'])){
		
			\clay\application::set('contact','map.frame',\clay\data\post('mapFrame','string'));
		
		}
		# Show Update Notification
		\clay\application::api('system','message','send','Contact Settings Updated');
		# Redirect if not using AJAX submit
		if(empty($_POST['dashForm'])){
			
			\clay::redirect($_SERVER['HTTP_REFERER']);
		}
	}
}