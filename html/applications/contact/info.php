<?php
/**
 * Contact Application
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Contact',
			'name' => 'contact',
			'version' => '1.0.2',
			'date' => 'September 16, 2017',
			'description' => 'Allows users to contact the admin.',
			'class' => 'User',
			'category' => 'Support',
			'depends' => array('applications' => array('dashboard')),
			'core' => FALSE
			);
?>