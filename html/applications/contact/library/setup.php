<?php
/**
* Contact Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

namespace application\contact\library;

\Clay\Module('Privileges');

/**
 * Contact Application Setup
 */
class setup {
	/**
	 * Install
	 */
	public static function install(){
		# Register Application Settings
		\Clay\Application::Set('contact','heading','');
		\Clay\Application::Set('contact','intro','');
		\Clay\Application::Set('contact','form.type','form');
		\Clay\Application::Set('contact','form.frame','');
		\Clay\Application::Set('contact','email','');
		\Clay\Application::Set('contact','map','0');
		\Clay\Application::Set('contact','map.frame','');
		\Clay\Application::Set('contact','info','0');
		\Clay\Application::Set('contact','info.data',serialize(array()));
		# Register Contact Privilege
		$privs = \Clay\Module::Object('Privileges');		
		$privs->Register('contact','Form','View');
		# Contact Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'contact', 'name' => 'contact', 'descr' => 'Contact Administration.'));
		\Clay\Module::Object('Plugins')->Setup($dashboard);
		# Hook Contact Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'contact', 'name' => 'contact'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );

		return true;
	}
	/**
	 * Upgrade
	 * @param string $version Installed version
	 */
	public static function upgrade($version){

		switch($version){

			case ($version <= '1.0'):
				# Contact Dashboard Plugin
				$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'contact', 'name' => 'contact', 'descr' => 'Contact Administration.'));
				\Clay\Module::Object('Plugins')->Setup($dashboard);

			case ($version <= '1.0.1'):
				# Hook Contact Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'contact', 'name' => 'contact'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );
				break;
		}

		return true;
	}
	/**
	 * Delete
	 */
	public static function delete(){

	    \Clay\Module::Object('Privileges')->remove('contact');
		\Clay\Module::Object('Plugins')->remove('contact');
		\Clay\Application::deleteSetting('contact');

	    return true;
	}
}
