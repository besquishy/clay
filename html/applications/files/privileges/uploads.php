<?php
namespace application\files\privilege;

\Library('Clay/Application/Privilege');

/**
 * File Uploads Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2013
 * @todo Add Status field in file_uploads table for use with 'PUBLIC', 'PRIVATE', 'DRAFT', etc
 */
class Uploads extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var unknown_type
	 */
	public $Request;
	
	/**
	 * Files Multimask Privilege
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. POSTID::1 = array('POSTID',1)

		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
			
			case 'AUTHOR':
				
				# Fetch some info about the requested File
				$file = \Clay\Application::API('files','uploads','getInfo',array('fid' => $this->Request));

				switch($scope[1]){
					
					case 'MYSELF':
						
						# Pseudo User - If assigned and User created the Post, returns TRUE
						if($file['uid'] == \Clay\Module::Object('User')->id()){
							
							return TRUE;
						}
						
						return FALSE;
						
					case $file['uid']:
						
						return TRUE;
				}
				
				return FALSE;
				
			case 'FILEID':
				
				if($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
				
			case 'PUBLIC':
				
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Experimental Administration tool
	 */
	public function Scopes($privilege, $scope = NULL){
		
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'AUTHOR', 'POSTID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
			
			case 'AUTHOR':
				
				$users = \Clay\Module::API('Users','getALL');
				
				# Pseudo User - allows privileges for the User that created an Upload
				$users[] = array('uid' => 'MYSELF', 'name' => 'MYSELF');
				
				# do a foreach loop here to make it match FILEID below
				
				foreach($users as $user){
					
					$item['id'] = $user['uid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					$item['title'] = NULL;
					$item['name'] = !empty($user['uname']) ? $user['uname'] : $user['name'];
					
					$option['items'][] = $item;
				}
				unset($users);
				$option['type'] = 'Users';
				return $option;
				
			case 'FILEID':
				
				$files = \Clay\Application::API('files','uploads','getAll');
				
				foreach($files as $file){
					
					$item['id'] = $file['fid'];
					$item['date'] = $file['pubdate'];
					$item['uid'] = $file['uid'];
					$item['name'] = !empty($file['name']) ? $file['name'] : 'Untitled';
					$item['title'] = NULL;
					
					$option['items'][] = $item;
				}
				
				unset($files);
				$option['type'] = 'File Uploads';
				return $option;
		}
	}
}