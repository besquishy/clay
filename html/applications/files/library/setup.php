<?php
namespace application\files\library;

\Clay\Module('Privileges');

/**
* Files Application
*
* @copyright (C) 2013-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/
class setup {

	public static function install(){
		\Library('ClayDB');
		$dbconn = \claydb::connect();
	    $datadict = $dbconn->datadict();
	    $tables = array('file_uploads' => array('table' => 'file_uploads'));
	    $datadict->registerTables($tables);
	    \claydb::tables('flush');
	    
	    $cols = array();
	    $cols['column'] = array('fid' => array('type' => 'ID'),
	    						'status' => array('type' => 'TINYINT', 'size' => '1'),
	    						'pubdate' => array('type' => 'INT', 'size' => '10', 'null' => FALSE),
	    						'uid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    						'name' => array('type' => 'VARCHAR', 'size' => '255'),
	    						'fname' => array('type' => 'VARCHAR', 'size' => '255', 'null' => FALSE),
	    						'ftype' => array('type' => 'VARCHAR', 'size' => '255', 'null' => FALSE));
	    
	    $cols['index'] = array('status' => array('type' => 'KEY', 'index' => 'status'),
	    						'date' => array('type' => 'KEY', 'index' => 'pubdate'),
	    						'userfile' => array('type' => 'UNIQUE', 'index' => 'uid,name'),
	    						'filename' => array('type' => 'UNIQUE', 'index' => 'fname'),
	    						'filetype' => array('type' => 'KEY', 'index' => 'ftype'));
	    
	    $datadict->createTable(\claydb::$tables['file_uploads'],$cols);

		\Clay\Module::Object('Privileges')->register('files','Uploads','Create');
		\Clay\Module::Object('Privileges')->register('files','Uploads','Update');
		\Clay\Module::Object('Privileges')->register('files','Uploads','Delete');
		\Clay\Module::Object('Privileges')->register('files','Uploads','View');

		return true;
	}
	public static function upgrade($version){

		switch($version){
			
		}
		return true;
	}
	public static function delete(){
		\Library('ClayDB');
		$dbconn = \claydb::connect();
	    $datadict = $dbconn->datadict();

	    $datadict->dropTable(\claydb::$tables['blog_posts']);

	    \Clay\Module::Object('Privileges')->remove('files');

	    return true;
	}
}
