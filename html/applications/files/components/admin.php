<?php
namespace application\contact\component;

/**
* Contact Application
*
* @copyright (C) 2013 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 *  Contact Admin Component
 * @author David
 *
 */
class main extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * Contact Admin View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		
		
		return $data;
	}
}