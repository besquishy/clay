<?php
namespace application\files\component;

/**
* Files Application
*
* @copyright (C) 2013 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 *  Files Upload Component
 * @author David
 *
 */
class image extends \Clay\Application\Component {

	/**
	 * Upload Image
	 */
	public function upload(){

		$data = array();

		if(isset($_FILES['file'])){

			$errors= array();
			$file_name = $_FILES['file']['name'];
			$file_size = $_FILES['file']['size'];
			$file_tmp = $_FILES['file']['tmp_name'];
			$file_type = $_FILES['file']['type'];
			$file_ext = \strtolower(\end(\explode('.',$file_name)));

			$extensions= array("jpeg","jpg","png");

			if(in_array($file_ext,$extensions)=== false){

				$error = TRUE;
				\clay\application::api('system','message','send','extension not allowed, please choose a JPEG or PNG file.');
			}

			if($file_size > 2097152){

				$error = TRUE;
				\clay\application::api('system','message','send','File size limit is 2 MB');
			}

			if(empty($error)){

				$new = $file_name.'_'.md5(time()).'.'.$file_ext;
				move_uploaded_file($file_tmp,\clay\WEB_PATH."uploads/images/".$new);
				$data['url'] = "uploads/images/".$new;
				\clay\application::api('system','message','send', "File: $file_name, Uploaded!");
				return $data;

			}
		} else {

			\clay\application::api('system','message','send', "No file was sent!");
		}
	}
}
