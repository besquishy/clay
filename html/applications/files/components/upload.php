<?php
namespace application\files\component;

/**
* Files Application
*
* @copyright (C) 2013 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 *  Files Upload Component
 * @author David
 *
 */
class upload extends \Clay\Application\Component {

	/**
	 * Upload View
	 */
	public function view(){

		$data = array();
		
		/*$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('blog','Post','View')){
			
			#throw new \Exception('Additional Privileges Are Required to View This Page.');
		}*/
		
		\Clay::Library('Styles');
		\clay\styles::addApplication('common','jquery-ui/jquery-ui.min.css');
		\Clay\Styles::addApplication('files','bootstrap-image-gallery.min.css');
		\Clay\Styles::addApplication('files','jquery.fileupload-ui.css');
		\Clay::Library('Scripts');
		\Clay\Scripts::addApplication('common','jquery/jquery.js');
		\Clay\Scripts::addApplication('common','jquery-ui/js/jquery-ui.min.js');
		\clay\scripts::addApplication('bootstrap','bootstrap.min.js');
		\Clay\Scripts::addApplication('files','tmpl.min.js');
		\Clay\Scripts::addApplication('files','load-image.min.js');
		\Clay\Scripts::addApplication('files','canvas-to-blob.min.js');
		\Clay\Scripts::addApplication('files','bootstrap-image-gallery.min.js');
		\Clay\Scripts::addApplication('files','jquery.iframe-transport.js');
		\Clay\Scripts::addApplication('files','jquery.fileupload.js');
		\Clay\Scripts::addApplication('files','jquery.fileupload-fp.js');
		\Clay\Scripts::addApplication('files','jquery.fileupload-ui.js');
		\Clay\Scripts::addApplication('files','main.js');
		return $data;
	}
}