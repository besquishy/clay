<?php
/**
 * Contact Application
 *
 * @copyright (C) 2013-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay
 */
$data = array('title' => 'Files',
			'name' => 'files',
			'version' => '1.0',
			'date' => 'September 10, 2017',
			'description' => 'Allows users to upload and manage files.',
			'class' => 'User',
			'category' => 'Content',
			'depends' => array('applications' => array('dashboard')),
			'core' => FALSE,
			'install' => FALSE
			);
?>