<?php
/**
* DataObjects Application
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\component;

/**
 *  DataObjects Objects Component
 * @author David
 *
 */
class objects extends \Clay\Application\Component
{

	/**
	 * DataObjects View
	 */
	public function view()
	{

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');

		/* # Privilege Check
		if (!$user->privilege('dataobjects','Main','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		*/
		
		$data['user'] = $user->Info();
		
		return $data;
	}
}