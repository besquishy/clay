<?php
/**
* DataObjects Application
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\component;

/**
 *  DataObjects Admin Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component
{

	/**
	 * DataObjects Admin View
	 */
	public function view()
	{

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if (!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Heading
		$data['heading'] = \Clay\Application::Setting('dataobjects','heading','');
		# Introduction
		$data['intro'] = \Clay\Application::Setting('dataobjects','intro','');
		# Form Type
		$data['type'] = \Clay\Application::Setting('dataobjects','form.type','frame');
		# Embedded Form HTML
		$data['frame'] = \Clay\Application::Setting('dataobjects', 'form.frame','');
		# DataObjects Email Address
		$data['email'] = \Clay\Application::Setting('dataobjects', 'email','');
		# Use Map?
		$data['map'] = \Clay\Application::Setting('dataobjects','map','0');
		# Embedded Map HTML
		$data['mapFrame'] = \Clay\Application::Setting('dataobjects','map.frame','');
		# Use Extra Info?
		$data['info'] = \Clay\Application::Setting('dataobjects','info','0');
		# Extra Info
		$infoData = \Clay\Application::Setting('dataobjects','info.data',serialize(array()));
		$data['infoData'] = unserialize($infoData);
		
		return $data;
	}

	/**
	 * DataObjects Admin Save
	 */
	public function save()
	{

		$user = \Clay\Module::API('User','Instance');
		
		if (!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Heading
		if ($_POST['heading'] !== $_POST['oldHeading']){
			
			\Clay\Application::set('dataobjects','heading',\Clay\Data\post('heading','string','text'));
		}
		# Introduction
		if ($_POST['intro'] !== $_POST['oldIntro']){

			\Clay\Application::set('dataobjects','intro',\Clay\Data\post('intro','string','text'));
		}
		# DataObjects Email Address
		if ((!empty($_POST['email'])) && ($_POST['email'] !== $_POST['oldEmail'])){

			\Clay\Application::set('dataobjects','email',\Clay\Data\post('email','email','email'));
		}
		# Form Type
		if ($_POST['type'] !== $_POST['oldType']){

			\Clay\Application::set('dataobjects','form.type',\Clay\Data\post('type','string','text'));
		}
		# Embedded Form HTML
		if ((!empty($_POST['frame'])) && ($_POST['frame'] !== $_POST['oldFrame'])){
		
			\Clay\Application::set('dataobjects','form.frame',\Clay\Data\post('frame','string'));
		}
		# Use Extra Info?
		if ($_POST['info'] !== $_POST['oldInfo']){
		
			\Clay\Application::set('dataobjects','info',\Clay\Data\post('info','string','text'));
		}

		# DataObjects Information Extra Data
		# Reset Info Data
		\Clay\Application::set('dataobjects','info.data','');
		# Save New Info Data
		\Clay\Application::set('dataobjects','info.data',serialize(\Clay\Data\post('infoData','isArray','',array())));

		# Use Map?
		if ($_POST['map'] !== $_POST['oldMap']){
			
			\Clay\Application::set('dataobjects','map',\Clay\Data\post('map','string','text'));
		}
		# Embedded Map HTML
		if ((!empty($_POST['mapFrame'])) && ($_POST['mapFrame'] !== $_POST['oldMapFrame'])){
		
			\Clay\Application::set('dataobjects','map.frame',\Clay\Data\post('mapFrame','string'));
		
		}
		# Show Update Notification
		\Clay\Application::API('system','message','send','DataObjects Settings Updated');
		# Redirect if not using AJAX submit
		if (empty($_POST['dashForm'])){
			
			\clay::redirect($_SERVER['HTTP_REFERER']);
		}
	}
}