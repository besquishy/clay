<?php
/**
 * DataObjects Application
 *
 * @copyright (C) 2018 David L Dyess II
 * @license GPL
 * @link https://daviddyess.com
 * @author David
 * @package Clay
 */
$data = array('title' => 'DataObjects',
			'name' => 'dataobjects',
			'version' => '0.1',
			'date' => 'May 20, 2018',
			'description' => 'Data Objects, Dynamic Data',
			'class' => 'System',
			'category' => 'Tools',
			'depends' => array('applications' => array('dashboard')),
			'core' => false,
			'install' => true
			);