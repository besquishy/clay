<?php
/**
* DataObjects Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\library;

/**
 * DataObjects Threads API
 * @author David
 *
 */
class objects
{
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All (or some) Comment Threads
	 * @param array $args
	 * @return array
	 */
	public static function get($args=array())
	{
		
		# Select a range starting number using 'startnum'
		$offset = !empty($args['startnum']) ? $args['startnum'] : '0';
		# Fetch 'items' number of Comment Threads, offset by 'startnum' if applicable
		if (!empty($args['items'])){
			
			if (!empty($args['startnum'])){
				
				$limit = $args['startnum'].','.$args['items'];
				
			} else {
				
				$limit = '0,'.$args['items'];
			}
			
		} else {
			
			$limit = '';
		}
		# @TODO: In the future we should create an API (perhaps system level?) for status codes, for now leave it user select
		$status = !empty($args['status']) ? $args['status'] : 1;
		# Return associative array
		return self::db()->get('doid, otype, appid, itemtype, itemid, name, descr, options FROM '.\claydb::$tables['do_objects'].' ORDER BY doid ASC',array(args['doid']),$limit);
	}
	
	/**
	 * Count Data Objects
	 * @param array $args - inert
	 * @return int
	 */
	public static function count($args=null)
	{

		$count = self::db()->get('COUNT(*) as objects FROM '.\claydb::$tables['do_objects']);
		return $count[0]['objects'];
	}
}