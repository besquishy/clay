<?php
/**
* DataObjects Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\library;

/**
 * Thread API
 * @author David
 *
 */
class object
{

	private static $INVALID_ID = 'A numeric Object ID (doid) must be supplied';

	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;

	/**
	 * Create a Thread
	 * @param array $args
	 */
	public static function create( $args )
	{

		if ( empty( $args['app']) AND !empty( $args['appid'] )){

			$args['app'] = \Clay\Application::getName( $args['appid'] );
		}

		if ( empty( $args['appid']) AND !empty( $args['app'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}

		if ( empty( $args['itemtype'] )){

			$args['itemtype'] = 1;
		}

		if ( !empty( $args['hook'] )){

			$item = \Clay\Application\Core::Item( $args['app'], $args['itemtype'], $args['itemid'] );

			$args['itemid'] = $item['id'];
 			$args['name'] = $item['title'];
 			$args['user'] = $item['uid'];
 			$args['itemtype'] = $item['itemtype'];
 			$args['date'] = $item['date'];
		}

		if ( empty( $args['options'] )){

			$args['options'] = \serialize(array());
		}

		return self::db()->add( \claydb::$tables['comment_threads']." (otype, appid, itemtype, itemid, name, descr, options) VALUES (?,?,?,?,?,?,?)",
							array( $args['otype'],$args['appid'],$args['itemtype'],$args['itemid'],$args['name'],$args['descr'],$args['options'] )
						);
	}

	/**
	 * New Comment
	 * @param array $args
	 */
	public static function comment( $args )
	{

		$t = self::db()->get( 'tcount FROM '.\claydb::$tables['comment_threads'].' WHERE tid = ?', array($args['tid'] ),'0,1' );
		$t['tcount']++;
		return self::db()->update( \claydb::$tables['comment_threads']." SET tdate = ?, tcount = ? WHERE tid = ?",
				array( $args['date'], $t['tcount'], $args['tid'] ),1
		);
	}

	/**
	 * Update a Thread
	 * @param array $args
	 */
	public static function update( $args )
	{

		return self::db()->update( \claydb::$tables['comment_threads']." SET tdate = ?, title = ? WHERE tid = ?",
								array( $args['updated'],$args['title'],$args['tid'] ),1
						);
	}

	/**
	 * Change the Status of a Thread
	 * @param array $args
	 */
	public static function status( $args ){

		return self::db()->update( \claydb::$tables['comment_threads']." SET status = ?, tdate = ? WHERE tid = ?",
				array( $args['status'],$args['updated'],$args['tid'] ), 1
		);
	}

	/**
	 * Delete a Thread
	 * @param array $args
	 */
	public static function delete( $args )
	{

		return self::db()->delete( \claydb::$tables['comment_threads']." WHERE tid = ?", array( $args['tid'] ), 1
						);
	}

	/**
	 * Get a Thread
	 * @param array $args
	 * @throws \Exception
	 */
	public static function get( $args=array() )
	{
		# Getting a specific Thread ID
		if ( empty( $args['appid'] ) AND empty( $args['app'] )){

			if ( empty( $args['tid'] )){

				Throw new \Exception( self::$INVALID_ID );
			}

			return self::db()->get( 'tid, status, appid, itemtype, itemid, uid, tpub, tdate, points, tcount, title, options FROM '.\claydb::$tables['comment_threads'].' WHERE tid = ?', array( $args['tid'] ),'0,1');
		}

		if ( !empty( $args['app'] ) AND empty( $args['appid'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}

		if ( is_null( $args['itemtype'])){

			$where = 'appid = ? AND itemtype IS NULL AND itemid = ?';
			$bind = array( $args['appid'], $args['itemid'] );
		} else {

			$where = 'appid = ? AND itemtype = ? AND itemid = ?';
			$bind = array( $args['appid'], $args['itemtype'], $args['itemid'] );
		}
		# Getting a Thread attached to an app
		return self::db()->get( 'doid, otype, appid, itemtype, itemid, name, descr, options FROM '.\claydb::$tables['do_objects']." WHERE $where",
								$bind, '0,1' );

	}

	/**
	 * Get Thread Info
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getInfo( $args=array() )
	{
		if (empty( $args['tid'] )){

			Throw new \Exception( self::$INVALID_ID );
		}

		return self::db()->get( 'tid, status, uid, tpub, tdate, points, tcount, title FROM '.\claydb::$tables['comment_threads'].' WHERE tid = ?',array( $args['tid'] ), '0,1' );
	}

	/**
	 * Get Thread's Author's User ID
	 * @param array $args
	 * @throws \Exception
	 */
	public static function getAuthor( $args=array() )
	{

		if (empty( $args['tid'] )){

			Throw new \Exception( self::$INVALID_ID );
		}

		return self::db()->get( 'uid FROM '.\claydb::$tables['comment_threads'].' WHERE tid = ?', array($args['tid'] ),'0,1' );
	}
}
