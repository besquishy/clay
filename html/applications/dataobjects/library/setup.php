<?php
/**
* DataObjects Application
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\library;

/**
 * DataObjects Setup API
 * @author David
 *
 */
class setup
{
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	/**
	 * Install
	 * @return boolean
	 */
	public static function install()
	{
		# Use the DB trait
		$dbconn = self::db();
		# Datadict is Used for Table Manipulation
	    $datadict = $dbconn->datadict();
		# DataObjects tables
		# Table Alias => Table Name
	    $tables = array( 'do_types' => array( 'table' => 'do_types' ),
						 'do_properties' => array( 'table' => 'do_properties' ), 
						 'do_objects' => array( 'table' => 'do_objects' ),
						 'do_object_properties' => array( 'table' => 'do_object_properties' ),
						 'do_data' => array( 'table' => 'do_data' ));
	    # Add DB Table names to data/config/sites/[site]/database.tables.php configuration file
		$datadict->registerTables( $tables );
		/*
	     * Object Types are used to define a schema for a Data Object Properties
	     */
	    # Object Types Table
	    $objectTypes = array();
	    $objectTypes['column'] = array( # Property Type ID
	    		'otype' => array('type' => 'ID'),
	    		# Property Type Name (unique)
	    		'name' => array('type' => 'VARCHAR', 'size' => '50', 'null' => FALSE),
	    		# Property Type Description
	    		'descr' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE),
	    		# Property Type Options
	    		'options' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE)
	    );
	    # Add indexes
	    $objectTypes['index'] = array('name' => array('type' => 'UNIQUE', 'index' => 'name'));
	    # Create the table using the corresponding 'key' from the $tables array above and $objects
		$datadict->createTable(\claydb::$tables['do_types'],$objectTypes);
		/*
	     * Properties provide functionality, in pieces (a form) or as a whole (component)
	     */
	    # Properties Table
	    $properties = array();
	    $properties['column'] = array(# Property ID (primary key)
	    							'propid' => array('type' => 'ID'),
	    							# Property Type
	    							'otype' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    							# Application ID (provider)
	    							'appid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => TRUE),
	    							# Property Name
	    							'name' => array('type' => 'VARCHAR', 'size' => '50', 'null' => FALSE),
	    							# Property Description
	    							'descr' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE),
	    							# Property Options
	    							'options' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE));
	    # Add indexes
	    $properties['index'] = array( 	'otype' => array('type' => 'KEY', 'index' => 'otype'),
	    								'application' => array('type' => 'KEY', 'index' => 'appid'),
	    								'property' => array('type' => 'UNIQUE', 'index' => 'otype,appid,name'));
	    # Create the table
		$datadict->createTable(\claydb::$tables['do_properties'],$properties);
		/*
	     * Objects are the containers that provide the overall structure to the data
	     */
	    # Objects Table
	    $objects = array();
	    $objects['column'] = array( # Object ID (primary key)
	    							'doid' => array('type' => 'ID'),
	    							# Object Type ID
	    							'otype' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    							# Application ID
	    							'appid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    							# Application Type ID
	    							'itemtype' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => TRUE),
	    							# Application Item ID
	    							'itemid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => TRUE),
	    							# Object Name (unique)
	    							'name' => array('type' => 'VARCHAR', 'size' => '50', 'null' => FALSE),
	    							# Object Description
	    							'descr' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE),
	    							# Object Options
	    							'options' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE)
		    						);
	    # Add indexes
	    $objects['index'] = array('otype' => array('type' => 'KEY', 'index' => 'otype'),
	    						  'application' => array('type' => 'KEY', 'index' => 'appid,itemtype,itemid'),
	    						  'object' => array('type' => 'UNIQUE', 'index' => 'appid,itemtype,itemid,name'));
	    # Create the table using the corresponding 'key' from the $tables array above and $objects
		$datadict->createTable(\claydb::$tables['do_objects'],$objects);
		/*
	     * Object Properties are the property assignments to individual Objects
	     */
	    # Object Properties Table
	    $objectProperties = array();
	    $objectProperties['column'] = array(# Object Property ID (primary key)
	    							'opid' => array('type' => 'ID'),
	    							# Object ID
									'doid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
									'pos' => array('type' => 'TINYINT', 'size' => '3', 'unsigned' => TRUE, 'null' => TRUE),
	    							# Property ID
	    							'propid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    							# Parent Property ID
	    							'popid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => TRUE),
	    							# Object Property Name
	    							'name' => array('type' => 'VARCHAR', 'size' => '50', 'null' => FALSE),
	    							# Object Property Display Name
	    							'field' => array('type' => 'VARCHAR', 'size' => '50', 'null' => TRUE),
	    							# Property Description
	    							'descr' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE),
	    							# Property Options
	    							'options' => array('type' => 'VARCHAR', 'size' => '255', 'null' => TRUE));
	    # Add indexes
	    $objectProperties['index'] = array( 'name' => array('type' => 'UNIQUE', 'index' => 'doid, name'),
	    									'propid' => array('type' => 'KEY', 'index' => 'propid'),
	    									'parentid' => array('type' => 'KEY', 'index' => 'popid'));
	    # Create the table
		$datadict->createTable(\claydb::$tables['do_object_properties'],$objectProperties);
		/*
	     * 	Object Data - a generic storage/data source for applications that use Data Objects
	     */
	    # Object Data Table
	    $objectData = array();
	    $objectData['column'] = array(# Object Data ID (primary key)
	    							'did' => array('type' => 'ID'),
	    							# Object ID
	    							'doid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    							# Property ID
	    							'opid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    							# Item ID
	    							'itemid' => array('type' => 'INT', 'size' => '10', 'unsigned' => TRUE, 'null' => FALSE),
	    							# Property Options
	    							'data' => array('type' => 'TEXT', 'null' => TRUE));
	    # Add indexes
	    $objectData['index'] = array('field' => array('type' => 'UNIQUE', 'index' => 'doid, opid, itemid'),
									 'opid' => array('type' => 'KEY', 'index' => 'opid'),
									 'data' => array('type' => 'FULLTEXT', 'index' => 'data'));
	    # Create the table
	    $datadict->createTable(\claydb::$tables['do_data'],$objectData);
	    # DataObjects Dashboard Plugin
		$dash = array('plugin', array('type' => 'dashboard', 'app' => 'dataobjects', 'name' => 'dataobjects', 'descr' => 'DataObjects Administrative Dashboard'));
		\Clay\Module::Object('Plugins')->Setup($dash);
		# Hook DataObjects Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'dataobjects', 'name' => 'dataobjects'),
					  'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		# DataObjects Plugin Type - DataObjects
		$pluginType = array('type', array('name' => 'dataobjects', 'descr' => 'Attach dynamic objects to applications'));
		\Clay\Module::Object('Plugins')->Setup($pluginType);
		# DataObjects Plugin - Container
		$plugin = array('plugin', array('type' => 'dataobjects', 'app' => 'dataobjects', 'name' => 'container', 'descr' => 'DataObjects Container'));
		\Clay\Module::Object('Plugins')->Setup($plugin);
		# Privileges
		# %priv_mask1% Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'dataobjects', 'Container', 'Create' );
		\Clay\Module::Object( 'Privileges' )->register( 'dataobjects', 'Container', 'Update' );
		\Clay\Module::Object( 'Privileges' )->register( 'dataobjects', 'Container', 'Delete' );
		\Clay\Module::Object( 'Privileges' )->register( 'dataobjects', 'Container', 'View' );

		return true;
	}
	/**
	 * Upgrade
	 * @param integer $version
	 * @return boolean
	 */
	public static function upgrade($version)
	{

		return true;
	}
	/**
	 * Uninstall
	 * @return boolean
	 */
	public static function delete()
	{
		# ClayDB Object
		$dbconn = self::db();
	    $datadict = $dbconn->datadict();
		# Drop Tables
	    $datadict->dropTable( \claydb::$tables['do_types'] );
		$datadict->dropTable( \claydb::$tables['do_properties'] );
		$datadict->dropTable( \claydb::$tables['do_objects'] );
		$datadict->dropTable( \claydb::$tables['do_objects_properties'] );
		$datadict->dropTable( \claydb::$tables['do_data'] );
		# Drop Privileges
	    \Clay\Module::Object( 'Privileges' )->remove( 'dataobjects' );
		# Drop Plugins
		\Clay\Module::Object('Plugins')->remove( 'dataobjects' );
		# Drop Settings
		\Clay\Application::Unset( 'dataobjects' );
	    return true;
	}
}
