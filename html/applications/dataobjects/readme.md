# Clay Starter App

This is an application for the Clay CMS you use as a boilerplate to build your own app.

## Search, Replace, Customize, Run

Search and replace the following with what you want to use in your app:

dataobjects - Lowercase name of your app (unique)

DataObjects - Uppercase name of your app

2018 - Copyright Year(s)

David L Dyess II

https://daviddyess.com

16 May 18

0.1

Data Objects

%app_class%

%app_category%

%is_core%

%can_install%

David - Author's Name

GPL - License

%db_table1% - Database table your app will use

%db_table2% - Secondary database table your app will use

%plugin_type% - Plugin Type your app will register (unique)

%plugin_type_descr% - Description of the Plugin Type your app will register (unique)

%plugin1_type% - Type of the Plugin your app will register (content, navigation, meta, etc.)

%plugin1_name% - Name of the Plugin your app will register (unique)

%plugin1_descr% - Description of the Plugin your app will register

%dashboard_name% - Name of the Dashboard menu your app will register (unique)

%dashboard_title% - Title of the Dashboard menu

%dashboard_descr%

%Priv_mask1% - First Privilege mask

%Priv_mask2% - Second Privilege mask