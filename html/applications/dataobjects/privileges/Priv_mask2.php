<?php
/**
* DataObjects Privilege
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\privilege;

\Library('Clay/Application/Privilege');

/**
 * DataObjects Privileges
 * @author David
 * @copyright 2018
 * @todo Add Status field in blog_posts table for use with 'PUBLIC', 'PRIVATE', 'DRAFT', etc
 */
class %Priv_mask2% extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var unknown_type
	 */
	public $Request;
	
	/**
	 * Multimask Privilege Validation
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. COMMENTID::1 = array('COMMENTID',1)

		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
			
			case 'AUTHOR':
				
				# Fetch some info about the requested %Priv_mask2%
				$post = \Clay\Application::API('dataobjects','post','getAuthor',array('cid' => $this->Request));

				switch($scope[1]){
					
					case 'MYSELF':
						
						# Pseudo User - If assigned and User created the Post, returns TRUE
						if ($post['uid'] == \Clay\Module::Object('User')->id()){
							
							return TRUE;
						}
						
						return FALSE;
						
					case $post['uid']:
						
						return TRUE;
				}
				
				return FALSE;
				
			case 'COMMENTID':
				
				if ($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
				
			case 'PUBLIC':
				
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Administration tool
	 */
	public function Scopes($privilege, $scope = NULL){
		
		if (empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'AUTHOR', 'COMMENTID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
			
			case 'AUTHOR':
				
				$users = \Clay\Module::API('Users','getALL');
				
				# Pseudo User - allows privileges for the User that created a Post
				$users[] = array('uid' => 'MYSELF', 'name' => 'MYSELF');
				
				# do a foreach loop here to make it match POSTID below
				
				foreach($users as $user){
					
					$item['id'] = $user['uid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					$item['title'] = NULL;
					$item['name'] = !empty($user['uname']) ? $user['uname'] : $user['name'];
					
					$option['items'][] = $item;
				}
				unset($users);
				$option['type'] = 'Users';
				return $option;
				
			case 'COMMENTID':
				
				$posts = \Clay\Application::API('dataobjects','posts','getAll');
				
				foreach($posts as $post){
					
					$item['id'] = $post['cid'];
					$item['date'] = $post['cpub'];
					$item['uid'] = $post['uid'];
					$item['title'] = !empty($post['title']) ? $post['title'] : 'Untitled';
					$item['name'] = NULL;
					
					$option['items'][] = $item;
				}
				
				unset($posts);
				$option['type'] = 'DataObjects';
				return $option;
		}
	}
}