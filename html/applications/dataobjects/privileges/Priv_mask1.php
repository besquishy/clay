<?php
/**
* DataObjects Privileges
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\privilege;

\Library('Clay/Application/Privilege');

/**
 * %Priv_mask1% Privileges
 * @author David
 * @copyright 2018
 * @todo Add Status field in blog_posts table for use with 'PUBLIC', 'PRIVATE', 'DRAFT', etc
 */
class %Priv_mask1% extends \Clay\Application\Privilege
{
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var unknown_type
	 */
	public $Request;
	
	/**
	 * %Priv_mask1% Multimask Privilege
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope)
	{
		
		# Extract the requested scope into array - e.g. THREADID::1 = array('THREADID',1)

		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
			
			case 'AUTHOR':
				
				# Fetch some info about the requested %Priv_mask1%
				$thread = \Clay\Application::API('dataobjects','thread','getAuthor',array('tid' => $this->Request));

				switch($scope[1]){
					
					case 'MYSELF':
						
						# Pseudo User - If assigned and User created the Post, returns TRUE
						if ($thread['uid'] == \Clay\Module::Object('User')->id()){
							
							return TRUE;
						}
						
						return FALSE;
						
					case $thread['uid']:
						
						return TRUE;
				}
				
				return FALSE;
				
			case 'THREADID':
				
				if ($scope[1] == $this->Request){
					
					return TRUE;
				}
				
				return FALSE;
				
			case 'PUBLIC':
				
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Experimental Administration tool
	 */
	public function Scopes($privilege, $scope = NULL)
	{
		
		if (empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL', 'AUTHOR', 'THREADID');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
			
			case 'AUTHOR':
				
				$users = \Clay\Module::API('Users','getALL');
				
				# Pseudo User - allows privileges for the User that created a %Priv_mask1%
				$users[] = array('uid' => 'MYSELF', 'name' => 'MYSELF');
				
				# do a foreach loop here to make it match THREADID below
				
				foreach($users as $user){
					
					$item['id'] = $user['uid'];
					$item['date'] = NULL;
					$item['uid'] = NULL;
					$item['title'] = NULL;
					$item['name'] = !empty($user['uname']) ? $user['uname'] : $user['name'];
					
					$option['items'][] = $item;
				}
				unset($users);
				$option['type'] = 'Users';
				return $option;
				
			case 'THREADID':
				
				$threads = \Clay\Application::API('dataobjects','threads','getAll');
				
				foreach($threads as $thread){
					
					$item['id'] = $thread['cid'];
					$item['date'] = $thread['cpub'];
					$item['uid'] = $thread['uid'];
					$item['title'] = !empty($thread['title']) ? $thread['title'] : 'Untitled';
					$item['name'] = NULL;
					
					$option['items'][] = $item;
				}
				
				unset($threads);
				$option['type'] = 'Threads';
				return $option;
		}
	}
}