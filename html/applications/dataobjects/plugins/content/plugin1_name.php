<?php
/**
 * DataObjects Plugin
 *
 * Plugins application DataObjects content plugin
 *
 * @copyright (C) 2018 David L Dyess II
 * @license GPL
 * @link https://daviddyess.com
 * @author David
 */

namespace application\dataobjects\plugin\content;

/**
 * DataObjects content plugin
 */
class %plugin1_name% extends \Clay\Application\Plugin
{
	
	public $PluginApp = 'dataobjects';
	public $PluginType = '%plugin1_type%';
	public $Plugin = '%plugin1_name%';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function view()
	{
		$data['msg'] = "This is the example plugin for view!";
		return $data;
	}
	/**
	 * Display - to be displayed on an item page
	 * @param unknown $args
	 * @return string
	 */
	public function display()
	{
		$data['msg'] = "This is the example plugin for display!";
		return $data;
	}
	
	public function addItem()
	{
		
		
	}
	
	public function create($args)
	{
		
		
	}
	
	public function editItem($args)
	{
		
		
	}
	
	public function update($args)
	{
		
		
	}
	
	public function delete($args)
	{
		
		
	}
}