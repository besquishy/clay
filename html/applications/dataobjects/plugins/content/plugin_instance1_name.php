<?php
/**
* DataObjects Application
*
* %plugin_instance1_name% Plugin Instance
*
* @copyright (C) 2015 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\plugin\content;

/**
 * %plugin_instance1_name% Plugin Instance
 */
class %plugin_instance1_name%
{
	
	public static function add()
	{
		
		$data = array();
		return $data;
	}
	
	public static function edit($args)
	{
		
		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}
	
	public static function display($args)
	{
		
		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}
}