<?php
/**
* DataObjects Dashboard Plugin
*
* @copyright (C) 2018 David L Dyess II
* @license GPL
* @link https://daviddyess.com
* @author David
*/

namespace application\dataobjects\plugin\dashboard;

/**
 * DataObjects Dashboard Plugin
 */
class dataobjects extends \Clay\Application\Plugin
{

	public $PluginApp = 'dataobjects';
	public $PluginType = 'dashboard';
	public $Plugin = 'dataobjects';

	/**
	 * DataObjects Dashboard
	 */
	public static function view()
	{
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if ($user->privilege('system','Admin','User')) {
			
			$data['admin'] = \Clay\Application::url('dataobjects','admin','view');
		}

		$data['view'] = \Clay\Application::url('dataobjects');
		
		return $data;
	}

}