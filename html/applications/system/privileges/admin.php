<?php
namespace application\system\privilege;

\Library('Clay/Application/Privilege');

/**
 * System (Administrative) Privileges
 * @author David L. Dyess II (david.dyess@gmail.com)
 * @copyright 2012-2013
 */
class Admin extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var unknown_type
	 */
	public $Request;
	
	/**
	 * Admin Privilege Mask
	 * @param array $scope
	 * @return boolean
	 */
	public function User($scope){
		
		return $this->Scope($scope);
	}
	
	/**
	 * Used to Validate Privilege Requests
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# At this point the only allowable 'scope' is 'ALL'
		switch($scope){
			
			case array('ALL'):
				# Allowed
				return TRUE;
		}
		
		# Role Privilege not within scope
		return FALSE;
	}
	
	/**
	 * Scope Generation
	 */
	public function Scopes($privilege, $scope = NULL){
		
		return array('ALL');
	}
}