<?php
namespace application\system\library;

/**
* ClayCMS
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/
class setup {

	public static function install(){
		
		\Library('Clay/Application');
		\clay\application::set('system','application','system');
		\clay\application::set('system','component','main');
		\clay\application::set('system','action','view');
		\clay\application::set('system','site.name','Clay Site Name');
		\clay\application::set('system','site.slogan','Dynamically Delicious');
		\clay\application::set('system','site.footer','');
		\clay\application::set('system','site.copyright','Copyright  &#169; Me 2017');
		\clay\application::set('system','theme','vision2');
		\clay\application::set('system','theme.page','default');
		\clay\application::set('system','version','1.3.0');
		\clay\application::set('system','shorturls','0');
		\clay\application::set('system','https','0');
		
		# Add Administration Privileges (ALL)

		$privs = \Clay\Module::Object('Privileges');
		
		$privs->Register('system','Admin','User');
		
		# Add admin privilege (above) to Admin Role.
		
		$adminPriv = $privs->Get('system','Admin','User');
		
		$roles = \Clay\Module::Object('Roles');
		
		$adminRoleID = $roles->getID('admin');
		
		$privs->addRole($adminRoleID, $adminPriv['pid'], 'ALL');

		# System Dashboard Plugin
		$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'system', 'name' => 'system', 'descr' => 'System Administration.'));
		\Clay\Module::Object('Plugins')->Setup($dashboard);

		# Hook System Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'system', 'name' => 'system'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		
		return true;
	}
	
	public static function upgrade($version){
		
		switch($version){			
			case ($version == '0.1'):
				\clay\application::set('system','shorturls','0');
			case ($version < '0.3'):
				\clay\application::set('system','https','0');
			case ($version < '0.5'):
				\clay\application::set('system','https.header.301','0');
			case ($version < '1.0'):
				\clay\application::set('system','version','1.0.0');
			case ($version <= '1.0'):
				# System Dashboard Plugin
				$dashboard = array('plugin', array('type' => 'dashboard', 'app' => 'system', 'name' => 'system', 'descr' => 'System Administration.'));
				\Clay\Module::Object('Plugins')->Setup($dashboard);
				\clay\application::set('system','version','1.0.1');
			case ($version <= '1.0.1'):
				\clay\application::set('system','version','1.0.2');
			case ($version <= '1.0.2'):
				# Hook System Dashboard Plugin to the Dashboard
				$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'system', 'name' => 'system'),
				'app' => 'dashboard');
				\Clay\Module::Object('Plugins')->Hook ( $hook );
				\Clay\Application::Set('system','version','1.0.3');
			case ($version <= '1.0.3'):
				\Clay\Application::Set('system','version','1.0.4');
			case ($version <= '1.0.4'):
				\Library('ClayDB');
				$dbconn = \claydb::connect();
				$datadict = $dbconn->datadict();
				# Uninstall Blocks Application
				\Clay\Module::Object('Privileges')->remove('blocks');
				\Clay\Module::Object('Plugins')->remove('blocks');
				\Clay\Module::API('Setup','Type','Application')->Remove('blocks');
				# Uninstall Services Module
				\Clay\Application::deleteSetting('apps','service.dashboard');
				\Clay\Application::deleteSetting('apps','service.blocks');
				\Clay\Application::deleteSetting('apps','service.menu');
				\Clay\Application::deleteSetting('apps','service.filters');
				\Clay\Module::API('Setup','Type','Module')->Remove('service');
				# Drop Tables
				$datadict->dropTable(\claydb::$tables['block_groups']);
				$datadict->dropTable(\claydb::$tables['block_group_members']);
				$datadict->dropTable(\claydb::$tables['block_instances']);
				$datadict->dropTable(\claydb::$tables['services']);
				$datadict->dropTable(\claydb::$tables['service_types']);
				# Upgrade to 1.0.5 for Testing
				\Clay\Application::Set('system','version','1.0.66');
			case ($version <= '1.0.66'):
				\Clay\Application::Set('system','version','1.0.7');
			case ($version <= '1.0.7'):
				\Clay\Application::Set('system','version','1.1.0');
			case ($version <= '1.1.0'):
				$t = \Clay\Application::Setting('system','theme');
				if( $t == 'vision' ){
					\Clay\Application::Set('system','theme','vision2');
				}
				\Clay\Application::Set('system','version','1.3.0');
				break;
		}
		return TRUE;
	}
	
	public static function delete($version){

	}
}