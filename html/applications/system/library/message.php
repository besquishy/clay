<?php
namespace application\system\library;
/**
* ClayCMS
*
* @copyright (C) 2007-2012 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/
class message {
	public static $count = 0;
	public static $time;
	
	public static function get(){
		if(!empty($_SESSION['message'])){
			$data = $_SESSION['message'];			
				//unset($_SESSION['message']);
				$_SESSION['message'][] = 'accessed '.self::$count.' times. Reset at '.time();
		}
		self::$count++;
		return !empty($data) ? $data : NULL;
	}

	public static function send($message){
		if(is_array($message)) goto multi;
		
		$_SESSION['message'][] = $message;
		
		goto end;
		
		multi:
		
		foreach($message as $messages){
			$_SESSION['message'][] = $messages;
		}
		
		end:
		return true;
	}
	
}