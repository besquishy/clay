<?php
namespace application\system\component;
/**
* Clay System Application
*
* @copyright (C) 2007-2017 David L Dyess II
* @license GPL {@link http://www.gnu.org/licenses/gpl.html}
* @link http://clay-project.com
* @author David L Dyess II (david.dyess@gmail.com)
*/

/**
 * System Administration Component
 * @author David
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * View Action
	 * @throws \Exception
	 */
	public function view(){
		
		$this->pageTitle = "System Administration";
		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){
			
			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}
	}

	/**
	 * System Settings
	 * @throws \Exception
	 */
	public function settings(){
		
		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){
			
			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}
		
		$this->pageTitle = 'System Settings';
		$data = array();
		# Application Setting
		$data['app'] = \clay\application::setting('system','application');
		$data['apps'] = \clay\application::api('apps','get','all');
		# Application Component
		$data['appComponent'] = \clay\application::setting('system','component');
		# Application Action
		$data['appAction'] = \clay\application::setting('system','action');
		# Site Name and Slogan
		$data['siteName'] = \clay\application::setting('system','site.name');
		$data['siteSlogan'] = \clay\application::setting('system','site.slogan');
		# Theme Setting
		$themes = file_exists(\clay\THEMES_PATH) ? scandir(\clay\THEMES_PATH) : array();
		$themenames = array();
		# Move this to an API
		foreach($themes as $theme){
			# Hide these
			if(($theme == '.') || ($theme == '..')){
			
				unset($theme);
				continue;
			}
			# Theme info.php files are not currently used
			#@todo: add info.php requirement for themes?			 
			#$themedata = \clay\application::info('theme',$theme);
			#if(!empty($themedata)){
			#	$themenames[] = $themedata['name'];
			#}
			$themenames[] = $theme;
		}
		# Current Theme
		$data['theme'] = \clay\application::setting('system','theme');
		# Available Themes
		$data['themes'] = $themenames;
		# Theme Page Template
		$data['pageTemplate'] = \clay\application::setting('system','theme.page');
		# Site Page Footer
		$data['siteFooter'] = \Clay\Application::Setting('system','site.footer','');
		# Site Copyright
		$data['siteCopyright'] = \Clay\Application::Setting('system','site.copyright','');
		# HTTPS
		$data['https'] = \Clay\Application::Setting('system','https','0');
		# HTTPS 301 Header
		$data['https301'] = \Clay\Application::Setting('system','https.header.301','0');
		return $data;
	}
	
	/**
	 * System Help
	 */
	public function help(){

	}

	/**
	 * PHP.ini Settings
	 * @throws \Exception
	 */
	public function ini(){
		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){
			
			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}
		$data = array();
		$directives = unserialize(\clay\application::setting('system','php.ini'));
		\Library('ClayDO/DataObject/Form');
		$form = new \ClayDO\DataObject\Form;
		$form->attributes(array('method' => 'post', 'action' => \clay\application::url('system','admin','save'),'class' => 'login-form','id' => 'system-admin-ini'));
		$form->container('set1','fieldset')->legend = 'New Directives';
		$form->container('set1')->intro = 'Add new php.ini settings';
		$form->container('set1')->property('name','text')->attributes(array('id' => 'nname', 'name' => 'newn[]','title' => 'Enter the setting name.'));
		$form->container('set1')->property('name')->label = "Name";
		$form->container('set1')->property('value','text')->attributes(array('id' => 'nvalue', 'name' => 'newv[]','title' => 'Enter the setting value.'));
		$form->container('set1')->property('value')->label = "Value";
		if(!is_array($directives)) goto end;
		$data['settings'] = !empty($directives) ? $directives : array();
		end:
		$form->container('footer','fieldset')->property('submit','input')->attributes(array('type' => 'submit', 'id' => 'submit', 'name' => 'submit', 'value' => 'Save'));
		$data['form'] = $form;
		return $data;
	}

	/**
	 * Save Changed Settings
	 * @throws \Exception
	 */
	public function save(){
		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){
			
			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}
		
		if(!empty($_POST['ini']))
			goto ini;
		if(!empty($_POST['settings']))
			goto settings;

		ini:
			# serialize php.ini settings and save to system, php.ini


		settings: #@todo: serialize into one setting?
			# save each setting in individual system, settings
			# Site Name
			if($_POST['siteName'] !== $_POST['oldName']){
			
				\clay\application::set('system','site.name',\clay\data\post('siteName','string','text'));
			}
			# Site Slogan
			if($_POST['siteSlogan'] !== $_POST['oldSlogan']){

				\clay\application::set('system','site.slogan',\clay\data\post('siteSlogan','string','text'));
			}
			# Site Footer
			if($_POST['siteFooter'] !== $_POST['oldFooter']){

				\clay\application::set('system','site.footer',\clay\data\post('siteFooter','string','text'));
			}
			# Site Copyright
			if($_POST['siteCopyright'] !== $_POST['oldCopyright']){

				\clay\application::set('system','site.copyright',\clay\data\post('siteCopyright','string','text'));
			}
			# Application
			if($_POST['app'] !== $_POST['oldApp']){
			
				\clay\application::set('system','application',\clay\data\post('app','string','text'));
			}
			# Application Component
			if($_POST['appComponent'] !== $_POST['oldComponent']){
			
				\clay\application::set('system','component',\clay\data\post('appComponent','string','text'));
			}
			# Application Action
			if($_POST['appAction'] !== $_POST['oldAction']){
			
				\clay\application::set('system','action',\clay\data\post('appAction','string','text'));
			}
			# Theme
			if($_POST['theme'] !== $_POST['oldTheme']){
				
				\clay\application::set('system','theme',\clay\data\post('theme','string','text'));
			}
			# Theme Page Template
			if($_POST['pageTemplate'] !== $_POST['oldPage']){
			
				\clay\application::set('system','theme.page',\clay\data\post('pageTemplate','string','text'));
			
			}
			# HTTPS
			if($_POST['https'] !== $_POST['oldHttps']){
			
				\clay\application::set('system','https',\clay\data\post('https','string','text'));
			}
			# HTTPS 301 Header
			if((!empty($_POST['https301'])) && ($_POST['https301'] !== $_POST['oldHttps301'])){
			
				\clay\application::set('system','https.header.301',\clay\data\post('https301','string','text'));
			}
			
		\clay\application::api('system','message','send','System Settings Updated');
			
		if(empty($_POST['dashForm'])){
			
			\clay::redirect($_SERVER['HTTP_REFERER']);
		}
	}

	/**
	 * Server Information - phpinfo()
	 * @throws \Exception
	 */
	public function system(){
		
		$this->pageTitle = "Server Information";
		
		if(!\Clay\Module::API('User','Instance')->Privilege('system','Admin','User')){
			
			throw new \Exception('Your account does not have sufficient privileges assigned to access this component');
		}
		
		ob_start () ;
		phpinfo () ;
		$pinfo = ob_get_contents () ;
		ob_end_clean () ;
		$data = array();
		// the name attribute "module_Zend Optimizer" of an ancher tag is not xhtml valid, so replace it with "module_Zend_Optimizer"
		$data['pinfo'] = str_replace ( "module_Zend Optimizer", "module_Zend_Optimizer", preg_replace ( '%^.*<body>(.*)</body>.*$%ms', '$1', $pinfo ) );
		return $data;
	}

	public function config(){

	}

}