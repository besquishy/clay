<div class="c-card">
	<div class="c-card__header">
		<div class="c-card__title c-card__title--bar">
			<i class="fa fa-globe"></i> System
		</div>
	</div>
	<div class="c-card__content">
		<?php if(!empty($settings))?><a href="<?= $settings; ?>" class="dashlink">Site Settings</a>
		<br />
		<?php if(!empty($server))?><a href="<?= $server; ?>" class="dashlink">Server Info</a>
		<!--<?php if(!empty($ini))?><a href="<?= $ini; ?>" class="dashlink">PHP.ini</a>-->
		<!--<?php if(!empty($help))?><a href="<?= $help; ?>" class="dashlink">Help</a>-->
		<h5>Clay <?= \Clay::version ?> (<a href="install.php" target="_blank">Installer</a>)</h5>
	</div>
</div>