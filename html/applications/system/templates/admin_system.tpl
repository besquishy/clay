<style type="text/css">
#phpinfo {}
#phpinfo pre {}
#phpinfo a:link {}
#phpinfo a:hover {}
#phpinfo table {width:100%;text-align:left;}
#phpinfo .center {}
#phpinfo .center table {}
#phpinfo .center th {}
#phpinfo td, th {}
#phpinfo h1 {}
#phpinfo h2 {border-top:2px solid #000000; border-bottom:3px solid #cccccc;}
#phpinfo .p {}
#phpinfo .e {border:1px solid #cccccc;}
#phpinfo .h {}
#phpinfo .v {border:1px solid #cccccc;}
#phpinfo .vr {}
#phpinfo img {}
#phpinfo hr {}
</style>
<section class="c-app">
  <div class="c-app__head"><h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
    Server Information</h1></div>
  <h2>Clay Version: <?= \Clay::version; ?></h2>
  <div class="c-app__content" id="phpinfo">
    <?= $pinfo; ?>
  </div>
</section>