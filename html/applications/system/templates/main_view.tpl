<section class="c-app">
	<div class="c-app__head"><h1>Clay</h1></div>
	<div class="c-app__content">
		This website is powered by Clay, a content management system built on top of the Clay Framework development platform.
	</div>
</section>