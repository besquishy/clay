<style type="text/css">
	div.inst-group {
		float:left;width:150px;padding:10px;text-align:center;
	}
	img.inst-icon {
		display: block;margin-left: auto;margin-right: auto;
	}
</style>
<section class="c-app">
	<div class="c-app__head">
	<h1>System Administration</h1>
	</div>
	<div class="c-app__content">
		<div class="inst-group">
			<img class="inst-icon" src="<?= \clay\application::image('application','system','blueprint.png'); ?>" />
			<p><a href="<?= \clay\application::url('system','admin','settings'); ?>">Site Settings</a></p>
		</div>
		<div class="inst-group">
			<img src="<?= \clay\application::image('application','system','info.png'); ?>" />
			<p><a href="<?= \clay\application::url('system','admin','system'); ?>">Server Information</a></p>
		</div>
		<div class="inst-group">
			<img src="<?= \clay\application::image('application','system','system.png'); ?>" />
			<p><a href="<?= \clay\application::url('system','admin','ini'); ?>">PHP.ini Settings</a></p>
		</div>
		<div class="inst-group">
			<img src="<?= \clay\application::image('application','system','help.png'); ?>" />
			<p><a href="<?= \clay\application::url('system','admin','help'); ?>">Help</a></p>
		</div>
	<br style="clear:both;" />
	</div>
</section>