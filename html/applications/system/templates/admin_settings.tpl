<section class="c-app">
	<div class="c-app__head"><h1><a href="<?= \clay\application::url('dashboard');?>" class="dashlink" title="Dashboard">Dashboard</a> / 
		System Settings</h1></div>
	<div class="c-app__content">
		<form v-on:submit.prevent="onSubmit('systemSettings', $event)" role="form" id="dashForm" class="system_settings" action="<?= \clay\application::url('system','admin','save');?>" method="post">
			<fieldset class="c-fieldset">
				<legend>Theme Settings</legend>
				<div class="c-fieldset__fields">
					<div class="u-1/4@sm">
					<label>Site Name</label>
					<input type="text" name="siteName" id="siteName" class="c-field" value="<?= $siteName?>" />
					<input type="hidden" name="oldName" id="oldName" value="<?= $siteName?>" />
					</div>
					<div class="u-1/4@sm">
					<label>Site Slogan</label>
					<input type="text" name="siteSlogan" id="siteSlogan" class="c-field" value="<?= $siteSlogan?>" />
					<input type="hidden" name="oldSlogan" id="oldSlogan" value="<?= $siteSlogan?>" />
					</div>
					<div class="u-1/4@sm">
					<label>Site Footer</label>
					<textarea name="siteFooter" id="siteFooter" class="c-field"><?= $siteFooter; ?></textarea>
					<input type="hidden" name="oldFooter" id="oldFooter" value="<?= $siteFooter; ?>" />
					</div>
					<div class="u-1/4@sm">	
					<label>Site Copyright</label>
					<input type="text" name="siteCopyright" id="siteCopyright" class="c-field" value="<?= $siteCopyright?>" />
					<input type="hidden" name="oldCopyright" id="oldCopyright" value="<?= $siteCopyright?>" />
					</div>
					<div class="u-1/4@sm">
					<label>Theme</label>
					<select name="theme" id="theme" class="c-field">
						<?php foreach($themes as $themename){ ?>
						<option value="<?= $themename ?>" <?php if($theme === $themename) echo 'selected="selected"'?>><?= $themename ?></option>
						<?php } ?>
					</select>
					<input type="hidden" name="oldTheme" id="oldTheme" value="<?= $theme?>" />
					</div>
					<div class="u-1/4@sm">
						<label>Theme Page Template</label>
						
						<input type="text" name="pageTemplate" id="pageTemplate" class="c-field" value="<?= $pageTemplate?>" />
						<span class="input-group-addon">.tpl</span>
						<input type="hidden" name="oldPage" id="oldPage" value="<?= $pageTemplate?>" />
						
					</div>
				</div>
			</fieldset>
			<fieldset class="c-fieldset">
				<legend>Default Application Settings</legend>
				<div class="c-fieldset__fields">
					<div class="u-1/4@sm">
					<label>Application</label>
					<select name="app" id="app" class="c-field">
						<?php foreach($apps as $appname){ ?>
						<option value="<?= $appname['name'] ?>" <?php if($app === $appname['name']) echo 'selected="selected"'?>><?= $appname['name'] ?></option>
						<?php } ?>
					</select>
					<input type="hidden" name="oldApp" id="oldApp" value="<?= $app?>" />
					</div>
					<div class="u-1/4@sm">
					<label>Application Component</label>
					<div class="input-group">
						<input type="text" name="appComponent" id="appComponent" class="c-field" value="<?= $appComponent?>" />
						<span class="input-group-addon">Class</span>
						<input type="hidden" name="oldComponent" id="oldComponent" value="<?= $appComponent?>" />
						</div>
					</div>
					<div class="u-1/4@sm">	
					<label>Application Action</label>
					<div class="input-group">
						<input type="text" name="appAction" id="appAction" class="c-field" value="<?= $appAction?>" />
						<span class="input-group-addon">Method</span>
						<input type="hidden" name="oldAction" id="oldAction" value="<?= $appAction?>" />
						</div>
					</div>
				</div>
			</fieldset>	
			<fieldset class="c-fieldset">
				<legend>Sessions</legend>
				<div class="c-fieldset__fields">
				</div>
			</fieldset>
			<fieldset class="c-fieldset">
				<legend>Global Settings</legend>
				<div class="c-fieldset__fields">
					<div class="c-alert u-bgcolor-danger">
						<strong>Warning:</strong> Ensure an HTTPS Connection Works Before Enabling "Force HTTPS"
					</div>
					<div class="u-1/12@sm">
						<label>Force HTTPS</label>
						<select name="https" id="https" class="c-field">
							<option value="0" <?php if($https === '0') echo 'selected="selected"'?>>No</option>
							<option value="1" <?php if($https === '1') echo 'selected="selected"'?>>Yes</option>
						</select>
						<input type="hidden" name="oldHttps" id="oldHttps" value="<?= $https?>" />
					</div>
					<div class="c-alert u-bgcolor-danger">
						<strong>Warning:</strong> The HTTP 301 Header - Moved Permanently Will "Permanently" Enable HTTPS
					</div>
					<div>
						<label class="u-1/6@sm">Enable HTTP 301 Header</label>
						<select name="https301" id="https301" class="c-field u-1/12@sm" <?php if($https === '0') echo 'disabled'?>>
							<option value="0" <?php if($https301 === '0') echo 'selected="selected"'?>>No</option>
							<option value="1" <?php if($https301 === '1') echo 'selected="selected"'?>>Yes</option>
						</select>
						<input type="hidden" name="oldHttps301" id="oldHttps301" value="<?= $https301?>" />
					</div>
				</div>
			</fieldset>
			<fieldset class="c-fieldset__controls">
				<div class="o-grid">
					<div class="o-grid__col u-1/3@sm">
						<button v-if="saved !== 'systemSettings'" type="submit" class="c-button" name="submit" id="submit"><i class="fa fa-check-circle fa-lg "></i>&emsp;Save Settings</button> 
						<button v-on:click="toEdit('systemSettings', $event)" v-if="saved == 'systemSettings'" type="button" class="c-button" name="edit" id="edit">Edit Settings</button>
					</div>
					<div class="o-grid__col u-1/3@sm">
						<div v-if="response == 'systemSettings'" id="systemSettings_submitResponse" class="c-alert u-pl@sm u-color-info u-weight-bold u-text-center"><i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i></div>
						<div v-if="error == 'systemSettings'" class="c-alert u-pl@sm u-color-error u-weight-bold u-text-center">An error occurred, please check the <a href="<?= \Clay\Application::URL( 'dashboard', 'main', 'errors' ); ?>">Error Log</a></div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</section>