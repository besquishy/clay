<?php
/**
 * Clay Framework
 *
 * @copyright (C) 2007-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Installer
 */
$data = array('title' => 'System',
			'name' => 'system',
			'version' => '1.3.0',
			'date' => 'January 4, 2018',
			'description' => 'Clay System Manager',
			'class' => 'System',
			'category' => 'Administration',
			'core' => TRUE,
			);
?>