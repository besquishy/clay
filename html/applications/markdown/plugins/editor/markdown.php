<?php
/**
 * Markdown Plugin
 *
 * Plugins application Markdown content plugin
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL
 * @link %link%
 * @author David L. Dyess II
 */

namespace application\markdown\plugin\editor;

/**
 * Markdown Editor Plugin
 */
class markdown extends \Clay\Application\Plugin {
	/**
	 * Plugin Application
	 * @var string
	 */
	public $PluginApp = 'markdown';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'editor';
	/**
	 * Plugin Name
	 * @var string
	 */
	public $Plugin = 'markdown';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function view(){
		$data['msg'] = "This is the example plugin for view!";
		return $data;
	}
	/**
	 * Display - to be displayed on an item page
	 * @param unknown $args
	 * @return string
	 */
	public function display(){
		$data['msg'] = "This is the example plugin for display!";
		return $data;
	}
	/**
	 * Editor Hook
	 * @param array $args
	 */
	public function editor($args) {
		# If $args are passed, we are likely editting an existing text
		if( !empty( $args )){
			# Check for existing text
			$md = \Clay\Application::API( 'markdown', 'text', 'get', $args );
		}
		if( !empty( $md )){
			$data['content'] = $md['body'];
		} else {
			# Set content to default for initial content creation
			$data['content'] = null;
		}
		return $data;
	}
	/**
	 * Create Hook - used when an item is created
	 * @param array $args
	 */
	public function create( $args ){

		return \Clay\Application::API( 'markdown', 'text', 'create', $args );	
	}
	/**
	 * Transform Hook - converts Markdown to HTML
	 * @param array $args
	 */
	public function transform( $args ){

		# Run the Composer Autoloader
		\Clay::Vendor();
		# Run the Parsedown markdown parser on the content
		$args['body'] = \Parsedown::instance()->text( $args['content'] ); 

		return array( 'content' => $args['body'] );
	}
	/**
	 * Update Hook - update existing text; creates text if doesn't exist
	 * @param array $args
	 */
	public function update($args){

		$md = \Clay\Application::API( 'markdown', 'text', 'get', $args );

		if( empty( $md )){

			return $this->create( $args );
		}
		
		return \Clay\Application::API( 'markdown', 'text', 'update', $args );
	}
	/**
	 * Delete Hook - delete hooked text
	 * @param array $args
	 */
	public function delete($args){
		
		return \Clay\Application::API('markdown','text','delete', $args);
	}
}