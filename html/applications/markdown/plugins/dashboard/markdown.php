<?php
/**
* Markdown Dashboard Service
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link %link%
* @author David L. Dyess II
*/

namespace application\markdown\plugin\dashboard;

/**
 * Markdown Dashboard Plugin
 */
class markdown extends \Clay\Application\Plugin {
	/**
	 * Plugin Application
	 * @var string
	 */
	public $PluginApp = 'markdown';
	/**
	 * Plugin Type
	 * @var string
	 */
	public $PluginType = 'dashboard';
	/**
	 * Plugin Name
	 * @var string
	 */
	public $Plugin = 'markdown';

	/**
	 * Markdown Dashboard
	 */
	public static function view(){
		
		$data = array();
		$user = \Clay\Module::API('User','Instance');
		
		if($user->privilege('markdown','Admin','Update')) {
			
			$data['admin'] = \clay\application::url('markdown','admin','view');
		}

		$data['view'] = \clay\application::url('markdown');
		
		return $data;
	}
}