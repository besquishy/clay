<?php
/**
* Markdown Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL
* @link %link%
* @author David L. Dyess II
*/

namespace application\markdown\library;

/**
 * Markdown Setup Library
 */
class setup {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	/**
	 * Install
	 * @return boolean
	 */
	public static function install(){
		# Use the DB trait
		$dbconn = self::db();
		# Datadict is Used for Table Manipulation
	    $datadict = $dbconn->datadict();
		# Markdown tables
		# Table Alias => Table Name
	    $tables = array( 'markdown' => array( 'table' => 'markdown' ));
	    # Add DB Table names to data/config/sites/[site]/database.tables.php configuration file
	    $datadict->registerTables( $tables );
	    # Prepare the Markdown table for creation
	    $cols = array();
	    $cols['column'] = array('mid' => array( 'type' => 'ID' ),
	    						'appid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
	    						'itemtype' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
	    						'itemid' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE, 'null' => FALSE ),
								'options' => array( 'type' => 'STRING' ),
	    						'body' => array( 'type' => 'TEXT' ));
	    # Markdown table indices
	    $cols['index'] = array( 'item' => array( 'type' => 'UNIQUE', 'index' => 'appid, itemtype, itemid' ));
	    # Create Threads Table
	    $datadict->createTable( \claydb::$tables['markdown'], $cols );
		# Editor Plugin Type
		$pluginType = array('type', array('name' => 'editor', 'descr' => 'Editors to be hooked to content applications.'));
		\Clay\Module::Object('Plugins')->Setup($pluginType);
	    # Markdown Dashboard Plugin
		$plugin = array('plugin', array('type' => 'dashboard', 'app' => 'markdown', 'name' => 'markdown', 'descr' => 'Manage Markdown Settings.'));
		\Clay\Module::Object('Plugins')->Setup($plugin);
		# Hook Markdown Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'markdown', 'name' => 'markdown'),
					  'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		# Markdown Plugin
		$plugin = array('plugin', array('type' => 'editor', 'app' => 'markdown', 'name' => 'markdown', 'descr' => 'Provides a comments system to application items.'));
		\Clay\Module::Object('Plugins')->Setup($plugin);
		# Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'markdown', 'Admin', 'Update' );

		return true;
	}
	/**
	 * Upgrade
	 * @param integer $version Installed version
	 * @return boolean
	 */
	public static function upgrade($version){

		return true;
	}
	/**
	 * Uninstall
	 * @return boolean
	 */
	public static function delete(){
		# ClayDB Object
		$dbconn = self::db();
	    $datadict = $dbconn->datadict();
		# Drop Tables
	    $datadict->dropTable( \claydb::$tables['markdown'] );
		# Drop Privileges
	    \Clay\Module::Object( 'Privileges' )->remove( 'markdown' );
		# Drop Plugins
		\Clay\Module::Object( 'Plugins' )->remove( 'markdown' );
		
		\Clay\Application::deleteSetting( 'markdown' );
	    return true;
	}
}
