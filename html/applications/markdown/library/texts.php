<?php
/**
* Markdown Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL
* @link %link%
* @author David L. Dyess II
*/

namespace application\markdown\library;

/**
 * Markdown Texts API
 * @author David L. Dyess II
 *
 */
class texts {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	
	/**
	 * Get All (or some) Markdown Texts
	 * @param array $args
	 * @return array
	 */
	public static function get($args=array()){
		
		# Select a range starting number using 'startnum'
		$offset = !empty($args['startnum']) ? $args['startnum'] : '0';
		# Fetch 'items' number of Comment Texts, offset by 'startnum' if applicable
		if(!empty($args['items'])){
			
			if(!empty($args['startnum'])){
				
				$limit = $args['startnum'].','.$args['items'];
				
			} else {
				
				$limit = '0,'.$args['items'];
			}
			
		} else {
			
			$limit = '';
		}
		# @TODO: In the future we should create an API (perhaps system level?) for status codes, for now leave it user select
		$status = !empty($args['status']) ? $args['status'] : 1;
		# Return associative array
		return self::db()->get('tid, status, appid, itemtype, itemid, uid, tpub, tdate, points, tcount, options FROM '.\claydb::$tables['comment_threads'].' WHERE status = ? ORDER BY tdate DESC',array($status),$limit);
	}
	
	/**
	 * Count Markdown Texts
	 * @param array $args - inert
	 * @return int
	 */
	public static function count($args){

		$count = self::db()->get('COUNT(*) as threads FROM '.\claydb::$tables['comment_threads']);
		return $count[0]['threads'];
	}

	/**
	 * Delete Markdown Texts
	 *
	 * @param array $args
	 * @return boolean
	 */
	public static function delete($args)
	{
		if( empty( $args['appid']) AND !empty( $args['app'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}
		$where = 'appid = ?';
		$params[] = $args['appid'];
		if( empty( $args['itemtype'] )){
			$args['itemtype'] = null;
			$where = $where.' AND itemtype IS NULL';
		} else {
			$where = $where.'AND itemtype = ?';
			$params[] = $args['itemtype'];
		}

		return self::db()->delete( \claydb::$tables['markdown']." WHERE $where", $params );
	}
}