<?php
/**
* Markdown Application
*
* @copyright (C) 2015 David L Dyess II
* @license GPL
* @link %link%
* @author David L. Dyess II
*/

namespace application\markdown\library;

/**
 * Markdown API
 * @author David L. Dyess II
 *
 */
class text 
{
	/**
	 * Invalid ID Error Message
	 * @var string
	 */
	private static $INVALID_ID = 'A numeric Markdown ID (mid) must be supplied';

	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;

	/**
	 * Create a Markdown
	 * @param array $args
	 */
	public static function create( $args )
	{

		/*if( empty( $args['app']) AND !empty( $args['appid'] )){

			$args['app'] = \Clay\Application::getName( $args['appid'] );
		}*/

		if( empty( $args['appid']) AND !empty( $args['app'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}

		if( empty( $args['itemtype'] )){

			$args['itemtype'] = null;
		}
		/*
		if( !empty( $args['hook'] )){

			$item = \Clay\Application\Core::Item( $args['app'], $args['itemtype'], $args['itemid'] );

			$args['itemid'] = $item['id'];
 			$args['title'] = $item['title'];
 			$args['user'] = $item['uid'];
 			$args['itemtype'] = $item['itemtype'];
 			$args['date'] = $item['date'];
		}*/

		if( empty( $args['options'] )){

			$args['options'] = null;
		}

		$args['body'] = $args['content'];

		$mid = self::db()->get( 'mid FROM '.\claydb::$tables['markdown'].' WHERE appid = ? AND itemtype = ? AND itemid = ?', array( $args['appid'],$args['itemtype'],$args['itemid'] ),'0,1' );

		if( empty( $mid )){

			return self::db()->add( \claydb::$tables['markdown']." (appid, itemtype, itemid, options, body) VALUES (?,?,?,?,?)",
								array( $args['appid'],$args['itemtype'],$args['itemid'],$args['options'],$args['content'] )
							);
		} else {

			return $mid;
		}
	}

	/**
	 * Update a Markdown
	 * @param array $args
	 */
	public static function update( $args )
	{

		if( empty( $args['appid']) AND !empty( $args['app'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}

		if( empty( $args['itemtype'] )){

			$args['itemtype'] = null;
		}

		if( empty( $args['options'] )){

			$args['options'] = null;
		}

		$args['body'] = $args['content'];

		# Getting a specific Markdown ID
		if( empty( $args['appid'] ) AND empty( $args['app'] )){

			if( empty( $args['mid'] )){

				Throw new \Exception( self::$INVALID_ID );
			}

			return self::db()->update( \claydb::$tables['markdown']." SET body = ? WHERE mid = ?",
								array( $args['body'], $args['mid'] ),1
						);
		}

		if (is_null($args['itemtype'])){
			return self::db()->update( \claydb::$tables['markdown']." SET body = ? WHERE appid = ? AND itemtype IS NULL AND itemid = ?", array( $args['body'], $args['appid'], $args['itemid'] ),1);
		}

		return self::db()->update( \claydb::$tables['markdown']." SET body = ? WHERE appid = ? AND itemtype = ? AND itemid = ?", array( $args['body'], $args['appid'], $args['itemtype'], $args['itemid'] ),1);
	}

	/**
	 * Delete Markdown Text
	 *
	 * @param array $args
	 * @return boolean
	 */
	public static function delete($args)
	{
		if( empty( $args['appid']) AND !empty( $args['app'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}
		
		$where = 'appid = ?';
		$params[] = $args['appid'];
		if( empty( $args['itemtype'] )){
			$args['itemtype'] = null;
			$where = $where.' AND itemtype IS NULL';
		} else {
			$where = $where.' AND itemtype = ?';
			$params[] = $args['itemtype'];
		}
		if (empty($args['itemid'])){
			$args['itemid'] = null;
			$where = $where.' AND itemid IS NULL';
		} else {
			$where = $where.' AND itemid = ?';
			$params[] = $args['itemid'];
		}
		return self::db()->delete( \claydb::$tables['markdown']." WHERE $where", $params );
	}

	/**
	 * Get a Markdown
	 * @param array $args
	 * @throws \Exception
	 */
	public static function get( $args=array() )
	{
		# Getting a specific Markdown ID
		if( empty( $args['appid'] ) AND empty( $args['app'] )){

			if( empty( $args['mid'] )){

				Throw new \Exception( self::$INVALID_ID );
			}

			return self::db()->get( 'mid, appid, itemtype, itemid, options, markdown FROM '.\claydb::$tables['markdown'].' WHERE mid = ?', array( $args['mid'] ),'0,1');
		}

		if( !empty( $args['app'] ) AND empty( $args['appid'] )){

			$args['appid'] = \Clay\Application::getID( $args['app'] );
		}

		if( is_null( $args['itemtype'])){

			//$where = 'appid = ? AND itemtype IS NULL AND itemid = ?';
			$where = 'appid = ? AND itemtype IS NULL AND itemid = ?';
			$bind = array( $args['appid'], $args['itemid'] );
		} else {

			$where = 'appid = ? AND itemtype = ? AND itemid = ?';
			$bind = array( $args['appid'], $args['itemtype'], $args['itemid'] );
		}
		# Getting a Markdown attached to an app
		return self::db()->get( 'mid, appid, itemtype, itemid, options, body FROM '.\claydb::$tables['markdown']." WHERE $where",
								$bind, '0,1' );

	}
}
