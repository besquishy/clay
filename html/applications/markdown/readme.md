# Clay Starter App

This is an application for the Clay CMS you use as a boilerplate to build your own app.

## Search, Replace, Customize, Run

Search and replace the following with what you want to use in your app:

markdown - Lowercase name of your app (unique)
Markdown - Uppercase name of your app
2017 - Copyright Year(s)
David L. Dyess II - Author's Name
GPL - License
%db_table1% - Database table your app will use
%db_table2% - Secondary database table your app will use
%plugin_type% - Plugin Type your app will register (unique)
%plugin_name% - Name of the Plugin your app will register (unique)
%plugin_title% - Title of the Plugin your app will register
%dashboard_name% - Name of the Dashboard menu your app will register (unique)
%dashboard_title% - Title of the Dashboard menu