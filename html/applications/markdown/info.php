<?php
/**
 * Markdown Application
 *
 * @copyright (C) 2017-2018 David L. Dyess II
 * @license GPL
 * @link %link%
 * @author David L. Dyess II
 * @package Clay
 */
$data = array('title' => 'Markdown',
			'name' => 'markdown',
			'version' => '1.0.0',
			'date' => 'April 22, 2018',
			'description' => 'Markdown editor and tools',
			'class' => 'System',
			'category' => 'Tools',
			'depends' => array('applications' => array('dashboard')),
			'core' => FALSE,
			'install' => TRUE
			);
?>