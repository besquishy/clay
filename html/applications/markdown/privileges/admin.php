<?php
/**
 * Markdown Privileges
 * @author David L. Dyess II
 * @copyright 2017
 * @todo Add Status field in blog_posts table for use with 'PUBLIC', 'PRIVATE', 'DRAFT', etc
 */

namespace application\markdown\privilege;

\Library('Clay/Application/Privilege');

/**
 * Markdown Admin Privilege
 */
class Admin extends \Clay\Application\Privilege {
	
	/**
	 * Item ID
	 * @var int
	 */
	public $Object;
	
	/**
	 * Requested Scope
	 * @var array
	 */
	public $Request;
	
	/**
	 * Multimask Privilege Validation
	 * @param array $scope
	 * @return boolean
	 */
	public function Scope($scope){
		
		# Extract the requested scope into array - e.g. COMMENTID::1 = array('COMMENTID',1)

		switch($scope[0]){
			
			case 'ALL':
				
				return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * Administration tool
	 * @param string $privilege
	 * @param string $scope
	 */
	public function Scopes($privilege, $scope = NULL){
		
		if(empty($scope)){
			
			switch($privilege){
				
				case 'Create':
					
					return array('ALL');
					
			}
			
			return array('ALL');
		}
		
		switch($scope){
			
			case 'ALL':
				
				return 'ALL';
		}
	}
}