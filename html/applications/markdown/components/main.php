<?php
/**
* Markdown Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link %link%
* @author David L. Dyess II
*/

namespace application\markdown\component;

/**
 *  Markdown Main Component
 * @author David L. Dyess II
 *
 */
class main extends \Clay\Application\Component {

	/**
	 * Markdown View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');

		/* # Privilege Check
		if(!$user->privilege('markdown','Main','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		*/
		
		$data['user'] = $user->Info();
		
		return $data;
	}
}