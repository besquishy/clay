<?php
/**
* Markdown Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link %link%
* @author David L. Dyess II
*/

namespace application\markdown\component;

/**
 *  Markdown Admin Component
 * @author David L. Dyess II
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Markdown Admin View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		
		return $data;
	}

	/**
	 * Markdown Admin Save
	 */
	public function save(){

		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Show Update Notification
		\clay\application::api('system','message','send','Markdown Settings Updated');
		# Redirect if not using AJAX submit
		if(empty($_POST['dashForm'])){
			
			\clay::redirect($_SERVER['HTTP_REFERER']);
		}
	}
}