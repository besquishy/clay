<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('kjv');?>" title="KJV Bible">Holy Bible</a></h1>
	</div>
	<div class="c-app__content">
		<form method="post" action="<?= \Clay\Application::URL( 'kjv', 'search' );?>">
			<div class="c-fieldset c-fieldset__fields">
				<input placeholder="Search" class="c-field" style="width: 300px;" type="text" name="q" id="q" value="<?= !empty( $query ) ? htmlspecialchars($query) : ''; ?>" size="20" />
				<input type="hidden" name="submitted" id="submitted" value="1" size="20" />
				<button class="c-button c-button--md c-button--success" type="submit" name="search" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
			</div>
		</form>
		<table class="c-table c-table--border">
			<tr class="c-table__row">
				<th class="c-table__head" colspan="2"> Old Testament </th> 
				<th class="c-table__head" colspan="2"> New Testament </th>
			</tr>
			<tr class="c-table__row">
				<td class="c-table__cell">
					<?php foreach( $books as $book ){
					if( $book['bid'] < 21 ){ ?>
					<p><a href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $book['bid'] )); ?>"><?= $book['book']; ?></a></p>
					<?php } } ?>
				</td>
				<td class="c-table__cell">
					<?php foreach( $books as $book ){
					if(( $book['bid'] > 21 ) AND ( $book['bid'] < 40 )){ ?>
					<p><a href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $book['bid'] )); ?>"><?= $book['book']; ?></a></p>
					<?php } } ?>
				</td>
				<td class="c-table__cell">
					<?php foreach( $books as $book ){
					if(( $book['bid'] < 54 ) AND ( $book['bid'] > 39 )){ ?>
					<p><a href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $book['bid'] )); ?>"><?= $book['book']; ?></a></p>
					<?php } } ?>
				</td>
				<td class="c-table__cell">
					<?php foreach( $books as $book ){
					if( $book['bid'] > 53 ){ ?>
					<p><a href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $book['bid'] )); ?>"><?= $book['book']; ?></a></p>
					<?php } } ?>
				</td>
			</tr>
		</table>
	</div>
</section>
