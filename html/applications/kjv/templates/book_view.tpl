<section class="c-app">
    <div class="c-app__head">
        <h1><a href="<?= \clay\application::url('kjv');?>" title="KJV Bible">Holy Bible</a> /
        <a href="<?= \Clay\Application::URL( 'kjv', 'main', 'view' );?>"><?= $testament; ?></a> / 
        <?= $book['book']; ?></h1>
    </div>
    <div class="c-app__content">
        <p class="c-fieldset c-fieldset__fields">
        <?php if( !empty( $previousBook )){ ?>
        <span>
            <a class="c-button c-button--default c-button--md" href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $previousBook['bid'] )); ?>"><i class="fa fa-angle-double-left"></i>  <?= $previousBook['book']; ?></a>
        </span>
        <?php }
        if( !empty( $nextBook )){ ?>
        <span>
            <a class="c-button c-button--primary c-button--md" href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $nextBook['bid'] )); ?>"><?= $nextBook['book']; ?> <i class="fa fa-angle-double-right"></i></a>
        </span>
        <?php } ?>
        </p>
        <div class="u-p@sm">
        <?php foreach( $chs as $ch ){ ?>
            <a href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $ch )); ?>"><?= $ch; ?></a> &nbsp; &nbsp; &nbsp; &nbsp; 
        <?php } ?>
        </div>
    </div>
</section>