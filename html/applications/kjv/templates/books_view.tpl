<section class="c-app">
    <div class="c-app__head">
        <h1><a href="<?= \clay\application::url('kjv');?>" title="KJV Bible">Holy Bible</a> /
        Books</h1>
    </div>
    <div class="c-app__content">
    <?php foreach( $books as $book ){ ?>
        <p><strong><?= $book['bid']; ?></strong> <?= $book['book']; ?></p>
    <?php } ?>
    </div>
</section>