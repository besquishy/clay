<section class="c-app">
    <div class="c-app__head">
        <h1><a href="<?= \Clay\Application::URL('kjv');?>" title="KJV Bible">Holy Bible</a> / 
        <a href="<?= \Clay\Application::URL( 'kjv', 'main', 'view' );?>"><?= $testament; ?></a> / 
        <a href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $book['bid'] ));?>"><?= $book['book']; ?></a> 
        <?= $ch; ?></h1>
    </div>
    <div class="c-app__content">
        <p class="c-fieldset c-fieldset__fields">
        <?php if( !empty( $previousBook )){ ?>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $previousBook['bid'] )); ?>"><i class="fa fa-angle-double-left"></i> <?= $previousBook['book']; ?></a>
        </span>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $previousBook['bid'], 'ch' => $previousBook['chs'] )); ?>"><i class="fa fa-angle-left"></i> <?= $previousBook['book'].' '.$previousBook['chs']; ?></a>
        </span>
        <?php }
        if( !empty( $previousChapter )){ ?>
        <span>
            <a class="c-button c-button--warning c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $previousChapter )); ?>"><i class="fa fa-chevron-left"></i> <?= $book['book'] .' '.$previousChapter; ?></a>
        </span>
        <?php }
        if( !empty( $nextChapter )){ ?>
        <span>
            <a class="c-button c-button--success c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $nextChapter )); ?>"><?= $book['book'] .' '.$nextChapter; ?> <i class="fa fa-chevron-right"></i></a>
        </span>
        <?php } 
        if( !empty( $nextBook )){ ?>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $nextBook['bid'], 'ch' => 1 )); ?>"><?= $nextBook['book'].' 1 '; ?> <i class="fa fa-angle-right"></i></a>
        </span>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $nextBook['bid'] )); ?>"><?= $nextBook['book']; ?> <i class="fa fa-angle-double-right"></i></a>
        </span>
        <?php } ?>
        </p>
        <div class="c-fieldset__fields">
            <a class="c-button c-button--md c-button--default <?= ($chapterStyle == 'list') ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $ch, 'view' => 'list' ));?>" title="List Style Display"><i class="fa fa-list-ol" aria-hidden="true"></i></a>
            <a class="c-button c-button--md c-button--default <?= ($chapterStyle == 'column') ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $ch, 'view' => 'column' ));?>" title="Column Style Display"><i class="fa fa-columns" aria-hidden="true"></i></a>
            <a class="c-button c-button--md c-button--default <?= ($chapterStyle == 'book') ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $ch, 'view' => 'book' ));?>" title="Paragraph Style Display"><i class="fa fa-book" aria-hidden="true"></i></a>
            <a class="c-button c-button--md c-button--default <?= ($chapterStyle == 'column-book') ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $ch, 'view' => 'column-book' ));?>" title="Paragraph & Column Style Display"><i class="fa fa-book" aria-hidden="true"></i><i class="fa fa-columns" aria-hidden="true"></i></a>
            <form style="display:inline;" method="post" action="<?= \Clay\Application::URL( 'kjv', 'search' );?>">
                <input class="c-field" placeholder="Search" type="text" name="q" id="q" value="<?= !empty( $query ) ? htmlspecialchars($query) : ''; ?>" size="20" style="width:200px;" />
                <input type="hidden" name="submitted" id="submitted" value="1" size="20" />
                <button class="c-button c-button--md c-button--success" type="submit" name="search"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
        </div>
        <div class="u-p@sm">
        <?php /*foreach( $chapter as $verse ){ ?>
            <p><sup><strong><?= $verse['ver']; ?></strong></sup>&nbsp; <?= $verse['txt']; ?></p>
        <?php } */?>
            <?php if($chapterStyle == 'list'){
            foreach( $chapter as $verse ){ ?>
            <p><sup><strong><?= $verse['ver']; ?></strong></sup>&nbsp; <?= $verse['txt']; ?></p>
            <?php } } ?>

            <?php if($chapterStyle == 'column'){ ?>
            <div style="border:none;width:50%;vertical-align:top;float:left;">
            <?php foreach( $chapter as $verse ){ 
            if($verse['ver'] <= ($columnCount)){ ?>
            <p><sup><strong><?= $verse['ver']; ?></strong></sup>&nbsp; <?= $verse['txt']; ?></p>
            <?php } } ?>
            </div><div style="border:none;width:50%;vertical-align:top;float:left;">
            <?php foreach( $chapter as $verse ){ 
            if($verse['ver'] >= ($columnCount + 1)){ ?>
            <p><sup><strong><?= $verse['ver']; ?></strong></sup>&nbsp; <?= $verse['txt']; ?></p>
            <?php } } ?>
            </div>
            <?php } ?>
            <br style="clear:both">

            <?php if($chapterStyle == 'book'){ ?>
            <?php foreach( $chapter as $verse ){ ?>
            <?= !empty( $verse['open'] ) ? $verse['open'] : ''; ?><sup><strong><?= $verse['ver']; ?></strong></sup>&nbsp; <?= $verse['txt']; ?>
            <?php } ?> </p> <?php } ?>
                
            <?php if($chapterStyle == 'column-book'){ ?>
            <div style="border:none;width:50%;vertical-align:top;float:left;padding:0 15px 0 0;">
            <?php foreach( $chapter as $verse ){ 
            if($verse['ver'] <= ($columnCount)){ ?>
            <?= !empty( $verse['open'] ) ? $verse['open'] : ''; ?><sup><strong><?= $verse['ver']; ?></strong></sup>&nbsp; <?= $verse['txt']; ?>
            <?php } } ?> </p>
            </div><div style="border:none;width:50%;vertical-align:top;float:left;padding:0 0 0 15px;">
            <?php foreach( $chapter as $verse ){ 
            if($verse['ver'] >= ($columnCount + 1)){ ?>
            <?= !empty( $verse['open'] ) ? $verse['open'] : ''; ?><sup><strong><?= $verse['ver']; ?></strong></sup>&nbsp; <?= $verse['txt']; ?>
            <?php } } ?> </p>
            </div>
            <?php } ?>
            <br style="clear:both">
        </div>
        <p class="c-fieldset c-fieldset__fields">
        <?php if( !empty( $previousBook )){ ?>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $previousBook['bid'] )); ?>"><i class="fa fa-angle-double-left"></i> <?= $previousBook['book']; ?></a>
        </span>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $previousBook['bid'], 'ch' => $previousBook['chs'] )); ?>"><i class="fa fa-angle-left"></i> <?= $previousBook['book'].' '.$previousBook['chs']; ?></a>
        </span>
        <?php }
        if( !empty( $previousChapter )){ ?>
        <span>
            <a class="c-button c-button--warning c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $previousChapter )); ?>"><i class="fa fa-chevron-left"></i> <?= $book['book'] .' '.$previousChapter; ?></a>
        </span>
        <?php }
        if( !empty( $nextChapter )){ ?>
        <span>
            <a class="c-button c-button--success c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $book['bid'], 'ch' => $nextChapter )); ?>"><?= $book['book'] .' '.$nextChapter; ?> <i class="fa fa-chevron-right"></i></a>
        </span>
        <?php } 
        if( !empty( $nextBook )){ ?>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $nextBook['bid'], 'ch' => 1 )); ?>"><?= $nextBook['book'].' 1 '; ?> <i class="fa fa-angle-right"></i></a>
        </span>
        <span>
            <a class="c-button c-button--primary c-button--sm" href="<?= \Clay\Application::URL( 'kjv', 'book', 'view', array( 'bid' => $nextBook['bid'] )); ?>"><?= $nextBook['book']; ?> <i class="fa fa-angle-double-right"></i></a>
        </span>
        <?php } ?>
        </p>
    </div>
</section>