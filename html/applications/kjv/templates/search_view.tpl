<section class="c-app">
	<div class="c-app__head">
		<h1><a href="<?= \clay\application::url('kjv');?>" title="KJV Bible">Holy Bible</a> /
		<?= !empty( $testament ) ? ucfirst( $testament ).' Testament / ' : ''; ?>Search</h1>
	</div>
	<div class="c-app__content">
		<form method="post" action="<?= \Clay\Application::URL( 'kjv', 'search' );?>">
			<div class="c-fieldset c-fieldset__fields">
				<input placeholder="Search" class="c-field" style="width: 300px;" type="text" name="q" id="q" value="<?= !empty( $query ) ? htmlspecialchars($query) : ''; ?>" size="20" />
				<input type="hidden" name="submitted" id="submitted" value="1" size="20" />
				<button class="c-button c-button--md c-button--success" type="submit" name="search" value="Search"><i class="fa fa-search" aria-hidden="true"></i></button>
			</div>
		</form>
		<p><h4>Examples:</h4>
			<ul>
				<li>All words: love is</li>
				<li>Exact: "love is"</li>
				<li>Expanded: love*</li>
			</ul>
		</p>
		<div>
			<?php if( !empty( $search )){ ?>
			<h2><?= $total; ?></strong> results were found!</h2>
			<p>
				<a class="c-button c-button--sm c-button--success <?= ($testament == NULL) ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'search', 'view', array( 'q' => htmlspecialchars($query) )); ?>">All &nbsp;<span class="c-badge"><?= $total; ?><?= ( !empty( $testament ) || !empty( $bid )) ? ' +' : ''; ?></span></a>
			<a class="c-button c-button--sm c-button--primary <?= ($testament == 'old') ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'search', 'view', array( 'q' => htmlspecialchars($query), 'testament' => 'old' )); ?>">Old Testament<?php if( !empty( $old ) ){?>  &nbsp;<span class="c-badge"><?= $old; ?></span><?php } ?></a>
				<a class="c-button c-button--sm c-button--info <?= ($testament == 'new') ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'search', 'view', array( 'q' => htmlspecialchars($query), 'testament' => 'new' )); ?>">New Testament<?php if( !empty( $new ) ){?>  &nbsp;<span class="c-badge"><?= $new; ?></span><?php } ?></a>
			</p>
			<div>
			<?php foreach( $book as $filter => $value ){ 
				if( !empty( $value['count'] )){ ?>
				<a style="margin-bottom: 2px;" class="c-button c-button--sm c-button--default <?= ($bid == $value['bid']) ? 'disabled' : ''; ?>" href="<?= \Clay\Application::URL( 'kjv', 'search', 'view', array( 'q' => htmlspecialchars($query), 'testament' => $testament, 'bid' => $value['bid'] )); ?>"><?= $value['name']; ?> &nbsp;<span class="c-badge"><?= $value['count']; ?></span></a>
			<?php } } ?>
			</div>
			<?php foreach( $search as $result ){ ?>
			<p>
				<h3><a href="<?= \Clay\Application::URL( 'kjv', 'chapter', 'view', array( 'bid' => $result['bid'], 'ch' => $result['ch'] )); ?>"><?= $book[$result['bid']]['name'].' '.$result['ch'].':'.$result['ver'] ?></a></h3>
				<?= $result['txt']; ?>
			</p>
			<?php } } ?>
			<?php if( !empty( $query ) AND empty( $search )){ ?>
				<h2>No Results Found!</h2>
			<?php } ?>
		</div>
	</div>
</section>
