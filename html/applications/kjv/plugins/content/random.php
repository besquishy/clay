<?php
namespace application\plugins\plugin\content;

/**
 * Plugins HTML Block
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 */
class html extends \Clay\Application\Plugin {
	
	public $PluginApp = 'plugins';
	public $PluginType = 'content';
	public $Plugin = 'html';
	
	/**
	 * View - to be displayed on a summary, list of items, or typical Clay view page
	 * @param unknown $args
	 * @return string
	 */
	public function view( $args ){
		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}
	/**
	 * Display - to be displayed on an item page
	 * @param unknown $args
	 * @return string
	 */
	public function display( $args ){

		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}

	public function add(){

		$data = array(true);
		return $data;
	}
	
	public function addItem(){
		
		
	}
	
	public function create($args){
		
		
	}

	public function edit( $args ){

		$data = array();
		$data['content'] = $args['text'];
		return $data;
	}
	
	public function editItem($args){
		
		
	}
	
	public function update($args){
		
		
	}
	
	public function delete($args){
		
		
	}
}