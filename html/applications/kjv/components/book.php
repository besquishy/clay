<?php
namespace application\kjv\component;

/**
* KJV Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link http://profoundgrace.org
* @author David L. Dyess II
*/

/**
 *  KJV Books Component
 * @author David L. Dyess II
 *
 */
class book extends \Clay\Application\Component {

	/**
	 * KJV View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');

		/* # Privilege Check
		if(!$user->privilege('kjv','Main','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		*/
		
		//$data['user'] = $user->Info();

		$bid = \Clay\Data\Get( 'bid', 'int', 'int');

		$data['testament'] = \Clay\Application::API( 'kjv', 'get', 'Testament', $bid);

		$book = \Clay\Application::API( 'kjv', 'get', 'Book', array( 'bid' => $bid ));

		$data['previousBook'] = \Clay\Application::API( 'kjv', 'get', 'Book', array( 'bid' => $bid - 1 ));
		
		$data['nextBook'] = \Clay\Application::API( 'kjv', 'get', 'Book', array( 'bid' => $bid + 1 ));

		$data['book'] = $book;

		$chs = 1;

		while ( $chs <= $book['chs'] ){

			$data['chs'][] = $chs;
			$chs++;
		}

		
		return $data;
	}
}