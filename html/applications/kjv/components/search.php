<?php
namespace application\kjv\component;

/**
* KJV Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link http://profoundgrace.org
* @author David L. Dyess II
*/

/**
 *  KJV Search Component
 * @author David L. Dyess II
 *
 */
class search extends \Clay\Application\Component {

	/**
	 * KJV Search View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');

		/* # Privilege Check
		if(!$user->privilege('kjv','Main','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		*/
		
		//$data['user'] = $user->Info();

		# Get the query, if there is one - uses Fetch so we can use GET or POST variables
		$query = htmlspecialchars_decode( \Clay\Data\Fetch( 'q', 'string', 'string' ));
		# Get Testament Filter
		$testament = \Clay\Data\Fetch( 'testament', 'string', 'string', NULL );
		# Get Book ID filter
		$bid = \Clay\Data\Fetch( 'bid', 'int', 'int', NULL );

		$data['query'] = $query;
		$data['testament'] = $testament;
		$data['bid'] = $bid;

		$books = \Clay\Application::API( 'kjv', 'get', 'Books');
		
		foreach( $books as $book ){
			
			$data['book'][$book['bid']]['name'] = $book['book'];
			$data['book'][$book['bid']]['bid'] = $book['bid'];
		}

		if( !empty( $query )){

			$args = array( 'query' => $query, 'bid' => $bid, 'testament' => $testament );

			$data['search'] = \Clay\Application::API( 'kjv', 'get', 'Query', $args );
		}
		if( !empty( $data['search'])){

			$data['total'] = 0;
			$data['old'] = 0;
			$data['new'] = 0;

			foreach( $data['search'] as $search ){

				$data['book'][$search['bid']]['count'] = empty( $data['book'][$search['bid']]['count'] ) ? 1 : ++$data['book'][$search['bid']]['count'];
				$data['total'] = ++$data['total'];
				
				if( $search['bid'] < 40 ){

					$data['old'] = ++$data['old'];
				}
				if( $search['bid'] > 39 ){
					
					$data['new'] = ++$data['new'];
				}
			}
		}

		return $data;
	}
}