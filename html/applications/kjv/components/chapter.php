<?php
namespace application\kjv\component;

/**
* KJV Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link http://profoundgrace.org
* @author David L. Dyess II
*/

/**
 *  KJV Chapter Component
 * @author David L. Dyess II
 *
 */
class chapter extends \Clay\Application\Component
{
	/**
	 * KJV View
	 */
	public function view()
	{
		$data = array();
		
		$user = \Clay\Module::API('User','Instance');

		/* # Privilege Check
		if(!$user->privilege('kjv','Main','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		*/
		
		//$data['user'] = $user->Info();
		# Book ID
		$bid = \Clay\Data\Get( 'bid', 'int', 'int');
		# Chapter ID
		$ch = \Clay\Data\Get( 'ch', 'int', 'int');
		# Set a default View Style
		if (empty($_SESSION['kjv.chapterStyle'])){
			$_SESSION['kjv.chapterStyle'] = 'column-book';
		}
		# View Style - fallsback to an application setting
		$chapterStyle = \Clay\Data\Get( 'view', 'string', 'string', \Clay\Application::Setting( 'kjv', 'chapterStyle', $_SESSION['kjv.chapterStyle'] ));
		# Compare available View styles and set a default for fallback
		switch ( $chapterStyle ){
			# If someone manually input an unavailable View style, this will reset it
			case 'list':
				break;			
			case 'column':
				break;
			case 'book':
				break;
			case 'column-book':
				break;
			default:
				$chapterStyle = 'column-book';
				break;
		}
		if (!empty($_SESSION['kjv.chapterStyle'])){
			# If the User's session variable for display style doesn't match, set it
			if( $_SESSION['kjv.chapterStyle'] !== $chapterStyle){
				$_SESSION['kjv.chapterStyle'] = $chapterStyle;
			}
		} else {
			# Set the User's session variable for display style
			$_SESSION['kjv.chapterStyle'] = $chapterStyle;
		}
		
		# Make it available to the template
		$data['chapterStyle'] = $chapterStyle;
		# Chapter ID
		$data['ch'] = $ch;
		# Old or New Testament
		$data['testament'] = \Clay\Application::API( 'kjv', 'get', 'Testament', $bid);
		# Get Book info
		$book = \Clay\Application::API( 'kjv', 'get', 'Book', array( 'bid' => $bid ));

		$data['book'] = $book;
		# Add a previous nav fallback
		$navPrevious = false;
		# Take Book's chapter count and determine if there is a previous chapter
		if ( $book['chs'] > 1 ){

			$data['previousChapter'] = $ch - 1;
			
			if ( !empty( $data['previousChapter'] )){
				# There is a previous chapter
				$navPrevious = true;
			}
		}
		# Add a next nav fallback
		$navNext = false;
		# Take Book's chapter count and determine if there is a next chapter
		if ( $book['chs'] != $ch ){

			$data['nextChapter'] = $ch + 1;
			# There is a next chapter
			$navNext = true;
		}
		# Get the previous book
		$data['previousBook'] = \Clay\Application::API( 'kjv', 'get', 'Book', array( 'bid' => $bid - 1 ));
		# Get the next book
		$data['nextBook'] = \Clay\Application::API( 'kjv', 'get', 'Book', array( 'bid' => $bid + 1 ));
		# Get the verses in this chapter
		$chapter = \Clay\Application::API( 'kjv', 'get', 'Chapter', array( 'bid' => $bid, 'ch' => $ch ));
		# Get paragraphs for 'book' and 'column-book' view styles
		if (( $chapterStyle == 'book' ) OR ( $chapterStyle == 'column-book' )){
			
			foreach ( $chapter as $verse ){

				$code = explode( " ", $verse['coding'] );
				
				if ( $code[0] == '&para;' ){

					if ( $verse['ver'] == 1 ){

						$verse['open'] = '<p>';
					} else {

						$verse['open'] = '</p><p>';
					}
					
					$verse['paragraph'] = true;	
				}

				$verses[] = $verse;
			}
		}
		# Count the verses
		$verseCount = count( $chapter );
		# Calculate Column length
		$data['columnCount'] = ($verseCount + ($verseCount % 2)) / 2;
		# If we changes the $chapter array, use the new array
		$data['chapter'] = !empty( $verses ) ? $verses : $chapter;
		
		return $data;
	}
}