<?php
namespace application\kjv\component;

/**
* KJV Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link http://profoundgrace.org
* @author David L. Dyess II
*/

/**
 *  KJV Books Component
 * @author David L. Dyess II
 *
 */
class books extends \Clay\Application\Component {

	/**
	 * KJV View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');

		/* # Privilege Check
		if(!$user->privilege('kjv','Main','View')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		*/
		
		//$data['user'] = $user->Info();

		$books = \Clay\Application::API( 'kjv', 'get', 'Books');

		$data['books'] = $books;
		
		return $data;
	}
}