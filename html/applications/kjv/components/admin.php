<?php
namespace application\kjv\component;

/**
* KJV Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link http://profoundgrace.org
* @author David L. Dyess II
*/

/**
 *  KJV Admin Component
 * @author David L. Dyess II
 *
 */
class admin extends \Clay\Application\Component {

	/**
	 * Default Page Template for this component
	 */
	public $page = 'dashboard';

	/**
	 * KJV Admin View
	 */
	public function view(){

		$data = array();
		
		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		/*# Heading
		$data['heading'] = \Clay\Application::Setting('kjv','heading','');
		# Introduction
		$data['intro'] = \Clay\Application::Setting('kjv','intro','');
		# Form Type
		$data['type'] = \Clay\Application::Setting('kjv','form.type','frame');
		# Embedded Form HTML
		$data['frame'] = \Clay\Application::Setting('kjv', 'form.frame','');
		# KJV Email Address
		$data['email'] = \Clay\Application::Setting('kjv', 'email','');
		# Use Map?
		$data['map'] = \Clay\Application::Setting('kjv','map','0');
		# Embedded Map HTML
		$data['mapFrame'] = \Clay\Application::Setting('kjv','map.frame','');
		# Use Extra Info?
		$data['info'] = \Clay\Application::Setting('kjv','info','0');
		# Extra Info
		$infoData = \Clay\Application::Setting('kjv','info.data',serialize(array()));
		$data['infoData'] = unserialize($infoData);*/
		
		return $data;
	}

	/**
	 * KJV Admin Save
	 */
	public function save(){

		$user = \Clay\Module::API('User','Instance');
		
		if(!$user->privilege('system','Admin','User')){
			
			throw new \Exception('Additional Privileges Are Required to View This Page.');
		}
		# Heading
		if($_POST['heading'] !== $_POST['oldHeading']){
			
			\clay\application::set('kjv','heading',\clay\data\post('heading','string','text'));
		}
		# Introduction
		if($_POST['intro'] !== $_POST['oldIntro']){

			\clay\application::set('kjv','intro',\clay\data\post('intro','string','text'));
		}
		# KJV Email Address
		if((!empty($_POST['email'])) && ($_POST['email'] !== $_POST['oldEmail'])){

			\clay\application::set('kjv','email',\clay\data\post('email','email','email'));
		}
		# Form Type
		if($_POST['type'] !== $_POST['oldType']){

			\clay\application::set('kjv','form.type',\clay\data\post('type','string','text'));
		}
		# Embedded Form HTML
		if((!empty($_POST['frame'])) && ($_POST['frame'] !== $_POST['oldFrame'])){
		
			\clay\application::set('kjv','form.frame',\clay\data\post('frame','string'));
		}
		# Use Extra Info?
		if($_POST['info'] !== $_POST['oldInfo']){
		
			\clay\application::set('kjv','info',\clay\data\post('info','string','text'));
		}

		# KJV Information Extra Data
		# Reset Info Data
		\Clay\Application::set('kjv','info.data','');
		# Save New Info Data
		\clay\application::set('kjv','info.data',serialize(\clay\data\post('infoData','isArray','',array())));

		# Use Map?
		if($_POST['map'] !== $_POST['oldMap']){
			
			\clay\application::set('kjv','map',\clay\data\post('map','string','text'));
		}
		# Embedded Map HTML
		if((!empty($_POST['mapFrame'])) && ($_POST['mapFrame'] !== $_POST['oldMapFrame'])){
		
			\clay\application::set('kjv','map.frame',\clay\data\post('mapFrame','string'));
		
		}
		# Show Update Notification
		\clay\application::api('system','message','send','KJV Settings Updated');
		# Redirect if not using AJAX submit
		if(empty($_POST['dashForm'])){
			
			\clay::redirect($_SERVER['HTTP_REFERER']);
		}
	}
}