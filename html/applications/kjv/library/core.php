<?php
namespace application\kjv\library;

/**
 * KJV Application
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL
 * @link http://profoundgrace.org
 * @author David L. Dyess II
 */

/**
 * KJV Core Library
 */
 class Core {
 	
 	/**
 	 * Import self::db() using a Trait \ClayDB\Connection
 	 */
 	use \ClayDB\Connection;
 	
 	/**
 	 * KJV Item Types (currently none)
 	 * @return array
 	 */
 	public static function ItemTypes(){
 		
 		return array();
 	}
 	/**
 	 * KJV Item Type
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function ItemType( $itemType ){
 		
 		return array();
 	}
 	/**
 	 * KJV Posts
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function Items( $itemType = NULL ){
 		# Get all Blog Posts
 		$posts = self::db()->get( 'pid, uid, title FROM '.\claydb::$tables['blog_posts'].' ORDER BY pid ASC' );
 		
 		foreach( $posts as $post ){
 			# Titles are optional, make sure we have a filler
 			if( empty( $post['title'] )){
 				
 				$post['title'] = 'Untitled';
 			}
 			$items[] = array( 'id' => $post['pid'],
 						      'name' => $post['title'],
 						      'title' => $post['title'],
 						      'uid' => $post['uid']
 							 );
 		}
 		
 		return $items;
 	}
 	/**
 	 * KJV Post
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Item( $args ){
 		
 		$post = self::db()->get( 'pid, uid, title FROM '.\claydb::$tables['blog_posts'].' WHERE pid = ? ORDER BY pid ASC', array($args[1]), '0,1' );
 		
 		return array( 'id' => $post['pid'],
 					   'name' => $post['title'],
 					   'title' => $post['title'],
 					   'uid' => $post['uid']
 					  );
 	}
 	/**
 	 * KJV Fields
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Fields( $args ){
 		
 		return array( 'uid', 'pubdate', 'title', 'body' );
 	}
 }