<?php
namespace application\kjv\library;

\Clay\Module( 'Privileges' );

/**
* KJV Application
*
* @copyright (C) 2017 David L Dyess II
* @license GPL
* @link http://profoundgrace.org
* @author David L. Dyess II
*/
class setup {
	
	/**
	 * Import self::db() using a Trait \ClayDB\Connection
	 */
	use \ClayDB\Connection;
	/**
	 * Install
	 * @return boolean
	 */
	public static function install(){
		# Use the DB trait
		$dbconn = self::db();
		# Datadict is Used for Table Manipulation
	    $datadict = $dbconn->datadict();
		# KJV tables
		# Table Alias => Table Name
	    $tables = array( 'kjv_books' => array( 'table' => 'kjv_books' ),
						 'kjv_bookmarks' => array( 'table' => 'kjv_bookmarks' ),
						 'kjv_greek' => array( 'table' => 'kjv_greek' ),
						 'kjv_hebrew' => array( 'table' => 'kjv_hebrew' ),
						 'kjv_verses' => array( 'table' => 'kjv_verses' ));
	    # Add DB Table names to data/config/sites/[site]/database.tables.php configuration file
		$datadict->registerTables( $tables );
		# Prepare the Books table for creation
	    $cols = array();
	    $cols['column'] = array( 'bid' => array( 'type' => 'ID' ),
	    						'book' => array( 'type' => 'STRING', 'size' => '255' ),
	    						'chs' => array( 'type' => 'INT','size' => '11' ),
	    						'abbr' => array( 'type' => 'TEXT' ));
	    # Create Books Table
	    $datadict->createTable( \claydb::$tables['kjv_books'], $cols );
		
		# Prepare the Bookmarks table for creation
	    $cols = array();
	    $cols['column'] = array( 'bmid' => array( 'type' => 'ID' ),
	    						'uid' => array( 'type' => 'INT', 'size' => '11' ),
	    						'vid' => array( 'type' => 'STRING', 'size' => '255' ),
	    						'note' => array( 'type' => 'TEXT' ));
		# Bookmarks table indices
		$cols['index'] = array( 'user' => array( 'type' => 'KEY', 'index' => 'uid,vid' ),
								'verse' => array( 'type' => 'KEY', 'index' => 'vid' ),
								'note' => array( 'type' => 'FULLTEXT', 'index' => 'note' ));
		# Create Bookmarks Table
	    $datadict->createTable( \claydb::$tables['kjv_bookmarks'], $cols );

		# Prepare the Greek table for creation
	    $cols = array();
	    $cols['column'] = array( 'gid' => array( 'type' => 'ID' ),
	    						'num' => array( 'type' => 'INT', 'size' => '11' ),
	    						'word' => array( 'type' => 'TEXT' ),
								'orig' => array( 'type' => 'TEXT' ),
								'tr' => array( 'type' => 'TEXT' ),
								'tdnt' => array( 'type' => 'TEXT' ),
								'phon' => array( 'type' => 'TEXT' ),
								'pos' => array( 'type' => 'TEXT' ),
								'st' => array( 'type' => 'TEXT' ),
								'ipd' => array( 'type' => 'TEXT' ));
		# Greek table indices
		$cols['index'] = array( 'number' => array( 'type' => 'KEY', 'index' => 'num' ));
		# Create Greek Table
	    $datadict->createTable( \claydb::$tables['kjv_greek'], $cols );

		# Prepare the Hebrew table for creation
	    $cols = array();
	    $cols['column'] = array( 'gid' => array( 'type' => 'ID' ),
	    						'num' => array( 'type' => 'INT', 'size' => '11' ),
	    						'word' => array( 'type' => 'TEXT' ),
								'orig' => array( 'type' => 'TEXT' ),
								'tr' => array( 'type' => 'TEXT' ),
								'tdnt' => array( 'type' => 'TEXT' ),
								'phon' => array( 'type' => 'TEXT' ),
								'pos' => array( 'type' => 'TEXT' ),
								'st' => array( 'type' => 'TEXT' ),
								'ipd' => array( 'type' => 'TEXT' ));
		# Hebrew table indices
		$cols['index'] = array( 'number' => array( 'type' => 'KEY', 'index' => 'num' ));
		# Create Hebrew Table
		$datadict->createTable( \claydb::$tables['kjv_hebrew'], $cols );
		
	    # Prepare the Verses table for creation
	    $cols = array();
	    $cols['column'] = array( 'vid' => array( 'type' => 'ID' ),
	    						'bid' => array( 'type' => 'INT', 'size' => '11' ),
	    						'ch' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
	    						'ver' => array( 'type' => 'INT', 'size' => '11', 'unsigned' => TRUE ),
	    						'txt' => array( 'type' => 'TEXT' ),
	    						'coding' => array( 'type' => 'TEXT' ));
	    # Verses table indices
	    $cols['index'] = array( 'chapter' => array( 'type' => 'KEY', 'index' => 'bid, ch' ),
	    						'text' => array( 'type' => 'FULLTEXT', 'index' => 'txt' ));
	    # Create Verses Table
	    $datadict->createTable( \claydb::$tables['kjv_verses'], $cols );

	    # Dashboard
		$dash = array( 'plugin', array( 'type' =>'dashboard','app' => 'kjv','name' => 'kjv','descr' => 'Manage Bible and Settings.' ));
		\Clay\Module::Object( 'Plugins' )->Setup( $dash );
		# Hook KJV Dashboard Plugin to the Dashboard
		$hook = array('plugin' => array('type' => 'dashboard', 'app' => 'kjv', 'name' => 'kjv'),
		'app' => 'dashboard');
		\Clay\Module::Object('Plugins')->Hook ( $hook );
		# KJV Random Plugin
		$plugin = array( 'plugin', array( 'type' => 'content', 'app' => 'kjv', 'name' => 'random', 'descr' => 'Display random Bible verses.', 'inst' => '1' ));
		\Clay\Module::Object('Plugins')->Setup( $plugin );
		# Privileges
		# Bible Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'kjv', 'Bible', 'Update' );
		\Clay\Module::Object( 'Privileges' )->register( 'kjv', 'Bible', 'View' );
		# Content Privileges
		\Clay\Module::Object( 'Privileges' )->register( 'kjv', 'Content', 'Create' );
		\Clay\Module::Object( 'Privileges' )->register( 'kjv', 'Content', 'Update' );
		\Clay\Module::Object( 'Privileges' )->register( 'kjv', 'Content', 'Delete' );
		\Clay\Module::Object( 'Privileges' )->register( 'kjv', 'Content', 'View' );

		return true;
	}
	/**
	 * Upgrade
	 * @param integer $version
	 * @return boolean
	 */
	public static function upgrade($version){

		return true;
	}
	/**
	 * Uninstall
	 * @return boolean
	 */
	public static function delete(){
		# ClayDB Object
		$dbconn = self::db();
	    $datadict = $dbconn->datadict();
		# Drop Tables
	    $datadict->dropTable( \claydb::$tables['kjv_books'] );
		$datadict->dropTable( \claydb::$tables['kjv_bookmarks'] );
		$datadict->dropTable( \claydb::$tables['kjv_greek'] );
		$datadict->dropTable( \claydb::$tables['kjv_hebrew'] );
		$datadict->dropTable( \claydb::$tables['kjv_verses'] );
		# Drop Privileges
	    \Clay\Module::Object( 'Privileges' )->Remove( 'kjv' );
		# Drop Hooks
		\Clay\Module::Object( 'Plugins' )->Remove( 'kjv' );
	    return true;
	}
}
