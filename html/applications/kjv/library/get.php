<?php
namespace application\kjv\library;

/**
 * KJV Application
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL
 * @link http://profoundgrace.org
 * @author David L. Dyess II
 */

/**
 * KJV Get Library
 */
 class Get {
 	
 	/**
 	 * Import self::db() using a Trait \ClayDB\Connection
 	 */
 	use \ClayDB\Connection;
	 
	/**
	 * Get Testament
	* @return array
	*/
	public static function Testament( $bid ){
		
		if( $bid < 40 ){
			
			return 'Old Testament';

		} else {

			return 'New Testament';
		}
	}

 	/**
 	 * Get All Books
 	 * @return array
 	 */
 	public static function Books(){
		 
		return self::db()->get( 'bid, book, chs, abbr FROM '.\claydb::$tables['kjv_books'].' ORDER BY bid ASC' );
	}
	
	/**
	 * Get Old Testament Books
	* @return array
	*/
	public static function OldTestament( $bid ){
		
		return self::db()->get( 'bid, book, chs, abbr FROM '.\claydb::$tables['kjv_books'].' WHERE bid < 40 ORDER BY bid ASC' );
	}

	/**
	 * Get New Testament Books
	* @return array
	*/
	public static function NewTestament( $bid ){
		
		return self::db()->get( 'bid, book, chs, abbr FROM '.\claydb::$tables['kjv_books'].' WHERE bid > 39 ORDER BY bid ASC' );
	}

 	/**
 	 * Get a Book
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function Book( $book ){
		 
		if( !empty( $book['bid'] )){

			$where = ' bid = ? ';
			$req = array( $book['bid'] );

		} elseif( !empty( $book['book'] )){

			$where = ' book = ? ';
			$req = array( $book['book'] );

		} else {

			return false;
		}

 		return self::db()->get( 'bid, book, chs, abbr FROM '.\claydb::$tables['kjv_books'].' WHERE '.$where.' ORDER BY bid ASC', $req, '0,1' );
 	}
 	/**
 	 * Get Chapter (verses)
 	 * @param integer $itemType
 	 * @return array
 	 */
 	public static function Chapter( $args ){
		 
		return self::db()->get( 'vid, bid, ch, ver, txt, coding FROM '.\claydb::$tables['kjv_verses'].' WHERE bid = ? AND ch = ? ORDER BY ver ASC', array( $args['bid'], $args['ch'] ));
 	}
 	/**
 	 * Get Verse
 	 * @param array $args - ([itemtype],[id])
 	 * @return array
 	 */
 	public static function Verse( $args ){
 		
 		
	}
	 
	/**
	 * Search Query
	* @return array
	*/
	public static function Query( $args ){
		// SELECT * FROM posts WHERE MATCH(title, body) AGAINST ('something' IN BOOLEAN MODE);
		//die(var_dump($query));
		if( !is_null( $args['bid'] )){
			
			$book = ' AND bid = ? ';

			return self::db()->get( 'vid, bid, ch, ver, txt FROM '.\claydb::$tables['kjv_verses']." WHERE MATCH(txt) AGAINST (? IN BOOLEAN MODE) $book ORDER BY bid, ch, ver ASC", array( $args['query'], $args['bid'] ));			
		
		} elseif( !is_null( $args['testament'] )){
			
			if( $args['testament'] == 'old' ){
				
				$testament = ' AND bid < 40 '; 
			
			} else {
				
				$testament = ' AND bid > 39 ';
			}

			return self::db()->get( 'vid, bid, ch, ver, txt FROM '.\claydb::$tables['kjv_verses']." WHERE MATCH(txt) AGAINST (? IN BOOLEAN MODE) $testament ORDER BY bid, ch, ver ASC", array( $args['query'] ));
			
		} else {

			return self::db()->get( 'vid, bid, ch, ver, txt FROM '.\claydb::$tables['kjv_verses']." WHERE MATCH(txt) AGAINST (? IN BOOLEAN MODE) ORDER BY bid, ch, ver ASC", array( $args['query'] ));
		}
	}
 }