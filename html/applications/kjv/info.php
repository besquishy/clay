<?php
/**
 * KJV Application
 *
 * @copyright (C) 2017 David L Dyess II
 * @license GPL
 * @link http://profoundgrace.org
 * @author David L. Dyess II
 * @package Clay
 */
$data = array('title' => 'KJV',
			'name' => 'kjv',
			'version' => '0.1',
			'date' => '19 October 2017',
			'description' => 'King James Version Bible',
			'class' => 'User',
			'category' => 'Content',
			'depends' => array('applications' => array('dashboard')),
			'core' => false,
			'install' => true
			);
?>