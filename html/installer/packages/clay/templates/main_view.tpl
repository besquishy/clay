<section class="c-app">
	<div class="c-app__head"><h1>Clay Setup Wizard</h1></div>
	<div class="c-app__content">
		<p class="u-bgcolor-info u-p@sm">This installer takes a minimalist approach to installing Clay Content Management System. Currently there are not a lot of configuration options. Most configuration options are handled through the CMS
		administration interface.</p>
		<?php $this->template($output); ?>
	</div>
</section>