<section class="c-app">
	<div class="c-app__head">
		<h1>Site Settings</h1>
	</div>
	<div class="c-app__content" id="clay_cfg">
		<div class="c-tabs--bar">
			<span v-on:click="pane = 'overview'" class="c-tabs__tab" v-bind:class="classObject('overview')">Overview</span>
			<span v-on:click="pane = 'databases'" class="c-tabs__tab" v-bind:class="classObject('databases')">Databases</span>
			<span v-on:click="pane = 'settings'" class="c-tabs__tab" v-bind:class="classObject('settings')">Settings</span>
		</div>
		<div v-if="pane == 'overview'" class="c-tabs__pane">
			This is the configuration panel for your Clay site.
		</div>

		<div v-if="pane == 'databases'" class="c-tabs__pane">
			<?php $databases->template($database);?>
		</div>

		<div v-if="pane == 'settings'" class="c-tabs__pane">
			<form method="post" action="<?= \installer\application::url('main','update'); ?>" class="configs">
				<fieldset class="c-fieldset">
					<legend>File Paths</legend>
					<div class="c-fieldset__fields">
						<p class="u-bgcolor-info u-p@sm">Note: All paths and folders should end with '/', but do not require a starting '/'. Paths can be several folders deep, while a Folder is a single folder.</p>
						<p class="u-bgcolor-danger u-p@sm">These are Path overrides, so if you haven't changed any paths or folders, you can leave these alone.</p>
						<p class="u-bgcolor-success u-p@sm">Clay appears to be located at <?= \clay\PATH; ?>/</p>
						<p class="u-bgcolor-info u-p@sm">Libraries are by default located in the top level. 'libraries/' resolves to <?= \clay\PATH; ?>/libraries/</p>
						<label for="libs" title="Libraries path, relative to Clay">Libraries Path:</label>
						<input class="c-field" type="text" name="paths[libs.dir]" id="libs" value="<?= $paths['libs.dir']; ?>" /> <br />
						<p class="u-bgcolor-info u-p@sm">Document root is by default located in the top level. 'html/' resolves to <?= \clay\PATH; ?>/html/</p>
						<label for="webpath" title="Path to your Document Root Folder, relative to Clay">Document Root Folder:</label>
						<input class="c-field" type="text" name="paths[web.path]" id="webpath" value="<?= $paths['web.path']; ?>" /> <br />
						<p class="u-bgcolor-info u-p@sm">Web Directory is relative to the Document root. This is the folder your index.php file is in.
						'test_site/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/</p>
						<label for="webdir" title="Name of this site's Web Directory, relative to Document Root">Web Directory:</label>
						<input class="c-field" type="text" name="paths[web.dir]" id="webdir" value="<?= $paths['web.dir']; ?>" /> <br />
						<p class="u-bgcolor-info u-p@sm">The parent path to your Applications Folder. The Applications Path is anything between Web Directory and the actual Applications Folder.
						'test_site/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/</p>
						<label for="appspath" title="Path to this site's Applications, relative to Web Directory">Applications Path:</label>
						<input class="c-field" type="text" name="paths[apps.path]" id="appspath" value="<?= $paths['apps.path']; ?>" /> <br />
						<p class="u-bgcolor-info u-p@sm">The Applications Folder is the folder where your applications reside.
						'applications/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/applications/</p>
						<label for="appsdir" title="Name of this site's Applications Directory, relative to Application Path">Applications Folder:</label>
						<input class="c-field" type="text" name="paths[apps.dir]" id="appsdir" value="<?= $paths['apps.dir']; ?>" />
						<p class="u-bgcolor-info u-p@sm">The parent path to your Themes Folder. The Themes Path is anything between Web Directory and the actual Themes Folder.
						'test_site/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/</p>
						<label for="themespath" title="Path to this site's Themes, relative to Web Directory">Themes Path:</label>
						<input class="c-field" type="text" name="paths[themes.path]" id="themespath" value="<?= $paths['themes.path']; ?>" /> <br />
						<p class="u-bgcolor-info u-p@sm">The Themes Folder is the folder where your themes reside.
						'themes/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/themes/</p>
						<label for="themesdir" title="Name of this site's Themes Directory, relative to Themes Path">Themes Folder:</label>
						<input class="c-field" type="text" name="paths[themes.dir]" id="themesdir" value="<?= $paths['themes.dir']; ?>" />
						<input type="hidden" name="paths[version]" value="<?= $version; ?>" id="version" />
					</div>
				</fieldset>
				<fieldset class="c-fieldset__controls">
					<input type="hidden" name="site" value="<?= \clay\data\get('site','string','base',\clay\data\get('s','string','base')); ?>" id="site" />
					<p><input class="c-button" type="submit" name="submit" value="Update Settings" id="submit" /></p>
				</fieldset>
			</form>
		</div>
	</div>
</section>
<script>
	var packVue = new Vue({
		el: '#clay_cfg',
		data: function() {
			return {
				pane: 'overview'
			}
    	},
		methods: {
			classObject: function (pane) {
				return {
					'c-tabs__tab--active': this.pane === pane
				}
			}
		},
	})
</script>