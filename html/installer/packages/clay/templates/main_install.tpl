<section class="c-app" id="clay_install">
	<p class="u-bgcolor-success u-p@sm"><strong>Let's get started!</strong></p>
	<h1>Installation Settings</h1>
	<?php if(!empty($message)) echo $message; ?>
	<h2>Step 1</h2>
	<button v-if="step > 1" v-on:click="toggle(1)" type="button" class="c-button">Show / Hide</button>
	<div v-if="step_1" class="databases">
		<?php $databases->template($database);?>
	</div>
	<div v-if="step > 1">
		<h2>Step 2</h2>
		<button v-if="step > 2" v-on:click="toggle(2)" type="button" class="cfgclicker c-button">Show / Hide</button>
		<h3>Configuration</h3>
		<p class="u-bgcolor-info u-p@sm">These are Path overrides, so if you haven't changed any paths or folders, you can leave these alone.</p>
		<form method="post" action="<?= \installer\application::url('main','create'); ?>" class="configs">
			<fieldset class="c-fieldset">
				<legend>File Paths</legend>
				<div class="c-fieldset__fields">
					<p class="u-bgcolor-danger u-p@sm">Note: All paths and folders should end with '/', but do not require a starting '/'. Paths can be several folders deep, while a Folder is a single folder.</p>
					
					<p class="u-bgcolor-success u-p@sm">Clay appears to be located at <?= \clay\PATH; ?>/</p>
					<hr />
					<p class="u-bgcolor-info u-p@sm">Libraries are by default located in the top level. 'library/' resolves to <?= \clay\PATH; ?>/library/</p>
					<label for="libs" title="Library path, relative to Clay">Library Path:</label>
					<input class="c-field" type="text" name="paths[libs.dir]" id="libs" value="<?= $paths['libs.dir']; ?>" /> <br />
					<hr />
					<p class="u-bgcolor-info u-p@sm">Document root is by default located in the top level. 'html/' resolves to <?= \clay\PATH; ?>/html/</p>
					<label for="webpath" title="Path to your Document Root Folder, relative to Clay">Document Root Folder:</label>
					<input class="c-field" type="text" name="paths[web.path]" id="webpath" value="<?= $paths['web.path']; ?>" /> <br />
					<hr />
					<p class="u-bgcolor-info u-p@sm">Web Directory is relative to the Document root. This is the folder your index.php file is in.
					'test_site/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/</p>
					<label for="webdir" title="Name of this site's Web Directory, relative to Document Root">Web Directory:</label>
					<input class="c-field" type="text" name="paths[web.dir]" id="webdir" value="<?= $paths['web.dir']; ?>" /> <br />
					<hr />
					<p class="u-bgcolor-info u-p@sm">The parent path to your Applications Folder. The Applications Path is anything between Web Directory and the actual Applications Folder.
					'test_site/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/</p>
					<label for="appspath" title="Path to this site's Applications, relative to Web Directory">Applications Path:</label>
					<input class="c-field" type="text" name="paths[apps.path]" id="appspath" value="<?= $paths['apps.path']; ?>" /> <br />
					<hr />
					<p class="u-bgcolor-info u-p@sm">The Applications Folder is the folder where your applications reside.
					'applications/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/applications/</p>
					<label for="appsdir" title="Name of this site's Applications Directory, relative to Application Path">Applications Folder:</label>
					<input class="c-field" type="text" name="paths[apps.dir]" id="appsdir" value="<?= $paths['apps.dir']; ?>" />
					<hr />
					<p class="u-bgcolor-info u-p@sm">The parent path to your Themes Folder. The Themes Path is anything between Web Directory and the actual Themes Folder.
					'test_site/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/</p>
					<label for="themespath" title="Path to this site's Themes, relative to Web Directory">Themes Path:</label>
					<input class="c-field" type="text" name="paths[themes.path]" id="themespath" value="<?= $paths['themes.path']; ?>" /> <br />
					<hr />
					<p class="u-bgcolor-info u-p@sm">The Themes Folder is the folder where your themes reside.
					'themes/' resolves to <?= \clay\PATH.'/'.$paths['web.path']; ?>test_site/themes/</p>
					<label for="themesdir" title="Name of this site's Themes Directory, relative to Themes Path">Themes Folder:</label>
					<input class="c-field" type="text" name="paths[themes.dir]" id="themesdir" value="<?= $paths['themes.dir']; ?>" />
					<input type="hidden" name="paths[version]" value="<?= $version; ?>" id="version" />
				</div>
			</fieldset>
			<fieldset class="c-fieldset">
				<legend>Administrator Account</legend>
				<div class="c-fieldset__fields">
					<label for="uname" title="Admin Username">Admin Username:</label>
					<input class="c-field" type="text" name="admin[uname]" id="libs" value="" /> <br />
					<label for="email1" title="Administrator's Email">Email Address:</label>
					<input class="c-field" type="text" name="admin[email]" id="email1" value="" /> <br />
					<label for="passw1" title="Password">Password:</label>
					<input class="c-field" type="text" name="admin[passw1]" id="passw1" value="" /> <br />
					<label for="passw2" title="Password">Confirm Password:</label>
					<input class="c-field" type="text" name="admin[passw2]" id="passw2" value="" /> <br />
				</div>
			</fieldset>
			<fieldset class="c-fieldset__controls">
				<input type="hidden" name="site" value="<?= \clay\data\get('site','string','base',\clay\data\get('s','string','base')); ?>" id="site" />
				<p><input class="c-button" type="submit" name="submit" value="Install Clay <?= $version; ?>" id="submit" /></p>
			</fieldset>
		</form>
	</div>
</section>
<script>
	var packVue = new Vue({
		el: '#clay_install',
		data: function() {
			return {
				step_1: <?= ($new_db) ? 'true' : 'false'; ?>,
				step_2: <?= ($new_config) ? 'true' : 'false'; ?>,
				step: <?= ($new_db) ? 1 : 2; ?>
			}
    	},
		methods: {
			toggle: function(val){
				switch(val){
					case 1:
						if(this.step_1 == true){ this.step_1 = false; break; }
						if(this.step_1 == false){ this.step_1 = true; break; }
						break;
					case 2:
						if(this.step_2 == true){ this.step_2 = false; break; }
						if(this.step_2 == false){ this.step_2 = true; break; }
						break;
					default: break;
				}
				return true;
			}
		}
	})
</script>