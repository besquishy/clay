<section class="c-app">	
	<h1>Clay Upgrade</h1>
	<?php if(!empty($upgrade)){ ?>
	<h2>Upgraded from Clay <?= $config; ?> to Clay <?= $upgrade; ?></h2>
		<?php if(!empty($modules)){ ?>
	<h2>Modules</h2>
			<?php if(!empty($modules['installed'])){ ?>
		<h3>Installed</h3>
		<ul>
				<?php foreach($modules['installed'] as $installed){ ?>
			<li><?= $installed; ?></li>
				<?php } ?>
		</ul>
		<?php } else {  ?>
		<ul><li>No Modules Installed</li></ul>
		<?php } ?>
			<?php if(!empty($modules['upgraded'])){ ?>
		<h3>Upgraded</h3>
		<ul>
				<?php foreach($modules['upgraded'] as $upgraded){ ?>
			<li><?= $upgraded; ?></li>
				<?php } ?>
		</ul>
			<?php } else {  ?>
		<ul><li>No Modules Upgraded</li></ul>
			<?php } 
			unset($installed); unset($upgraded); 
		} ?>
		<?php if(!empty($applications)){ ?>
	<h2>Applications</h2>
			<?php if(!empty($applications['installed'])){ ?>
		<h3>Installed</h3>
		<ul>
				<?php foreach($applications['installed'] as $installed){ ?>
			<li><?= $installed; ?></li>
				<?php } ?>
		</ul>
			<?php } else { ?>
		<ul><li>No Applications Installed</li></ul>
			<?php } ?>
			<?php if(!empty($applications['upgraded'])){ ?>
		<h3>Upgraded</h3>
		<ul>
				<?php foreach($applications['upgraded'] as $upgraded){ ?>
			<li><?= $upgraded; ?></li>
				<?php } ?>
		</ul>
			<?php } else { ?>
		<ul><li>Not Applications Upgraded</li></ul>
			<?php } 
			unset($installed); unset($upgraded); 
		}
	} else { ?>

	<h3>Upgrade Clay from <?= $package['old']?> to <?= $package['new']?></h3>
	<form action="<?= \Installer\Application::URL('main', 'upgrade');?>" method="POST">
	<input type="hidden" value="1" name="init" id="init" />
	<input class="c-button" type="submit" value="Upgrade Now" name="submit" id="submit" />
	</form>
	<?php } ?>
</section>