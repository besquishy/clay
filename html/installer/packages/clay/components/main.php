<?php
namespace application\clay\component;
/**
 * Clay
 * 
 * An Application Development Platform built on the Clay Framework
 * @copyright (C) 2012-2017 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package Clay Installer Package
 */

/**
 * Clay Package Main Component
 * @author David
 *
 */
class main extends \Clay\Application\Component {
	
	/**
	 * Contains data from the configuration file of the installed site
	 * @var array
	 */
	public $config;
	
	/**
	 * Contains data from the clay/package.php file
	 * @var array
	 */
	public $package;
	
	/**
	 * Core Modules and Applications
	 * @var array
	 */
	
	public $core = array('modules' => array('Setup', 'Plugins', 'Users', 'Session', 'User', 'Roles', 'Privileges'),
						 'applications' => array('dashboard','plugins','apps','users','filters','system'));
	
	/**
	 * Page template
	 */
	public $page = 'installer';
	
	/**
	 * Populate our properties when the object is instantiated
	 */
	public function __construct(){
		
		$this->config = \installer::siteConfig(\installer\SITE,'config');
		$this->package = \installer\application::package('clay');
	}
	
	/**
	 * Default Action - Loads Install() or Upgrade Action
	 */
	public function view(){
		\Clay::Library('Scripts');
		\Clay\Scripts::addApplication('common','vue.min.js', 'head');
		$data = array();
		# If no configuration data exists, assume this is a new Install
		if(empty($this->config)){
			
			$data['output'] = $this->action('install');
			return $data;
		}
		# Installed version is older than package version, this is an upgrade.
		if(!empty($this->config) && $this->config['version'] < $this->package['version']){
			
			$data['output'] = $this->action('upgrade');
			return $data;
		}
		# Installed and up to date, just show configuration settings.
		$data['output'] = $this->action('configure');
		return $data;
	}
	
	/**
	 * Installer
	 * @return array
	 */
	protected function install(){
		# Pull in default path info
		$data['paths'] = \clay\application::api('clay','setup','paths');		
		# Check for existing databases for this Site
		if(!\clay\application::api('installer','setup','database',\installer\SITE)){
			# Site Config exists, but not Database Config file
			$data['new_config'] = false;
			$data['new_db'] = true;
			
		} else {
			# Site Config doesn't exist, but Database Config file does
			$data['new_config'] = true;
			$data['new_db'] = false;
		}
		# Get Clay's package version
		$data['version'] = \clay\application::api('clay','setup','version');
		# Load the Install package's Databases component
		$data['databases'] = \clay\application('installer','databases');
		# Use the Site action of Databases component
		$data['database'] = $data['databases']->action('site');
		# Check for error or other notifications stored in the session data
		$data['message'] = !empty($_SESSION['msg']) ? $_SESSION['msg'] : '';
		# Clear Notifications
		unset($_SESSION['msg']);
		# Return template data
		\Clay\Scripts::addApplication('common','vue.min.js','head');
		return $data;
	}
	
	/**
	 * Create a New Site - Form action handler from Install()
	 */
	public function create(){
		# Set Mode to Installer (for Callbacks, etc.)
		\Clay::$mode = 'Installer';
		# Fetch POST 'paths' data from the form
		$paths = \clay\data\post('paths');
		# Set paths to template data
		$data['paths'] = $paths;
		
		$apps = array();
		# Build applications path, from provided form data or defaults
		$apps['dir'] = !empty($paths['apps.dir']) ? $paths['apps.dir'] : 'applications/';
		$apps['path'] = !empty($paths['apps.path']) ? \clay\WEB_PATH.$paths['web.dir'].$paths['apps.path'].$apps['dir'] : \clay\WEB_PATH.$paths['web.dir'].$apps['dir'];
		# Set apps to template data
		$data['apps'] = $apps;
		# Fetch POST 'admin' data from the form
		$admin = \clay\data\post('admin');
		# Password comparison
		if($admin['passw1'] != $admin['passw2']){
			
			$_SESSION['msg'] = "Passwords did not match! Please re-enter the same password twice.";
			\clay::redirect($_SERVER['HTTP_REFERER']);
			
		} else {
			
			# Password Encryption
			$admin['key'] = md5(time().mt_rand());
			$admin['passw'] = hash_hmac('sha512', $admin['passw1'], $admin['key']);
			unset($admin['passw1']);
			unset($admin['passw2']);
		}
		# Site Configuration name - ie. default
		\claydb::$cfg = \installer\SITE;
		# Import Application Library - should be imported already, but just in case
		\Library('Clay/Application');
		# Load up the Module Setup API	
		$modSetup = \Clay\Module::API('Setup', 'Type', 'Module');
		# 2nd argument of install() - 1 = CORE(INSTALLED), 2 = INSTALLED, 3 = INITIATED (for installations that require actions), 4 = NOT INSTALLED(REMOVED)
		
		# Set our Modules path
		# $modSetup->path = $mods['path']; # Not yet optional
		
		# Install the Setup Module - created database tables for tracking Modules and Applications
		$modSetup->install('Setup', 1, array(), TRUE);
		# Install the rest of the Core module in order of dependency
		foreach($this->core['modules'] as $mod){
			# We manually installed Setup (above) with bypass option (TRUE)
			if($mod === 'Setup'){ continue; } 
			
			$modSetup->Install($mod, 1);
		}
		/*
		$modSetup->install('Plugins', 1);		
		$modSetup->install('Users', 1);		
		$modSetup->install('Session', 1);		
		$modSetup->install('User', 1);		
		$modSetup->install('Roles', 1);		
		$modSetup->install('Privileges', 1);		
		*/		
		# Add Guest User
	    $guestUserID = \Clay\Module::API('Users','Add',array( 'state' => 1,
	    													'username' => 'Guest',
	    													'email' => 'guest',
	    													'password' => 'guest'));
		# Add Admin User
	    $admin['uid'] = \Clay\Module::API('Users','Add',array( 'state' => 1,
	    													'username' => $admin['uname'],
	    													'email' => $admin['email'],
	    													'password' => $admin['passw']));
		# Add Admin Encryption Key
	    \Clay\Module\Users::Set($admin['uid'], 'key', $admin['key']);
		# Roles Module
		$roles = \Clay\Module::Object('Roles');
	    # Add Everyone Role
		$everyoneRoleID = $roles->Register('everyone', 1, 'General');
		# Add User Role
		$userRoleID = $roles->Register('user', 1, 'Members', $everyoneRoleID);
	    # Add Admin Role
		$admin['rid'] = $roles->Register('admin', 1, 'Administrators');
	    # Assign Admin User the Admin Role
	    $roles->addUser($admin['uid'], $admin['rid']);
	    # Assign Guest User the Everyone Role
	    $roles->addUser($guestUserID, $everyoneRoleID);
		# Load up the Application Setup API		
		$appSetup = \Clay\Module::API('Setup', 'Type', 'Application');
		# Set our Applications path
		$appSetup->path = $apps['path'];
		# Install the Core applications in order of dependency		
		foreach($this->core['applications'] as $app){
			
			$appSetup->Install($app, 1);
		}
		
		\Clay::$mode = 'Live';
		/*
		$appSetup->install('dashboard', 1);		
		$appSetup->install('plugins', 1);		
		$appSetup->install('blocks', 1);		
		$appSetup->install('apps', 1); 		
		$appSetup->install('users', 1);		
		$appSetup->install('filters', 1);		
		$appSetup->install('system', 1);
		*/
		# Save the Configuration Data to the config file
		\installer::setSiteConfig(\installer\SITE, 'config', $paths);
		# Redirect to Install() - which should redirect to Configuration
		\clay::redirect($_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Configuration Editor
	 * @return array
	 */
	protected function configure(){
		
		$data = array();
		$data['databases'] = \clay\application('installer', 'databases');
		$data['database'] = $data['databases']->action('site');
		$data['paths'] = \installer::siteConfig(\installer\SITE,'config');
		$data['version'] = \clay\application::api('clay', 'setup', 'version');
		return $data;
	}
	
	/**
	 * Update action for configuration()
	 * @return redirect (HTTP_REFERRER)
	 */
	public function update(){
		$paths = \clay\data\post('paths');
		\installer::setSiteConfig(\installer\SITE, 'config', $paths);
		\clay::redirect($_SERVER['HTTP_REFERER']);
	}
	
	/**
	 * Upgrade
	 * @return array
	 */
	public function upgrade(){
		
		$init = \Clay\Data\Post('init','string','string');
		$data = array();
				
		$config = \Installer::siteConfig(\installer\SITE, 'config');
		
		$package =  \Installer\Application::Package(\installer\PACKAGE);
		
		if(!empty($init)){
			
			if($package['version'] > $config['version']){
				
				$data['config'] = $config['version'];
				$data['upgrade'] = $package['version'];
				# Override ClayDB's Database Configuration
				\claydb::$cfg = \installer\SITE;
				# Import Application Library - should be imported already, but just in case
				\Library('Clay/Application');				
				/* 
				 * 2nd argument of Install() / 3rd argument of Upgrade():
				 * 1 = CORE(INSTALLED)
				 * 2 = INSTALLED
				 * 3 = INITIATED (for installations that require actions)
				 * 4 = NOT INSTALLED(REMOVED)
				*/
				# Core Module Installations/Upgrades
				# Load up the Module Setup API	
				$modSetup = \Clay\Module::API('Setup', 'Type', 'Module');
				# Template Data for Modules
				$data['modules'] = array();
				$data['modules']['installed'] = array();
				$data['modules']['upgraded'] = array();
				
				foreach($this->core['modules'] as $mod){
					# Find out File System version and Registered version
					$modInfo = \Clay\Module::Info($mod);
					$modVer = \Clay\Module::getVersion($mod);
					# Check for Installation
					if(!\Clay\Module::isInstalled($mod)){
						# Module is Not Installed
						$modSetup->Install($mod, 1);
						# Add Installation to Template Data
						$data['modules']['installed'][] = $mod;
						
					} else {
						# Module is Installed
						if($modInfo['version'] > $modVer){
							# Module Upgrade Available
							$modSetup->Upgrade($mod, 1, $modVer);
							$data['modules']['upgraded'][] = $mod;
						}
					}
				}
				# Core Application Installations/Upgrades
				# Load up the Application Setup API
				$appSetup = \Clay\Module::API('Setup', 'Type', 'Application');
				# Template Data for Applications
				$data['applications'] = array();
				$data['applications']['installed'] = array();
				$data['applications']['upgraded'] = array();
				# Apps' Path for Setup
				$appsDir = !empty($this->config['apps.dir']) ? $this->config['apps.dir'] : 'applications/';
				$appsPath = !empty($this->config['apps.path']) ? \clay\WEB_PATH.$this->config['web.dir'].$this->config['apps.path'].$this->config['dir'] : \clay\WEB_PATH.$this->config['web.dir'].$this->config['apps.dir'];
				define('Installer\APPS_PATH', $appsPath);
				$appSetup->path = $appsPath;
				
				foreach($this->core['applications'] as $app){
					# Find out File System version and Registered version
					$appInfo = \Clay\Application::Info('application', $app);
					$appVer = \Clay\Application::getVersion($app);
					# 
					//if($app == 'system') die(var_dump(\Clay\Application::Info('application', $app)));
					//if($app == 'system') die(var_dump(\Clay\Application::getVersion($app)));

					//if($app == 'system') die('installed = '.$appVer.' and new = '.$appInfo['version']);
					if(!\Clay\Application::getVersion($app)){
						# Application is Not Installed
						$appSetup->Install($app, 1);
						$data['applications']['installed'][] = $app;
					} else {
						# Application is Installed
						if($appInfo['version'] > $appVer){
							# Application Upgrade Available
							$appSetup->Upgrade($app, 1, $appVer);
							$data['applications']['upgraded'][] = $app;
						}
					}
				}
			}
			
			# Upgrade Site Configuration File
			$config['version'] = $package['version'];
			\Installer::setSiteConfig(\installer\SITE, 'config', $config);
			# Upgrade Installer Configuration File - use $config version in case it wasn't updated
			\Installer::Upgrade(\installer\SITE, $config['version']);
			
		} else {
			
			if($config['version'] < $package['version']){
				
				$data['package']['old'] = $config['version'];
				$data['package']['new'] = $package['version'];
			}
		}
		
		return $data;
	}
}