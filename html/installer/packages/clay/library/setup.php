<?php
namespace application\clay\library;
/**
 * Clay Content Management System
 * 
 * An Application Development Platform built on the Clay Framework
 * @copyright (C) 2012 David L Dyess II
 * @license GPL {@link http://www.gnu.org/licenses/gpl.html}
 * @link http://clay-project.com
 * @author David L Dyess II (david.dyess@gmail.com)
 * @package ClayCMS Installer Package
 */

/**
 * Clay Setup API
 * @author David
 *
 */
class setup {

	public static $path = null;

	public static function version(){
		$data = \installer\application::package('clay');
		return $data['version'];
	}

	/*public static function config(){
		return array (		# Use if you want to use an index.php file in /installer/
							#'web.dir' => 'installer/',
							'web.path' => 'html/',
	 						# conf value is only needed when no configuration file is specified
							//'conf' => 'installer',
							//'siteName' => 'Clay Installer',
		  					//'siteSlogan' => 'Web Site Manager',
		  					'themes.path' => 'claycms/',
		  					'theme' => 'ctx-1',
		  					'page' => 'default',
							'apps.dir' => 'packages/',
							'apps.path' => 'claycms/',
			  				'application' => 'installer',
			  				'component' => 'main',
			  				'action' => 'view');
	}*/

	public static function paths(){
		return array (	'libs.dir' => 'library/',
						'web.path' => 'html/',
						'web.dir' => '',
						'apps.path' => '',
						'apps.dir' => 'applications/',
		 				'themes.path' => '',
						'themes.dir' => 'themes/'
		);
	}
}
?>