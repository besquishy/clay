<section class="c-app">
	<div class="c-app__head"><h1>Help System</h1></div>
	<div class="c-app__content">
			<p class="u-bgcolor-info u-p@sm">We are still working on documentation for the Clay Framework and the Clay Installer.</p>
			<p class="u-bgcolor-container u-p@sm">Most of the resources will be online at clay-project.com.</p>
			<h4><a href="<?= \installer\application::url('help','definitions'); ?>">Definitions</a></h4>
	</div>
