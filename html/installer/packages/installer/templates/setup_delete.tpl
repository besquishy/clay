<section class="c-app">
	<div class="c-app__head"><h1>Delete a Site Installation</h1></div>
	<div class="c-app__content">
	<?php if(!empty($message)){?><h3><?= $message ;?></h3><?php } ?>
	<form method="post" action="?com=setup&act=delete">
		<fieldset class="c-fieldset"><legend>Delete <?= $site; ?>?</legend>
			<div class="c-fieldset__controls">
				<p class="u-bgcolor-info u-p@sm">Configuration files will be backed up to the data/backups folder.</p>
				<p class="u-bgcolor-danger u-p@sm">Are you sure you want to delete this package installation?</p>
				<input type="hidden" name="site" id="site" value="<?= $site; ?>" /> <input type="hidden" name="confirm" id="confirm" value="1" />
				<input class="c-button" type="submit" name="submit" value="Yes, Delete <?= $site; ?>" id="submit" />
			</div>
		</fieldset>
	</form>
	</div>
</section>