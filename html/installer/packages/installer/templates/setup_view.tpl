<section class="c-app">
	<style type="text/css">
	.package {float:left;width:200px;padding:10px;margin:2px;border: 3px solid white;cursor:pointer;}
	.package:hover{border:3px solid #9ED0D0;}
	</style>
	<div class="c-app__head"><h1>Site Installations</h1></div>
	<div class="c-app__content" id="packages">
	<?php if(!empty($message)){?><h3><?= $message ;?></h3><?php } ?>

		<div>
			<form method="post" action="?com=setup&act=add">
				<fieldset class="c-fieldset"><legend>New Installation</legend>
					<div class="c-fieldset__fields">
						<p>Clay utilizes a packages system, which allows developers to shape the Clay Framework to their purpose. </p>
						<?php if(!empty($pkgs)){ ?>
						<p>
						<label for="new_conf" title="Create a new website configuration.">Name:</label>
						<input class="c-field" type="text" name="new_conf" id="new_conf" maxlength="64" value="" />
						</p>
						<h5>Select a package to install:</h5>
						<?php foreach($pkgs as $pkg){?>
						<div v-on:click="setPackage('<?= $pkg['name']; ?>', '<?= $pkg['version']; ?>')" class="package" v-bind:class="classObject('<?= $pkg['name']; ?>')" id="<?= $pkg['name']; ?>" title="<?= $pkg['version']; ?>">
						<?= $pkg['title'].' '.$pkg['version'];?>
						<p><?= $pkg['description']; ?></p>
						</div>
						<?php } ?>
						<p style="clear:both;">
						<label for="pkgdisplay" title="Package to install.">Package: <span class="u-color-success " v-if="package">{{ package }}</span></label>
						</p>
						<input type="hidden" name="pkgver" id="pkgver" v-model="version" /> <input type="hidden" name="pkgchoice" id="pkgchoice" v-model="package" />
						<input class="c-button" type="submit" name="submit" value="Create" id="submit" />
						<?php } else { ?>
						<div><h2>Oops!</h2>No Installation packages were found. Please add a package to the [web]/installer/packages/ folder.</div>
						<?php } ?>
					</div>
				</fieldset>
			</form>
		</div>
		<div>
			<fieldset class="c-fieldset"><legend>Installations</legend>
				<div class="c-fieldset__fields">
					<?php if(!empty($confs)) { foreach($confs as $conf => $pkg){?>
					<div class="u-bgcolor-container u-p"><h6><?= $conf; ?> (<?= $pkg['package'].' '.$pkg['version'];?>)</h6>
						<a href="<?= \installer\application::url(array('s' => $conf)); ?>" class="c-button c-button--sm c-button--primary"><?= !empty($pkg['update']) ? 'Upgrade to '.$pkg['update'] : 'Setup'; ?></a> 
						<a href="<?= \installer\application::url('setup','delete',array('site' => $conf)); ?>" class="c-button c-button--sm c-button--danger">Delete</a> 
						<a v-on:click="rename = '<?= $conf; ?>'" class="c-button c-button--sm c-button--info" rel="<?= $conf; ?>">Rename</a>
					</div>
					<div v-if="rename == '<?= $conf; ?>'" class="rename rename-<?= $conf; ?>">
						<form method="post" action="?com=setup&act=rename" id="rename-<?= $conf; ?>" class="rename">
							<fieldset class="c-fieldset c-fieldset__controls"><legend>Rename <?= $conf; ?></legend>
								<p>
									<label for="new_conf" title="Rename a website configuration.">Name:</label>
									<input class="c-field" type="text" name="new_conf" id="new_conf" maxlength="64" value="" />
								</p>
								<input type="hidden" name="old_conf" id="old_conf" value="<?= $conf; ?>" />
								<input class="c-button" type="submit" name="submit" value="Rename" id="submit-rename" />
								<button v-on:click.prevent="rename = false" class="c-button c-button--danger">Cancel</button>
							</fieldset>
						</form>
					</div>
		<?php } } else { echo "Please create an installation."; }?>
				</div>
			</fieldset>
		</div>
		<div>
			<fieldset class="c-fieldset"><legend>Restore an Installation</legend>
				<div class="c-fieldset__fields">
					<?php if(!empty($backups)) { foreach($backups as $backup){?>
					<p><?= $backup['name']; ?> (<?= $backup['package'].' '.$backup['version'];?>) <a href="<?= \installer\application::url('setup','restore',array('site' => $backup['name'])); ?>" class="c-button c-button--sm c-button--accent">Restore</a>  <a href="<?= \installer\application::url('setup','delbackup',array('site' => $backup['name'])); ?>"  class="c-button c-button--sm c-button--danger">Delete</a></p>
					<?php } } else { ?><p>No restorable backups could be found.</p> <?php }?>
				</div>
			</fieldset>
		</div>
	</div>
</section>
<script>
	var packVue = new Vue({
		el: '#packages',
		data: function() {
			return {
				package: false,
				version: false,
				rename: false
			}
		},
		methods: {
			setPackage: function (package, version, e) {
				this.package = package;
				this.version = version;
			},
			classObject: function (package) {
				return {
					'u-bgcolor-primary': this.package === package
				}
			}
		},
		computed: {
			
		}
	})
</script>