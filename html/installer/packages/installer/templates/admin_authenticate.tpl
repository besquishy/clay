<section class="c-app">
  <div class="c-app__head"><h1>Authenticate</h1></div>
  <div class="c-app__content" id="auth">
    <form method="post" action="?app=installer">
      <fieldset class="c-fieldset"><legend>Security Check</legend>
        <div class="c-fieldset__fields">
          <p>
          <label for="pass1" title="Password">Password:</label>
          <input class="c-field" type="password" name="passcode" id="pass1" maxlength="128" />
          </p>  
        </div>
      </fieldset>
      <fieldset class="c-fieldset__controls">
        <input class="c-button" type="submit" name="submit" value="Authenticate" id="submit" /> 
        <input v-if="reset == false" v-on:click="reset = true" class="c-button" type="button" class="recover" value="Reset Passcode" id="reset" />
      </fieldset>
    </form>
    <form v-if="reset" method="post" class="recovery" action="?app=installer">
      <fieldset class="c-fieldset"><legend>Q&amp;A</legend>
        <div class="c-fieldset__fields">
          <p>
          <label for="q" title="Question">Question:</label>
          <?= $question; ?>
          </p>
          <p>
          <label for="a" title="Answer">Answer:</label>
          <input class="c-field" type="text" name="answer" id="a" maxlength="200" />
          </p>
        </div>
      </fieldset>
      <fieldset class="c-fieldset__controls">
        <input class="c-button" type="submit" name="submit" value="Evaluate" id="submit" /> 
        <input v-on:click="reset = false" type="button" class="c-button" value="Cancel" id="recovery" />
      </fieldset>
    </form>
  </div>
</section>
<script>
	var packVue = new Vue({
		el: '#auth',
		data: function() {
			return {
				reset: false
			}
    }
	})
</script>