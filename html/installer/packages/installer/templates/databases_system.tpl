<section class="c-app">
	<div class="c-app__head"><h1>System Databases</h1></div>
	<div class="c-app__content" id="sysdb">
	<?php if(!empty($message)){?><h3><?= $message ;?></h3><?php } ?>
	<?php if(!empty($nodbs)){?>
		<h3 class="u-bgcolor-danger">No System Databases have been stored!</h3>
	<?php }?>
	<div>
		<form method="post" action="?com=databases&act=add">
			<fieldset class="c-fieldset"><legend>Add a New System Database</legend>
				<div class="c-fieldset__fields">
					<table>
						<tr><td class="align-right">
							<label for="dbhost" title="">Database Driver:</label>
						</td><td>
							<!--  <input class="c-field" type="text" name="dbs[dbtype]" id="dbtype" maxlength="64" value="" /> -->
							<select class="c-field" name="dbs[dbtype]" id="dbtype">
								<?php foreach($dbexts as $db => $driver){
								if(!empty($driver)) echo '<option value="'.$driver.'">'.$db.'</option>';
								} ?>
								<option value="" selected>Select a DB Extension</option>
							</select>

						</td></tr>
						<tr><td class="align-right">
							<label for="dbhost" title="">Database Host:</label>
						</td><td>
							<input class="c-field" type="text" name="dbs[dbhost]" id="dbhost" maxlength="64" value="" />
						</td></tr>
						<tr><td class="align-right">
							<label for="dbuser" title="">Database User:</label>
						</td><td>
							<input class="c-field" type="text" name="dbs[dbuser]" id="dbuser" maxlength="64" value="" />
						</td></tr>
						<tr><td class="align-right">
							<label for="dbpass" title="">Database Password:</label>
						</td><td>
							<input class="c-field" type="password" name="dbs[dbpass]" id="dbpass" maxlength="64" value="" />
						</td></tr>
					</table>
				</div>
			</fieldset>
			<fieldset class="c-fieldset__controls">
				<input class="c-button" type="submit" name="submit" value="Add" id="submit" />
			</fieldset>
		</form>
	</div>
	<?php
	if(!empty($dbs)){
	foreach($dbs as $key => $value){ ?>
	<div>
		<form method="post" action="?com=databases&act=update" class="db<?= $key; ?>">
			<fieldset class="c-fieldset"><legend>db<?= $key;?></legend>
				<div class="c-fieldset__fields">
					<table>
						<tr><td class="align-right">
							<label for="dbhost" title="">Database Driver:</label>
						</td><td>
							<!--  <input class="c-field" type="text" name="dbs[dbtype]" id="dbtype<?= $key;?>" maxlength="64" value="<?= $value['type'];?>" /> -->
							<select class="c-field" name="dbs[dbtype]" id="dbtype<?= $key;?>">
								<?php foreach($dbexts as $db => $driver){
								$status = ($value['type'] == $driver) ? ' selected' : '';
								if(!empty($driver)) echo '<option value="'.$driver.'"'.$status.'>'.$db.'</option>';
								if(!empty($status)) $statuschng = 1;
								}
								if(empty($statuschng)) echo '<option value="" selected>Select a DB Extension</option>'; ?>
							</select>
						</td></tr>
						<tr><td class="align-right">
							<label for="dbhost" title="">Database Host:</label>
						</td><td>
							<input class="c-field" type="text" name="dbs[dbhost]" id="dbhost<?= $key;?>" maxlength="64" value="<?= $value['host'];?>" />
						</td></tr>
						<tr><td class="align-right">
							<label for="dbuser" title="">Database User:</label>
						</td><td>
							<input class="c-field" type="text" name="dbs[dbuser]" id="dbuser<?= $key;?>" maxlength="64" value="<?= $value['usern'];?>" />
						</td></tr>
						<tr><td class="align-right">
							<label for="dbpass" title="">Database Password:</label>
						</td><td>
							<input class="c-field" type="password" name="dbs[dbpass]" id="dbpass<?= $key;?>" maxlength="64" value="<?= $value['passw'];?>" />
						</td></tr>
					</table>
				</div>
			</fieldset>
			<fieldset class="c-fieldset__controls">
				<input type="hidden" name="dbindex" id="dbindex<?= $key; ?>" value="<?= $key; ?>" />
				<input class="c-button" type="submit" name="submit" value="Update" id="submit" /> 
				<input v-on:click="delDB = 'db_<?= $key; ?>'" class="c-button" type="button" name="delete" value="Delete" id="db<?= $key; ?>" class="delete" />
			</fieldset>
		</form>
		<form v-if="delDB == 'db_<?= $key; ?>'" method="post" action="?com=databases&act=delete" class="deletion deldb<?= $key; ?>">
			<fieldset class="c-fieldset"><legend>Delete db<?= $key;?>?</legend>
				<div class="c-fieldset__controls">
					<p>Are you sure you want to delete this from the system?</p>
					<input type="hidden" name="dbindex" id="dbindex<?= $key; ?>" value="<?= $key; ?>" /> <input type="hidden" name="dbs" id="dbs<?= $key; ?>" value="1" />
					<input class="c-button" type="submit" name="submit" value="Yes, Delete It" id="submit" /> 
					<input v-on:click="delDB = false" class="c-button" type="button" name="cancel" value="Cancel" id="db_<?= $key; ?>" class="cancel" />
				</div>
			</fieldset>
		</form>
	</div>

	<?php }
	}
	?>
	</div>
</section>
<script>
	var packVue = new Vue({
		el: '#sysdb',
		data: function() {
			return {
				delDB: false
			}
    	}
	})
</script>