<section class="c-app">
	<div class="c-app__head"><h1>Restore a Site Installation</h1></div>
	<div class="c-app__content">
	<?php if(!empty($message)){?><h3><?= $message ;?></h3><?php } ?>
	<form method="post" action="?com=setup&act=restore">
		<fieldset class="c-fieldset"><legend>Restore <?= $site; ?>?</legend>
			<div class="c-fieldset__controls">
				<p class="u-bgcolor-danger u-p@sm">If an installation already exists named <?= $site; ?>, it will be replaced by the backup.</p>
				<p class="u-bgcolor-danger u-p@sm">Are you sure you want to restore this package installation backup?</p>
				<input type="hidden" name="site" id="site" value="<?= $site; ?>" /> <input type="hidden" name="confirm" id="confirm" value="1" />
				<input class="c-button" type="submit" name="submit" value="Yes, Restore <?= $site; ?>" id="submit" />
			</div>
		</fieldset>
	</form>
	</div>
</section>