<section class="c-app">
  <div class="c-app__head"><h1>Installer Setup</h1></div>
  <div class="c-app__content">
  <h2>Welcome</h2>
  <p class="u-bgcolor-container u-p@sm">Welcome to the Clay Unified Installer. This installer is used to install packages, built using the Clay Framework and associated API. 
  Here you will setup the installer and secure it for future access. You will then be able to install packages provided to the installer.
  </p>
  <p class="u-bgcolor-container u-p@sm">
  Most packages will use this installer for minimal installation, providing more in-depth setup options within the packages' application 
  platform. Some packages are utility packages, those will require configuration changes directly from the Installer.
  </p>
  <?php if(!empty($error)) {
  if(!empty($error['data'])) { ?>
  <h3>Setup Assistance:</h3>
  <p class="u-bgcolor-danger u-p@sm">The Installer can not write to <?= \clay\DATA_PATH; ?>.</p>
  <p class="u-bgcolor-info u-p@sm">If this is the first time running the Clay Installer, you will need to create the folder <?= \clay\DATA_PATH; ?> before the Installer can be initiated.</p>
  <p class="u-bgcolor-info u-p@sm">You must give the server permission to write to <?= \clay\DATA_PATH; ?> before the Installer can be initiated.</p>
  <?php } if(!empty($error['config'])) { ?>
  <h3>Setup Assistance:</h3>
  <p class="u-bgcolor-danger u-p@sm">The Installer can not write to <?= \clay\CFG_PATH; ?>.</p>
  <p class="u-bgcolor-container u-p@sm">You must give the server permission to write to <?= \clay\CFG_PATH; ?> before the Installer can be initiated.</p>
  <p class="u-bgcolor-info u-p@sm">Note: You can also delete this directory and allow the server to create its own.</p>
  <?php } } else { ?>
  <h2>Step 1:</h2>
  <p class="u-bgcolor-info u-p@sm">Please verify the paths below are accurate. Also, please physically ensure the Data path and any subfolders within it are writable or owned by the web server.</p>
    <h3>File Structure</h3>
    <p class="u-bgcolor-container u-p@sm">Clay path is <?= \clay\PATH; ?></p>
    <p class="u-bgcolor-container u-p@sm">Data path is <?= \clay\DATA_PATH; ?> and <?= $data_dir_status; ?></p>
    <p class="u-bgcolor-container u-p@sm">Data Configuration path is <?= \clay\CFG_PATH; ?> and <?= $cfg_dir_status; ?></p>
    <p class="u-bgcolor-container u-p@sm">Web path is <?= \clay\WEB_PATH; ?></p>
  <h2>Step 2:</h2>
  <p class="u-bgcolor-info u-p@sm">
  Please provide a Password (which you will need to access the Installer) and a Passkey (used to encrypt your Password).
  </p>
  <form method="post" action="<?= \installer\application::url('admin','setup'); ?>">
  <fieldset class="c-fieldset"><legend>Installer Security</legend>
    <div class="c-fieldset__fields">
      <p class="u-bgcolor-container u-p@sm">
        Please enter and confirm a Passsword to secure the Installer. This is a security measure to prevent unauthorized individuals from accessing the Unified Installer.
      </p>
      <?php if(!empty($msg)){ echo "<p><h3>$msg</h3></p>"; } ?>
      <p>
        <label for="pass1" title="Password">Password:</label>
        <input class="c-field" type="password" name="pass" id="pass1" maxlength="128" />
      </p>
      <p>
        <label for="pass2" title="Passcode">Confirm Password:</label>
        <input class="c-field" type="password" name="passconf" id="pass2" maxlength="128" />
      </p>
      <p class="u-bgcolor-container u-p@sm">Please enter a Passkey to secure the Installer. The Passkey is used for encryption only and will not be required the next time you access the Unified Installer.</p>
      <p>
        <label for="key1" title="Encryption Key">Passkey:</label>
        <input class="c-field" type="password" name="key" id="key1" maxlength="128" />
      </p>
    </div>
  </fieldset>
  <p class="u-bgcolor-info u-p@sm">
    Please enter a Recovery Question and Answer set for resetting your password.
  </p>
  <fieldset class="c-fieldset"><legend>Password Recovery</legend>
    <div class="c-fieldset__fields">
      <p class="u-bgcolor-container u-p@sm">
        Your question will be displayed during password recovery. Consider this Question and Answer set as being as sensitive as your actual Password.
      </p>
      <p>
        <label for="pass1" title="Recovery Question">Question:</label>
        <input class="c-field" type="text" name="question" id="quest1" maxlength="200" />
      </p>
      <p>
        <label for="pass2" title="Recovery Answer">Answer:</label>
        <input class="c-field" type="text" name="answer" id="answ2" maxlength="200" />
      </p>
      <input type="hidden" name="initiate" value="true" id="iniedit" />
      <input class="c-button" type="submit" name="submit" value="Submit" id="submit" />
    </div>
  </fieldset>
  </form>
  <h2>Step 3:</h2>
  <p class="u-bgcolor-success u-p@sm">1..2..3... GO!</p>
  <?php } ?>
  </div>
</section>