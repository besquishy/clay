<section class="c-app">
  <div class="c-app__head"><h1>Ending Session</h1></div>
  <div class="c-app__content">
    <form method="post" action="?com=admin&act=logout">
      <fieldset class="c-fieldset"><legend>Logout</legend>
        <div class="c-fieldset__fields">
          <label for="pass1" title="Passcode">Sign out:</label>
          <input class="c-button" type="hidden" name="logout_confirm" id="lo_confirm" maxlength="64" value="1" /> <input class="c-button" type="submit" name="submit" value="Good Bye" id="submit" />
        </div>
      </fieldset>
    </form>
  </div>
</section>