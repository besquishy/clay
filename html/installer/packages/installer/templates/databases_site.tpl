<h2>Site Databases for <?= \clay\data\get('site','string','',\clay\data\get('s','string','','Unknown')); ?></h2>

<?php if(!empty($nodbs)){?>
	<h3 class="alert">No System Databases have been stored!</h3>
<?php }?>
<div>
	<form method="post" action="?com=databases&act=add">
		<fieldset class="c-fieldset"><legend>Add a New System Database</legend>
			<div class="c-fieldset__fields">
				<table>
					<tr><td class="align-right">
						<label for="dbhost" title="">Database Driver:</label>
					</td><td>
						<!--  <input class="c-field" type="text" name="dbs[dbtype]" id="dbtype" maxlength="64" value="" /> -->
						<select class="c-field" name="dbs[dbtype]" id="dbtype">
							<?php foreach($dbexts as $db => $driver){
							if(!empty($driver)) echo '<option value="'.$driver.'">'.$db.'</option>';
							} ?>
							<option value="" selected>Select a DB Extension</option>
						</select>

					</td></tr>
					<tr><td class="align-right">
						<label for="dbhost" title="">Database Host:</label>
					</td><td>
						<input class="c-field" type="text" name="dbs[dbhost]" id="dbhost" maxlength="64" value="" />
					</td></tr>
					<tr><td class="align-right">
						<label for="dbuser" title="">Database User:</label>
					</td><td>
						<input class="c-field" type="text" name="dbs[dbuser]" id="dbuser" maxlength="64" value="" />
					</td></tr>
					<tr><td class="align-right">
						<label for="dbpass" title="">Database Password:</label>
					</td><td>
						<input class="c-field" type="password" name="dbs[dbpass]" id="dbpass" maxlength="64" value="" />
					</td></tr>
				</table>
			</div>
		</fieldset>
		<fieldset class="c-fieldset__controls">
			<input class="c-button" type="submit" name="submit" value="Add" id="submit" />
		</fieldset>
	</form>
</div>

<div>
	<?php if(!empty($dbs)){ if(empty($cfg)) {?> <h3 class="alert">No Site Databases have been stored!</h3> 
	<p class="u-bgcolor-danger u-p@sm">
		ClayDB requires the primary Database Configuration Name to be 'default'. Please create the 'default' database configuration.
	</p>
	<?php }?>
	<form method="post" action="?com=databases&act=add">
		<fieldset class="c-fieldset"><legend>Choose a Database for this site</legend>
			<div class="c-fieldset__fields">
				<table>
					<tr><td class="align-right">
						<label for="dbhost" title="">DB Configuration Name:</label>
					</td><td>
						<input class="c-field" type="text" name="dbcfg" maxlength="64" value="default" />
					</td></tr>
					<tr><td class="align-right">
						<label for="dbuser" title="">Table Prefix:</label>
					</td><td>
						<input class="c-field" type="text" name="prefix" maxlength="64" value="" placeholder="example: cl" />
					</td></tr>
					<tr><td class="align-right">
						<label for="dbpass" title="">Database Server:</label>
					</td><td>
						<select class="c-field" name="dbcon">
	<?php foreach($dbs as $key => $value){?>
							<option value="<?= $key; ?>"><?= $value['type'] .' :: '. $value['usern'] . '@' . $value['host']; ?></option>
	<?php }?>
							<option value="" selected>Select a Connection</option>
						</select>
					</td></tr>
					<tr><td class="align-right">
						<label for="dbname" title="">Database Name:</label>
					</td><td>
						<input class="c-field" type="text" name="dbname" maxlength="64" value="" placeholder="example: <?= \clay\data\get('site','string','',\clay\data\get('s','string')); ?>" />
					</td></tr>
				</table>
			</div>
		</fieldset>
		<fieldset class="c-fieldset__controls">
			<input type="hidden" name="site" value="<?= \clay\data\get('site','string','base',\clay\data\get('s','string','base')); ?>" />
			<input class="c-button" type="submit" name="submit" value="Add" />
		</fieldset>
	</form>
</div>
<?php }
if(!empty($cfg)){
foreach($cfg as $key => $value){ ?>
<div>
	<form method="post" action="?com=databases&act=update">
		<fieldset class="c-fieldset"><legend><?= $key;?></legend>
			<div class="c-fieldset__fields">
				<table>
					<tr><td class="align-right">
						<label for="dbhost" title="">DB Configuration Name:</label>
					</td><td>
						<input class="c-field" type="text" name="dbcfg" maxlength="64" value="<?= $key; ?>" />
					</td></tr>
					<tr><td class="align-right">
						<label for="dbuser" title="">Table Prefix:</label>
					</td><td>
						<input class="c-field" type="text" name="prefix" maxlength="64" value="<?= $value['prefix']; ?>" />
					</td></tr>
					<tr><td class="align-right">
						<label for="dbpass" title="">Database Server:</label>
					</td><td>
						<select class="c-field" name="dbcon">
	<?php foreach($dbs as $dbkey => $dbvalue){
	if($dbkey == $value['connection']){?>
							<option value="<?= $dbkey; ?>" selected="selected"><?= $dbvalue['type'] .' :: '. $dbvalue['usern'] . '@' . $dbvalue['host']; ?></option>
	<?php } else { ?>
							<option value="<?= $dbkey; ?>"><?= $dbvalue['type'] .' :: '. $dbvalue['usern'] . '@' . $dbvalue['host']; ?></option>
	<?php } }?>
						</select>
					</td></tr>
					<tr><td class="align-right">
						<label for="dbname" title="">Database Name:</label>
					</td><td>
						<input class="c-field" type="text" name="dbname" maxlength="64" value="<?= $value['database']; ?>" />
					</td></tr>
				</table>
			</div>
		</fieldset>
		<fieldset class="c-fieldset__controls">
			<input type="hidden" name="site" id="site" value="<?= $site; ?>" />
			<input class="c-button" type="submit" name="submit" value="Update" />
		</fieldset>
	</form>
</div>
<?php }
} ?>