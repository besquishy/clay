<section class="c-app">
	<div class="c-app__head"><h1>Delete a Site Backup</h1></div>
	<div class="c-app__content">
	<?php if(!empty($message)){?><h3><?= $message ;?></h3><?php } ?>
	<form method="post" action="?com=setup&act=delbackup">
		<fieldset class="c-fieldset"><legend>Delete backup of <?= $site; ?>?</legend>
			<div class="c-fieldset__controls">
				<p class="u-bgcolor-danger u-p@sm">Are you sure you want to delete this package installation?</p>
				<input type="hidden" name="site" id="site" value="<?= $site; ?>" /> <input type="hidden" name="confirm" id="confirm" value="1" />
				<input class="c-button" type="submit" name="submit" value="Yes, Delete <?= $site; ?>" id="submit" />
			</div>
		</fieldset>
	</form>
	</div>
</section>