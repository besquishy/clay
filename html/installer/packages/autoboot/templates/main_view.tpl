<section class="c-app">
	<div class="c-app__head"><h1>Autoboot - Boot Selector Utility</h1></div>
	<div class="c-app__content">
		<p class="u-bgcolor-container u-p@sm">
			This is a simple utitity package to be used as the 'default' site configuration. It allows you to use a Multisite configuration
			where you are able to host more than one domain, using a single copy of Clay Framework and associated platforms.
		</p>
		<p class="u-bgcolor-container u-p@sm">
			Note: It is not recommended nor necessary to have 'www.' in the domain name.
		</p>
		<h3>Configurations</h3>
		<form method="post" action="<?= \installer\application::url('main','create'); ?>" class="configs">
			<fieldset class="c-fieldset">
				<legend>Domain Names</legend>
				<div class="c-fieldset__fields">
					<?php if(!empty($configs)) { $inid = 0; $index = 0; foreach($configs as $domain => $config){ ?>
					<p>
						<label>Domain Name</label>
						<input class="c-field" type="text" name="conf[<?= $index; ?>][name]" id="<?= 'id'.$inid++; ?>" value="<?= $domain; ?>" />
						<label>Configuration Name</label>
						<input class="c-field" type="text" name="conf[<?= $index; ?>][conf]" id="<?= 'id'.$inid++; ?>" value="<?= $config; ?>" />
					</p>
					<?php $index++; } } else { ?>
					<p class="u-bgcolor-info u-p@sm">No Domain settings exist!</p>
					<?php } ?>
				</div>
			</fieldset>
			<fieldset class="c-fieldset">
				<legend>New Domain (optional)</legend>
				<div class="c-fieldset__fields">
					<p>
						<label>New Domain Name</label>
						<input class="c-field" type="text" name="conf[new][name]" id="<?= 'id'.$inid++; ?>" value="" />
						<label>Configuration Name</label>
						<input class="c-field" type="text" name="conf[new][conf]" id="<?= 'id'.$inid++; ?>" value="" />
					</p>
				</div>
			</fieldset>
			<fieldset class="c-fieldset__controls">
				<p>
					<input class="c-button" type="submit" name="submit" value="Update Configurations" id="submit" />
				</p>
			</fieldset>
		</form>
	</div>
</section>