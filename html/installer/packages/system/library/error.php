<?php
namespace application\system\library;
/**
 * Utility for catching PHP errors and creates an alert at runtime
 * Based on ExceptionThrower class by Jason Hinkle
 * @author Jason Hinkle, David Dyess II
 * @copyright  1997-2011 VerySimple, Inc. (original), 2015 David Dyess II (derived)
 * @license    http://www.gnu.org/licenses/lgpl.html  LGPL
 * @version 1.0
 */
class error {
  # Display deprecated messages (on by default)
	public static $IGNORE_DEPRECATED = false;

	/**
	 * Start redirecting PHP errors
	 * @param int $level PHP Error level to catch (Default = E_ALL & ~E_DEPRECATED)
	 */
	public static function Start($level = null) {

		if ($level == null) {
      # We only support PHP 5.4+, so no need to check for this
			/*if (defined("E_DEPRECATED")) {*/
				$level = E_ALL & ~E_DEPRECATED ;
			/*}
			else
			{
				// php 5.2 and earlier don't support E_DEPRECATED
				$level = E_ALL;
				self::$IGNORE_DEPRECATED = true;
			}*/
		}
		set_error_handler("\application\system\library\\error::HandleError", $level);
	}

	/**
	 * Stop redirecting PHP errors
	 */
	public static function Stop()	{
		restore_error_handler();
	}

	/**
	 * Fired by the PHP error handler function.  Calling this function will
	 * always throw an exception unless error_reporting == 0.  If the
	 * PHP command is called with @ preceeding it, then it will be ignored
	 * here as well.
	 *
	 * @param string $code
	 * @param string $string
	 * @param string $file
	 * @param string $line
	 * @param string $context
	 */
	public static function HandleError ($error, $message, $file, $line, $context) {
		/// ignore supressed errors
		if (error_reporting() == 0) return;
		if (self::$IGNORE_DEPRECATED && strpos($string,"deprecated") === true) return true;

		self::record(array('date' => \date('D, d M Y H:i:s O'), 'error' => $error, 'message' => $message, 'file' => $file, 'line' => $line, 'context' => $context, 'times' => 1));
	}
  /**
   * Save an error message
   * @param array $error (see HandleError)
   */
  public static function record ($error) {
			/*start:
			$key = rand();
			if(!empty($_SESSION['error'][$key])) goto start;
      $_SESSION['error'][$key] = $error;*/
			if(file_exists(\clay\LOG_PATH.'errors.php')){

				include(\clay\LOG_PATH.'errors.php');
			}
			# Determine if this an Object from Exception or an Error message
			if( is_object( $error )){
				# Exception Message:
				$checksum = md5($error->getMessage().$error->getFile().$error->getLine());
				if(!empty($data[$checksum])){

					$data[$checksum]['times']++;

				} else {
					$exception = array(
						'error' => 'Exception',
						'message' => $error->getMessage(),
						'file' => $error->getFile(),
						'context' => $error->getTraceAsString(),
						'date' => \date("Y-m-d H:i:s"),
						'line' => $error->getLine(),
						'times' => 1
					);
					$data[$checksum] = $exception;
				}

			} else {
				# Error Message:
				$checksum = md5($error['error'].$error['message'].$error['file'].$error['line']);

				if(!empty($data[$checksum])){

					$data[$checksum]['times']++;

				} else {
					$error['error'] = self::code($error['error']);
					$data[$checksum] = $error;
				}
			}

			if(!empty($data[$checksum]['context'])){
				$data[$checksum]['context'] = print_r($data[$checksum]['context'],1);
			}

			$content = "<?php\n" . '$data = ' . var_export($data,1).";\n ?>";
			$file = fopen(\clay\LOG_PATH.'errors.php', "w");

			if(fwrite($file, $content)){

				fclose($file);
				return true;

			} else {
				# Exception would be nice here? //throw new Exception('clay::setConfig() was unable to write to '.$config.'. Please check file permissions.');
				return false;
			}
  }
	/**
	 * Get errors
	 */
	public static function get(){

		if(file_exists(\clay\LOG_PATH.'errors.php')){

			include(\clay\LOG_PATH.'errors.php');
		}
		return !empty($data) ? $data : NULL;
	}

	/**
   * Clear error messages
   * @param array $id (message id)
   */
  public static function clear ($id = NULL) {

		if(file_exists(\clay\LOG_PATH.'errors.php')){

			include(\clay\LOG_PATH.'errors.php');
		}

		if(!is_null($id)){

			unset($data[$id]);

		} else {

			$data = array();
		}

		$content = "<?php\n" . '$data = ' . var_export($data,1).";\n ?>";
		$file = fopen(\clay\LOG_PATH.'errors.php', "w");

		if(fwrite($file, $content)){

			fclose($file);
			return true;

		} else {
			# Exception would be nice here? //throw new Exception('clay::setConfig() was unable to write to '.$config.'. Please check file permissions.');
			return false;
		}
  }

	public static function code($errno){

		switch($errno){
			case 1: return 'Fatal Error'; break;
			case 2: return 'Warning'; break;
			case 4: return 'Parsing Error'; break;
			case 8: return 'Notice'; break;
			case 16: return 'Core Error'; break;
			case 32: return 'Core Warning'; break;
			case 64: return 'Compile Error'; break;
			case 128: return 'Compile Warning'; break;
			case 256: return 'User Error'; break;
			case 512: return 'User Warning'; break;
			case 1024: return 'User Notice'; break;
			case 2048: return 'Runtime Notice'; break;
			case 4096: return 'Catchable Fatal Error'; break;
			case 8192: return 'Deprecated Notice'; break;
			case 16384: return 'Deprecated Notice (Clay)'; break;
			default: return 'Error'; break;
		}
	}

}
